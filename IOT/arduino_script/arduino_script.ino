
#include <LoRa.h>
#include <SPI.h>

#define BUTTON1 5
#define LED1 4

#define LED2 6
#define BUTTON2 7

#define DEVICE_ID "<ByteMe>"
//send 1 byte for the group number
//send 1 byte for the button number

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(BUTTON1, INPUT);
  pinMode(LED1, OUTPUT);
  pinMode(BUTTON2, INPUT);
  pinMode(LED2, OUTPUT);
  
  Serial.println("Setup LoRa shield...");
  LoRa.setPins(10, 9, 2);
  
  if (!LoRa.begin(868100000)) {
    Serial.println("Starting LoRa failed!");
    while(1);
  }
  LoRa.setSyncWord(0x34);
  Serial.println("LoRa shield initialized");
  Serial.println("Signalling ready by flashing LED 3 times");
  for(int i=0; i<3; i++) {
    digitalWrite(LED1, HIGH);
    delay(200);
    digitalWrite(LED1, LOW);
    delay(400);
  }
  Serial.println("Ready!");
  
}
byte groupnumber = 9;
long count = 0;

void sendClick(char btnnumber[]) {
  Serial.print("Sending packet ");
  Serial.print(count);Serial.println("... ");
  int status = LoRa.beginPacket();
  if (status) {
    LoRa.print(DEVICE_ID);
    LoRa.print(groupnumber);
    LoRa.print(btnnumber);
    LoRa.print(count++);
    if(LoRa.endPacket()){
      Serial.print("packet sent");
    }
   
  } else {
    Serial.println("Error sending packet");
  }

}
int debounceDelay= 3000;
unsigned long lastDebounceTime = 0;
int led1State = -1;
int button1State = LOW;
int button2State = LOW;
char button1[] = "10001";
char button2[] = "10002";
void loop() {
  // put your main code here, to run repeatedly:
  button1State = digitalRead(BUTTON1);
  button2State = digitalRead(BUTTON2);
 
  //filter out any noise by setting a time buffer
  if ( (millis() - lastDebounceTime) > debounceDelay) {
 
    //if the button has been pressed, lets toggle the LED from "off to on" or "on to off"
    if ( (button1State == HIGH)) {
      Serial.println("sending data of button 1");
      sendClick(button1);
      Serial.println(millis());
      digitalWrite(LED1, HIGH); //turn LED on
      lastDebounceTime = millis(); //set the current time
      Serial.println(lastDebounceTime);
    }
    else if ( (button1State == HIGH)) {
 
      digitalWrite(LED1, LOW); //turn LED off
      led1State = -led1State; //now the LED is off, we need to change the state
      lastDebounceTime = millis(); //set the current time
    }//close if/else

    if ( (button2State == HIGH)) {
      Serial.println("sending data of button 2");
      sendClick(button2);
      Serial.println(millis());
      digitalWrite(LED2, HIGH); //turn LED on
      lastDebounceTime = millis(); //set the current time
      Serial.println(lastDebounceTime);
    }
    else if ( (button2State == HIGH)) {
 
      digitalWrite(LED2, LOW); //turn LED off
      led1State = -led1State; //now the LED is off, we need to change the state
      lastDebounceTime = millis(); //set the current time
    }//close if/else
 
  }//close if(time buffer)

  
  if (digitalRead(BUTTON1)){
    digitalWrite(LED1, HIGH);
  }else{
    digitalWrite(LED1, LOW);
  }
  if (digitalRead(BUTTON2)){
    digitalWrite(LED2, HIGH);
  }else{
    digitalWrite(LED2, LOW);
  }
 
}//close void loop
