#!/bin/bash
FILE=/var/iot/channels/ByteMe
TOPIC=IOT
SERVER=mqttserver.domein
PORT=1883
for COUNT in `seq 45`
do
	if [ -f $FILE ]
	then
		mosquitto_pub -h $SERVER -p $PORT -t $TOPIC -f $FILE
		rm $FILE
	fi
	sleep 1	
done