package be.kdg.cityOfIdeas.activity

import android.Manifest
import android.Manifest.permission.CAMERA
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import be.kdg.cityOfIdeas.helper.*
import be.kdg.cityofideas.R
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, ZXingScannerView.ResultHandler {
    private lateinit var txtOrganisation: TextView
    private lateinit var txtInfo: TextView
    private lateinit var imgOrganisation: AppCompatImageView

    val REQUEST_CAMERA = 1

    private var initializeOtherComponents: Boolean = true

    private lateinit var scannerView: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        scannerView = ZXingScannerView(this@MainActivity)

        setSupportActionBar(toolbar)
        initializeViews()
        if(initializeOtherComponents){
            addEventHandlers()

            val url = "$PICASSO_URL/organisations/${CURRENT_ORGANISATION.name!!.toLowerCase()}/organisation/images/${CURRENT_ORGANISATION.image}"
            getPicasso(this@MainActivity).load(url).error(R.drawable.no_image_found).into(imgOrganisation)

            val toggle = object: ActionBarDrawerToggle(
                    this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
                override fun onDrawerOpened(drawerView: View) {
                    super.onDrawerOpened(drawerView)

                    nav_view.getHeaderView(0).findViewById<TextView>(R.id.txtOrganisation).text = CURRENT_ORGANISATION.name!!

                    if(isLoggedIn){
                        nav_view.getHeaderView(0).findViewById<TextView>(R.id.txtUserNameNavigationDrawer).text = APPUSER.userName
                        nav_view.menu.findItem(R.id.login).title = "Log out"

                        nav_view.menu.findItem(R.id.navAccount).isVisible = true
                    }else{
                        nav_view.getHeaderView(0).findViewById<TextView>(R.id.txtUserNameNavigationDrawer).text = "Je bent een anonieme gebruiker"
                        nav_view.menu.findItem(R.id.login).title = "Log in"

                        nav_view.menu.findItem(R.id.navAccount).isVisible = false
                    }
                }
            }
            drawer_layout.addDrawerListener(toggle)
            toggle.syncState()

            nav_view.setNavigationItemSelectedListener(this)
        }
    }

    fun initializeViews(){
        txtOrganisation = findViewById(R.id.txtOrganisation)
        txtInfo = findViewById(R.id.txtInfo)
        imgOrganisation = findViewById(R.id.imgOrganisation)

        if(CURRENT_ORGANISATION.name != null){
            val text = "Welkom bij de stad ${CURRENT_ORGANISATION.name}!"
            val info = CURRENT_ORGANISATION.description!!

            txtOrganisation.text = text
            txtInfo.text = info
        }else{
            setContentView(R.layout.no_content_organisation)
            initializeOtherComponents = false
        }
    }

    fun addEventHandlers(){
        fab.setOnClickListener {
            setContentView(scannerView)

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(checkPermission()){

                }else{
                    requestPermission()
                }
            }
        }
    }

    override fun handleResult(result: Result?) {
        if(result!=null){
            val text = result.text

            if(text.toLowerCase().contains("questionlist")){
                val splitted = text.split("=")
                val id = splitted[1]

                val intent = Intent(this@MainActivity, QuestionListDetail::class.java)
                intent.putExtra(QUESTIONLIST_ID, id)
                startActivity(intent)

            }else if(text.toLowerCase().contains("ideation")){
                val splitted = text.split("/")
                val id = splitted[splitted.size-1]
                val intent = Intent(this@MainActivity, IdeationDetail::class.java)
                intent.putExtra(IDEATION_ID, id)
                startActivity(intent)
            }
        }
    }

    fun checkPermission(): Boolean{
        return (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
    }

    fun requestPermission(){
        ActivityCompat.requestPermissions(this@MainActivity, arrayOf(CAMERA), REQUEST_CAMERA)
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: Array<Int>){
        when(requestCode){
            REQUEST_CAMERA -> {
                if(grantResults.isNotEmpty()){
                    val cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                    if(cameraAccepted){

                    }else{
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            if(shouldShowRequestPermissionRationale(CAMERA)){
                                displayAlertMessage("City of ideas vraagt toegang tot de camera", DialogInterface.OnClickListener{
                                    dialog, which ->

                                    requestPermissions(arrayOf(CAMERA), REQUEST_CAMERA)
                                })
                                return
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkPermission()){
                if(scannerView == null){
                    scannerView = ZXingScannerView(this@MainActivity)
                    setContentView(scannerView)
                }

                scannerView.setResultHandler(this@MainActivity)
                scannerView.startCamera()
            }else{
                requestPermission()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        scannerView.stopCamera()
    }

    fun displayAlertMessage(message: String, listener: DialogInterface.OnClickListener){
        AlertDialog.Builder(this@MainActivity)
                .setMessage(message)
                .setPositiveButton("OK", listener)
                .setNegativeButton("Cancel", null)
                .create()
                .show()
    }

    override fun onBackPressed() {
        if(initializeOtherComponents){
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            } else {
                super.onBackPressed()
            }
        }else{
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navProjects -> {
                val intent = Intent(this@MainActivity, ProjectOverview::class.java)
                startActivity(intent)
            }
            R.id.login -> {
                if(isLoggedIn){
                    item.title = resources.getString(R.string.title_activity_login)
                    logOutAppUser()
                    val intent = Intent(this@MainActivity, MainActivity::class.java)
                    startActivity(intent)
                }else{
                    item.title = resources.getString(R.string.title_activity_logout)
                    val intent = Intent(this@MainActivity, LoginActivity::class.java)
                    startActivity(intent)
                }
            }
            R.id.navInfo -> {
                val intent = Intent(this@MainActivity, InfoActivity::class.java)
                startActivity(intent)
            }
            R.id.navAccount -> {
                val intent = Intent(this@MainActivity, ManageAccountActivity::class.java)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
