package be.kdg.cityOfIdeas.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.Button
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.adapter.IdeaAdapter
import be.kdg.cityOfIdeas.helper.getIdeaAPI
import be.kdg.cityOfIdeas.helper.getIdeationAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.Idea
import be.kdg.cityOfIdeas.model.Ideation
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val IDEATION = "ideation"
const val QUESTIONLIST_ID = "questionlist_id"
const val ALLOW_IMAGE = "allow_image"
const val ALLOW_VIDEO = "allow_video"
const val IDEATION_ID = "ideation_id"
const val IDEA_ID = "idea_id"
const val PROJECT_ID = "project_id"

class IdeationIdeas : AppCompatActivity(), IdeaAdapter.IdeaClickListener {
    private lateinit var rvIdeas: androidx.recyclerview.widget.RecyclerView
    private lateinit var btnGiveIdea: Button

    var ideas: Array<Idea> = arrayOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ideation_ideas)

        initialiseViews()
        addEventHandlers()
    }

    fun initialiseViews(){
        btnGiveIdea = findViewById(R.id.btnGiveIdea)
        rvIdeas = findViewById(R.id.rvIdeas)
        rvIdeas.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@IdeationIdeas)

        rvIdeas.layoutAnimation = AnimationUtils.loadLayoutAnimation(this@IdeationIdeas, R.anim.layoutanimation_up_to_down)

        doApiCalls()
    }

    fun addEventHandlers(){
        btnGiveIdea.setOnClickListener {
            getIdeationAPI().fetchIdeation(intent.getStringExtra(IDEATION)).enqueue(object: Callback<Ideation>{
                override fun onFailure(call: Call<Ideation>, t: Throwable) {showErrorPopUp(this@IdeationIdeas)}
                override fun onResponse(call: Call<Ideation>, response: Response<Ideation>) {
                    val ideation = response.body()!!
                    val newIntent = Intent(this@IdeationIdeas, GiveIdeaActivity::class.java)
                    newIntent.putExtra(IDEATION_ID, ideation.id)
                    newIntent.putExtra(ALLOW_IMAGE, ideation.allowImage)
                    newIntent.putExtra(ALLOW_VIDEO, ideation.allowVideo)
                    newIntent.putExtra(IDEATION, intent.getStringExtra(IDEATION))
                    startActivity(newIntent)
                }
            })
        }
    }

    fun doApiCalls(){
        getIdeaAPI().fetchIdeasFromIdeation(intent.getStringExtra(IDEATION)).enqueue(object: Callback<Array<Idea>>{
            override fun onFailure(call: Call<Array<Idea>>, t: Throwable) {showErrorPopUp(this@IdeationIdeas)}
            override fun onResponse(call: Call<Array<Idea>>, response: Response<Array<Idea>>) {
                ideas = response.body()!!
                rvIdeas.adapter = IdeaAdapter(this@IdeationIdeas, this@IdeationIdeas, ideas, resources)

                rvIdeas.adapter!!.notifyDataSetChanged()
                rvIdeas.scheduleLayoutAnimation()
            }
        })
    }

    override fun onIdeaClicked(position: Int) {
        val intent = Intent(this@IdeationIdeas, IdeaReactionDetailActivity::class.java)
        intent.putExtra(IDEA_ID, ideas[position].id)
        startActivity(intent)
    }

    override fun onResume() {
        doApiCalls()

        super.onResume()
    }
}
