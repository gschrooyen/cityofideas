package be.kdg.cityOfIdeas.model

import org.joda.time.DateTime

class Ideation(
        val id: String,
        val imageName: String,
        val begin: DateTime,
        val end: DateTime,
        val shares: Array<Share>,
        val type: IdeationType,
        val titleIdeation: String,
        val descriptionIdeation: String,
        val allowImage: Boolean,
        val allowVideo: Boolean,
        val ideas: Array<Idea>
)