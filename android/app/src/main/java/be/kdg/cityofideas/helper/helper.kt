package be.kdg.cityOfIdeas.helper

import android.content.Context
import android.widget.Toast
import be.kdg.cityOfIdeas.api.*
import be.kdg.cityOfIdeas.model.Organisation
import be.kdg.cityOfIdeas.model.User
import okhttp3.OkHttpClient
import org.joda.time.DateTime
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.security.cert.CertificateException
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.GsonBuilder
import com.google.gson.Gson
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import org.joda.time.Days
import org.joda.time.format.DateTimeFormat

var APPUSER: User = User(null, "anoniem", null, null, null, null, null, null, null, null, null)
var CURRENT_ORGANISATION: Organisation = Organisation(null, null, null, null, null, null, null, null, null, null)

var isLoggedIn = false
val dateFormatter = DateTimeFormat.forPattern("dd-MM-YYYY")!!

const val PICASSO_URL = "https://10.0.2.2:5001"
var BASE_URL = ""

fun getPicasso(context: Context): Picasso{
    val okHttpDownloader = OkHttp3Downloader(getUnsafeOkHttpClient().build())

    return Picasso.Builder(context).downloader(okHttpDownloader).build()
}

fun showErrorPopUp(context: Context){
    Toast.makeText(context, "Kan geen verbinding maken met de server, probeer opnieuw", Toast.LENGTH_LONG).show()
}

fun setBaseURL(organisationName: String){
    BASE_URL = "https://10.0.2.2:5001/${organisationName.toLowerCase()}/"
}

fun setBaseOrganisation(organisation: Organisation){
    CURRENT_ORGANISATION = organisation
}

fun resetCurrentOrganisation(){
    CURRENT_ORGANISATION = Organisation(null, null, null, null, null, null, null, null, null, null)
}

fun setAppUser(user: User){
    APPUSER = user
    isLoggedIn = true
}

fun logOutAppUser(){
    APPUSER = User("anoniem", null, null, null, null, null, null, null, null, null, null)
    isLoggedIn = false
}

fun getOrganisationAPI(): OrganisationAPI{
    return getRetrofitOrganisations().create(OrganisationAPI::class.java)
}

fun getProjectAPI(): ProjectAPI {
    return getRetrofit().create(ProjectAPI::class.java)
}

fun getIdeationAPI(): IdeationAPI {
    return getRetrofit().create(IdeationAPI::class.java)
}

fun getQuestionListAPI(): QuestionListAPI {
    return getRetrofit().create(QuestionListAPI::class.java)
}

fun getIdeaAPI(): IdeaAPI {
    return getRetrofit().create(IdeaAPI::class.java)
}

fun getUserAPI(): UserAPI {
    return getRetrofit().create(UserAPI::class.java)
}

fun getRetrofit(): Retrofit{
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(getUnsafeOkHttpClient().build())
        .addConverterFactory(GsonConverterFactory.create(getBuilder()))
        .build()
}

fun getRetrofitOrganisations(): Retrofit{
    return Retrofit.Builder()
            .baseUrl(PICASSO_URL)
            .client(getUnsafeOkHttpClient().build())
            .addConverterFactory(GsonConverterFactory.create(getBuilder()))
            .build()
}

fun getBuilder(): Gson{
    val gson = GsonBuilder().registerTypeAdapter(DateTime::class.java, DateTimeTypeAdapter()).create()
    return gson
}

internal class DateTimeTypeAdapter : JsonDeserializer<DateTime> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext): DateTime {
        return DateTime.parse(json.asString)
    }
}

fun getUnsafeOkHttpClient(): OkHttpClient.Builder {
    try {
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                return arrayOf()
            }
        })

        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
        val sslSocketFactory = sslContext.socketFactory

        val builder = OkHttpClient.Builder()
        builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
        builder.hostnameVerifier { _, _ -> true }

        return builder
    } catch (e: Exception) {
        throw RuntimeException(e)
    }
}

fun getPercentageBetweenTwoDates(begin: DateTime, end: DateTime): Int{
    val daysToEnd = Days.daysBetween(begin, end).days
    val daysToNow = Days.daysBetween(begin, DateTime.now()).days

    val result = (daysToNow.toDouble()/daysToEnd.toDouble()*100).toInt()

    if(result < 0){
        return 0
    }else if(result > 100){
        return 100
    }
    return result
}