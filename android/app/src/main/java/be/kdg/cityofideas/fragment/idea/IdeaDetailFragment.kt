package be.kdg.cityOfIdeas.fragment.idea


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.cityOfIdeas.activity.IDEA_ID
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.adapter.ImageAdapter
import be.kdg.cityOfIdeas.helper.getIdeaAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.Idea
import be.kdg.cityOfIdeas.model.Like
import be.kdg.cityOfIdeas.model.Share
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IdeaDetailFragment : androidx.fragment.app.Fragment() {
    private lateinit var txtIdeaTitle: TextView
    private lateinit var txtIdeaDescription: TextView
    private lateinit var txtLikes: TextView
    private lateinit var txtShares: TextView
    private lateinit var btnLike: Button
    private lateinit var btnShare: Button
    private lateinit var rvImages: RecyclerView

    private lateinit var ideaId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_idea_detail, container, false)

        ideaId = arguments!!.getString(IDEA_ID)!!

        initializeViews(view)
        updateInfo()

        return view
    }

    fun initializeViews(view: View){
        txtIdeaTitle = view.findViewById(R.id.txtIdeaTitle)
        txtIdeaDescription = view.findViewById(R.id.txtIdeaDescription)
        txtLikes = view.findViewById(R.id.txtLikes)
        txtShares = view.findViewById(R.id.txtShares)
        btnLike = view.findViewById(R.id.btnLike)
        btnShare = view.findViewById(R.id.btnShare)
        rvImages = view.findViewById(R.id.rvImages)
    }

    fun updateInfo(){
        getIdeaAPI().fetchIdea(ideaId).enqueue(object:Callback<Idea>{
            override fun onFailure(call: Call<Idea>, t: Throwable) {
                showErrorPopUp(context!!)
            }
            override fun onResponse(call: Call<Idea>, response: Response<Idea>) {
                val idea = response.body()!!

                txtIdeaTitle.text = idea.title
                txtIdeaDescription.text = idea.description

                if(idea.imageNames.isNotEmpty()){
                    val adapter = ImageAdapter(context!!, idea.imageNames)
                    val layoutManager = GridLayoutManager(context!!, 2)

                    rvImages.adapter = adapter
                    rvImages.layoutManager = layoutManager
                }

                btnLike.setOnClickListener {
                    getIdeaAPI().addLikeToIdea(idea.id!!, Like()).enqueue(object: Callback<Idea> {
                        override fun onFailure(call: Call<Idea>, t: Throwable) {showErrorPopUp(context!!)}
                        override fun onResponse(call: Call<Idea>, response: Response<Idea>) {
                            updateInfo()
                        }
                    })
                }

                btnShare.setOnClickListener {
                    getIdeaAPI().addShareToIdea(idea.id!!, Share()).enqueue(object: Callback<Idea> {
                        override fun onFailure(call: Call<Idea>, t: Throwable) {showErrorPopUp(context!!)}
                        override fun onResponse(call: Call<Idea>, response: Response<Idea>) {
                            updateInfo()
                        }
                    })
                }

                doApiCalls(idea.id!!)
            }
        })
    }

    fun doApiCalls(ideaId: String){
        getIdeaAPI().fetchLikesFromIdea(ideaId).enqueue(object: Callback<Array<Like>>{
            override fun onFailure(call: Call<Array<Like>>, t: Throwable) {showErrorPopUp(context!!)}
            override fun onResponse(call: Call<Array<Like>>, response: Response<Array<Like>>) {
                val likesText = response.body()!!.size.toString() + " likes"
                txtLikes.text = likesText
            }
        })

        getIdeaAPI().fetchSharesFromIdea(ideaId).enqueue(object: Callback<Array<Share>>{
            override fun onFailure(call: Call<Array<Share>>, t: Throwable) {showErrorPopUp(context!!)}
            override fun onResponse(call: Call<Array<Share>>, response: Response<Array<Share>>) {
                val sharesText =  response.body()!!.size.toString() + " shares"
                txtShares.text = sharesText
            }
        })
    }
}
