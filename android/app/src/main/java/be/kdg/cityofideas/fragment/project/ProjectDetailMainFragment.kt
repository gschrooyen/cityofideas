package be.kdg.cityOfIdeas.fragment.project

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import be.kdg.cityOfIdeas.activity.PROJECT_ID
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.helper.*
import be.kdg.cityOfIdeas.model.Like
import be.kdg.cityOfIdeas.model.Project
import be.kdg.cityOfIdeas.model.Share
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectDetailMainFragment : androidx.fragment.app.Fragment() {
    private lateinit var imgProject : ImageView
    private lateinit var txtProjectTitle: TextView
    private lateinit var txtMainQuestion: TextView
    private lateinit var txtDescription: TextView
    private lateinit var txtBeginDate: TextView
    private lateinit var txtEndDate: TextView
    private lateinit var progressDates: ProgressBar
    private lateinit var txtLikes: TextView
    private lateinit var txtShares: TextView
    private lateinit var btnLike: Button
    private lateinit var btnShare: Button

    private lateinit var project: Project

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_project_detail_main, container, false)

        getProjectAPI().fetchProject(arguments!!.getString(PROJECT_ID)!!).enqueue(object: Callback<Project>{
            override fun onFailure(call: Call<Project>, t: Throwable) {showErrorPopUp(context!!)}
            override fun onResponse(call: Call<Project>, response: Response<Project>) {
                project = response.body()!!

                initialiseViews(view)
                updateInfo()
            }
        })

        return view
    }

    fun initialiseViews(view: View){
        imgProject = view.findViewById(R.id.imgProject)
        txtProjectTitle = view.findViewById(R.id.txtProjectTitle)
        txtMainQuestion = view.findViewById(R.id.txtMainQuestion)
        txtDescription = view.findViewById(R.id.txtDescription)
        txtBeginDate = view.findViewById(R.id.txtBeginDate)
        txtEndDate = view.findViewById(R.id.txtEndDate)
        progressDates = view.findViewById(R.id.progressDates)
        txtLikes = view.findViewById(R.id.txtLikes)
        txtShares = view.findViewById(R.id.txtShares)
        btnLike = view.findViewById(R.id.btnLike)
        btnShare = view.findViewById(R.id.btnShare)
    }

    fun updateInfo(){
        val url = "$PICASSO_URL/organisations/${CURRENT_ORGANISATION.name!!.toLowerCase()}/organisation/projects/images/${project.imageName}"
        getPicasso(context!!).load(url).error(R.drawable.no_image_found).into(imgProject)
        txtProjectTitle.text = project.name
        txtMainQuestion.text = project.mainQuestion
        txtDescription.text = project.description
        txtBeginDate.text = project.begin.toString(dateFormatter)
        txtEndDate.text = project.end.toString(dateFormatter)
        progressDates.progress = getPercentageBetweenTwoDates(project.begin, project.end)
        if(progressDates.progress < 100){
            progressDates.progressTintList = resources.getColorStateList(R.color.colorPrimary)
        }else{
            progressDates.progressTintList = resources.getColorStateList(R.color.lime)
        }

        val likesText = project.likes.size.toString() + " keer geliked"
        txtLikes.text = likesText
        val sharesText = project.shares.size.toString() + " keer geshared"
        txtShares.text = sharesText

        btnLike.setOnClickListener {
            getProjectAPI().addLike(project.id, Like()).enqueue(object: Callback<Project>{
                override fun onFailure(call: Call<Project>, t: Throwable) {showErrorPopUp(context!!)}
                override fun onResponse(call: Call<Project>, response: Response<Project>) {
                    project = response.body()!!
                    updateInfo()
                }
            })
        }

        btnShare.setOnClickListener {
            getProjectAPI().addShare(project.id, Share()).enqueue(object: Callback<Project>{
                override fun onFailure(call: Call<Project>, t: Throwable) {showErrorPopUp(context!!)}
                override fun onResponse(call: Call<Project>, response: Response<Project>) {
                    project = response.body()!!
                    updateInfo()
                }
            })
        }
    }
}
