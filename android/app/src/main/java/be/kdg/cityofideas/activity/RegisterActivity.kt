package be.kdg.cityOfIdeas.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputEditText
import android.widget.*
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.model.User
import be.kdg.cityOfIdeas.helper.dateFormatter
import be.kdg.cityOfIdeas.helper.getUserAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {
    private lateinit var txtFirstName: TextInputEditText
    private lateinit var txtLastName: TextInputEditText
    private lateinit var spGender: Spinner
    private lateinit var txtPostalCode: TextInputEditText
    private lateinit var txtBirthDate: TextInputEditText
    private lateinit var txtEmail: TextInputEditText
    private lateinit var txtPassword: TextInputEditText
    private lateinit var txtPasswordConfirm: TextInputEditText
    private lateinit var btnRegister: Button
    var birthDate: DateTime = DateTime.now()
    var textInputEditTexts = ArrayList<TextInputEditText>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initializeViews()
        addEventHandlers()
    }

    fun initializeViews(){
        txtFirstName = findViewById(R.id.txtFirstName)
        txtLastName = findViewById(R.id.txtLastName)
        spGender = findViewById(R.id.spGender)
        txtPostalCode = findViewById(R.id.txtPostalCode)
        txtBirthDate = findViewById(R.id.txtBirthDate)
        txtEmail = findViewById(R.id.txtEmail)
        txtPassword = findViewById(R.id.txtPassword)
        txtPasswordConfirm = findViewById(R.id.txtPasswordConfirm)
        btnRegister = findViewById(R.id.btnRegister)

        initializeSpinner()
    }

    fun initializeSpinner(){
        val items = arrayOf("Man", "Vrouw", "Onbekend")
        spGender.adapter = ArrayAdapter<String>(this, R.layout.list_item_spinner, items)
    }

    fun addEventHandlers(){
        btnRegister.setOnClickListener {
            if(checkInputs()){
                val userName = txtFirstName.text.toString() + "." + txtLastName.text.toString().substring(0,1)

                val user = User(null,
                    userName,
                    txtFirstName.text.toString(),
                    txtLastName.text.toString(),
                    txtPostalCode.text.toString().toInt(),
                    txtEmail.text.toString(),
                    spGender.selectedItem.toString().substring(0,1).toUpperCase(),
                    false,
                    false,
                    null,
                    txtPassword.text.toString())

                getUserAPI().registerUser(user).enqueue(object: Callback<User> {
                    override fun onFailure(call: Call<User>, t: Throwable) {
                        showErrorPopUp(this@RegisterActivity)
                    }
                    override fun onResponse(call: Call<User>, response: Response<User>) {
                        if(!response.isSuccessful){
                            Toast.makeText(this@RegisterActivity, "Email is al in gebruik!", Toast.LENGTH_LONG).show()
                        }else{
                            startActivity(Intent(this@RegisterActivity, MainActivity::class.java))
                        }
                    }
                })
            }else{
                textInputEditTexts[0].requestFocus()
                for(i in textInputEditTexts){
                    i.setText("")
                }
            }
        }
    }

    fun checkInputs(): Boolean{
        textInputEditTexts.add(txtFirstName)
        textInputEditTexts.add(txtLastName)
        textInputEditTexts.add(txtPostalCode)
        textInputEditTexts.add(txtBirthDate)
        textInputEditTexts.add(txtEmail)
        textInputEditTexts.add(txtPassword)
        textInputEditTexts.add(txtPasswordConfirm)

        for(i in textInputEditTexts){
            if(i.text.isNullOrEmpty()){
                return false
            }
        }

        if(txtPassword.text.toString() != txtPasswordConfirm.text.toString())
            return false

        try {
            birthDate = dateFormatter.parseDateTime(txtBirthDate.text.toString())
        }catch (e: Exception){
            return false
        }

        return true
    }
}
