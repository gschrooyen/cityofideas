package be.kdg.cityOfIdeas.api

import be.kdg.cityOfIdeas.model.Question
import be.kdg.cityOfIdeas.model.QuestionList
import retrofit2.Call
import retrofit2.http.*

interface QuestionListAPI {
    @GET("api/questionlists/getQuestionList/{projectId}")
    fun fetchQuestionListsFromProject(@Path("projectId") projectId: String): Call<Array<QuestionList>>

    @GET("api/questionlists/getQuestionListSingle/{questionListId}")
    fun fetchQuestionList(@Path("questionListId") questionListId: String): Call<QuestionList>

    @Headers("Content-Type: application/json")
    @POST("api/questionlists/updateQuestion/{questionId}")
    fun updateQuestion(@Path("questionId") questionId: String, @Body answers: Array<String>): Call<Question>
}