package be.kdg.cityOfIdeas.api

import be.kdg.cityOfIdeas.model.Like
import be.kdg.cityOfIdeas.model.Project
import be.kdg.cityOfIdeas.model.Share
import retrofit2.Call
import retrofit2.http.*

interface ProjectAPI {
    @GET("api/projects/getProjects")
    fun fetchProjects(): Call<Array<Project>>

    @GET("api/projects/getProject/{projectId}")
    fun fetchProject(@Path("projectId") projectId: String): Call<Project>

    @GET("api/projects/getLikesFromProject/{projectId}")
    fun fetchLikesFromProject(@Path("projectId") projectId: String): Call<Array<Like>>

    @GET("api/projects/getSharesFromProject/{projectId}")
    fun fetchSharesFromProject(@Path("projectId") projectId: String): Call<Array<Share>>

    @Headers("Content-Type: application/json")
    @POST("api/projects/addLike/{projectId}")
    fun addLike(@Path("projectId") projectId: String, @Body like: Like): Call<Project>

    @Headers("Content-Type: application/json")
    @POST("api/projects/addShare/{projectId}")
    fun addShare(@Path("projectId") projectId: String, @Body share: Share): Call<Project>
}