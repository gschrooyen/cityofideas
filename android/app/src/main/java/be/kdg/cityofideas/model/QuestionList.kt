package be.kdg.cityOfIdeas.model

class QuestionList(
        val id: String?,
        val title: String?,
        val description: String?,
        val open: Boolean?,
        val questions: Array<Question>?,
        val shares: Array<Share>
)