package be.kdg.cityOfIdeas.model

import android.graphics.Bitmap
import org.joda.time.DateTime

data class Project(
        val id: String,
        val name: String,
        val mainQuestion: String,
        val description: String,
        val begin: DateTime,
        val end: DateTime,
        val imageName: String,
        var imageBitmap: Bitmap,
        val organisation: Organisation,
        val placement: Placement,
        val likes: Array<Like>,
        val shares: Array<Share>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Project

        if (id != other.id) return false
        if (name != other.name) return false
        if (mainQuestion != other.mainQuestion) return false
        if (description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + mainQuestion.hashCode()
        result = 31 * result + description.hashCode()
        return result
    }
}