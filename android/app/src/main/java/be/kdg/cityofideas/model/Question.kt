package be.kdg.cityOfIdeas.model

class Question(
        val id: String,
        val title: String,
        val required: Boolean,
        val options: List<Option>,
        var awnsers: Array<String>,
        val choiseQuestionType: Int,
        val openQuestionType: Int
)