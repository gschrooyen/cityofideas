package be.kdg.cityOfIdeas.model

class Organisation(
        val id: String?,
        val name: String?,
        val description: String?,
        val afgesloten: Boolean?,
        val formattedName: String?,
        val url: String?,
        val image: String?,
        val zipCode: String?,
        val users: List<User>?,
        val projects: List<Project>?
)