package be.kdg.cityOfIdeas.adapter

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import be.kdg.cityOfIdeas.helper.*
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.model.Like
import be.kdg.cityOfIdeas.model.Project
import be.kdg.cityOfIdeas.model.Share
import kotlinx.android.synthetic.main.list_item_project_overview.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectAdapter(val listener: ProjectClickListener, val projects: Array<Project>, val context: Context, val resources: Resources) : androidx.recyclerview.widget.RecyclerView.Adapter<ProjectAdapter.ProjectViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ProjectViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.list_item_project_overview, viewGroup, false)

        return ProjectViewHolder(view)
    }

    override fun getItemCount(): Int {
        return projects.size
    }

    override fun onBindViewHolder(viewHolder: ProjectViewHolder, position: Int) {
        val project = projects[position]

        getPicasso(context)
                .load("$PICASSO_URL/organisations/${CURRENT_ORGANISATION.name!!.toLowerCase()}/organisation/projects/images/${project.imageName}")
                .error(R.drawable.no_image_found)
                .into(viewHolder.imgProject)

        viewHolder.txtProjectTitle.text = project.name
        viewHolder.txtBeginDate.text = project.begin.toString(dateFormatter)
        viewHolder.txtEndDate.text = project.end.toString(dateFormatter)
        viewHolder.progressDates.progress = getPercentageBetweenTwoDates(project.begin, project.end)
        if(viewHolder.progressDates.progress < 100){
            viewHolder.progressDates.progressTintList = resources.getColorStateList(R.color.colorPrimary)
        }else{
            viewHolder.progressDates.progressTintList = resources.getColorStateList(R.color.lime)
        }

        viewHolder.itemView.setOnClickListener {
            listener.onProjectClicked(position)
        }

        getProjectAPI().fetchLikesFromProject(project.id).enqueue(object: Callback<Array<Like>> {
            override fun onFailure(call: Call<Array<Like>>, t: Throwable) {showErrorPopUp(context)}
            override fun onResponse(call: Call<Array<Like>>, response: Response<Array<Like>>) {
                val likes = response.body()!!.size.toString() + " likes"
                viewHolder.txtLikes.text = likes
            }
        })

        getProjectAPI().fetchSharesFromProject(project.id).enqueue(object: Callback<Array<Share>>{
            override fun onFailure(call: Call<Array<Share>>, t: Throwable) {showErrorPopUp(context)}
            override fun onResponse(call: Call<Array<Share>>, response: Response<Array<Share>>) {
                val shares = response.body()!!.size.toString() + " shares"
                viewHolder.txtShares.text = shares
            }
        })
    }

    class ProjectViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        val imgProject = view.imgProject
        val txtProjectTitle = view.txtProjectTitle
        val txtBeginDate = view.txtBeginDate
        val txtEndDate = view.txtEndDate
        val progressDates = view.progressDates
        val txtLikes = view.txtLikes
        val txtShares = view.txtShares
    }

    interface ProjectClickListener{
        fun onProjectClicked(position: Int)
    }
}