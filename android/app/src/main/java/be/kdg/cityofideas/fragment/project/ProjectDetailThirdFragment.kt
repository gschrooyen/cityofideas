package be.kdg.cityOfIdeas.fragment.project


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import be.kdg.cityOfIdeas.activity.PROJECT_ID
import be.kdg.cityOfIdeas.activity.QUESTIONLIST_ID
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.activity.QuestionListDetail
import be.kdg.cityOfIdeas.adapter.QuestionListAdapter
import be.kdg.cityOfIdeas.helper.getProjectAPI
import be.kdg.cityOfIdeas.helper.getQuestionListAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.QuestionList
import be.kdg.cityOfIdeas.model.Project
import retrofit2.Call

import retrofit2.Callback
import retrofit2.Response

class ProjectDetailThirdFragment : androidx.fragment.app.Fragment(), QuestionListAdapter.QuestionListListener {
    private lateinit var rvQuestionLists: androidx.recyclerview.widget.RecyclerView
    private lateinit var project: Project
    private lateinit var questionLists: Array<QuestionList>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_project_detail_third, container, false)

        getProjectAPI().fetchProject(arguments!!.getString(PROJECT_ID)!!).enqueue(object: Callback<Project>{
            override fun onFailure(call: Call<Project>, t: Throwable) {
                showErrorPopUp(context!!)
            }
            override fun onResponse(call: Call<Project>, response: Response<Project>) {
                project = response.body()!!

                initialiseViews(view)
                doApiCalls()
            }
        })

        return view
    }

    fun initialiseViews(view: View){
        rvQuestionLists = view.findViewById(R.id.rvQuestionLists)
        rvQuestionLists.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        rvQuestionLists.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layoutanimation_up_to_down)
    }

    fun doApiCalls() {
        getQuestionListAPI().fetchQuestionListsFromProject(project.id).enqueue(object: Callback<Array<QuestionList>>{
            override fun onResponse(call: Call<Array<QuestionList>>, response: Response<Array<QuestionList>>) {
                if(response.isSuccessful){
                    questionLists = response.body()!!

                    if(response.body() != null){
                        rvQuestionLists.adapter = QuestionListAdapter(this@ProjectDetailThirdFragment, questionLists)
                        rvQuestionLists.adapter!!.notifyDataSetChanged()
                        rvQuestionLists.scheduleLayoutAnimation()
                    }
                }

            }

            override fun onFailure(call: Call<Array<QuestionList>>, t: Throwable) {
                showErrorPopUp(context!!)
            }
        })
    }

    override fun onQuestionListClicked(position: Int) {
        val intent = Intent(context, QuestionListDetail::class.java)
        intent.putExtra(QUESTIONLIST_ID, questionLists[position].id)
        startActivity(intent)
    }
}
