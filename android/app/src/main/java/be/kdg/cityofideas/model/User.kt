package be.kdg.cityOfIdeas.model

import org.joda.time.DateTime

class User(
    val userId: String?,
    val userName: String?,
    val firstName: String?,
    val lastName: String?,
    val address: Int?,
    val email: String?,
    val gender: String?,
    val verified: Boolean?,
    val blocked: Boolean?,
    val birthDate: DateTime?,
    val passwordHash: String?
)