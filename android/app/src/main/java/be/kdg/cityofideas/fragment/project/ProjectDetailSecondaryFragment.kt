package be.kdg.cityOfIdeas.fragment.project


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import be.kdg.cityOfIdeas.activity.*
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.adapter.IdeationAdapter
import be.kdg.cityOfIdeas.helper.getIdeationAPI
import be.kdg.cityOfIdeas.helper.getProjectAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.Ideation
import be.kdg.cityOfIdeas.model.Project
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectDetailSecondaryFragment : androidx.fragment.app.Fragment(), IdeationAdapter.IdeationClickListener {
    private lateinit var rvIdeations: androidx.recyclerview.widget.RecyclerView
    private lateinit var project: Project
    private var ideations: Array<Ideation> = arrayOf()

    override fun onIdeationClicked(pos: Int) {
        val intent = Intent(context, IdeationDetail::class.java)
        intent.putExtra(IDEATION_ID, ideations[pos].id)
        startActivity(intent)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_project_detail_secondary, container, false)

        getProjectAPI().fetchProject(arguments!!.getString(PROJECT_ID)!!).enqueue(object: Callback<Project>{
            override fun onFailure(call: Call<Project>, t: Throwable) {showErrorPopUp(context!!)}
            override fun onResponse(call: Call<Project>, response: Response<Project>) {
                project = response.body()!!

                initialiseViews(view)
                doApiCalls()
            }
        })

        return view
    }

    private fun initialiseViews(view: View){
        rvIdeations = view.findViewById(R.id.rvIdeations)
        rvIdeations.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        rvIdeations.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layoutanimation_up_to_down)
    }

    fun doApiCalls(){
        getIdeationAPI().fetchIdeationsFromProject(project.id).enqueue(object: Callback<Array<Ideation>>{
            override fun onFailure(call: Call<Array<Ideation>>, t: Throwable) {
                showErrorPopUp(context!!)
            }
            override fun onResponse(call: Call<Array<Ideation>>, response: Response<Array<Ideation>>) {
                ideations = response.body()!!
                if(response.body() != null){
                    rvIdeations.adapter = IdeationAdapter(this@ProjectDetailSecondaryFragment, resources, ideations)

                    rvIdeations.adapter!!.notifyDataSetChanged()
                    rvIdeations.scheduleLayoutAnimation()
                }
            }
        })
    }
}
