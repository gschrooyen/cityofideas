package be.kdg.cityOfIdeas.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.helper.dateFormatter
import be.kdg.cityOfIdeas.model.Reaction
import kotlinx.android.synthetic.main.list_item_ideation_ideas_reactions.view.*

class ReactionAdapter(val reactions: Array<Reaction>): androidx.recyclerview.widget.RecyclerView.Adapter<ReactionAdapter.ReactionViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ReactionViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.list_item_ideation_ideas_reactions, viewGroup, false)

        return ReactionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return reactions.size
    }

    override fun onBindViewHolder(viewHolder: ReactionViewHolder, position: Int) {
        val reaction = reactions[position]
        viewHolder.txtReactionTitle.text = reaction.comment
        viewHolder.txtUserName.text = reaction.user.userName
        viewHolder.txtDate.text = reaction.time!!.toString(dateFormatter)
    }

    class ReactionViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        val txtReactionTitle = view.txtReactionTitle
        val txtUserName = view.txtUserName
        val txtDate = view.txtDate
    }
}