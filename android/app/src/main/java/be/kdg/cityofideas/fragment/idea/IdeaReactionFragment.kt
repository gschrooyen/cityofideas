package be.kdg.cityOfIdeas.fragment.idea


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.TextView
import be.kdg.cityOfIdeas.activity.IDEA_ID
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.adapter.ReactionAdapter
import be.kdg.cityOfIdeas.helper.APPUSER
import be.kdg.cityOfIdeas.helper.getIdeaAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.Idea
import be.kdg.cityOfIdeas.model.Reaction
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IdeaReactionFragment : androidx.fragment.app.Fragment() {
    private lateinit var rvReactions: androidx.recyclerview.widget.RecyclerView
    private lateinit var btnReact: Button
    private lateinit var txtReactionInput: TextView

    private lateinit var ideaId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_idea_reaction, container, false)

        ideaId = arguments!!.getString(IDEA_ID)!!

        initializeViews(view)
        updateInfo()

        return view
    }

    fun initializeViews(view: View){
        rvReactions = view.findViewById(R.id.rvReactions)
        btnReact = view.findViewById(R.id.btnReact)
        txtReactionInput = view.findViewById(R.id.txtReactionInput)
    }

    fun updateInfo(){
        getIdeaAPI().fetchIdea(ideaId).enqueue(object:Callback<Idea>{
            override fun onFailure(call: Call<Idea>, t: Throwable) {
                showErrorPopUp(context!!)
            }
            override fun onResponse(call: Call<Idea>, response: Response<Idea>) {
                val idea = response.body()!!

                rvReactions.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

                btnReact.setOnClickListener {
                    if(txtReactionInput.text.isNotBlank()){
                        getIdeaAPI().addReactionToIdea(idea.id!!, Reaction(txtReactionInput.text.toString(), APPUSER, null)).enqueue(object:
                                Callback<Idea> {
                            override fun onFailure(call: Call<Idea>, t: Throwable) {showErrorPopUp(context!!)}
                            override fun onResponse(call: Call<Idea>, response: Response<Idea>) {
                                txtReactionInput.onEditorAction(EditorInfo.IME_ACTION_DONE)
                                txtReactionInput.text = ""
                                updateInfo()
                            }
                        })
                    }
                }

                doApiCalls(idea.id!!)
            }
        })
    }

    fun doApiCalls(ideaId: String){
        getIdeaAPI().fetchReactionsFromIdea(ideaId).enqueue(object: Callback<Array<Reaction>>{
            override fun onFailure(call: Call<Array<Reaction>>, t: Throwable) {showErrorPopUp(context!!)}
            override fun onResponse(call: Call<Array<Reaction>>, response: Response<Array<Reaction>>) {
                rvReactions.adapter = ReactionAdapter(response.body()!!)

                rvReactions.scrollToPosition(rvReactions.adapter!!.itemCount-1)
            }
        })
    }
}
