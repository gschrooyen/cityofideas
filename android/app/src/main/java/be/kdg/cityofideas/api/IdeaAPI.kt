package be.kdg.cityOfIdeas.api

import be.kdg.cityOfIdeas.model.Idea
import be.kdg.cityOfIdeas.model.Like
import be.kdg.cityOfIdeas.model.Reaction
import be.kdg.cityOfIdeas.model.Share
import retrofit2.Call
import retrofit2.http.*

interface IdeaAPI {
    @GET("api/ideas/getIdeasFromIdeation/{ideationId}")
    fun fetchIdeasFromIdeation(@Path("ideationId") ideationId: String): Call<Array<Idea>>

    @GET("api/ideas/getIdea/{ideaId}")
    fun fetchIdea(@Path("ideaId") ideaId: String): Call<Idea>

    @GET("api/ideas/getLikesFromIdea/{ideaId}")
    fun fetchLikesFromIdea(@Path("ideaId") ideaId: String): Call<Array<Like>>

    @GET("api/ideas/getSharesFromIdea/{ideaId}")
    fun fetchSharesFromIdea(@Path("ideaId") ideaId: String): Call<Array<Share>>

    @GET("api/ideas/getReactionsFromIdea/{ideaId}")
    fun fetchReactionsFromIdea(@Path("ideaId") ideaId: String): Call<Array<Reaction>>

    @Headers("Content-Type: application/json")
    @POST("api/ideas/addLikeToIdea/{ideaId}")
    fun addLikeToIdea(@Path("ideaId") ideaId: String, @Body like: Like): Call<Idea>

    @Headers("Content-Type: application/json")
    @POST("api/ideas/addShareToIdea/{ideaId}")
    fun addShareToIdea(@Path("ideaId") ideaId: String, @Body share: Share): Call<Idea>

    @Headers("Content-Type: application/json")
    @POST("api/ideas/addReactionToIdea/{ideaId}")
    fun addReactionToIdea(@Path("ideaId") ideaId: String, @Body reaction: Reaction): Call<Idea>
}