package be.kdg.cityOfIdeas.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.model.QuestionList
import kotlinx.android.synthetic.main.list_item_questionlist.view.*

class QuestionListAdapter(val listener: QuestionListListener, val questionLists: Array<QuestionList>): androidx.recyclerview.widget.RecyclerView.Adapter<QuestionListAdapter.QuestionListViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): QuestionListViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.list_item_questionlist, viewGroup, false)

        return QuestionListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return questionLists.size
    }

    override fun onBindViewHolder(viewHolder: QuestionListViewHolder, position: Int) {
        val questionList = questionLists[position]

        viewHolder.txtQuestionListTitle.text = questionList.title
        viewHolder.txtDescription.text = questionList.description

        viewHolder.itemView.setOnClickListener {
            listener.onQuestionListClicked(position)
        }
    }

    class QuestionListViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        val txtQuestionListTitle = view.txtQuestionListTitle!!
        val txtDescription = view.txtDescription!!
    }

    interface QuestionListListener{
        fun onQuestionListClicked(position: Int)
    }
}