package be.kdg.cityOfIdeas.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.adapter.ProjectAdapter
import be.kdg.cityOfIdeas.helper.getProjectAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.Project
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectOverview : AppCompatActivity(), ProjectAdapter.ProjectClickListener {
    private lateinit var rvProjectOverview: androidx.recyclerview.widget.RecyclerView
    private var projects: Array<Project> = arrayOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_overview)

        initialiseViews()
        doApiCalls()
    }

    fun initialiseViews(){
        rvProjectOverview = findViewById(R.id.rvProjectOverview)

        rvProjectOverview.layoutAnimation = AnimationUtils.loadLayoutAnimation(this@ProjectOverview, R.anim.layoutanimation_up_to_down)
    }

    fun doApiCalls(){
        getProjectAPI().fetchProjects().enqueue(object: Callback<Array<Project>>{
            override fun onResponse(call: Call<Array<Project>>, response: Response<Array<Project>>) {
                val p = response.body()
                if(p != null){
                    projects = p

                    rvProjectOverview.adapter = ProjectAdapter(this@ProjectOverview, p, this@ProjectOverview, resources)
                    rvProjectOverview.layoutManager =
                        androidx.recyclerview.widget.LinearLayoutManager(this@ProjectOverview)

                    rvProjectOverview.adapter!!.notifyDataSetChanged()
                    rvProjectOverview.scheduleLayoutAnimation()
                }else{
                    setContentView(R.layout.no_content)
                }
            }

            override fun onFailure(call: Call<Array<Project>>, t: Throwable) {
                showErrorPopUp(this@ProjectOverview)
            }
        })
    }

    override fun onProjectClicked(position: Int) {
        val intent = Intent(this, ProjectDetail::class.java)
        intent.putExtra(PROJECT_ID, projects[position].id)
        startActivity(intent)
    }

    override fun onResume() {
        doApiCalls()
        super.onResume()
    }
}
