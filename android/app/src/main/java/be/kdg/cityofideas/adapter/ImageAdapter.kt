package be.kdg.cityOfIdeas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import be.kdg.cityOfIdeas.helper.CURRENT_ORGANISATION
import be.kdg.cityOfIdeas.helper.PICASSO_URL
import be.kdg.cityOfIdeas.helper.getPicasso
import be.kdg.cityOfIdeas.model.Image
import be.kdg.cityofideas.R
import kotlinx.android.synthetic.main.list_item_image.view.*

class ImageAdapter(val context: Context, val images: Array<Image>): androidx.recyclerview.widget.RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ImageAdapter.ImageViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.list_item_image, viewGroup, false)

        return ImageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(viewHolder: ImageAdapter.ImageViewHolder, position: Int) {
        getPicasso(context)
                .load("$PICASSO_URL/organisations/${CURRENT_ORGANISATION.name!!.toLowerCase()}/organisation/projects/ideations/ideas/images/${images[position].imageName}")
                .error(R.drawable.no_image_found)
                .into(viewHolder.imgIdea)
    }

    class ImageViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        val imgIdea = view.imgIdea
    }
}