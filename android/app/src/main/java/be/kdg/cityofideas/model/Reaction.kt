package be.kdg.cityOfIdeas.model

import be.kdg.cityOfIdeas.model.User
import org.joda.time.DateTime

class Reaction(
    val comment: String,
    val user: User,
    val time: DateTime?
)