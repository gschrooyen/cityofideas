package be.kdg.cityOfIdeas.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import be.kdg.cityOfIdeas.helper.*
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.model.Idea
import be.kdg.cityOfIdeas.model.Ideation
import be.kdg.cityOfIdeas.model.Share
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IdeationDetail : AppCompatActivity() {
    var ideationId = "0"

    private lateinit var txtIdeationTitle: TextView
    private lateinit var txtDescription: TextView
    private lateinit var txtBeginDate: TextView
    private lateinit var txtEndDate: TextView
    private lateinit var progressDates: ProgressBar
    private lateinit var btnShares: Button
    private lateinit var btnComment: Button
    private lateinit var txtShares: TextView
    private lateinit var txtIdeas: TextView
    private lateinit var imgIdeation: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ideation_detail)

        ideationId = intent.getStringExtra(IDEATION_ID)

        initialiseViews()
        updateInfo()
    }

    fun initialiseViews(){
        txtIdeationTitle = findViewById(R.id.txtIdeationTitle)
        txtDescription = findViewById(R.id.txtDescription)
        txtBeginDate = findViewById(R.id.txtBeginDate)
        txtEndDate = findViewById(R.id.txtEndDate)
        progressDates = findViewById(R.id.progressDates)
        btnShares = findViewById(R.id.btnShares)
        btnComment = findViewById(R.id.btnComment)
        txtShares = findViewById(R.id.txtShares)
        txtIdeas = findViewById(R.id.txtIdeas)
        imgIdeation = findViewById(R.id.imgIdeation)
    }

    fun updateInfo(){
        getIdeationAPI().fetchIdeation(ideationId).enqueue(object: Callback<Ideation>{
            override fun onFailure(call: Call<Ideation>, t: Throwable) {showErrorPopUp(this@IdeationDetail)}
            override fun onResponse(call: Call<Ideation>, response: Response<Ideation>) {
                if(response.isSuccessful){
                    val ideation = response.body()!!

                    txtIdeationTitle.text = ideation.titleIdeation
                    txtDescription.text = ideation.descriptionIdeation
                    txtBeginDate.text = ideation.begin.toString(dateFormatter)
                    txtEndDate.text = ideation.end.toString(dateFormatter)
                    progressDates.progress = getPercentageBetweenTwoDates(ideation.begin, ideation.end)
                    if(progressDates.progress < 100){
                        progressDates.progressTintList = resources.getColorStateList(R.color.colorPrimary)
                    }else{
                        progressDates.progressTintList = resources.getColorStateList(R.color.lime)
                    }

                    val url = "$PICASSO_URL/organisations/${CURRENT_ORGANISATION.name!!.toLowerCase()}/organisation/projects/ideations/images/${ideation.imageName}"
                    getPicasso(this@IdeationDetail).load(url).error(R.drawable.no_image_found).into(imgIdeation)
                }else{
                    startActivity(Intent(this@IdeationDetail, OrganisationActivity::class.java))
                    showErrorPopUp(this@IdeationDetail)
                }
            }
        })

        addEventHandlers()
        doApiCalls()
    }

    fun doApiCalls(){
        getIdeationAPI().fetchSharesFromIdeation(ideationId).enqueue(object: Callback<Array<Share>>{
            override fun onFailure(call: Call<Array<Share>>, t: Throwable) {showErrorPopUp(this@IdeationDetail)}
            override fun onResponse(call: Call<Array<Share>>, response: Response<Array<Share>>) {
                if(response.code() != 200){
                    showErrorPopUp(this@IdeationDetail)
                    startActivity(Intent(this@IdeationDetail, MainActivity::class.java))
                }else{
                    if(response.isSuccessful){
                        val sharesText = response.body()!!.size.toString() + " keer geshared"
                        txtShares.text = sharesText
                    }else{
                        startActivity(Intent(this@IdeationDetail, OrganisationActivity::class.java))
                        showErrorPopUp(this@IdeationDetail)
                    }
                }
            }
        })

        getIdeationAPI().fetchIdeasFromIdeation(ideationId).enqueue(object: Callback<Array<Idea>>{
            override fun onFailure(call: Call<Array<Idea>>, t: Throwable) {showErrorPopUp(this@IdeationDetail)}
            override fun onResponse(call: Call<Array<Idea>>, response: Response<Array<Idea>>) {
                if(response.code() != 200){
                    showErrorPopUp(this@IdeationDetail)
                    startActivity(Intent(this@IdeationDetail, MainActivity::class.java))
                }else{
                    if(response.isSuccessful){
                        val ideasText = response.body()!!.size.toString() + " ideëen"
                        txtIdeas.text = ideasText
                    }else{
                        startActivity(Intent(this@IdeationDetail, OrganisationActivity::class.java))
                        showErrorPopUp(this@IdeationDetail)
                    }
                }
            }
        })
    }

    fun addEventHandlers(){
        btnComment.setOnClickListener {
            val intent = Intent(this, IdeationIdeas::class.java)
            intent.putExtra(IDEATION, ideationId)
            startActivity(intent)
        }

        btnShares.setOnClickListener {
            getIdeationAPI().addShareToIdeation(ideationId, Share()).enqueue(object: Callback<Ideation>{
                override fun onFailure(call: Call<Ideation>, t: Throwable) {showErrorPopUp(this@IdeationDetail)}
                override fun onResponse(call: Call<Ideation>, response: Response<Ideation>) {
                    if (response.isSuccessful){
                        updateInfo()
                    }else{
                        startActivity(Intent(this@IdeationDetail, OrganisationActivity::class.java))
                        showErrorPopUp(this@IdeationDetail)
                    }
                }
            })
        }
    }

    override fun onResume() {
        updateInfo()

        super.onResume()
    }
}
