package be.kdg.cityOfIdeas.activity

import com.google.android.material.tabs.TabLayout
import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.fragment.project.ProjectDetailMainFragment
import be.kdg.cityOfIdeas.fragment.project.ProjectDetailSecondaryFragment
import be.kdg.cityOfIdeas.fragment.project.ProjectDetailThirdFragment
import kotlinx.android.synthetic.main.activity_project_detail.*
import java.lang.Exception

class ProjectDetail : AppCompatActivity() {
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_detail)

        setSupportActionBar(toolbar)
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        container.adapter = mSectionsPagerAdapter

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_project_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    inner class SectionsPagerAdapter(fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            val toReturn: Fragment

            when(position){
                0 -> toReturn = ProjectDetailMainFragment()
                1 -> toReturn = ProjectDetailSecondaryFragment()
                2 -> toReturn = ProjectDetailThirdFragment()
                else -> throw Exception("Cannot initialize Fragment")
            }

            val bundle = Bundle()
            bundle.putString(PROJECT_ID, intent.getStringExtra(PROJECT_ID))
            toReturn.arguments = bundle
            return toReturn
        }

        override fun getCount(): Int {
            return 3
        }
    }
}
