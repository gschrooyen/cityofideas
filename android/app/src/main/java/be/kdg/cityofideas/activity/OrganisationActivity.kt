package be.kdg.cityOfIdeas.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import be.kdg.cityOfIdeas.helper.*
import be.kdg.cityOfIdeas.model.Organisation
import be.kdg.cityofideas.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrganisationActivity : AppCompatActivity() {
    private lateinit var txtAutoComplete: AppCompatAutoCompleteTextView
    private lateinit var btnChooseCity: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organisation)

        initializeAutoCompleteTextView()
        addEventHandlers()
    }

    fun initializeAutoCompleteTextView(){
        btnChooseCity = findViewById(R.id.btnChooseCity)
        txtAutoComplete = findViewById(R.id.txtPostalCodeAutoComplete)
        val postalCodes: ArrayList<String> = ArrayList()

        var adapter: ArrayAdapter<String>

        getOrganisationAPI().fetchAllOrganisations().enqueue(object: Callback<List<Organisation>> {
            override fun onFailure(call: Call<List<Organisation>>, t: Throwable) {showErrorPopUp(this@OrganisationActivity)}
            override fun onResponse(call: Call<List<Organisation>>, response: Response<List<Organisation>>) {
                if(response.code() == 200){
                    for(organisation in response.body()!!){
                        val stringToAdd = organisation.name + " - " + organisation.zipCode
                        postalCodes.add(stringToAdd)
                    }

                    adapter = ArrayAdapter(this@OrganisationActivity, android.R.layout.simple_list_item_1, postalCodes)
                    txtAutoComplete.setAdapter(adapter)
                }
            }
        })
    }

    fun addEventHandlers() {
        btnChooseCity.setOnClickListener {
            val organisationName = txtAutoComplete.text.split(" ")[0]
            setBaseURL(organisationName)

            getOrganisationAPI().fetchOrganisation(organisationName.toUpperCase()).enqueue(object: Callback<Organisation>{
                override fun onFailure(call: Call<Organisation>, t: Throwable) {showErrorPopUp(this@OrganisationActivity)}
                override fun onResponse(call: Call<Organisation>, response: Response<Organisation>) {
                    if(response.code() == 200){
                        setBaseOrganisation(response.body()!!)

                    }
                }
            })

            try {
                Thread.sleep(1500)
            } catch (e: InterruptedException) {
            }


            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        resetCurrentOrganisation()
        super.onResume()
    }
}
