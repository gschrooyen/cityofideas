package be.kdg.cityOfIdeas.api

import be.kdg.cityOfIdeas.model.Idea
import be.kdg.cityOfIdeas.model.Ideation
import be.kdg.cityOfIdeas.model.Share
import retrofit2.Call
import retrofit2.http.*

interface IdeationAPI {
    @GET("api/ideations/getIdeationsFromProject/{projectId}")
    fun fetchIdeationsFromProject(@Path("projectId") projectId: String): Call<Array<Ideation>>

    @GET("api/ideations/getIdeation/{ideationId}")
    fun fetchIdeation(@Path("ideationId") ideationId: String): Call<Ideation>

    @GET("api/ideations/getSharesFromIdeation/{ideationId}")
    fun fetchSharesFromIdeation(@Path("ideationId") ideationId: String): Call<Array<Share>>

    @GET("api/ideations/getIdeasFromIdeation/{ideationId}")
    fun fetchIdeasFromIdeation(@Path("ideationId") ideationId: String): Call<Array<Idea>>

    @Headers("Content-Type: application/json")
    @POST("api/ideations/addShare/{ideationId}")
    fun addShareToIdeation(@Path("ideationId") ideationId: String, @Body share: Share): Call<Ideation>

    @Headers("Content-Type: application/json")
    @POST("api/ideations/addIdeaToIdeation/{ideationId}")
    fun addIdeaToIdeation(@Path("ideationId") ideationId: String, @Body idea: Idea): Call<Ideation>
}