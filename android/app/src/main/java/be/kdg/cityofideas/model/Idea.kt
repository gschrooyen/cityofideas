package be.kdg.cityOfIdeas.model

data class Idea(
        val id: String?,
        val title: String,
        val description: String,
        val reactions: Array<Reaction>?,
        val likes: Array<Like>?,
        val imageNames: Array<Image>,
        val shares: Array<Share>?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Idea

        if (id != other.id) return false
        if (title != other.title) return false
        if (description != other.description) return false
        if (!reactions!!.contentEquals(other.reactions!!)) return false
        if (!likes!!.contentEquals(other.likes!!)) return false
        if (!shares!!.contentEquals(other.shares!!)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + reactions!!.contentHashCode()
        result = 31 * result + likes!!.contentHashCode()
        result = 31 * result + shares!!.contentHashCode()
        return result
    }
}