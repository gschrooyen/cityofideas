package be.kdg.cityOfIdeas.api

import be.kdg.cityOfIdeas.model.Organisation
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface OrganisationAPI {
    @GET("api/organisations/getAllOrganisations")
    fun fetchAllOrganisations(): Call<List<Organisation>>

    @GET("api/organisations/getOrganisation/{organisationName}")
    fun fetchOrganisation(@Path("organisationName") organisationName: String): Call<Organisation>
}