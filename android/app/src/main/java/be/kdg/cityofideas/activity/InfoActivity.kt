package be.kdg.cityOfIdeas.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import be.kdg.cityofideas.R

class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
    }
}
