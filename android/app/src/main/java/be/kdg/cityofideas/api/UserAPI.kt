package be.kdg.cityOfIdeas.api

import be.kdg.cityOfIdeas.model.User
import retrofit2.Call
import retrofit2.http.*

interface UserAPI {
    @Headers("Content-Type: application/json")
    @POST("api/users/authenticate")
    fun authenticateUser(@Body user: User): Call<User>

    @Headers("Content-Type: application/json")
    @POST("api/users/register")
    fun registerUser(@Body user: User): Call<User>

    @Headers("Content-Type: application/json")
    @POST("api/users/updateUser")
    fun updateUser(@Body user: User): Call<User>
}