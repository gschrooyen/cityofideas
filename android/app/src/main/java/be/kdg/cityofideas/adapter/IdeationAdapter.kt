package be.kdg.cityOfIdeas.adapter

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.helper.dateFormatter
import be.kdg.cityOfIdeas.helper.getPercentageBetweenTwoDates
import be.kdg.cityOfIdeas.model.Ideation
import kotlinx.android.synthetic.main.list_item_ideation_overview.view.*

class IdeationAdapter(val listener: IdeationClickListener, val resources: Resources, val ideations: Array<Ideation>): androidx.recyclerview.widget.RecyclerView.Adapter<IdeationAdapter.IdeationViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): IdeationViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.list_item_ideation_overview, viewGroup, false)

        return IdeationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ideations.size
    }

    override fun onBindViewHolder(viewHolder: IdeationViewHolder, position: Int) {
        val ideation = ideations[position]

        viewHolder.txtIdeationTitle.text = ideation.titleIdeation
        val text = ideation.ideas.size.toString() + " ideeën"
        viewHolder.txtIdeas.text = text
        val sharesText = ideation.shares.size.toString() + " shares"
        viewHolder.txtShares.text = sharesText
        viewHolder.txtBeginDate.text = ideation.begin.toString(dateFormatter)
        viewHolder.txtEndDate.text = ideation.end.toString(dateFormatter)
        viewHolder.progressDates.progress = getPercentageBetweenTwoDates(ideation.begin, ideation.end)
        if(viewHolder.progressDates.progress < 100){
            viewHolder.progressDates.progressTintList = resources.getColorStateList(R.color.colorPrimary)
        }else{
            viewHolder.progressDates.progressTintList = resources.getColorStateList(R.color.lime)
        }

        viewHolder.itemView.setOnClickListener {
            listener.onIdeationClicked(position)
        }
    }

    class IdeationViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        val txtIdeationTitle = view.txtIdeationTitle
        val txtIdeas = view.txtIdeas
        val txtShares = view.txtShares
        val txtBeginDate = view.txtBeginDate
        val txtEndDate = view.txtEndDate
        val progressDates = view.progressDates
    }

    interface IdeationClickListener{
        fun onIdeationClicked(pos: Int)
    }
}