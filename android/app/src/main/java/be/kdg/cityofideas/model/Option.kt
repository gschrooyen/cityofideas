package be.kdg.cityOfIdeas.model

class Option(
    val id: String,
    val title: String
)