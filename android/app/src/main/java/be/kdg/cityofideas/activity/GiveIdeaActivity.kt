package be.kdg.cityOfIdeas.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.textfield.TextInputEditText
import android.view.View
import android.widget.Button
import android.widget.TextView
import be.kdg.cityOfIdeas.helper.getIdeationAPI
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.Idea
import be.kdg.cityOfIdeas.model.Ideation
import be.kdg.cityofideas.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val PICK_IMAGE = 1
const val PICK_VIDEO = 2

class GiveIdeaActivity : AppCompatActivity() {
    private lateinit var txtIdeaTitle: TextInputEditText
    private lateinit var txtIdeaDescription: TextInputEditText
    private lateinit var btnSelectImage: Button
    private lateinit var btnSelectVideo: Button
    private lateinit var btnGiveIdea: Button

    private lateinit var txtImageSelected: TextView
    private lateinit var txtVideoSelected: TextView

    var allowImage = false
    var allowVideo = false
    var ideationId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_give_idea)

        allowImage = intent.getBooleanExtra(ALLOW_IMAGE, false)
        allowVideo = intent.getBooleanExtra(ALLOW_VIDEO, false)
        ideationId = intent.getStringExtra(IDEATION_ID)

        initializeViews()
        addEventHandlers()
    }

    fun initializeViews(){
        txtIdeaTitle = findViewById(R.id.txtIdeaTitle)
        txtIdeaDescription = findViewById(R.id.txtIdeaDescription)
        btnSelectImage = findViewById(R.id.btnSelectImage)
        btnSelectVideo = findViewById(R.id.btnSelectVideo)
        btnGiveIdea = findViewById(R.id.btnGiveIdea)

        txtImageSelected = findViewById(R.id.txtImageSelected)
        txtVideoSelected = findViewById(R.id.txtVideoSelected)

        txtImageSelected.visibility = View.INVISIBLE
        txtVideoSelected.visibility = View.INVISIBLE

        if(allowImage && allowVideo){
            btnSelectImage.visibility = View.VISIBLE
            btnSelectVideo.visibility = View.VISIBLE
        }else if(allowImage){
            btnSelectImage.visibility = View.VISIBLE
            btnSelectVideo.visibility = View.INVISIBLE
        }else if(allowVideo){
            btnSelectImage.visibility = View.INVISIBLE
            btnSelectVideo.visibility = View.VISIBLE
        }else{
            btnSelectImage.visibility = View.INVISIBLE
            btnSelectVideo.visibility = View.INVISIBLE
        }
    }

    fun addEventHandlers(){
        btnGiveIdea.setOnClickListener {
            if(checkInputs()){
                getIdeationAPI().addIdeaToIdeation(ideationId, Idea(null, txtIdeaTitle.text.toString(), txtIdeaDescription.text.toString(), arrayOf(), arrayOf(), arrayOf(), arrayOf())).enqueue(object: Callback<Ideation>{
                    override fun onFailure(call: Call<Ideation>, t: Throwable) {
                        showErrorPopUp(this@GiveIdeaActivity)
                    }
                    override fun onResponse(call: Call<Ideation>, response: Response<Ideation>) {
                        val newIntent = Intent (this@GiveIdeaActivity, IdeationIdeas::class.java)
                        newIntent.putExtra(IDEATION, intent.getStringExtra(IDEATION))
                        startActivity(newIntent)
                    }
                })
            }
        }

        btnSelectImage.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, resources.getString(R.string.select_image)), PICK_IMAGE)
        }

        btnSelectVideo.setOnClickListener {
            val intent = Intent()
            intent.type = "video/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, resources.getString(R.string.select_video)), PICK_VIDEO)
        }
    }

    fun checkInputs(): Boolean{
        if(txtIdeaTitle.text.isNullOrBlank() || txtIdeaDescription.text.isNullOrBlank())
            return false

        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == PICK_IMAGE){
            txtImageSelected.visibility = View.VISIBLE
        }else if(requestCode == PICK_VIDEO){
            txtVideoSelected.visibility = View.VISIBLE
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
