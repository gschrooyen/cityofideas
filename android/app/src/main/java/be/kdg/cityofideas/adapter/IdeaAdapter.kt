package be.kdg.cityOfIdeas.adapter

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.helper.dateFormatter
import be.kdg.cityOfIdeas.model.Idea
import kotlinx.android.synthetic.main.list_item_ideation_ideas.view.*

class IdeaAdapter(val context: Context, val listener: IdeaClickListener, val ideas: Array<Idea>, val resources: Resources): androidx.recyclerview.widget.RecyclerView.Adapter<IdeaAdapter.IdeaViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): IdeaViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.list_item_ideation_ideas, viewGroup, false)

        return IdeaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ideas.size
    }

    override fun onBindViewHolder(viewHolder: IdeaViewHolder, position: Int) {
        val idea = ideas[position]

        viewHolder.txtIdeaTitle.text = idea.title

        if(idea.likes!!.isNotEmpty()){
            val likesText = idea.likes.size.toString() + " likes"
            viewHolder.txtLikes.text = likesText
        }else{
            viewHolder.txtLikes.text = resources.getString(R.string.zero_likes)
        }

        if(idea.shares!!.isNotEmpty()){
            val sharesText = idea.shares.size.toString() + " shares"
            viewHolder.txtShares.text = sharesText
        }else{
            viewHolder.txtShares.text = resources.getString(R.string.zero_shares)
        }

        val reactionText = idea.reactions!!.size.toString() + " reacties"
        viewHolder.txtReactions.text = reactionText
        if(idea.reactions.isNotEmpty()){
            if(idea.reactions.size > 1){
                viewHolder.txtUserName1.text = idea.reactions[idea.reactions.size-1].user.userName
                viewHolder.txtFirstReaction.text = idea.reactions[idea.reactions.size-1].comment
                viewHolder.txtDate1.text = idea.reactions[idea.reactions.size-1].time!!.toString(dateFormatter)

                viewHolder.txtUserName2.text = idea.reactions[idea.reactions.size-2].user.userName
                viewHolder.txtSecondReaction.text = idea.reactions[idea.reactions.size-2].comment
                viewHolder.txtDate2.text = idea.reactions[idea.reactions.size-2].time!!.toString(dateFormatter)
            }else{
                viewHolder.txtUserName1.text = idea.reactions[idea.reactions.size-1].user.userName
                viewHolder.txtFirstReaction.text = idea.reactions[idea.reactions.size-1].comment
                viewHolder.txtDate1.text = idea.reactions[idea.reactions.size-1].time!!.toString(dateFormatter)
            }
        }

        viewHolder.itemView.setOnClickListener {
            listener.onIdeaClicked(position)
        }
    }

    class IdeaViewHolder(view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        val txtIdeaTitle = view.txtIdeaTitle
        val txtLikes = view.txtLikes
        val txtShares = view.txtShares
        val txtReactions = view.txtReactions
        val txtFirstReaction = view.txtFirstReaction
        val txtSecondReaction = view.txtSecondReaction
        val txtUserName1 = view.txtUserName1
        val txtUserName2= view.txtUserName2
        val txtDate1 = view.txtDate1
        val txtDate2 = view.txtDate2
    }

    interface IdeaClickListener{
        fun onIdeaClicked(position: Int)
    }
}