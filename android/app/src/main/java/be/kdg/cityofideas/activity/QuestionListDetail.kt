package be.kdg.cityOfIdeas.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatImageView
import be.kdg.cityOfIdeas.helper.*
import be.kdg.cityOfIdeas.model.Answer
import be.kdg.cityOfIdeas.model.Question
import be.kdg.cityOfIdeas.model.QuestionList
import be.kdg.cityofideas.R
import be.kdg.cityOfIdeas.model.Option
import com.google.android.gms.maps.MapView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuestionListDetail : AppCompatActivity() {
    private lateinit var parent: LinearLayout
    private lateinit var btnNextQuestion: Button
    private lateinit var txtErrorQuestionList: TextView

    private lateinit var txtQuestionListTitle: TextView
    private lateinit var progressQuestionList: ProgressBar
    private lateinit var txtProgress: TextView

    private var views: ArrayList<View> = ArrayList()
    private var questions: Array<Question> = arrayOf()

    private val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    private val fullWidthLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    private var currentQuestionIndex = 0

    private lateinit var questionList: QuestionList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question_list_detail)

        getQuestionListAPI().fetchQuestionList(intent.getStringExtra(QUESTIONLIST_ID)).enqueue(object: Callback<QuestionList>{
            override fun onFailure(call: Call<QuestionList>, t: Throwable) {showErrorPopUp(this@QuestionListDetail)}
            override fun onResponse(call: Call<QuestionList>, response: Response<QuestionList>) {
                if(response.isSuccessful){
                    questionList = response.body()!!
                    questions = questionList.questions!!

                    initialiseViews()
                    addEventHandlers()
                    loadViews()
                    updateInfo()
                }else{
                    startActivity(Intent(this@QuestionListDetail, OrganisationActivity::class.java))
                }
            }
        })
    }

    fun initialiseViews(){
        parent = findViewById(R.id.parent)
        btnNextQuestion = findViewById(R.id.btnNextQuestion)

        txtQuestionListTitle = findViewById(R.id.txtQuestionListTitle)
        progressQuestionList = findViewById(R.id.progressQuestionList)
        txtProgress = findViewById(R.id.txtProgress)
        txtErrorQuestionList = findViewById(R.id.txtErrorQuestionList)
    }

    fun addEventHandlers(){
        val size = questionList.questions!!.size

        btnNextQuestion.setOnClickListener {
            addAnswer()

            getQuestionListAPI().updateQuestion(questions[currentQuestionIndex].id, questions[currentQuestionIndex].awnsers).enqueue(object: Callback<Question>{
                override fun onFailure(call: Call<Question>, t: Throwable) {}
                override fun onResponse(call: Call<Question>, response: Response<Question>) {
                    if(response.isSuccessful){

                    }
                }
            })

            if(currentQuestionIndex == size-1){
                startActivity(Intent(this@QuestionListDetail, MainActivity::class.java))
                Toast.makeText(this@QuestionListDetail, resources.getString(R.string.questionlist_filled), Toast.LENGTH_LONG).show()
            }else{
                currentQuestionIndex++

                updateInfo()
                loadViews()
            }


        }
    }

    fun addAnswer() {
        val question = questions[currentQuestionIndex]
        val answers: ArrayList<String> = ArrayList()
        question.awnsers = arrayOf()


        if (question.options.isNullOrEmpty()) {
            val view = views[0]
            when (view) {
                is EditText -> answers.add(view.text.toString())
                is Spinner -> answers.add(view.selectedItem.toString())
                is MapView -> answers.add("mapview wordt niet ondersteund in android")
            }
        } else {
            for (view in views) {
                when (view) {
                    is CheckBox -> {
                        if (view.isChecked)
                            answers.add(view.text.toString())
                    }
                    is RadioButton -> {
                        if (view.isChecked)
                            answers.add(view.text.toString())
                    }
                    is Spinner -> {
                        answers.add(view.selectedItem.toString())
                    }
                }
            }
        }

        question.awnsers = answers.toTypedArray()
    }

    fun updateInfo(){
        txtQuestionListTitle.text = questionList.title

        val text = "Vraag: ${currentQuestionIndex +1}/${questionList.questions!!.size}"
        txtProgress.text = text

        val progress = getProgress(questionList.questions!!.size)
        progressQuestionList.progress = progress
        when{
            progress<25 -> progressQuestionList.progressTintList = resources.getColorStateList(R.color.red)
            progress<50 -> progressQuestionList.progressTintList = resources.getColorStateList(R.color.facebook)
            progress<75 -> progressQuestionList.progressTintList = resources.getColorStateList(R.color.colorPrimary)
            progress<100 -> progressQuestionList.progressTintList = resources.getColorStateList(R.color.orange)
            else -> progressQuestionList.progressTintList = resources.getColorStateList(R.color.lime)
        }
    }

    fun loadViews(){
        parent.removeAllViews()
        views.clear()

        if(!questions.isNullOrEmpty()){
            val question = questions[currentQuestionIndex]

            addQuestionToView(question)

            if(question.options.isNullOrEmpty()){
                when(question.openQuestionType){
                    0 -> addEditText(false)
                    1 -> addEditText(true)
                    2 -> addMap()
                }
            }else{
                when(question.choiseQuestionType){
                    0 -> addRadioButton(question.options)
                    1 -> addCheckBox(question.options)
                    2 -> addDropDownMenu(question.options)
                }
            }
        }
    }

    fun addQuestionToView(question: Question){
        val questionTitle = TextView(this@QuestionListDetail)
        questionTitle.setTextColor(resources.getColor(R.color.white))
        questionTitle.text = question.title
        questionTitle.textSize = 18f
        questionTitle.layoutParams = layoutParams
        parent.addView(questionTitle)
    }

    fun addEditText(multiLine: Boolean){
        val et = EditText(this@QuestionListDetail)
        et.setTextColor(resources.getColor(R.color.white))

        views.add(et)

        fullWidthLayoutParams.setMargins(8,8,8,8)
        et.layoutParams = fullWidthLayoutParams
        if(multiLine){
            et.setSingleLine(false)
            et.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
        }

        parent.addView(et)
    }

    fun addRadioButton(options: List<Option>){
        val radioGroup = RadioGroup(this@QuestionListDetail)

        for(option in options){
            val radioButton = RadioButton(this@QuestionListDetail)

            views.add(radioButton)

            radioButton.setTextColor(resources.getColor(R.color.white))

            radioButton.text = option.title
            radioButton.layoutParams = layoutParams
            radioGroup.addView(radioButton)
        }

        radioGroup.layoutParams = layoutParams
        parent.addView(radioGroup)
    }

    fun addCheckBox(options: List<Option>){
        for(option in options){
            val checkBox = CheckBox(this@QuestionListDetail)
            checkBox.setTextColor(resources.getColor(R.color.white))
            views.add(checkBox)

            checkBox.text = option.title
            checkBox.layoutParams = layoutParams

            checkBox.setOnCheckedChangeListener{ _, isChecked ->
                checkBox.isChecked = isChecked
            }

            parent.addView(checkBox)
        }
    }

    fun addDropDownMenu(options: List<Option>){
        val optionsAsString: ArrayList<String> = ArrayList()

        for(option in options){
            optionsAsString.add(option.title)
        }

        val spinner = Spinner(this@QuestionListDetail)
        val adapter = ArrayAdapter<String>(this@QuestionListDetail, R.layout.list_item_spinner, optionsAsString)
        spinner.adapter = adapter

        views.add(spinner)

        parent.addView(spinner)
    }

    fun addMap(){
        val map = AppCompatImageView(this@QuestionListDetail)

        map.layoutParams = layoutParams
        map.setImageResource(R.drawable.no_content)
        views.add(map)

        parent.addView(map)
    }

    fun getProgress(size: Int): Int{
        return ((currentQuestionIndex+1).toDouble()/size.toDouble()*100).toInt()
    }

    override fun onResume() {
        getQuestionListAPI().fetchQuestionList(intent.getStringExtra(QUESTIONLIST_ID)).enqueue(object: Callback<QuestionList>{
            override fun onFailure(call: Call<QuestionList>, t: Throwable) {
                startActivity(Intent(this@QuestionListDetail, OrganisationActivity::class.java))
                showErrorPopUp(this@QuestionListDetail)
            }
            override fun onResponse(call: Call<QuestionList>, response: Response<QuestionList>) {
                if(response.isSuccessful){
                    currentQuestionIndex = 0
                    questionList = response.body()!!
                    questions = questionList.questions!!

                    initialiseViews()
                    addEventHandlers()
                    loadViews()
                    updateInfo()
                }else{
                    startActivity(Intent(this@QuestionListDetail, OrganisationActivity::class.java))
                    showErrorPopUp(this@QuestionListDetail)
                }
            }
        })

        super.onResume()
    }
}
