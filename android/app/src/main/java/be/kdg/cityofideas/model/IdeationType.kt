package be.kdg.cityOfIdeas.model

enum class IdeationType {
    IDEATION1, IDEATION2
}