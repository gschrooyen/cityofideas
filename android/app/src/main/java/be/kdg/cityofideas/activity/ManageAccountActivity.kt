package be.kdg.cityOfIdeas.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import be.kdg.cityOfIdeas.helper.APPUSER
import be.kdg.cityOfIdeas.helper.getUserAPI
import be.kdg.cityOfIdeas.helper.setAppUser
import be.kdg.cityOfIdeas.helper.showErrorPopUp
import be.kdg.cityOfIdeas.model.User
import be.kdg.cityofideas.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ManageAccountActivity : AppCompatActivity() {
    private lateinit var txtEmail: TextView
    private lateinit var txtFirstName: TextView
    private lateinit var txtLastName: TextView
    private lateinit var txtUserName: TextView
    private lateinit var btnChangeUser: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_account)

        initializeViews()
        updateInfo()
        addEventHandlers()
    }

    fun initializeViews() {
        txtEmail = findViewById(R.id.txtEmail)
        txtFirstName = findViewById(R.id.txtFirstName)
        txtLastName = findViewById(R.id.txtLastName)
        txtUserName = findViewById(R.id.txtUserName)
        btnChangeUser = findViewById(R.id.btnChangeUser)
    }

    fun updateInfo() {
        txtEmail.text = APPUSER.email
        txtFirstName.text = APPUSER.firstName!!.toLowerCase()
        txtLastName.text = APPUSER.lastName!!.toLowerCase()
        txtUserName.text = APPUSER.userName!!.toLowerCase()
    }

    fun addEventHandlers() {
        txtFirstName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val text = p0.toString() + "." + txtLastName.text.substring(0,1)
                txtUserName.text = text
            }
        })

        txtLastName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(!p0.isNullOrBlank()){
                    val text = txtFirstName.text.toString() + "." + p0.substring(0,1)
                    txtUserName.text = text
                }
            }
        })

        btnChangeUser.setOnClickListener {
            if (checkUserInput()) {
                val newUser = User(APPUSER.userId, txtUserName.text.toString(), txtFirstName.text.toString(), txtLastName.text.toString(), APPUSER.address, APPUSER.email, APPUSER.gender, APPUSER.verified, APPUSER.blocked, APPUSER.birthDate, APPUSER.passwordHash)

                getUserAPI().updateUser(newUser).enqueue(object : Callback<User> {
                    override fun onFailure(call: Call<User>, t: Throwable) {
                        showErrorPopUp(this@ManageAccountActivity)
                    }

                    override fun onResponse(call: Call<User>, response: Response<User>) {
                        if (response.code() == 200) {
                            setAppUser(response.body()!!)

                            startActivity(Intent(this@ManageAccountActivity, MainActivity::class.java))
                        }
                    }
                })
            }
        }
    }

    fun checkUserInput(): Boolean {
        if (txtFirstName.text.count() < 1 || txtLastName.text.count() < 1)
            return false

        return true
    }
}