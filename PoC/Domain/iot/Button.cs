using System.ComponentModel.DataAnnotations;
using PoC.Domain.input;
using PoC.Domain.inputmethod;

namespace PoC.Domain.iot
{
    public class Button
    {
        [Key] public string Id { get; set; }
        public Input Input { get; set; }
        public Option Option { get; set; }
        public Poster Poster { get; set; }
        
    }
}