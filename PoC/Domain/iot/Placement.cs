using System;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.datatype;
using PoC.Domain.project;

namespace PoC.Domain.iot
{
    public class Placement
    {
        [Key] public string Id { get; set; }
        public DateTime Begin { get; set; }
        public DateTime End { get; set; }
        public Location Location { get; set; }
        
        //foreign keys
        //public Poster Poster { get; set; }
        public Project Project { get; set; }
        
    }
}