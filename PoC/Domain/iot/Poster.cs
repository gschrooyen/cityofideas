using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using QRCoder;

namespace PoC.Domain.iot
{
    public class Poster
    {
        [Key] public string Id { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public List<Button> Buttons { get; set; }
        //Managed with NUGET package (QRCoder)
        [NotMappedAttribute]
        public QRCode QrCode { get; set; }
        
        public Placement CurrentPlacement { get; set; }
        
        //foreign keys
        public List<Placement> Placements { get; set; }
        //this is a Question Or an Idea
        
        public Question Question { get; set; }
        public Ideation Ideation { get; set; }
        public Idea Idea { get; set; }
        //This is an Awnser or a Vote Respectively

        public Poster()
        {
            
            Placements = new List<Placement>();
        }
    }
}