using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.input;
using PoC.Domain.input.interfaces;
using PoC.Domain.project;

namespace PoC.Domain.inputmethod
{
  public abstract class InputMethod : ISharable, IValidatableObject
  {
    [Key] public string Id { get; set; }
    [DataType(DataType.Date)] public DateTime Begin { get; set; }
    [DataType(DataType.Date)] public DateTime End { get; set; }

    [StringLength(2000, ErrorMessage = "Maximaal 2000 tekens")]
    public string Description { get; set; }

    [StringLength(100, ErrorMessage = "Maximaal 100 tekens")]
    public string Title { get; set; }

    //foreign keys
    public Fase Fase { get; set; }
    public IEnumerable<Share> Shares { get; set; }

    public InputMethod()
    {
      Shares = new List<Share>();
    }

    IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
    {
      List<ValidationResult> errors = new List<ValidationResult>();

      if (End <= Begin)
      {
        errors.Add(new ValidationResult("Einddatum kan niet voor de Startdatum liggen"));
      }

      if (End <= DateTime.Now)
      {
        errors.Add(new ValidationResult("Einddatum kan niet in het verleden liggen"));
      }

      return errors;
    }
  }
}