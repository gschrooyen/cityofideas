using System.Collections.Generic;

namespace PoC.Domain.inputmethod
{
    public class QuestionList : InputMethod
    {
        public bool Open { get; set; }
       
        
        //foreign keys
        public IEnumerable<Question> Questions { get; set; }
        
        //public Fase Fase { get; set; }

        public QuestionList()
        {
            Questions = new List<Question>();
        }
    }
}