using System.ComponentModel.DataAnnotations;
using PoC.Domain.inputMethod;

namespace PoC.Domain.inputmethod
{
    public class Option
    {
        [Key]
        public string Id { get; set; }

        public string Title { get; set; }
        //foreign keys
        public ChoiceQuestion Question { get; set; }
    }
}