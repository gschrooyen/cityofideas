using System.Collections.Generic;
using PoC.Domain.datatype;
using PoC.Domain.inputmethod;

namespace PoC.Domain.inputMethod
{
    public class ChoiceQuestion: Question
    {
        public ChoiseQuestionType ChoiseQuestionType { get; set; }
        public List<Option> Options { get; set; }

        public ChoiceQuestion()
        {
            Options = new List<Option>();
            }
        
    }
}