using PoC.Domain.datatype;

namespace PoC.Domain.inputmethod
{
    public class OpenQuestion: Question
    {
        public OpenQuestionType OpenQuestionType { get; set; }
        public float Longtitude { get; set; }
        public float Lattitude { get; set; }
    }
}