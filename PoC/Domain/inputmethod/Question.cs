using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.input;
using PoC.Domain.iot;
using PoC.Domain.iot.interfaces;

namespace PoC.Domain.inputmethod
{
    public abstract class Question: IPrintable
    {
        
        [Key]
        public string id { get; set; }
        public string Title { get; set; }
        public bool Required { get; set; }
        public string Rang { get; set; }
        
        
        //foreign keys
        public IEnumerable<Answer> Awnsers { get; set; }
        public IEnumerable<Poster> Posters { get; set; }
        public QuestionList QuestionList { get; set; }


        public Question()
        {
            Awnsers = new List<Answer>();
            Posters = new List<Poster>();
        }
    }
}