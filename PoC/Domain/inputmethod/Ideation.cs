using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.datatype;
using PoC.Domain.input;

namespace PoC.Domain.inputmethod
{
    public class Ideation : InputMethod
    {
        public IdeationType Type { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Maximaal 30 characters")]
        public string TitleIdeation { get; set; }
        [Required]
        [StringLength(2000, ErrorMessage = "Maximaal 200 characters")]
        public string DescriptionIdeation { get; set; }

        public string BackgroundLink { get; set; }
        
        public string ImageName { get; set; }
        // Naam Image in wwwRoot
        // De verschillende InputMogelijkheden die mogen meegegeven worden.
        public bool AllowImage { get; set; }
        public int NumberOfImages { get; set; }
        public bool ImageRequired { get; set; }
        public bool AllowVideo { get; set; }
        public int NumberOfVideos { get; set; }
        public bool VideoRequired { get; set; }
        public bool AllowVideoLink { get; set; }
        public bool VideoLinkRequired { get; set; }
        
        
        // Om Ideation af te sluiten
        public bool HideIdeation { get; set; }
        //TODO: implement after poc
        //public IEnumerable<IdeationInput> AllowedInputs { get; set; }
        
        //foreign keys
        public List<Idea> Ideas { get; set; }

        public Ideation()
        {
            Ideas = new List<Idea>();
            HideIdeation = false;
        }
    }
}