using System.ComponentModel.DataAnnotations;
using PoC.Domain.input;

namespace PoC.Domain.inputmethod
{
    public class Image
    {
        [Key]
        public string ImageName { get; set; }

        //Foreign Key
        public Idea Idea { get; set; }
        
        public Image(string imageName)
        {
            this.ImageName = imageName;
        }
    }
}