using System;
using System.Collections.Generic;
using PoC.Domain.input.interfaces;
using PoC.Domain.inputmethod;
using PoC.Domain.iot;
using PoC.Domain.iot.interfaces;
using PoC.Domain.moderation;
using PoC.Domain.moderation.interfaces;
using PoC.Domain.user;

namespace PoC.Domain.input
{
    public class Idea : Input, IModeratable, IPrintable, ISharable, IReactable, ILikeable, IVoteable
    {
        public string Title { get; set; }
        public string Description { get; set; }
        
        // Image,Video,VideoLink
        
        public IEnumerable<Image> ImageNames { get; set; } 
        public IEnumerable<Video> VideoNames { get; set; }

        public string VideoLinkName { get; set; }
        //foreign keys
        public IEnumerable<Reaction> Reactions { get; set; }
        public IEnumerable<Like> Likes { get; set; }
        
        public IEnumerable<Vote> Votes { get; set; }
        public IEnumerable<Share> Shares { get; set; }
        public IEnumerable<Poster> Posters { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        public IEnumerable<MediaIdea> MediaIdeas { get; set; }
        public Ideation Ideation { get; set; }
        public new Boolean Deleted { get; set; }

        public Idea()
        {
            Reactions = new List<Reaction>();
            Likes = new List<Like>();
           Votes = new List<Vote>();
            Shares = new List<Share>();
            Posters = new List<Poster>();
            Notifications = new List<Notification>();
            MediaIdeas = new List<MediaIdea>();
            
            ImageNames = new List<Image>();
            VideoNames = new List<Video>();
        }

        public Idea(string title, string description) : this()
        {
            Title = title;
            Description = description;
            Time = DateTime.Now;
            
        }

        public string GetId()
        {
            return Id;
        }

        public Ideation GetParent()
        {
            return Ideation;
        }

        string IReactable.GetId()
        {
            return GetId();
        }

        string ILikeable.GetId()
        {
            return GetId();
        }

    }
}