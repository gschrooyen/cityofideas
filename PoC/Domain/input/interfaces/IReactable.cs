using PoC.Domain.inputmethod;

namespace PoC.Domain.input.interfaces
{
    public interface IReactable
    {
        //TODO: implement
        string GetId();
        Ideation GetParent();
    }
}