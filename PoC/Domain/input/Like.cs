using System.ComponentModel.DataAnnotations.Schema;
using PoC.Domain.datatype;
using PoC.Domain.input.interfaces;

namespace PoC.Domain.input
{
    public class Like : Input
    {
        public VoteType Type { get; set; }
        
        //foreign keys
        [NotMappedAttribute]
        public ILikeable Likeable { get; set; }

        
    }
}