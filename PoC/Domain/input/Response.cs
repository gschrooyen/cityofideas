using PoC.Domain.datatype;

namespace PoC.Domain.input
{
    public class Response : Input
    {
        public string Title { get; set; }
        public string Status { get; set; }
        public ResponseType Type { get; set; }
        
        //foreign keys
        
    }
}