using System;
using System.Collections.Generic;
using PoC.Domain.iot;
using PoC.Domain.datatype;
using PoC.Domain.input.interfaces;
using PoC.Domain.inputmethod;
using PoC.Domain.moderation;
using PoC.Domain.moderation.interfaces;
using PoC.Domain.user;

namespace PoC.Domain.input
{
    public class Answer :Input, IModeratable, IVoteable
    {
        public AnswerType AnswerType { get; set; }
        public string Title { get; set; }
        //foreign keys
        public Question QuestionAwnsered { get; set; }
        public Poster Poster { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        public IEnumerable<Vote> Votes { get; set; }
        
        public new Boolean Deleted { get; set; }

        public Answer()
        {
            Time = DateTime.Now;
        }

        public Answer(string title): this()
        {
            Title = title;
        }

        public Answer(string title, User user): this(title)
        {
            this.User = user;
        }

        public string GetId()
        {
            return Id;
        }
    }
}