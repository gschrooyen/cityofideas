using System.ComponentModel.DataAnnotations.Schema;
using PoC.Domain.datatype;
using PoC.Domain.input.interfaces;


namespace PoC.Domain.input
{
    public class Share : Input
    {
        public Platform Platform { get; set; }
        public string Url { get; set; }
        public VoteType Type { get; set; }
        
        //foreign keys
        //idea, Project Or InputMethod
        [NotMappedAttribute]
        public ISharable Shareable { get; set; }
    }
}