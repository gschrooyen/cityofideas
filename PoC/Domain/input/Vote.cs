using System.ComponentModel.DataAnnotations.Schema;
using PoC.Domain.datatype;
using PoC.Domain.input.interfaces;
using PoC.Domain.iot;
using PoC.Domain.iot.interfaces;

namespace PoC.Domain.input
{
    public class Vote : Input, IPrintableInput
    {
        public VoteType Type { get; set; }
        
        //foreign keys
        [NotMappedAttribute]
        public IVoteable Voteable { get; set; }
        public Poster Poster { get; set; }
    }
}