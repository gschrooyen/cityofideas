using System;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.user;

namespace PoC.Domain.input
{
    public abstract class Input
    {
        [Key]
        public string Id { get; set; }
        public DateTime Time { get; set; }
        
        public Boolean Deleted { get; set; }
        
        //foreign keys
        public User User { get; set; }

        public Input()
        {
            Time = DateTime.Now;
        }
    }
}