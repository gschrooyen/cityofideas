using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using PoC.Domain.input.interfaces;
using PoC.Domain.moderation;
using PoC.Domain.moderation.interfaces;
using PoC.Domain.user;

namespace PoC.Domain.input
{
    public class Reaction : Input, IModeratable, ILikeable
    {
        public string Comment { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        
        //foreign keys
        public Idea Idea { get; set; }
        [NotMappedAttribute]
        public IReactable Reactable { get; set; }
        //public Answer Answer { get; set; }

        public Reaction()
        {
            Notifications = new List<Notification>();
            Time = DateTime.Now;
        }

        public Reaction(string comment): this()
        {
            Comment = comment;
        }
        public Reaction(string comment, User user): this(comment)
        {
            Comment = comment;
            User = user;
        }

        public Reaction(IReactable reactable, string comment)
        {
            Comment = comment;
            Reactable = reactable;
        }

        public Reaction(IReactable reactable, string comment, User user): this(comment, user)
        {
            if (reactable.GetType() == typeof(Idea))
            {
               Console.WriteLine("is idea");
               Idea = (Idea) reactable;
               //Answer = null;
            }
            else
            {
                Idea = null;
            }
            Reactable = reactable;
        }

        public string GetId()
        {
            return Id;
        }

        
    }
}