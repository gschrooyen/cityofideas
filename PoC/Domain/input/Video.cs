using System.ComponentModel.DataAnnotations;
using PoC.Domain.input;

namespace PoC.Domain.inputmethod
{
    public class Video
    {
        [Key]
        public string VideoName { get; set; }

        //Foreign Key
        public Idea Idea { get; set; }
        
        public Video(string videoName)
        {
            this.VideoName = videoName;
        }
    }
}