
namespace PoC.Domain.input
{
    public class MediaIdea :Idea
    {
        public string Filename { get; set; }
        public string Folder { get; set; }
        public string Filetype { get; set; }
    }
}