using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.project;
using PoC.Domain.user;

namespace PoC.Domain.organisation
{
  public class Organisation
  {
    [Key] public string id { get; set; }

    [Required(ErrorMessage = "Naam verplicht in te vullen")]
    [StringLength(50, ErrorMessage = "Maximaal 50 tekens")]
    public string Name { get; set; }

    public string FormattedName { get; set; }

    [Required(ErrorMessage = "Beschrijving verplicht in te vullen")]
    [StringLength(500, ErrorMessage = "Maximaal 500 tekens")]
    public string Description { get; set; }

    [Required(ErrorMessage = "Featured Text verplicht in te vullen")]
    [StringLength(200, ErrorMessage = "Maximaal 200 tekens")]
    public string FeaturedText { get; set; }

    [Required(ErrorMessage = "Postcode verplicht in te vullen")]
    [Range(1000,10000, ErrorMessage = "Geef een geldige postcode in (1000-10000).")]
    public string ZipCode { get; set; }

    [Required(ErrorMessage = "Gsm/Telefoonnummer verplicht in te vullen")]
    public string TelephoneNumber { get; set; }

    [Required(ErrorMessage = "Straat verplicht in te vullen")]
    public string Street { get; set; }

    [Required(ErrorMessage = "Stad verplicht in te vullen")]
    public string City { get; set; }

    [Required(ErrorMessage = "Land verplicht in te vullen")]
    public string Country { get; set; }

    [Required(ErrorMessage = "Email verplicht in te vullen")]
    public string Email { get; set; }


    //Afbeeldingen
    public string Image { get; set; }
    public string Logo { get; set; }
    public string Dia1 { get; set; }
    public string Dia2 { get; set; }
    public string Dia3 { get; set; }

    //Kleuren die superadmin kan kiezen.
    public string PrimaryColor { get; set; }
    public string SecondaryColor { get; set; }
    public string TertiaryColor { get; set; }

    public Boolean Closed { get; set; }

    //foreign keys
    public IEnumerable<User> Users { get; set; }
    public IEnumerable<Project> Projects { get; set; }


    public Organisation()
    {
      Users = new List<User>();
      Projects = new List<Project>();
      Closed = false;
    }
  }
}