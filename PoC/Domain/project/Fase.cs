using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.inputmethod;
using PoC.Domain.datatype;

namespace PoC.Domain.project
{
    public class Fase : IValidatableObject
    {
        [Key] public string Id { get; set; }
        
        public ProjectFase ProjectFase { get; set; }

        [Required(ErrorMessage = "Naam verplicht in te vullen")]
        [StringLength(100,ErrorMessage = "Maximaal 100 tekens")]
        public string FaseName { get; set; }

        [DataType(DataType.Date)]
        public DateTime Begin { get; set; }

        [DataType(DataType.Date)]
        public DateTime End { get; set; }
        //foreign keys
        //public Project Project { get; set; }
        public List<InputMethod> InputMethods { get; set; }

        public Fase()
        {
            InputMethods = new List<InputMethod>();
        }
        
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (End < DateTime.Now)
            {
                errors.Add(new ValidationResult("Einde van de fase kan niet in het verleden liggen"));
            }

            if (End <= Begin)
            {
                errors.Add(new ValidationResult("Einde van de fase kan niet voor het begin van de fase liggen"));
            }

            return errors;
        }
    }
}