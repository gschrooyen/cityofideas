using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.input.interfaces;
using PoC.Domain.iot;
using PoC.Domain.organisation;

namespace PoC.Domain.project
{
    public class Project : ISharable, ILikeable, IValidatableObject
    {
        [Key] public string Id { get; set; }

    [Required(ErrorMessage = "Naam verplicht in te vullen")]
    [StringLength(100, ErrorMessage = "Maximaal 100 tekens")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Hoofdvraag verplicht in te vullen")]
    [StringLength(500, ErrorMessage = "Maximaal 500 tekens")]
    public string MainQuestion { get; set; }

    [Required(ErrorMessage = "Beschrijving verplicht in te vullen")]
    [StringLength(2000, ErrorMessage = "Maximaal 2000 tekens")]
    public string Description { get; set; }

        [Required] public Category Category { get; set; }

    [DataType(DataType.Date)] public DateTime Begin { get; set; }
    [DataType(DataType.Date)] public DateTime End { get; set; }

    public string ImageName { get; set; }
    public bool Closed { get; set; }

    //foreign keys
    //TODO: deze moet ooit ook Resuired worden misschien best in sprint 2
    public Organisation Organisation { get; set; }
    public IEnumerable<Placement> Placements { get; set; }
    public IEnumerable<Share> Shares { get; set; }
    public IEnumerable<Like> Likes { get; set; }
    [Required] public Fase CurrentFase { get; set; }
    public List<Fase> PastFases { get; set; }

    public Project()
    {
      Placements = new List<Placement>();
      Shares = new List<Share>();
      Likes = new List<Like>();
      PastFases = new List<Fase>();
      Closed = false;
    }

        public string GetId()
        {
            return Id;
        }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (End < DateTime.Now)
            {
                errors.Add(new ValidationResult("Einde van het project kan niet in het verleden liggen"));
            }

      if (End <= Begin)
      {
        errors.Add(new ValidationResult("Einde van het project kan niet voor het begin van het project liggen"));
      }

      if (CurrentFase.End < DateTime.Now)
      {
        errors.Add(new ValidationResult("Einde van de fase kan niet in het verleden liggen"));
      }

            if (CurrentFase.End <= CurrentFase.Begin)
            {
                errors.Add(new ValidationResult("Einde van de fase kan niet voor het begin van de fase liggen"));
            }

            return errors;
        }
    }
}