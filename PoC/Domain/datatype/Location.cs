using System.ComponentModel.DataAnnotations;

namespace PoC.Domain.datatype
{
    public class Location
    {
        [Key] public string LocationId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}