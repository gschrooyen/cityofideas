namespace PoC.Domain.datatype
{
    public enum AnswerType
    {
        OPEN,
        SINGLE_CHOICE,
        MULTIPLE_CHOICE,
        DROPDOWN,
        EMAILADRESS
    }
}