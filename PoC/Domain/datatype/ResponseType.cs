namespace PoC.Domain.datatype
{
    public enum ResponseType
    {
        TEXT, IMAGE, VIDEO, TAG, MAP
    }
}