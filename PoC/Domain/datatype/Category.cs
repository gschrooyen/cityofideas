namespace PoC.Domain.datatype
{
  public enum Category
  {
    ECONOMIE,
    GEZONDHEID,
    MILIEU,
    VERKEER
  }
}