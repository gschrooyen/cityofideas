namespace PoC.Domain.datatype
{
    public enum NotificationType
    {
        DOUBLE, INAPPROPRIATE_LANGUAGE, IRRELEVANT, ADVERTISMENT,
        COMMERCIAL
    }
}