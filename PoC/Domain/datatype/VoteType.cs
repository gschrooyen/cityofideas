namespace PoC.Domain.datatype
{
    public enum VoteType
    {
        NOT_CONFIRMED, CONFIRMED, CONFIRMED_ACCOUNT
    }
}