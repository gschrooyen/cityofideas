namespace PoC.Domain.datatype
{
    public enum ChoiseQuestionType
    {
        SINGLE_CHOICE, MULTIPLE_CHOICE, DROPDOWN
    }
}