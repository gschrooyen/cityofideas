using System.ComponentModel.DataAnnotations;

namespace PoC.Domain.datatype
{
    public class Address
    {
        [Key]
        public string AdressId { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
    }
}