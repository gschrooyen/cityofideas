namespace PoC.Domain.datatype
{
    public enum ModerationAction
    {
        IGNORE, APPROVE, ADJUST, REDIRECT, BLOCK,
        DELETE_POST
    }
}