using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.input;
using PoC.Domain.moderation;
using PoC.Domain.organisation;

namespace PoC.Domain.user
{
    public class User
    {
        [Key]
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PasswordHash { get; set; }
        public int Address { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public bool Verified { get; set; }
        public bool Blocked { get; set; }

        public bool Deleted { get; set; }
        public DateTime Birthdate { get; set; }
        //foreign keys
        public Organisation Organisation { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        public List<Input> Inputs { get; set; }

        public User()
        {
            Notifications = new List<Notification>();
            Inputs = new List<Input>();
        }
    }
}