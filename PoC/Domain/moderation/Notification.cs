using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.user;

namespace PoC.Domain.moderation
{
    public class Notification
    {
        [Key] 
        public string NotificationId { get; set; }
        public DateTime Time { get; set; }
        public NotificationType Type { get; set; }
        public string Description { get; set; }
        public bool Moderated { get; set; }
        
        //foreign keys
        public IEnumerable<Moderation> Moderations { get; set; }
        public User User { get; set; }
        public Reaction Reaction { get; set; }
        public Idea Idea { get; set; }
        public Answer Answer { get; set; }

        private Notification()
        {
            Moderated = false;
            Moderations = new List<Moderation>();
            Time = DateTime.Now;
        }

        public Notification(NotificationType reason, string description, Reaction reaction) :this()
        {
            Reaction = reaction;
            Idea = null;
            Answer = null;
            Type = reason;
            Description = description;
            
        }
        
        public Notification(NotificationType reason, string description) :this()
        {
            
            Idea = null;
            Answer = null;
            Type = reason;
            Description = description;
            
        }
        public Notification(NotificationType reason, string description, Reaction reaction, User user) :this()
        {
            Reaction = reaction;
            Idea = null;
            Answer = null;
            Type = reason;
            Description = description;
            User = user;
        }

        public void Moderate(Moderation moderation)
        {
            Moderated = true;
            var moderations = Moderations.ToList();
            moderations.Add(moderation);
            Moderations = moderations;
        }
    }
}