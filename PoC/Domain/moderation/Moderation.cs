using System.ComponentModel.DataAnnotations;
using PoC.Domain.datatype;

namespace PoC.Domain.moderation
{
    public class Moderation
    {
        [Key] public string ModerationId { get; set; }
        public ModerationAction Action { get; set; }
        
        //foreign key
        public Notification Notification { get; set; }

        public Moderation()
        {
            
        }

        public Moderation(ModerationAction action, Notification notification)
        {
            Action = action;
            Notification = notification;
        }
    }
}