using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using PoC.BL.manager.InputMethod;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.inputMethod;

namespace PoC.Controllers
{
    [Route("{org}/[controller]")]
    public class QuestionListController : Controller
    {
        private readonly IInputMethodManager _mgr;

        public QuestionListController(IInputMethodManager mgr)
        {
            _mgr = mgr;
            }

        // GET        
        public IActionResult Index(string id)
        {
            var questionList = _mgr.GetQuestionList(id);

            //weet nog niet of dit klopt, maar werkt wel
            var test = _mgr.GetQuestionsOfList(questionList.Id);

            var questionListQuestions = test.ToList();
            foreach (var question in questionListQuestions)
            {
                if (question.GetType() != typeof(ChoiceQuestion)) continue;
                var q = (ChoiceQuestion) question;
                q.Options = _mgr.GetOptionsOfChoiceQuestion(q).ToList();
            }

            questionList.Questions = questionListQuestions;
            return View(questionList);
        }

        [HttpPost]
        [Route("edit/{id}")]
        public IActionResult EditQuestionListFromForm(IFormCollection formValues)
        {
            Console.WriteLine(formValues.Keys.ToString());
            List<Question> questionList = new List<Question>();
            for (int i = 0; i < int.Parse(formValues["questionCount"].ToString()); i++)
            {
                switch (formValues["questionType_" + i.ToString()])
                {
                    case "OPEN_QUESTION":
                        if (addOpenQuestion(i, formValues, OpenQuestionType.OPEN_QUESTION) != null)
                        {
                            questionList.Add(addOpenQuestion(i, formValues, OpenQuestionType.OPEN_QUESTION));
                        }
                        break;
                    case "EMAIL":
                        if (addOpenQuestion(i, formValues, OpenQuestionType.EMAIL) != null)
                        {
                            questionList.Add(addOpenQuestion(i, formValues, OpenQuestionType.EMAIL));
                        }
                        break;
                    case "MAP":
                        if (addOpenQuestion(i, formValues, OpenQuestionType.MAP) != null)
                        {
                            questionList.Add(addOpenQuestion(i, formValues, OpenQuestionType.MAP));
                        }
                        break;
                    case "SINGLE_CHOICE":
                        if (addChoiceQuestion(i, formValues, ChoiseQuestionType.SINGLE_CHOICE) != null)
                        {
                            questionList.Add(addChoiceQuestion(i, formValues, ChoiseQuestionType.SINGLE_CHOICE));
                        }
                        break;
                    case "MULTIPLE_CHOICE":
                        if (addChoiceQuestion(i, formValues, ChoiseQuestionType.MULTIPLE_CHOICE) != null)
                        {
                            questionList.Add(addChoiceQuestion(i, formValues, ChoiseQuestionType.MULTIPLE_CHOICE));
                        }
                        break;
                    case "DROPDOWN":
                        if (addChoiceQuestion(i, formValues, ChoiseQuestionType.DROPDOWN) != null)
                        {
                            questionList.Add(addChoiceQuestion(i, formValues, ChoiseQuestionType.DROPDOWN));
                        }
                        break;
                    
                }
            }

            var questionListId = formValues["questionlistId"].ToString();
            var ques = _mgr.GetQuestionList(questionListId);
            ques.Questions = new List<Question>();
            ques.Questions = questionList;
            ques.Title = formValues["title"];
            ques.Description = formValues["description"];
            ques.Begin = DateTime.Parse(formValues["begindatum"]);
            ques.End = DateTime.Parse(formValues["einddatum"]);

            _mgr.ChangeQuestionList(ques);
            
            var newQ = _mgr.GetQuestionList(questionListId);

            return RedirectToAction("EditQuestionList", new {id = questionListId});
        }

        private Question addOpenQuestion(int i, IFormCollection formValues, OpenQuestionType questionType)
        {
            var question = new OpenQuestion()
            {
                Title = formValues["questionTitle_"+i.ToString()],
                Awnsers = new List<Answer>(),
                OpenQuestionType = questionType
            };

            return question;
        }

        private Question addChoiceQuestion(int i, IFormCollection formValues, ChoiseQuestionType questionType)
        {
            List<Option> options = new List<Option>();
            //option_id_nbr
            var questionId = formValues["questionid_" + i.ToString()].ToString();

            int temp = 0;
            foreach (var key in formValues.Keys)
            {
                if (key.Contains("option_"+questionId))
                {
                    temp++;
                }
            }
            //var optionCount = int.Parse(formValues["optioncount_"+i.ToString()].ToString());

            for (int u = 0; u < temp; u++)
            {
                if (formValues["option_" + questionId + "_" + u.ToString()].ToString().Trim() != "")
                {
                    options.Add(new Option()
                    {
                        Title = formValues["option_"+questionId+"_"+u.ToString()]
                    });
                }
            }

            if (options.Count > 0)
            {
                var question = new ChoiceQuestion()
                {
                    Title = formValues["questionTitle_"+i.ToString()],
                    Awnsers = new List<Answer>(),
                    ChoiseQuestionType = questionType,
                    Options = options
                };

                return question;
            }else
            {
                return null;
            }
        }

        [HttpPost]
        [Route("addChoiceQuestionToQuestionList/{id}")]
        public IActionResult AddChoiceQuestionToQuestionList(string questionListId, string titleChoiceQuestion,
            int rangChoiceQuestion, string choiceQuestionType, List<string> options)
        {
            List<Option> optionsQuestion = new List<Option>();
            foreach (var option in options)
            {
                if (option == null)
                {
                    continue;
                }

                if (option.Length > 0)
                {
                    Option optionQuestion = new Option()
                    {
                        Title = option
                    };
                    optionsQuestion.Add(optionQuestion);
                }
            }


            QuestionList questionList = _mgr.GetQuestionList(questionListId);

            ChoiceQuestion choiceQuestion = new ChoiceQuestion()
            {
                Title = titleChoiceQuestion,
                Rang = rangChoiceQuestion.ToString(),
                ChoiseQuestionType = (ChoiseQuestionType) Enum.Parse(typeof(ChoiseQuestionType), choiceQuestionType),
                Options = optionsQuestion
            };


            _mgr.AddQuestionToQuestionlist(questionList.Id, choiceQuestion);

            return RedirectToAction("EditQuestionList", new {id = questionList.Id});
        }

        [HttpPost]
        [Route("edit/removeChoiceQuestion/{choiceQuestionId}")]
        public void RemoveChoiceQuestion(string choiceQuestionId)
        {
            Question question = _mgr.GetChoiceQuestion(choiceQuestionId);
            ChoiceQuestion choiceQuestion = (ChoiceQuestion) question;

            _mgr.RemoveChoiceQuestion(choiceQuestion.id);
        }

        [HttpPost]
        [Route("editChoiceQuestion/{id}")]
        public IActionResult EditChoiceQuestion(string id, string editChoiceQuestionId, string editTitleChoiceQuestion,
            int editRangChoiceQuestion, string editChoiceQuestionType, List<string> editOptions)
        {
            Question question = _mgr.GetChoiceQuestion(editChoiceQuestionId);

            ChoiceQuestion choiceQuestion = (ChoiceQuestion) question;

            choiceQuestion.Title = editTitleChoiceQuestion;
            choiceQuestion.Rang = editRangChoiceQuestion.ToString();
            choiceQuestion.ChoiseQuestionType =
                (ChoiseQuestionType) Enum.Parse(typeof(ChoiseQuestionType), editChoiceQuestionType);

            List<Option> optionsQuestion = new List<Option>();
            foreach (var option in editOptions)
            {
                if (option == null)
                {
                    continue;
                }

                if (option.Length > 0)
                {
                    Option optionQuestion = new Option()
                    {
                        Title = option
                    };
                    optionsQuestion.Add(optionQuestion);
                }
            }

            choiceQuestion.Options = optionsQuestion;

            QuestionList questionList = _mgr.GetQuestionList(id);
            if (ModelState.IsValid)
            {
                _mgr.ChangeQuestion(choiceQuestion);
            }

            return RedirectToAction("EditQuestionList", new {id = questionList.Id});
        }


        [HttpPost]
        [Route("addOpenQuestionToQuestionList/{id}")]
        public IActionResult AddOpenQuestionToQuestionList(string questionListId, string titleOpenQuestion,
            int rangOpenQuestion, string openQuestionType)
        {
            QuestionList questionlist = _mgr.GetQuestionList(questionListId);

            OpenQuestion openQuestion = new OpenQuestion()
            {
                Title = titleOpenQuestion,
                Rang = rangOpenQuestion.ToString(),
                OpenQuestionType = (OpenQuestionType) Enum.Parse(typeof(OpenQuestionType), openQuestionType)
            };

            _mgr.AddQuestionToQuestionlist(questionlist.Id, openQuestion);

            return RedirectToAction("EditQuestionList", new {id = questionlist.Id});
        }

        [HttpPost]
        [Route("edit/removeOpenQuestion/{openQuestionId}")]
        public void RemoveOpenQuestion(string openQuestionId)
        {
            Question question = _mgr.GetOpenQuestion(openQuestionId);

            _mgr.RemoveOpenQuestion(question.id);
        }

        [HttpPost]
        [Route("editOpenQuestion/{id}")]
        public IActionResult EditOpenQuestion(string id, string editOpenQuestionId, string editTitleOpenQuestion,
            int editRangOpenQuestion, string editOpenQuestionType)
        {
            QuestionList questionList = _mgr.GetQuestionList(id);

            Question question = _mgr.GetOpenQuestion(editOpenQuestionId);

            OpenQuestion openQuestion = (OpenQuestion) question;

            openQuestion.Title = editTitleOpenQuestion;
            openQuestion.Rang = editRangOpenQuestion.ToString();
            openQuestion.OpenQuestionType =
                (OpenQuestionType) Enum.Parse(typeof(OpenQuestionType), editOpenQuestionType);
            if (ModelState.IsValid)
            {
                _mgr.ChangeQuestion(openQuestion);
            }

            return RedirectToAction("EditQuestionList", new {id = questionList.Id});
        }

        [HttpGet]
        [Route("edit/{id}")]
        public IActionResult EditQuestionList(string id)
        {
            QuestionList questionList = _mgr.GetQuestionList(id);
            var test = _mgr.GetQuestionsOfList(questionList.Id);

            var questionListQuestions = test.ToList();
            foreach (var question in questionListQuestions)
            {
                if (question.GetType() != typeof(ChoiceQuestion)) continue;
                var q = (ChoiceQuestion) question;
                q.Options = _mgr.GetOptionsOfChoiceQuestion(q).ToList();
            }

            questionList.Questions = questionListQuestions;
            return View(questionList);
        }
    }
}