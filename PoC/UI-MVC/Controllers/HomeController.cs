﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using PoC.BL.manager.organisation;
using PoC.Models;

namespace PoC.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IOrganisationManager _mgr;
        
        public HomeController(IOrganisationManager mgr)
        {
            _mgr = mgr;
        }

        [Route("/{org}/home")]
        public IActionResult Home(string org)
        {
            ViewData["organisationImage"] = org;
            
            return View(_mgr.GetOrganisationByName(org.ToUpper()));
        }
        
        
        
        public IActionResult Index()
        {
           
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [Route("/{org}/Contact")]
        public IActionResult Contact(string org)
        {
            
            return View(_mgr.GetOrganisationByName(org.ToUpper()));
        }

        [HttpPost]
        [Route("/{org}/Contact")]
        public IActionResult Contact(ContactViewModel contactViewModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Home");
            }

            return View();
        }
    }
}
