using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.organisation;
using PoC.BL.manager.project;
using PoC.BL.manager.user;
using PoC.Domain.organisation;
using PoC.Domain.project;
using PoC.Domain.user;
using PoC.Models;

namespace PoC.Controllers
{
    //added the policy for this type of user:
    [Authorize(Roles = "SuperAdmin")]
    [Route("[controller]")]
    public class SuperadminController : Controller
    {
        private readonly IOrganisationManager _mgr;
        private readonly IUserManager _userManager;
        private readonly UserManager<PoCIdentityUser> _identity;
        private readonly IProjectManager _projectManager;

        //File Management
        private readonly IHostingEnvironment _hostingEnvironment;


        public SuperadminController(IOrganisationManager mgr, IUserManager userManager,
            UserManager<PoCIdentityUser> identity, IHostingEnvironment hostingEnvironment,
            IProjectManager projectManager)
        {
            _mgr = mgr;
            _userManager = userManager;
            _identity = identity;
            _hostingEnvironment = hostingEnvironment;
            _projectManager = projectManager;
        }

        [Route("/superadmin/ManageAdmins")]
        public IActionResult ManageAdmins()
        {
            KeepCurrentOrganisation();

            var modtask = _identity.GetUsersInRoleAsync("Moderator");

            var admintask = _identity.GetUsersInRoleAsync("Admin");

            var superadmintask = _identity.GetUsersInRoleAsync("SuperAdmin");
            var users = _userManager.GetUsers();
            modtask.Wait();
            admintask.Wait();
            superadmintask.Wait();
            Console.WriteLine(admintask.Result.Count);
            Console.WriteLine(superadmintask.Result.Count);
            var admins = admintask.Result.Except(superadmintask.Result).Select(sa => sa.Id).ToList();
            Console.WriteLine(admins.Count);
            var retusers = new List<User>();
            foreach (var user in users)
            {
                if (admins.Contains(user.UserId))
                {
                    retusers.Add(user);
                }
            }


            return View(retusers);
        }

        [Route("/superadmin/demoteAdmin/{uid}")]
        public IActionResult DemoteAdmin(string uid)
        {
            var usrtask = _identity.GetUsersInRoleAsync("Admin");
            usrtask.Wait();
            var mods = usrtask.Result;
            PoCIdentityUser user = null;
            foreach (var mod in mods)
            {
                if (mod.Id == uid)
                {
                    user = mod;
                }
            }

            if (user is null)
            {
                return NotFound(uid);
            }

            _identity.RemoveFromRoleAsync(user, "Admin");
            return Ok(uid);
        }


        [Route("/superadmin/promoteAdmin/{uid}")]
        public IActionResult PromoteAdmin(string uid)
        {
            var usrtask = _identity.GetUsersInRoleAsync("Admin");
            usrtask.Wait();
            var mods = usrtask.Result;
            PoCIdentityUser user = null;
            foreach (var mod in mods)
            {
                if (mod.Id == uid)
                {
                    user = mod;
                }
            }

            if (user is null)
            {
                return NotFound(uid);
            }

            _identity.AddToRoleAsync(user, "SuperAdmin");
            return Ok(uid);
        }

        [HttpGet]
        [Route("/superadmin/CreateOrganisation")]
        public IActionResult CreateOrganisation()
        {
            KeepCurrentOrganisation();

            
            return View();
        }

        [HttpPost]
        [Route("/superadmin/CreateOrganisation")]
        public IActionResult CreateOrganisation(OrganisationModel organisationModel)
        {
            if (!ModelState.IsValid)
            {
                return View(organisationModel);
            }

            CreateDirectoriesOrganisation(organisationModel.Name);
            var uploadOrganisationPath =
                "organisations/" + organisationModel.Name.ToLower().Replace(" ", "") + "/organisation";

            var fileNameImage = "";
            if (ExtensionMethods.FileManagement.CheckFileExtension(organisationModel.ImageOrganisation.FileName))
            {
                var uploadImagePath = uploadOrganisationPath + "/images";
                fileNameImage = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,
                    uploadImagePath, organisationModel.ImageOrganisation);
            }

            var fileNameLogo = "";
            if (ExtensionMethods.FileManagement.CheckFileExtension(organisationModel.Logo.FileName))
            {
                var uploadLogoPath = uploadOrganisationPath + "/logos";
                fileNameLogo = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,
                    uploadLogoPath, organisationModel.Logo);
            }

            var fileNameDia1 = "";
            var uploadDiasPath = uploadOrganisationPath + "/dias";
            if (ExtensionMethods.FileManagement.CheckFileExtension(organisationModel.ImageOrganisation.FileName))
            {
                fileNameDia1 = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,
                    uploadDiasPath, organisationModel.Dia1);
            }

            var fileNameDia2 = "";
            if (ExtensionMethods.FileManagement.CheckFileExtension(organisationModel.ImageOrganisation.FileName))
            {
                fileNameDia2 = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,
                    uploadDiasPath, organisationModel.Dia2);
            }

            var fileNameDia3 = "";
            if (ExtensionMethods.FileManagement.CheckFileExtension(organisationModel.ImageOrganisation.FileName))
            {
                fileNameDia3 = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,
                    uploadDiasPath, organisationModel.Dia3);
            }

            _mgr.AddOrganisationViewModel(organisationModel.Name, fileNameImage, fileNameLogo, fileNameDia1,
                fileNameDia2,
                fileNameDia3, organisationModel.TelephoneNumber, organisationModel.Street, organisationModel.City,
                organisationModel.Country, organisationModel.Email,
                organisationModel.Description, organisationModel.FeaturedText, organisationModel.ZipCode,
                organisationModel.PrimaryColor, organisationModel.SecondaryColor, organisationModel.TertiaryColor);
            return RedirectToAction("ManageOrganisations");
        }

        [Route("/superadmin/manageorganisations")]
        public IActionResult ManageOrganisations()
        {
            KeepCurrentOrganisation();

            List<Organisation> orgs = _mgr.GetAllOrganisations();
            return View(orgs);
        }


        [Authorize(Roles = "SuperAdmin")]
        [Route("/superadmin/deleteorganisation/{id}")]
        public void DeleteOrganisation(string id)
        {
            Organisation organisation = _mgr.GetOrganisation(id);

            _mgr.CloseOrganisation(organisation);
        }

        [HttpGet]
        [Route("/superadmin/editorganisation/{id}")]
        public IActionResult EditOrganisation(string id)
        {
            KeepCurrentOrganisation();

            Organisation organisation = _mgr.GetOrganisation(id);

            

            return View(organisation);
        }

        [HttpPost]
        [Route("/superadmin/editorganisation/{id}")]
        public IActionResult EditOrganisation(string id, Organisation organisation, IFormFile imageOrganisation,
            IFormFile logoOrganisation, IFormFile dia1, IFormFile dia2, IFormFile dia3)
        {
            if (!ModelState.IsValid)
            {
                return View(organisation);
            }

            var uploadOrganisationPath =
                "organisations/" + organisation.Name.ToLower().Replace(" ", "") + "/organisation";
            var uploadImagePath = uploadOrganisationPath + "/images";
            var uploadLogoPath = uploadOrganisationPath + "/logos";
            var uploadDiasPath = uploadOrganisationPath + "/dias";

            AddFileToOrg(organisation, imageOrganisation, uploadImagePath, "image");
            AddFileToOrg(organisation, logoOrganisation, uploadLogoPath, "logo");
            AddFileToOrg(organisation, dia1, uploadDiasPath, "dia1");
            AddFileToOrg(organisation, dia2, uploadDiasPath, "dia2");
            AddFileToOrg(organisation, dia3, uploadDiasPath, "dia3");
            _mgr.ChangeOrganisation(organisation);

            return RedirectToAction("ManageOrganisations");
        }

        [HttpGet]
        [Route("/superadmin/copyorganisation/{id}")]
        public IActionResult CopyOrganisation(string id)
        {
            KeepCurrentOrganisation();

            Organisation organisation = _mgr.GetOrganisation(id);
            return View(organisation);
        }

        [Route("/superadmin/copyorganisation/{id}")]
        public IActionResult CopyOrganisation(Organisation organisation, string oldOrganisationName,
            IFormFile copyImageOrganisation,
            IFormFile copyLogoOrganisation, IFormFile copyDia1Organisation, IFormFile copyDia2Organisation,
            IFormFile copyDia3Organisation)
        {
            
            if (!ModelState.IsValid)
            {
                return View(organisation);
            }

            if (organisation.Name.ToLower().Replace(" ","").Equals(oldOrganisationName.ToLower().Replace(" ","")))
            {
                organisation.Name = oldOrganisationName + " Copy";
            }
            
            var fileNameImage = "";
            var fileNameLogo = "";
            var fileNameDia1 = "";
            var fileNameDia2 = "";
            var fileNameDia3 = "";          

            var uploadOldOrganisationPath =
                "organisations/" + oldOrganisationName.ToLower().Replace(" ", "") + "/organisation";
            var uploadOldImagePath = uploadOldOrganisationPath + "/images";
            var uploadOldLogoPath = uploadOldOrganisationPath + "/logos";
            var uploadOldDiasPath = uploadOldOrganisationPath + "/dias";

            var uploadCopyOrganisationPath =
                "organisations/" + organisation.Name.ToLower().Replace(" ", "") + "/organisation";
            var uploadCopyImagePath = uploadCopyOrganisationPath + "/images";
            var uploadCopyLogoPath = uploadCopyOrganisationPath + "/logos";
            var uploadCopyDiasPath = uploadCopyOrganisationPath + "/dias";
            CreateDirectoriesOrganisation(organisation.Name);
            if (copyImageOrganisation == null)
                fileNameImage = CopyAndMoveFile(uploadOldImagePath, uploadCopyImagePath, organisation.Image);
            if (copyImageOrganisation != null)
                fileNameImage = AddFileToOrg(organisation, copyImageOrganisation, uploadCopyImagePath, "image");
            if (copyLogoOrganisation == null)
                fileNameLogo = CopyAndMoveFile(uploadOldLogoPath, uploadCopyLogoPath, organisation.Logo);
            if (copyLogoOrganisation != null)
                fileNameLogo = AddFileToOrg(organisation, copyLogoOrganisation, uploadCopyLogoPath, "logo");
            if (copyDia1Organisation == null)
                if (organisation.Dia1 != null)
                {
                    fileNameDia1 = CopyAndMoveFile(uploadOldDiasPath, uploadCopyDiasPath, organisation.Dia1);
                }

            if (copyDia1Organisation != null)
                if (organisation.Dia1 != null)
                {
                    fileNameDia1 = AddFileToOrg(organisation, copyDia1Organisation, uploadCopyDiasPath, "dia1");
                }

            if (copyDia2Organisation == null)
                if (organisation.Dia2 != null)
                {
                    fileNameDia2 = CopyAndMoveFile(uploadOldDiasPath, uploadCopyDiasPath, organisation.Dia2);
                }

            if (copyDia2Organisation != null)
                if (organisation.Dia2 != null)
                {
                    fileNameDia2 = AddFileToOrg(organisation, copyDia2Organisation, uploadCopyDiasPath, "dia2");
                }

            if (copyDia3Organisation == null)
                if (organisation.Dia3 != null)
                {
                    fileNameDia3 = CopyAndMoveFile(uploadOldDiasPath, uploadCopyDiasPath, organisation.Dia3);
                }

            if (copyDia3Organisation != null)
                if (organisation.Dia3 != null)
                {
                    fileNameDia3 = AddFileToOrg(organisation, copyDia3Organisation, uploadCopyDiasPath, "dia3");
                }

            Organisation org = _mgr.CopyOrganisation(organisation, fileNameImage, fileNameLogo, fileNameDia1,
                fileNameDia2, fileNameDia3);


            return RedirectToAction("ManageOrganisations");
        }

        private string CopyAndMoveFile(string sourcePath, string destinationPath, string pictureName)
        {
            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, sourcePath, pictureName);
            var destinationPathCopy = Path.Combine(_hostingEnvironment.WebRootPath, "temporary\\", pictureName);
            var fileName = Path.GetFileName(filePath);
            var fileNameBase = fileName.Split('_')[0];
            var moveFileName = fileNameBase + "_" + Guid.NewGuid().ToString().Substring(0, 4) +
                               Path.GetExtension(filePath);
            var destinationPathMove =
                Path.Combine(_hostingEnvironment.WebRootPath, destinationPath, moveFileName);
            System.IO.File.Copy(filePath, destinationPathCopy);
            System.IO.File.Move(destinationPathCopy, destinationPathMove);
            return moveFileName;
        }

        private string AddFileToOrg(Organisation organisation, IFormFile file, string path, string type)
        {
            string fileName = "";
            if (file != null)
            {
                if ((Path.GetExtension(file.FileName).Equals(".jpg") ||
                     Path.GetExtension(file.FileName).Equals(".png") ||
                     Path.GetExtension(file.FileName).Equals(".jpeg")))
                {
                    fileName = ExtensionMethods.FileManagement.CreateFileName(file.FileName);
                    var uploadToRoot = Path.Combine(_hostingEnvironment.WebRootPath, path);
                    var filePath = Path.Combine(uploadToRoot, fileName);
                    FileStream fileStream = new FileStream(filePath, FileMode.Create);
                    file.CopyTo(fileStream);
                    fileStream.Close();

                    switch (type)
                    {
                        case "image":
                            organisation.Image = fileName;
                            break;
                        case "logo":
                            organisation.Logo = fileName;
                            break;
                        case "dia1":
                            organisation.Dia1 = fileName;
                            break;
                        case "dia2":
                            organisation.Dia2 = fileName;
                            break;
                        case "dia3":
                            organisation.Dia3 = fileName;
                            break;
                    }
                }
            }

            return fileName;
        }

        private void CreateDirectoriesOrganisation(string organisationName)
        {
            // Alle Paths
            string directoryPath = Path.Combine(_hostingEnvironment.WebRootPath, "organisations",
                organisationName.ToLower().Replace(" ", ""));
            string organisationDirectoryPath = Path.Combine(directoryPath, "organisation");
            string organisationImagesPath = Path.Combine(organisationDirectoryPath, "images");
            string organisationLogosPath = Path.Combine(organisationDirectoryPath, "logos");
            string organisationDiasPath = Path.Combine(organisationDirectoryPath, "dias");
            string projectsDirectoryPath = Path.Combine(organisationDirectoryPath, "projects");
            string projectsImagesPath = Path.Combine(projectsDirectoryPath, "images");
            string ideationsDirectoryPath = Path.Combine(projectsDirectoryPath, "ideations");
            string ideationsImagesPath = Path.Combine(ideationsDirectoryPath, "images");
            string ideasDirectoryPath = Path.Combine(ideationsDirectoryPath, "ideas");
            string ideasImagesPath = Path.Combine(ideasDirectoryPath, "images");
            string ideasVideosPath = Path.Combine(ideasDirectoryPath, "videos");

            //Aanmaken Paths

            //Organisation
            Directory.CreateDirectory(organisationImagesPath);
            Directory.CreateDirectory(organisationLogosPath);
            Directory.CreateDirectory(organisationDiasPath);

            //Organisation
            Directory.CreateDirectory(organisationImagesPath);
            string keepOrgImages = "keep_" + Guid.NewGuid().ToString().Substring(0, 4) + ".txt";
            string keepOrgImagesDirectory = organisationImagesPath + "/" + keepOrgImages;
            System.IO.File.WriteAllText(keepOrgImagesDirectory, "KeepDirectory");
            Directory.CreateDirectory(organisationLogosPath);
            string keepOrgLogos = "keep_" + Guid.NewGuid().ToString().Substring(0, 4) + ".txt";
            string keepOrgLogosDirectory = organisationLogosPath + "/" + keepOrgLogos;
            System.IO.File.WriteAllText(keepOrgLogosDirectory, "KeepDirectory");
            Directory.CreateDirectory(organisationDiasPath);
            string keepOrgDias = "keep_" + Guid.NewGuid().ToString().Substring(0, 4) + ".txt";
            string keepOrgDiasDirectory = organisationDiasPath + "/" + keepOrgDias;
            System.IO.File.WriteAllText(keepOrgDiasDirectory, "KeepDirectory");

            Directory.CreateDirectory(projectsImagesPath);

            Directory.CreateDirectory(projectsImagesPath);
            string keepProjectsImages = "keep_" + Guid.NewGuid().ToString().Substring(0, 4) + ".txt";
            string keepProjectsImagesDirectory = projectsImagesPath + "/" + keepProjectsImages;
            System.IO.File.WriteAllText(keepProjectsImagesDirectory, "KeepDirectory");
            // Ideations

            Directory.CreateDirectory(ideationsImagesPath);
            string keepIdeationsImages = "keep_" + Guid.NewGuid().ToString().Substring(0, 4) + ".txt";
            string keepIdeationsImagesDirectory = ideationsImagesPath + "/" + keepIdeationsImages;
            System.IO.File.WriteAllText(keepIdeationsImagesDirectory, "KeepDirectory");

            Directory.CreateDirectory(ideasImagesPath);
            Directory.CreateDirectory(ideasVideosPath);
        }



        private void KeepCurrentOrganisation()
        {
            if (HttpContext.Request.Headers["Referer"].ToString() != null)
            {
                var referer = HttpContext.Request.Headers["Referer"];
                var orgLink = "";
                if (referer.Count == 1)
                {
                    orgLink = referer.ToString().Split("/")[3];
                }

                if (!orgLink.Equals("superadmin") && !orgLink.Equals(""))
                {
                    TempData["Orglink"] = orgLink;
                }
            }

            TempData.Keep(key: "Orglink");
        }
    }
}