using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.user;
using PoC.Domain.user;

namespace PoC.Controllers {
    
    //added the policy for this type of user:
    [Authorize]
    [Route("{org}/[controller]")]
    public class AdminController : Controller
    {

        private readonly IUserManager _userManager;
        private readonly UserManager<PoCIdentityUser> _identity;
        
        public AdminController(IUserManager userManager, UserManager<PoCIdentityUser> identity)
        {
            _userManager = userManager;
            _identity = identity;
            
        }
        
        [Authorize(Roles = "Admin")]
        
        public IActionResult Index() {
            return View();
        }

        [Route("manageModerators")]
        public IActionResult ManageModerators(string org)
        {
            var identitymod = _identity.GetUsersInRoleAsync("Moderator");
            var identityadmin = _identity.GetUsersInRoleAsync("Admin");
            var mods = new List<User>();
            identitymod.Wait();
            identityadmin.Wait();
            var admins = identityadmin.Result;
            var users = identitymod.Result;
            foreach (var admin in admins)
            {
                users.Remove(admin);
            }
            var ids = users.Select(user => user.Id).ToList();
            foreach (var user in _userManager.GetUsers())
            {
                if (ids.Contains(user.UserId))
                {
                    mods.Add(user);
                }
            }
            
            
            return View(mods);
        }
        
        [Route("manageUsers")]
        public IActionResult ManageUsers(string org)
        {
            var identityusrs = _identity.GetUsersInRoleAsync("User");
            var identitymods = _identity.GetUsersInRoleAsync("Moderator");
            var usrs = new List<User>();
            identityusrs.Wait();
            identitymods.Wait();
            var users = identityusrs.Result;
            var mods = identitymods.Result;
            foreach (var poCIdentityUser in mods)
            {
                users.Remove(poCIdentityUser);
            }
            var ids = users.Select(user => user.Id).ToList();
            foreach (var user in _userManager.GetUsers())
            {
                if (ids.Contains(user.UserId))
                {
                    usrs.Add(user);
                }
            }

            return View(usrs);
        }

        [Route("demoteMod/{uid}")]
        public IActionResult DemoteMod(string uid)
        {
            var usrtask = _identity.GetUsersInRoleAsync("Moderator");
            usrtask.Wait();
            var mods = usrtask.Result;
            PoCIdentityUser user = null;
            foreach (var mod in mods)
            {
                if (mod.Id == uid)
                {
                    user = mod;
                }
            }

            if (user is null)
            {
                return NotFound(uid);
            }
            _identity.RemoveFromRoleAsync(user, "Moderator");
            return Ok(uid);
        }

        [Route("removeUser/{uid}")]
        public IActionResult RemoveUser(string uid)
        {
            var usertask = _identity.GetUsersInRoleAsync("User");
            usertask.Wait();
            var users = usertask.Result;
            PoCIdentityUser usr = null;
            foreach (var user in users)
            {
                if (user.Id == uid)
                {
                    usr = user;
                }
            }

            if (usr is null)
            {
                return NotFound(uid);
            }

            _identity.DeleteAsync(usr);
            var foo = _userManager.RemoveUser(uid);
            foreach (var input in foo.Inputs)
            {
                input.Deleted = true;
            }
            return Ok(uid);
        }

        [Route("promoteToMod/{uid}")]
        public IActionResult PromoteToMod(string uid)
        {
            var usertask = _identity.GetUsersInRoleAsync("User");
            usertask.Wait();
            var users = usertask.Result;
            PoCIdentityUser usr = null;
            foreach (var user in users)
            {
                if (user.Id == uid)
                {
                    usr = user;
                }
            }

            if (usr is null)
            {
                return NotFound(uid);
            }

            _identity.AddToRoleAsync(usr, "Moderator");
            return Ok(uid);
        }
        
        [Route("promoteMod/{uid}")]
        public IActionResult PromoteMod(string uid)
        {
            var usertask = _identity.GetUsersInRoleAsync("Moderator");
            usertask.Wait();
            var users = usertask.Result;
            PoCIdentityUser usr = null;
            foreach (var user in users)
            {
                if (user.Id == uid)
                {
                    usr = user;
                }
            }

            if (usr is null)
            {
                return NotFound(uid);
            }

            _identity.AddToRoleAsync(usr, "Admin");
            return Ok(uid);
        }

        [Route("block/{uid}")]
        public IActionResult Block(string uid)
        {
            var usr = _userManager.GetUser(uid);
            usr.Blocked = true;
            foreach (var input in usr.Inputs)
            {
                input.Deleted = true;
            }

            _userManager.ChangeUser(usr);
            return Ok(uid);
        }

        [Route("unblock/{uid}")]
        public IActionResult Unblock(string uid)
        {
            var usr = _userManager.GetUser(uid);
            usr.Blocked = false;
            foreach (var input in usr.Inputs)
            {
                input.Deleted = false;
            }

            _userManager.ChangeUser(usr);
            return Ok(uid);
        }
    }
    
    
}