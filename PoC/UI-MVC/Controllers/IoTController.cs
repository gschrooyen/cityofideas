using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using PoC.Domain.iot;
using Microsoft.AspNetCore.Mvc;
using PoC.BL.manager.input;
using PoC.BL.manager.iot;

namespace PoC.Controllers
{
    
    [Route("{org}/[controller]")]
    public class IoTController : Controller
    {
        private readonly IIotManager _mgr;
        public IoTController(IIotManager mgr)
        {
            _mgr = mgr;
            }
        // GET
        [Route("manageiot")]
        public IActionResult ManageIoT()
        {
            List<Poster> posters = _mgr.GetPosters();
            return View(posters);
        }

        [HttpGet]
        [Route("addPoster")]
        [Authorize(Roles = "Admin")]
        public IActionResult AddPoster()
        {
            return View();
        }

        [HttpPost]
        [Route("addposter")]
        public IActionResult AddPoster(IFormCollection formValues)
        {
            Poster p = _mgr.MakePosterFromForm(formValues);
            
            return RedirectToAction("ManageIoT");
        }
        
        [HttpGet]
        [Route("/[controller]/addvote/{button}")]
        public void AddVote(string button)
        {
            Console.WriteLine(button);
            _mgr.AddVote(button);
        }

        [HttpGet]
        [Route("editposter/{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult Editposter(string id)
        {

            return View(_mgr.GetPoster(id));
        }
        
        [HttpPost]
        [Route("editposter/{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult Editposter(string id, IFormCollection formValues)
        {
            Poster p = _mgr.GetPoster(id);
            _mgr.ChangePosterFromForm(p, formValues);
            return RedirectToAction("manageiot");
        }
        
        [Route("deactivateposter/{id}")]
        [Authorize(Roles = "Admin")]
        public void DeactivatePoster(string id)
        {
            Poster poster = _mgr.GetPoster(id);

            _mgr.ClosePoster(poster);
        }

        [HttpGet]
        [Route("viewPoster/{id}")]
        [Authorize(Roles = "Admin")]
        public IActionResult Viewposter(string id)
        {
            Poster p = _mgr.GetPoster(id);
            return View(p);
        }
    }
}