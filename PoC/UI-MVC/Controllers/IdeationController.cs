using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.input;
using PoC.BL.manager.InputMethod;
using PoC.BL.manager.project;
using PoC.BL.manager.user;
using PoC.Domain.input;
using PoC.Domain.inputmethod;

namespace PoC.Controllers
{
  [Route("{org}/[controller]")]
  public class IdeationController : Controller
  {
    private readonly IInputMethodManager _mgr;
    private readonly IProjectManager _projectManager;
    private readonly UserManager<PoCIdentityUser> _usermgr;
    private readonly IHostingEnvironment _hostingEnvironment;
    private readonly IUserManager _realUserManager;
    private readonly IInputManager _inputManager;

    public IdeationController(IInputMethodManager inputMethodManager, IProjectManager projectManager,
      UserManager<PoCIdentityUser> usermgr, IHostingEnvironment hostingEnvironment, IUserManager realUserManager,
      IInputManager inputManager)
    {
      _mgr = inputMethodManager;
      _projectManager = projectManager;
      _usermgr = usermgr;
      _hostingEnvironment = hostingEnvironment;
      _realUserManager = realUserManager;
      _inputManager = inputManager;
    }


    // GET
    [HttpGet]
    [Route("{id}")]
    public IActionResult Index(string id, string org)
    {
      ViewData["ideationImage"] = org;

      var ideation = _mgr.GetIdeation(id);
      _mgr.ChangeIdeation(ideation);
      Console.WriteLine(ideation);
      return View(ideation);
    }

    [Authorize]
    [Route("/Ideation/AddReactionToIdea")]
    public void AddReactionToIdea(string text, string ideaId)
    {
      var UserId = _usermgr.GetUserId(User);
      var user = _realUserManager.GetUser(UserId);
      var reactions = user.Inputs.ToList();
      Reaction reaction = new Reaction(_inputManager.GetIdea(ideaId), text, user);
      reaction.Time = DateTime.Now;
      reactions.Add(reaction);
      user.Inputs = reactions;
      _realUserManager.ChangeUser(user);

      Idea idea = _inputManager.GetIdea(ideaId);
      var reactionss = idea.Reactions.ToList();
      reactionss.Add(reaction);
      idea.Reactions = reactionss;
      _inputManager.ChangeIdea(idea);
      //_mgr.addReactionToIdea(ideaId, text.Trim(), _usermgr.GetUserId(User));
    }

    [Authorize]
    [Route("{id}/AddIdeaToIdeation")]
    public IActionResult AddIdeaToIdeation(string org, string titleIdea, string descriptionIdea, string ideationId,
      List<IFormFile> imageIdea, List<IFormFile> videoIdea, string videoLinkIdea)
      
    {
      var id = _usermgr.GetUserId(User);
      var user = _realUserManager.GetUser(id);
      Idea idea = new Idea(titleIdea, descriptionIdea);
      Ideation ideation = _mgr.GetIdeation(ideationId);
      if (ideation.AllowImage && imageIdea != null)
      {
        foreach (var i in imageIdea)
        {
          if (ExtensionMethods.FileManagement.CheckFileExtension(i.FileName))
          {
            var imageIdeaOrgString = "organisations/" + org + "/organisation/projects/ideations/ideas/images";
            var imageFileName = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,imageIdeaOrgString, i);
            var names = idea.ImageNames.ToList();
            names.Add(new Image(imageFileName));
            idea.ImageNames = names.AsEnumerable();
            
            _mgr.ChangeIdeation(ideation);
            
          }
        }
      }

      if (ideation.AllowVideo && videoIdea != null)
      {
        foreach (var vid in videoIdea)
        {
          if (Path.GetExtension(vid.FileName).Equals(".mp4") ||
              Path.GetExtension(vid.FileName).Equals(".webm") ||
              Path.GetExtension(vid.FileName).Equals(".ogg"))
          {
            var videoIdeaOrgString = "organisations/" + org + "/organisation/projects/ideations/ideas/videos";
            var videoFileName = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,videoIdeaOrgString, vid);

            var names = idea.VideoNames.ToList();
            names.Add(new Video(videoFileName));
            idea.VideoNames = names.AsEnumerable();
          }
        }
      }

      if (ideation.AllowVideoLink && videoLinkIdea != null)
      {
        var videoLinkName = videoLinkIdea.Substring(videoLinkIdea.LastIndexOf('=') + 1);
        idea.VideoLinkName = videoLinkName;
      }

      _mgr.AddIdeaToIdeation(ideationId, idea, _usermgr.GetUserId(User));


      return RedirectToAction("Index", new {id = ideationId});
    }

    [Route("/Ideation/addLikeToIdea/{ideaId}/{userId}")]
        public void AddLikeToIdea(string ideaId, string userId)
        {
            _mgr.AddLikeToIdea(ideaId, _realUserManager.GetUser(userId));
        }

    [Route("/Ideation/addShareToIdea")]
    public void AddShareToIdea(string ideaId)
    {
      _mgr.AddShareToIdea(ideaId);
    }

    [Route("/Ideation/AddShare")]
    public void AddShare(string id)
    {
      _mgr.AddShareToIdeation(id);
    }

    [HttpGet]
    [Route("edit/{id}")]
    public IActionResult EditIdeation(string id)
    {
      Ideation ideation = _mgr.GetIdeation(id);
      return View(ideation);
    }

        
    [HttpPost]
    [Route("edit/{id}")]
    public IActionResult EditIdeation(string org, Ideation ideation, IFormFile imageIdeation)
    {
      if (!ModelState.IsValid)
      {
        if (ModelState["Begin"].Errors.Count == 1)
        {
          ModelState["Begin"].Errors.Clear();
          ModelState.AddModelError("Begin", "Begindatum verplicht in te vullen");
}
        if (ModelState["End"].Errors.Count == 1)
        {
          ModelState["End"].Errors.Clear();
          ModelState.AddModelError("End", "Einddatum verplicht in te vullen");
        }

        return View(ideation);
      }

      if (imageIdeation != null)
      {
        if (ExtensionMethods.FileManagement.CheckFileExtension(imageIdeation.FileName))
        {
          {
            var ideationOrgString = "organisations/" + org + "/organisation/projects/ideations/images";
            var fileName = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,ideationOrgString, imageIdeation);
            ideation.ImageName = fileName;
          }
        }
      }

      _mgr.ChangeIdeation(ideation);
      return RedirectToAction("Index", new {id = ideation.Id});
    }


    [HttpPost]
    [Route("DeleteIdeation/{id}")]
    public void DeleteIdeation(string id)
    {
      Ideation ideation = _mgr.GetIdeation(id);


      _mgr.CloseIdeation(ideation);
    }

    [HttpPost]
    [Route("{id}/DeleteIdea")]
    public void DeleteIdea(string id, string ideaId)
    {
      Ideation ideation = _mgr.GetIdeation(id);
      _inputManager.RemoveIdea(ideaId);
    }
  }
}