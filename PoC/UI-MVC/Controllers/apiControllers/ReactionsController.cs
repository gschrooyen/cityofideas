using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PoC.BL.manager.input;
using PoC.Domain.input;
using PoC.Domain.moderation;

namespace PoC.Controllers.apiControllers
{
    [Route("{org}/api/[controller]")]
    [ApiController]
    public class ReactionsController : ControllerBase
    {
        private readonly IInputManager _manager;

        public ReactionsController(IInputManager inputManager)
        {
            _manager = inputManager;
        }
        
        //GET https://localhost:5001/api/reactions/getReactionsOfIdea/{iid}
        [HttpGet("{iid}")]
        [Route("getReactionsOfIdea/{iid}")]
        public IActionResult GetReactionsOfIdea(string iid)
        {
            return Ok(_manager.GetReactionsOfIdea(iid));
        }

        [HttpPost("{iid}")]
        [Route("addReaction/{iid}")]
        public IActionResult AddReactionToIdea(string iid, [FromBody] Reaction reaction)
        {
            var idea = _manager.GetIdea(iid);
            if (idea == null)
            {
                return BadRequest("er is geen idee met id:" + iid);
            }

            var reactions = idea.Reactions.ToList();
            reactions.Add(reaction);
            idea.Reactions = reactions;
            _manager.ChangeIdea(idea);
            return Ok(idea);
        }
        /*
         * POST https://localhost:5001/api/reactions/addNotification/{rid}}
            Content-Type: application/json Cookie: key=first-value

            {   place the notification here    }

         */
        
        [HttpPost("{rid}")]
        [Route("addNotification/{rid}")]
        public IActionResult AddNotification(string rid, [FromBody] Notification notification)
        {
            Console.WriteLine(rid);
            Console.WriteLine(notification.NotificationId);
            var reaction = _manager.GetReaction(rid);
            if (reaction == null)
            {
                return BadRequest("the reaction wasn't found in the database try another id");
            }

            var notifications = reaction.Notifications.ToList();
            notifications.Add(notification);
            reaction.Notifications = notifications;
            _manager.ChangeReaction(reaction);
            return Ok(reaction);
        }
    }
}