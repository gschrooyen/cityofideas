using Microsoft.AspNetCore.Mvc;
using PoC.BL.manager.organisation;

namespace PoC.Controllers.apiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganisationsController : ControllerBase
    {
        private readonly IOrganisationManager _organisationManager;

        public OrganisationsController(IOrganisationManager organisationManager)
        {
            _organisationManager = organisationManager;
        }
        
        [HttpGet]
        [Route("getAllOrganisations")]
        public IActionResult GetAllOrganisations()
        {
            var organisations = _organisationManager.GetAllOrganisations();
            if (organisations == null)
            {
                return BadRequest();
            }

            return Ok(organisations);
        }

        [HttpGet]
        [Route("getOrganisation/{organisationName}")]
        public IActionResult GetOrganisation(string organisationName)
        {
            var organisation = _organisationManager.GetOrganisationByName(organisationName);
            if (organisation == null)
            {
                return BadRequest();
            }

            return Ok(organisation);
        }
    }
}