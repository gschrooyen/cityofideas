using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PoC.Domain.inputmethod;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.InputMethod;
using PoC.BL.manager.project;
using PoC.BL.manager.user;
using PoC.Domain.input;
using PoC.Domain.inputMethod;
using PoC.Models.DTO;

namespace PoC.Controllers.apiControllers
{
    [Route("{org}/api/[controller]")]
    [ApiController]
    public class QuestionListsController : ControllerBase
    {
        private readonly IInputMethodManager _manager;
        private readonly IProjectManager _projectManager;
        private readonly UserManager<PoCIdentityUser> _identity;
        private readonly IUserManager _userManager;

        public QuestionListsController(IInputMethodManager manager, IProjectManager projectManager, UserManager<PoCIdentityUser> identity, IUserManager userManager)
        {
            _manager = manager;
            _projectManager = projectManager;
            _identity = identity;
            _userManager = userManager;
        }
        
        /*
         *
         * GET https://localhost:5001/api/questionlists/getQuestionList/{pid}
         */
        
        [HttpGet("{pid}")]
        [Route("getQuestionList/{pid}")]
        public IActionResult Get(string pid)
        {
            var questionLists = _manager.GetQuestionListFromProject(_projectManager.GetProject(pid));
            if (questionLists == null)
            {
                return NoContent();
            }

            foreach (var q in questionLists)
            {
                foreach (var question in q.Questions)
                {
                    if (question.GetType() == typeof(ChoiceQuestion))
                    {
                        var choiceQuestion = (ChoiceQuestion) question;

                        choiceQuestion.Options = _manager.GetOptionsOfChoiceQuestion(choiceQuestion).ToList();
                    }
                }
            }

            return Ok(questionLists);
        }
        
        [HttpGet("{questionListId}")]
        [Route("getQuestionListSingle/{questionListId}")]
        public IActionResult getQuestionListSingle(string questionListId)
        {
            var questionList = _manager.GetQuestionListWithoudAnswers(questionListId);
            if (questionList == null)
                return BadRequest();
            
            foreach (var question in questionList.Questions)
            {
                if (question.GetType() == typeof(ChoiceQuestion))
                {
                    var choiceQuestion = (ChoiceQuestion) question;

                    choiceQuestion.Options = _manager.GetOptionsOfChoiceQuestion(choiceQuestion).ToList();
                }
            }
            
            return Ok(questionList);
        }
        
        [HttpPost("{questionId}")]
        [Route("updateQuestion/{questionId}")]
        public IActionResult updateQuestion(string questionId, [FromBody] String[] answers)
        {
            if (_manager.GetOpenQuestion(questionId) != null)
            {
                var question = _manager.GetOpenQuestion(questionId);
                var awnsers = question.Awnsers.ToList();
                awnsers.AddRange(answers.Select(answer => new Answer(answer)));
                question.Awnsers = awnsers;

                _manager.ChangeQuestion(question);
            }else if (_manager.GetChoiceQuestion(questionId) != null)
            {
                var question = _manager.GetChoiceQuestion(questionId);
                var awnsers = question.Awnsers.ToList();
                awnsers.AddRange(answers.Select(answer => new Answer(answer)));
                question.Awnsers = awnsers;

                _manager.ChangeQuestion(question);
            }
            return Ok();
        }

        /*
         * GET https://localhost:5001/api/questionlists/getQuestionsOfList/{id}
         */
        
        [HttpGet("{id}")]
        [Route("getQuestionsOfList/{id}")]
        public IActionResult GetQuestions(string id)
        {
            var questions = _manager.GetQuestionsOfList(id);
            if (questions == null || !questions.Any())
            {
                return NoContent();
            }

            return Ok(questions);
        }
        
        /*POST https://localhost:5001/api/questionlists/UpdateList/{id}
        Content-Type: application/json Cookie: key=first-value
        
        {
            vul hier de questionlist in
        }
        */
        //Content type is belangrijk

        
        [HttpPost("{id}")]
        [Route("UpdateList/{id}")]
        public void PostQuestionlist([FromBody] QuestionList questionList)
        {
            Console.WriteLine(questionList.Id + " hey");
            foreach (var question in questionList.Questions)
            {
                
                if (question.GetType() == typeof(OpenQuestion))
                {
                   var q = _manager.GetOpenQuestion(question.id);
                   var qawnsers = q.Awnsers.ToList();
                   qawnsers.AddRange(question.Awnsers);
                   q.Awnsers = qawnsers;
                   _manager.ChangeQuestion(q);
                }
                else
                {
                    var q = _manager.GetChoiceQuestion(question.id);
                    var qawnsers = q.Awnsers.ToList();
                    qawnsers.AddRange(question.Awnsers);
                    q.Awnsers = qawnsers;
                    _manager.ChangeQuestion(q);
                }
                
                
            }
        }

        [HttpGet]
        [Route("getquestion/{id}")]
        public IActionResult getquestion(string id)
        {
            return Ok(_manager.GetChoiceQuestion(id));
        }
    }
}