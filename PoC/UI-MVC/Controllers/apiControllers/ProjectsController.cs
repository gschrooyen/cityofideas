using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using PoC.BL.manager.organisation;
using PoC.BL.manager.project;
using PoC.Domain.input;

namespace PoC.Controllers.apiControllers
{
    [Route("{org}/api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectManager _manager;
        private readonly IOrganisationManager _organisationManager;

        public ProjectsController(IProjectManager manager, IOrganisationManager organisationManager)
        {
            _manager = manager;
            _organisationManager = organisationManager;
        }
        
        //GET api/Projects/getProjects
        [Route("getProjects")]
        public IActionResult Get(string org)
        {
            var result = _organisationManager.GetOrganisationByName(org.ToUpper()).Projects;
            if (result == null || !EnumerableExtensions.Any(result))
            {
                return NoContent();
            }

            return Ok(result);
        }
        
        //GET api/projects/getproject/{id}
        [HttpGet("{id}")]
        [Route("getProject/{id}")]
        public IActionResult Getproject(string id)
        {
            var project = _manager.GetProject(id);
            if (project == null)
            {
                return NotFound(id);
            }

            return Ok(project);
        }

        

        //POST api/projects/addLike/{pid}
        /*
         *     POST https://localhost:5001/api/projects/addlike/{pid}}
               Content-Type: application/json Cookie: key=first-value

               {  like hier plaatsen    }
         */
        [HttpPost("{pid}")]
        [Route("Addlike/{pid}")]
        public IActionResult AddLike(string pid , [FromBody] Like like)
        {
            //TODO deze logica moet ergens in de managers en repo's staan aub
            //TODO controle inbouwen
            var proj = _manager.GetProject(pid);
            var likees = proj.Likes.ToList();
            likees.Add(like);
            proj.Likes = likees;
            _manager.ChangeProject(proj);
            return Ok(proj);
        }
            
        /*
         *     POST https://localhost:5001/api/projects/addShare/{pid}}
               Content-Type: application/json Cookie: key=first-value

               {  share hier plaatsen    }
         */
        
        [HttpPost("{pid}")]
        [Route("addShare/{pid}")]
        public IActionResult AddShare(string pid, [FromBody] Share share)
        {
            var proj = _manager.GetProject(pid);
            var shares = proj.Shares.ToList();
            shares.Add(share);
            proj.Shares = shares;
            _manager.ChangeProject(proj);
            return Ok(proj);
        }

        [HttpGet("{pid}")]
        [Route("getLikesFromProject/{pid}")]
        public IActionResult GetLikesFromProject(string pid)
        {
            var project = _manager.GetProject(pid);
            if (project == null)
            {
                return NoContent();
            }

            return Ok(project.Likes);
        }
        
        [HttpGet("{pid}")]
        [Route("getSharesFromProject/{pid}")]
        public IActionResult GetSharesFromProject(string pid)
        {
            var project = _manager.GetProject(pid);
            if (project == null)
            {
                return NoContent();
            }

            return Ok(project.Shares);
        }
    }
}