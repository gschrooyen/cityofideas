using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PoC.BL.manager.input;
using PoC.BL.manager.InputMethod;
using PoC.Domain.input;
using PoC.Domain.moderation;

namespace PoC.Controllers.apiControllers
{
    [Route("{org}/api/[controller]")]
    [ApiController]
    public class IdeasController : ControllerBase
    {
        private readonly IInputManager _manager;
        private readonly IInputMethodManager _inputMethodManager;

        public IdeasController(IInputManager manager, IInputMethodManager inputMethodManager)
        {
            _inputMethodManager = inputMethodManager;
            _manager = manager;
        }
        
        [HttpGet("{ideationId}")]
        [Route("getIdeasFromIdeation/{ideationId}")]
        public IActionResult GetIdeasFromIdeation(string ideationId)
        {
            return Ok(_inputMethodManager.GetIdeation(ideationId).Ideas);
        }
        
        [HttpGet("{ideaId}")]
        [Route("getIdea/{ideaId}")]
        public IActionResult getIdea(string ideaId)
        {
            var idea = _manager.GetIdea(ideaId);
            if (idea == null)
                return BadRequest();

            return Ok(idea);
        }

        [HttpGet("ideaId")]
        [Route("getReactionsFromIdea/{ideaId}")]
        public IActionResult GetReactionsFromIdea(string ideaId)
        {
            var idea = _manager.GetIdea(ideaId);
            
            return Ok(idea.Reactions);
        }

        [HttpGet("ideaId")]
        [Route("getLikesFromIdea/{ideaId}")]
        public IActionResult GetLikesFromIdea(string ideaId)
        {
            return Ok(_manager.GetIdea(ideaId).Likes);
        }
        
        [HttpGet("ideaId")]
        [Route("getSharesFromIdea/{ideaId}")]
        public IActionResult GetSharesFromIdea(string ideaId)
        {
            return Ok(_manager.GetIdea(ideaId).Shares);
        }
        
        //iid == IDEA id
        [HttpPost("{ideaId}")]
        [Route("addLikeToIdea/{ideaId}")]
        public IActionResult AddLikeToIdea(string ideaId, [FromBody] Like like)
        {
            var idea = _manager.GetIdea(ideaId);
            if (idea == null)
            {
                
                return BadRequest(ideaId);
            }
            var likes = idea.Likes.ToList();
            likes. Add(like);
            idea.Likes = likes;
            _manager.ChangeIdea(idea);
            return Ok(idea);
        }
        
        //iid == IDEA id
        [HttpPost("{ideaId}")]
        [Route("addShareToIdea/{ideaId}")]
        public IActionResult AddShareToIdea(string ideaId, [FromBody] Share share)
        {
            var idea = _manager.GetIdea(ideaId);
            if (idea == null)
            {
                
                return BadRequest(ideaId);
            }
            var shares = idea.Shares.ToList();
            shares.Add(share);
            idea.Shares = shares;
            _manager.ChangeIdea(idea);
            return Ok(idea);
        }
        
        //iid = IDEA id
        [HttpPost("{ideaId}")]
        [Route("addReactionToIdea/{ideaId}")]
        public IActionResult AddReactionToIdea(string ideaId, [FromBody] Reaction reaction)
        {
            var idea = _manager.GetIdea(ideaId);
            if (idea == null)
            {
                
                return BadRequest(ideaId);
            }
            var reactions = idea.Reactions.ToList();
            reaction.Time = DateTime.Now;
            reactions.Add(reaction);
            idea.Reactions = reactions;
            _manager.ChangeIdea(idea);
            return Ok(idea);
        }

        [HttpPost("{ideaId}")]
        [Route("addNotificationToIdea/{ideaId}")]
        public IActionResult AddNotificationToIdea(string ideaId, [FromBody] Notification notification)
        {
            var idea = _manager.GetIdea(ideaId);
            if (idea == null)
            {           
                return BadRequest(ideaId);
            }
            var notifications = idea.Notifications.ToList();
            notifications.Add(notification);
            idea.Notifications = notifications;
            _manager.ChangeIdea(idea);
            return Ok(idea);
        }
    }
}