using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.user;
using PoC.Domain.user;
using PoC.Models.Services;

namespace PoC.Controllers.apiControllers
{
    [Route("{org}/api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserManager _userManager;
        private readonly UserManager<PoCIdentityUser> _identity;
        public UsersController(IUserService userService, IUserManager userManager, UserManager<PoCIdentityUser> identity)
        {
            _userService = userService;
            _identity = identity;
            _userManager = userManager;
        }
        
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]PoCIdentityUser userParam)
        {
            var user = _userService.CorrectPassword(userParam.Email, userParam.PasswordHash);
            if (user == null)
                return BadRequest(new { message = "Does not work" });

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("updateUser")]
        public IActionResult UpdateUser([FromBody]User userParam)
        {
            var user = _userManager.GetUserByEmail(userParam.Email);
            user.UserName = userParam.UserName;
            user.FirstName = userParam.FirstName;
            user.LastName = userParam.LastName;
            
            var identityUser = _identity.FindByEmailAsync(userParam.Email).Result;
            if (identityUser == null)
                return BadRequest();
            
            _userManager.ChangeUser(user);
            identityUser.UserName = user.Email;
            identityUser.NormalizedUserName = user.Email.ToUpper();
            _identity.UpdateAsync(identityUser);
            return Ok(userParam);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]User user)
        {
            PoCIdentityUser poCIdentityUser = new PoCIdentityUser();
            poCIdentityUser.Email = user.Email;
            poCIdentityUser.NormalizedEmail = user.Email.ToUpper();
            poCIdentityUser.UserName = user.Email;
            poCIdentityUser.NormalizedUserName = user.Email.ToUpper();
            poCIdentityUser.UserName = user.Email;
            
            var succeeded = _identity.CreateAsync(poCIdentityUser, user.PasswordHash).Result.Succeeded;
            if (!succeeded)
            {
                return BadRequest();
            }
            
            _identity.AddToRoleAsync(poCIdentityUser, "User");
            
            user.UserId = poCIdentityUser.Id;

            user.PasswordHash = null;
            _userManager.AddUser(user);
            
            return Ok(user);
        }
        
        // GET
        [Authorize(Roles = "Moderator")]
        [HttpGet]
        [Route("searchUsers")]
        public List<User> GetUsers(string org)
        {
            var identityusrs = _identity.GetUsersInRoleAsync("User");
            var identitymods = _identity.GetUsersInRoleAsync("Moderator");
            var usrs = new List<User>();
            identityusrs.Wait();
            identitymods.Wait();
            var users = identityusrs.Result;
            var mods = identitymods.Result;
            foreach (var poCIdentityUser in mods)
            {
                users.Remove(poCIdentityUser);
            }
            var ids = users.Select(user => user.Id).ToList();
            foreach (var user in _userManager.GetUsers())
            {
                if  (ids.Contains(user.UserId))
                {
                    usrs.Add(user);
                }
            }

            return usrs;
        }

        [Authorize(Roles = "Moderator")]
        [HttpGet]
        [Route("searchMods")]
        public List<User> GetMods(string org)
        {
            var identityusrs = _identity.GetUsersInRoleAsync("Moderator");
            var identitymods = _identity.GetUsersInRoleAsync("Admin");
            var usrs = new List<User>();
            identityusrs.Wait();
            identitymods.Wait();
            var users = identityusrs.Result;
            var mods = identitymods.Result;
            foreach (var poCIdentityUser in mods)
            {
                users.Remove(poCIdentityUser);
            }
            var ids = users.Select(user => user.Id).ToList();
            foreach (var user in _userManager.GetUsers())
            {
                if  (ids.Contains(user.UserId))
                {
                    usrs.Add(user);
                }
            }

            return usrs;
        }
        
        [HttpGet]
        [Authorize(Roles = "SuperAdmin")]
        [Route("SearchAdmins")]
        public IActionResult ManageAdmins(string org)
        {
            var admintask = _identity.GetUsersInRoleAsync("Admin");
            
            var superadmintask = _identity.GetUsersInRoleAsync("SuperAdmin");
            var users = _userManager.GetUsers();
            admintask.Wait();
            superadmintask.Wait();
            Console.WriteLine(admintask.Result.Count);
            Console.WriteLine(superadmintask.Result.Count);
            var admins = admintask.Result.Except(superadmintask.Result).Select(sa => sa.Id).ToList();
            Console.WriteLine(admins.Count);
            var retusers = new List<User>();
            foreach (var user in users)
            {
                if (admins.Contains(user.UserId))
                {
                    retusers.Add(user);
                }
            }
            
            return Ok(retusers);
        }
    }
}