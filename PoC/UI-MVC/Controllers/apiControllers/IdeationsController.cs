using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using PoC.BL.manager.InputMethod;
using PoC.BL.manager.project;
using PoC.Domain.input;

namespace PoC.Controllers.apiControllers
{
    [Route("{org}/api/[controller]")]
    [ApiController]
    public class IdeationsController : ControllerBase
    {
        private readonly IInputMethodManager _manager;
        private readonly IProjectManager _projectManager;

        public IdeationsController(IInputMethodManager manager, IProjectManager projectManager)
        {
            _manager = manager;
            _projectManager = projectManager;
        }
        
        //GET /api/Ideations
        [HttpGet("{pid}")]
        [Route("getIdeationsFromProject/{pid}")]
        public IActionResult GetIdeation(string pid)
        {
            var project = _projectManager.GetProject(pid);
            var ideations = _manager.GetIdeationsFromProject(project);
            
            return Ok(ideations);
        }

        [HttpGet("{ideationId}")]
        [Route("getIdeation/{ideationId}")]
        public IActionResult GetIdeationSingle(string ideationId)
        {
            var ideation = _manager.GetIdeation(ideationId);
            if (ideation == null)
                return BadRequest();

            return Ok(ideation);
        }

        [HttpGet("{ideationId}")]
        [Route("getSharesFromIdeation/{ideationId}")]
        public IActionResult GetSharesFromIdeation(string ideationId)
        {
            var ideation = _manager.GetIdeation(ideationId);
            if (ideation == null)
            {
                return NoContent();
            }

            return Ok(ideation.Shares);
        }

        [HttpGet("{ideationId}")]
        [Route("getIdeasFromIdeation/{ideationId}")]
        public IActionResult GetIdeasFromIdeation(string ideationId)
        {
            var ideation = _manager.GetIdeation(ideationId);
            if (ideation == null)
            {
                return NoContent();
            }

            return Ok(ideation.Ideas);
        }

        [HttpPost]
        [Route("addShare/{ideationId}")]
        public IActionResult AddShare(string ideationId, [FromBody] Share share)
        {
            var ideation = _manager.GetIdeation(ideationId);
            var shares = ideation.Shares.ToList();
            shares.Add(share);
            ideation.Shares = shares;
            _manager.ChangeIdeation(ideation);
            return Ok(ideation);
        }

        [HttpPost]
        [Route("addIdeaToIdeation/{ideationId}")]
        public IActionResult AddIdeaToIdeation(string ideationId, Idea idea)
        {
            var ideation = _manager.GetIdeation(ideationId);
            var ideas = ideation.Ideas.ToList();
            ideas.Add(idea);
            ideation.Ideas = ideas;
            _manager.ChangeIdeation(ideation);
            return Ok(ideation);
        }
    }
}