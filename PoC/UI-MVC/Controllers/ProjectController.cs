using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PoC.BL.manager.InputMethod;
using PoC.BL.manager.organisation;
using PoC.BL.manager.project;
using PoC.BL.manager.user;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.organisation;
using PoC.Domain.project;
using PoC.Models;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace PoC.Controllers
{
  //[Authorize(Roles = "SuperAdmin")]
  [Route("{org}/projecten")]
  public class ProjectController : Controller
  {
    private readonly IProjectManager _mgr;
    private readonly IOrganisationManager _organisationManager;
    private readonly IInputMethodManager _inputmgr;
    private readonly IUserManager _userManager;

    //File Management
    private readonly IHostingEnvironment _hostingEnvironment;


    public ProjectController(IProjectManager mgr, IInputMethodManager inputmgr,
      IHostingEnvironment hostingEnvironment, IOrganisationManager organisationManager,
      IUserManager userManager)
    {
      _mgr = mgr;
      _inputmgr = inputmgr;
      _hostingEnvironment = hostingEnvironment;
      _organisationManager = organisationManager;
      _userManager = userManager;
    }


    private IEnumerable<Project> SortProjects(string org)
    {
      IEnumerable<Project> projects = _organisationManager.GetOrganisationByName(org.ToUpper()).Projects;
      var projs = new List<Project>();
      foreach (var project in projects)
      {
        var proj = _mgr.GetProject(project.Id);
        projs.Add(proj);
      }

      projs.ToList().Sort((p1, p2) => p1.Begin.CompareTo(p2.Begin));

      return projs;
    }

    // GET
    [HttpGet]
    public IActionResult Index(string org)
    {
      ViewData["projectImage"] = org;

      IEnumerable<Project> projects = SortProjects(org);
      return View(projects);
    }


    [HttpGet]
    [Route("/{org}/projecten/Detail/{id}")]
    public IActionResult Detail(string org, string id)
    {
      ViewData["projectImage"] = org;

      Project proj = _mgr.GetProject(id);
      //NullReferenceException: Object reference not set to an instance of an object.
      //PoC.Controllers.ProjectController.Detail(string id) in ProjectController.cs, line 50 bij Edit
      foreach (var pastfase in proj.PastFases)
      {
        List<InputMethod> list = new List<InputMethod>();
        foreach (InputMethod method in _mgr.GetInputMethodsOfFase(pastfase.Id)) list.Add(method);
        pastfase.InputMethods = list;
      }

      List<InputMethod> list1 = new List<InputMethod>();
      foreach (InputMethod method in _mgr.GetInputMethodsOfFase(proj.CurrentFase.Id)) list1.Add(method);
      proj.CurrentFase.InputMethods = list1;
      return View(proj);
    }

    [Route("/projecten/addLikeToProject/{pid}/{uid}")]
    public void addLikeToProject(string pid, string uid)
    {
      _mgr.AddLikeToProject(_mgr.GetProject(pid), _userManager.GetUser(uid));
    }

    [Route("/projecten/addShareToProject/{pid}")]
    public void AddShareToProject(string pid)
    {
      _mgr.AddShareToProject(_mgr.GetProject(pid));
    }

    [HttpPost]
    [Route("Detail/{id}")]
    public IActionResult Detail(IFormCollection formData, string id, string org)
    {
      QuestionList questionList = _mgr.GetQuestionList(formData["listId"]);
      List<Question> questions = questionList.Questions.ToList();
      List<Answer> answers = new List<Answer>();
      ViewData["projectImage"] = org;
      

      foreach (var question in questions)
      {
        var t = formData[question.id].Count;
        if (t != 0)
        {
          answers = question.Awnsers.ToList();
          foreach (var value in formData[question.id])
          {
            answers.Add(new Answer(value));
            question.Awnsers = answers;
          }

          _inputmgr.ChangeQuestion(question);
        }
      }

      var p = _mgr.GetProject(id);
      return View(p);
    }
    [HttpGet]
    [Route("SearchProjects")]
    public IActionResult SearchProjects()
    {
      List<Project> searchProjects = new List<Project>();
      return View(searchProjects);
    }
    
     
    [HttpPost]
    [Route("SearchProjects")]
    public IActionResult SearchProjects(string org, string keywordProjects)
    {
      ViewData["organisation"] = org;
      List<Project> searchProjects = new List<Project>();
      if (keywordProjects != null)
      {
        IEnumerable<Project> projects = SortProjects(org);
        searchProjects = projects.Where(project =>
          project.Name.ToLower().Replace(" ", "").Contains(keywordProjects.ToLower().Replace(" ", ""))).ToList();
      }

      return View(searchProjects);
    }
    // Admin
    [Authorize(Roles = "Admin, SuperAdmin")]
    [HttpGet]
    [Route("ManageProjects")]
    public IActionResult ManageProjects(string org)
    {
      IEnumerable<Project> projects = SortProjects(org);

      return View(projects);
    }

    [HttpGet]
    [Route("EditProject/{id}")]
    public IActionResult EditProject(string id)
    {
      Project project = _mgr.GetProject(id);
      return View(project);
    }

    [HttpPost]
    [Route("EditProject/{id}")]
    public IActionResult EditProject(string org, Project project, IFormFile imageProject)
    {
      if (!ModelState.IsValid)
      {
        if (ModelState["Begin"].Errors.Count == 1)
        {
          ModelState["Begin"].Errors.Clear();
          ModelState.AddModelError("Begin", "Begindatum verplicht in te vullen");
        }

        if (ModelState["End"].Errors.Count == 1)
        {
          ModelState["End"].Errors.Clear();
          ModelState.AddModelError("End", "Einddatum verplicht in te vullen");
        }

        if (ModelState["CurrentFase.Begin"].Errors.Count == 1)
        {
          ModelState["CurrentFase.Begin"].Errors.Clear();
          ModelState.AddModelError("CurrentFase.Begin", "Begindatum Fase verplicht in te vullen");
        }

        if (ModelState["CurrentFase.End"].Errors.Count == 1)
        {
          ModelState["CurrentFase.End"].Errors.Clear();
          ModelState.AddModelError("CurrentFase.End", "Einddatum Fase verplicht in te vullen");
        }

        return View(project);
      }

      if (imageProject != null)
      {
        if (ExtensionMethods.FileManagement.CheckFileExtension(imageProject.FileName))
        {
          {
            var projectOrgstring = "organisations/" + org + "/organisation/projects/images";
            var fileName = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,projectOrgstring, imageProject);
            project.ImageName = fileName;
          }
        }
      }

      _mgr.ChangeProject(project);
      return RedirectToAction("ManageProjects");
    }

    // Methode renamen naar Afsluiten/Heropstarten (?)
    [HttpPost]
    [Route("DeleteProject/{id}")]
    public void DeleteProject(string id)
    {
      Project project = _mgr.GetProject(id);


      _mgr.CloseProject(project);
    }


    [HttpGet]
    [Route("CreateProject")]
    public IActionResult CreateProject()
    {
      return View();
    }

    [HttpPost]
    [Route("CreateProject")]
    public IActionResult CreateProject(string org, ProjectViewModel projectViewModel)
    {
      if (!ModelState.IsValid)
      {
        if (ModelState["Begin"].Errors.Count == 1)
        {
          ModelState["Begin"].Errors.Clear();
          ModelState.AddModelError("Begin", "Begindatum verplicht in te vullen");
        }

        if (ModelState["End"].Errors.Count == 1)
        {
          ModelState["End"].Errors.Clear();
          ModelState.AddModelError("End", "Einddatum verplicht in te vullen");
        }

        if (ModelState["Fase.Begin"].Errors.Count == 1)
        {
          ModelState["Fase.Begin"].Errors.Clear();
          ModelState.AddModelError("Fase.Begin", "Begindatum Fase verplicht in te vullen");
        }

        if (ModelState["Fase.End"].Errors.Count == 1)
        {
          ModelState["Fase.End"].Errors.Clear();
          ModelState.AddModelError("Fase.End", "Einddatum Fase verplicht in te vullen");
        }

        return View(projectViewModel);
      }

      var fileName = "";
      if (ExtensionMethods.FileManagement.CheckFileExtension(projectViewModel.ImageCreateProject.FileName))
      {
        var projectOrgstring = "organisations/" + org + "/organisation/projects/images";
        fileName = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,projectOrgstring, projectViewModel.ImageCreateProject);
      }

      Organisation organisation = _organisationManager.GetOrganisationByName(org.ToUpper().Replace(" ", ""));

      Project project = _mgr.AddProjectViewModel(projectViewModel.Name, fileName,
        projectViewModel.MainQuestion, projectViewModel.Description, projectViewModel.Category, projectViewModel.Begin,
        projectViewModel.End, projectViewModel.Fase.FaseName, projectViewModel.Fase.ProjectFase,
        projectViewModel.Fase.Begin, projectViewModel.Fase.End);

      _organisationManager.AddProjectToOrganisation(organisation, project);

      return RedirectToAction("Detail", new {id = project.Id});
    }

    [HttpGet]
    [Route("CreateFaseProject/{id}")]
    public IActionResult CreateFaseProject()
    {
      return View();
    }

    [HttpPost]
    [Route("CreateFaseProject/{id}")]
    public IActionResult CreateFaseProject(string id, FaseViewModel faseViewModel)
    {
      Project project = _mgr.GetProject(id);

      if (!ModelState.IsValid)
      {
        if (ModelState["Begin"].Errors.Count == 1)
        {
          ModelState["Begin"].Errors.Clear();
          ModelState.AddModelError("Begin", "Begindatum verplicht in te vullen");
        }

        if (ModelState["End"].Errors.Count == 1)
        {
          ModelState["End"].Errors.Clear();
          ModelState.AddModelError("End", "Einddatum verplicht in te vullen");
        }

        return View(faseViewModel);
      }
      
      _mgr.AddFaseToProject(project,faseViewModel.FaseName,faseViewModel.ProjectFase,faseViewModel.Begin,faseViewModel.End);
      return RedirectToAction("Detail", new {id = project.Id});
    }

    [HttpGet]
    [Route("inputMethodPastFase/{id}")]
    public IActionResult InputMethodsPastFase(string id)
    {
      Fase pastFase = _mgr.GetFase(id);
      List<InputMethod> list = new List<InputMethod>();
      foreach (InputMethod method in _mgr.GetInputMethodsOfFase(pastFase.Id)) list.Add(method);
      pastFase.InputMethods = list;
      return View(pastFase);
    }

    [HttpGet]
    [Route("CreateIdeationFase/{id}")]
    public IActionResult CreateIdeationFase()
    {
      return View();
    }

    [HttpPost]
    [Route("CreateIdeationFase/{id}")]
    public IActionResult CreateIdeationFase(string org, string id, IdeationViewModel ideationViewModel)
    {
      Project project = _mgr.GetProject(id);


      if (!ModelState.IsValid)
      {
        if (ModelState["Begin"].Errors.Count == 1)
        {
          ModelState["Begin"].Errors.Clear();
          ModelState.AddModelError("Begin", "Begindatum verplicht in te vullen");
        }

        if (ModelState["End"].Errors.Count == 1)
        {
          ModelState["End"].Errors.Clear();
          ModelState.AddModelError("End", "Einddatum verplicht in te vullen");
        }

        return View(ideationViewModel);
      }

      var fileName = "";
      if (ExtensionMethods.FileManagement.CheckFileExtension(ideationViewModel.ImageIdeation.FileName))
      {
        var ideationOrgstring = "organisations/" + org + "/organisation/projects/ideations/images";
        fileName = ExtensionMethods.FileManagement.UploadFile(_hostingEnvironment.WebRootPath,ideationOrgstring, ideationViewModel.ImageIdeation);
      }

      Ideation ideation = _inputmgr.AddIdeationViewModel(ideationViewModel.Type, ideationViewModel.TitleIdeation,
        ideationViewModel.DescriptionIdeation, ideationViewModel.Begin, ideationViewModel.End,
        ideationViewModel.BackgroundLink, fileName,
        ideationViewModel.AllowImage, ideationViewModel.NumberOfImages, ideationViewModel.ImageRequired,
        ideationViewModel.AllowVideo, ideationViewModel.NumberOfVideos, ideationViewModel.VideoRequired,
        ideationViewModel.AllowVideoLink, ideationViewModel.VideoLinkRequired);
      _mgr.AddIdeationToProject(project, ideation);

      return RedirectToAction("Index", "Ideation", new {id = ideation.Id});
    }

    [HttpGet]
    [Route("CreateQuestionListFase/{id}")]
    public IActionResult CreateQuestionListFase()
    {
      return View();
    }

    [HttpPost]
    [Route("CreateQuestionListFase/{id}")]
    public IActionResult CreateQuestionListFase(string id, QuestionListViewModel questionListViewModel)
    {
      Project project = _mgr.GetProject(id);

      if (!ModelState.IsValid)
      {
        if (ModelState["Begin"].Errors.Count == 1)
        {
          ModelState["Begin"].Errors.Clear();
          ModelState.AddModelError("Begin", "Begindatum verplicht in te vullen");
        }

        if (ModelState["End"].Errors.Count == 1)
        {
          ModelState["End"].Errors.Clear();
          ModelState.AddModelError("End", "Einddatum verplicht in te vullen");
        }

        return View(questionListViewModel);
      }


      QuestionList questionList = _inputmgr.AddQuestionListViewModel(questionListViewModel.Title,
        questionListViewModel.Description, questionListViewModel.Begin, questionListViewModel.End);
      _mgr.AddQuestionListToProject(project, questionList);


      return RedirectToAction("Index", "QuestionList", new {id = questionList.Id});
    }
  }
}