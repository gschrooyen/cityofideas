using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace PoC.Controllers
{
  [Route("error")]
  public class ErrorController : Controller
  {
    // GET
    [Route("404")]
    public IActionResult Index()
    {
      return View();
    }
  }
}