using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.input;
using PoC.BL.manager.moderation;
using PoC.BL.manager.user;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.moderation;


namespace PoC.Controllers {
    
    //added the policy for this type of user:
    [Authorize(Roles = "Moderator, Admin, SuperAdmin")]
    [Route("{org}/[controller]")]
    public class ModeratorController :Controller
    {
        
        private readonly IModerationManager _mgr;
        private readonly UserManager<PoCIdentityUser> _usermgr;
        private readonly IInputManager _inputManager;
        private IUserManager _usrMgr;
        public ModeratorController(IModerationManager mgr, UserManager<PoCIdentityUser> usermgr, IInputManager inputManager, IUserManager iUsermgr)
        {
            _inputManager = inputManager;
            _usermgr = usermgr;
            _mgr = mgr;
            _usrMgr = iUsermgr;
        }
        
        [HttpGet]
        [Authorize(Roles = "Moderator")]
        public IActionResult Index()
        {
            return View(_inputManager.GetMarkedReactions());
        }
        
        [Authorize]
        [Route("/Moderator/report")]
        public void Report(string reason, string extra, string commentId)
        {
            
            NotificationType type;
            switch (reason)
            {
                case "language": 
                    type = NotificationType.INAPPROPRIATE_LANGUAGE;
                    break;
                case "commercial":
                    type= NotificationType.ADVERTISMENT;
                    break;
                case "unneccesary":
                    type = NotificationType.IRRELEVANT;
                    break;
                default:
                    type = NotificationType.DOUBLE;
                    break;
            }
            
            var r = _inputManager.GetReaction(commentId);

            var user = _usrMgr.GetUser(_usermgr.GetUserId(User));
            
            _inputManager.AddNotification(r.Id, user.UserId, type, extra);
        }

        [Authorize(Roles = "Moderator")]
        [Route("/Moderator/ignore/{id}")]
        public void Ignore(string id)
        {
            var notification = _mgr.GetNotification(id);
            var moderation = new Moderation(ModerationAction.IGNORE, notification);
            notification.Moderate(moderation);
            _mgr.ChangeNotification(notification);

        }

        [Authorize(Roles = "Moderator")]
        [Route("/Moderator/Delete/{id}")]
        public void Delete(string id)
        {
            var notification = _mgr.GetNotification(id);
            var moderation = new Moderation(ModerationAction.DELETE_POST, notification);
            if (notification.Reaction != null)
            {
                Reaction r = notification.Reaction;
                r.Deleted = true;
                _inputManager.ChangeReaction(r);
            }else if (notification.Answer != null)
            {
                Answer a = notification.Answer;
                a.Deleted = true;
                _inputManager.ChangeAwnser(a);
            }else if (notification.Idea != null)
            {
                Idea i = notification.Idea;
                i.Deleted = true;
                _inputManager.ChangeIdea(i);
            }
            notification.Moderate(moderation);
            _mgr.ChangeNotification(notification);
        }

        [Authorize(Roles = "Moderator")]
        [Route("/Moderator/Block/{userId}/{nid}")]
        public void Block(string userId, string nid)
        {
            var notification = _mgr.GetNotification(nid);
            var moderation = new Moderation(ModerationAction.BLOCK, notification);
            var user = _usrMgr.GetUser(userId);
            user.Blocked = true;
            _usrMgr.ChangeUser(user);
            notification.Moderate(moderation);
            _mgr.ChangeNotification(notification);
            _mgr.LoopBlocked(user);
        }
    }
}