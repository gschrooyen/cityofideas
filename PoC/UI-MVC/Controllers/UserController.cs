using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PoC.Controllers {
    [Authorize(Policy = "AtLeast18")]
    [Authorize(Roles = "User")]
    [Route("{org}/[controller]")]
    public class UserController {
        
    }
}