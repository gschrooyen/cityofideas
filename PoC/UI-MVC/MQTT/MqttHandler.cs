using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Configuration;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;

namespace PoC.MQTT
{
    public class MqttHandler
    {
        public MqttHandler(IConfiguration configuration)
        {
            Console.WriteLine("MQTT starting");
            
            var opts = new MqttClientOptionsBuilder()
                .WithTcpServer(configuration.GetSection("ips").GetSection("mosquitto").Value) // Port is optional
                .Build();

            var client = new MqttFactory().CreateMqttClient();
            client.ConnectAsync(opts, CancellationToken.None);

            client.UseConnectedHandler(async e =>
            {
                Console.WriteLine("### CONNECTED WITH SERVER ###");

                // Subscribe to a topic
                await client.SubscribeAsync(new TopicFilterBuilder().WithTopic("IOT").Build());

                Console.WriteLine("### SUBSCRIBED ###");
            });
            
            client.UseApplicationMessageReceivedHandler(e =>
            {
                HttpClient http = new HttpClient();
                Console.WriteLine(e.ApplicationMessage.Payload);
                http.GetAsync(new Uri(configuration.GetSection("ips").GetSection("host").Value + "/IoT/addvote/" + Encoding.UTF8.GetString(e.ApplicationMessage.Payload)));
                
            });
        }
    }
}