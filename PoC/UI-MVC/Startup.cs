﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PoC.Areas.Identity.Data;
using PoC.Areas.Identity.Services;
using PoC.BL.manager.input;
using PoC.BL.manager.iot;
using PoC.BL.manager.InputMethod;
using PoC.BL.manager.moderation;
using PoC.BL.manager.organisation;
using PoC.BL.manager.project;
using PoC.BL.manager.user;
using PoC.DAL.EF;
using PoC.DAL.repo.inputMethod;
using PoC.DAL.repo.iot;
using PoC.DAL.repo.Input;
using PoC.DAL.repo.moderation;
using PoC.DAL.repo.organisation;
using PoC.DAL.repo.project;
using PoC.DAL.repo.user;
using PoC.Models;
using PoC.Models.Services;
using PoC.MQTT;


namespace PoC {
    public class Startup
    {
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.Configure<CookiePolicyOptions>(options => {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<PoCIdentityContext>();
            services.AddDefaultIdentity<PoCIdentityUser>(config => {
                    //add service for confirmation email
                    config.SignIn.RequireConfirmedEmail = false;   
                })
                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<PoCIdentityContext>();

            //Identity password settings.
            services.Configure<IdentityOptions>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequiredLength = 3;
                options.Password.RequiredUniqueChars = 1;
            });

            //setting up different authentications (chaining)
            services.AddAuthentication()
                .AddFacebook(facebookOptions => {
                    //should be saved differently in configuration. See documentation.
                    //facebook authentication
                    facebookOptions.AppId = "2362587690694698";
                    facebookOptions.AppSecret = "9cdbdcd479098bee909587cd472cc9e7";
                    facebookOptions.ClaimActions.MapJsonKey(ClaimTypes.Gender, "gender");
                    facebookOptions.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                    facebookOptions.ClaimActions.MapJsonKey(ClaimTypes.Surname, "surname");
                    facebookOptions.ClaimActions.MapJsonKey(ClaimTypes.PostalCode, "postalcode");
                    facebookOptions.ClaimActions.MapJsonKey(ClaimTypes.DateOfBirth, "date_birth");
                    facebookOptions.SaveTokens = true;
                    facebookOptions.Events.OnCreatingTicket = ctx => 
                    {
                        if (!(ctx.Properties.GetTokens() is List<AuthenticationToken> tokens))
                            return Task.CompletedTask;
                        tokens.Add(new AuthenticationToken() {
                            Name = "TicketCreated",
                            Value = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
                        });
                        ctx.Properties.StoreTokens(tokens);

                        return Task.CompletedTask;
                    };
                })
                
                .AddGoogle(googleOptions => {
                    //google authentication
                    googleOptions.ClientId = "673076903188-27rh2imoo5s7csdek5u643h12m4fgd8d.apps.googleusercontent.com";
                    googleOptions.ClientSecret = "gT8HohmPs-fEyZtxzodxU1TC";
                    googleOptions.Scope.Add("https://www.googleapis.com/auth/plus.login");
                    googleOptions.ClaimActions.MapJsonKey(ClaimTypes.Gender, "gender");
                    googleOptions.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                    googleOptions.ClaimActions.MapJsonKey(ClaimTypes.Surname, "surname");
                    googleOptions.ClaimActions.MapJsonKey(ClaimTypes.PostalCode, "postalcode");
                    googleOptions.ClaimActions.MapJsonKey(ClaimTypes.DateOfBirth, "date_birth");
                    googleOptions.SaveTokens = true;
                    googleOptions.Events.OnCreatingTicket = ctx => 
                    {
                        if (!(ctx.Properties.GetTokens() is List<AuthenticationToken> tokens))
                            return Task.CompletedTask;
                        tokens.Add(new AuthenticationToken() {
                            Name = "TicketCreated",
                            Value = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)
                        });
                        ctx.Properties.StoreTokens(tokens);

                        return Task.CompletedTask;
                    };
                });
            
            //adding email verification
            services.AddTransient<IEmailSender, EmailSender>();
            services.Configure<AuthMessageSenderOptions>(Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddMvc().AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            
            //adding authorisation claims
            services.AddAuthorization(options => {
                options.AddPolicy("User", policy => policy.RequireClaim("UserID"));
                options.AddPolicy("Admin", policy => policy.RequireRole("Admin"));
                options.AddPolicy("Moderator", policy => policy.RequireClaim("ModeratorID"));
                options.AddPolicy("Superadmin", policy => policy.RequireClaim("SuperadminID"));
                
                //add authorisation policies:
                options.AddPolicy("AtLeast18", policy => policy.Requirements.Add(new MinimumAgeRequirement(18)));
            });

            services.AddDbContext<PoCDbContext>();
            services.AddScoped<IInputMethodManager, InputMethodManager>();
            services.AddScoped<IInputMethodRepository, InputMethodRepository>();
            services.AddScoped<IModerationManager, ModerationManager>();
            services.AddScoped<IModerationRepository, ModerationRepository>();
            services.AddScoped<IProjectManager, ProjectManager>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IInputManager, InputManager>();
            services.AddScoped<IInputRepository, InputRepository>();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IOrganisationManager, OrganisationManager>();
            services.AddScoped<IOrganisationRepository, OrganisationRepository>();
            services.AddScoped<IIotManager, IotManager>();
            services.AddScoped<IIotRepository, IotRepository>();
            services.AddScoped<MqttHandler>();
            //ANDROID AUTHENTICATION
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(/*x =>
                {   x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }*/)
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddScoped<IUserService, UserService>();
            //ANDROID AUTHENTICATION
            
            
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider services) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("error/404");
                app.UseStatusCodePagesWithRedirects("error/404");
                app.UseHsts();
            }

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                PoCDbInitializer.Initialize(serviceScope.ServiceProvider.GetRequiredService<PoCDbContext>());
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
          

            app.UseAuthentication();


            app.Use(async (httpContext, next) =>
            {
                await next();
                if (httpContext.Response.StatusCode == 404 && !httpContext.Response.HasStarted)
                {
                    httpContext.Request.Path = "/error/404";
                    await next();
                }
            });
            
            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

           CreateUserRoles(services).Wait();
           
           //ANDROID AUTHENTICATION
           app.UseCors(x => x
               .AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader());
        }
        
        private async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<PoCIdentityUser>>();
            var realUserManager = serviceProvider.GetRequiredService<IUserManager>();
    
            //Adding Admin Role
            var roleCheck = await roleManager.RoleExistsAsync("Admin");
            var roleCheck1 = await roleManager.RoleExistsAsync("User");
            var roleCheck2 = await roleManager.RoleExistsAsync("Moderator");
            var roleCheck3 = await roleManager.RoleExistsAsync("SuperAdmin");

            //create the roles and seed them to the database
            if (!roleCheck)
            {
                await roleManager.CreateAsync(new IdentityRole("Admin"));
            }
            if (!roleCheck1) {
                await roleManager.CreateAsync(new IdentityRole("User"));
            }
            if (!roleCheck2) {
                await roleManager.CreateAsync(new IdentityRole("Moderator"));
            }

            if (!roleCheck3) {
                await roleManager.CreateAsync(new IdentityRole("SuperAdmin"));
            }
            
            //fetch users from regular database to add to identity database

            if (userManager.FindByEmailAsync("ruben.vanloo@student.kdg.be").Result == null) {
                var user1 = realUserManager.GetUserByEmail("ruben.vanloo@student.kdg.be");
            
                var tempUser1 = new PoCIdentityUser{ UserName = user1.Email, Email = user1.Email, Id = user1.UserId};
                await userManager.CreateAsync(tempUser1, user1.PasswordHash);
                await userManager.AddToRoleAsync(tempUser1, "User");
                realUserManager.ChangeUser(user1);
            
                var user = await userManager.FindByEmailAsync("ruben.vanloo@student.kdg.be");
                await userManager.AddToRolesAsync(user, new[] {"Moderator", "Admin", "SuperAdmin"});
            }
            if (userManager.FindByEmailAsync("philippe.colpaert@student.kdg.be").Result == null) {
                var user1 = realUserManager.GetUserByEmail("philippe.colpaert@student.kdg.be");
            
                var tempUser1 = new PoCIdentityUser{ UserName = user1.Email, Email = user1.Email, Id = user1.UserId};
                await userManager.CreateAsync(tempUser1, user1.PasswordHash);
                await userManager.AddToRoleAsync(tempUser1, "User");
                realUserManager.ChangeUser(user1);
            
                var user = await userManager.FindByEmailAsync("philippe.colpaert@student.kdg.be");
                await userManager.AddToRoleAsync(user, "Moderator");
            }
            if (userManager.FindByEmailAsync("eline.meeusen@student.kdg.be").Result == null) {
                var user1 = realUserManager.GetUserByEmail("eline.meeusen@student.kdg.be");
            
                var tempUser1 = new PoCIdentityUser{ UserName = user1.Email, Email = user1.Email, Id = user1.UserId};
                await userManager.CreateAsync(tempUser1, user1.PasswordHash);
                await userManager.AddToRoleAsync(tempUser1, "User");
                realUserManager.ChangeUser(user1);
            
                var user = await userManager.FindByEmailAsync("eline.meeusen@student.kdg.be");
                await userManager.AddToRolesAsync(user, new[] {"Moderator", "Admin"});
            }
            if (userManager.FindByEmailAsync("ward.driesen@student.kdg.be").Result == null) {
                var user1 = realUserManager.GetUserByEmail("ward.driesen@student.kdg.be");
            
                var tempUser1 = new PoCIdentityUser{ UserName = user1.Email, Email = user1.Email, Id = user1.UserId};
                await userManager.CreateAsync(tempUser1, user1.PasswordHash);
                await userManager.AddToRoleAsync(tempUser1, "User");
                realUserManager.ChangeUser(user1);
            
                var user = await userManager.FindByEmailAsync("ward.driesen@student.kdg.be");
                await userManager.AddToRolesAsync(user, new[] {"Moderator", "Admin"});
            }
            if (userManager.FindByEmailAsync("glenn.schrooyen@student.kdg.be").Result == null) {
                var user1 = realUserManager.GetUserByEmail("glenn.schrooyen@student.kdg.be");
            
                var tempUser1 = new PoCIdentityUser{ UserName = user1.Email, Email = user1.Email, Id = user1.UserId};
                await userManager.CreateAsync(tempUser1, user1.PasswordHash);
                await userManager.AddToRoleAsync(tempUser1, "User");
                realUserManager.ChangeUser(user1);
            
                var user = await userManager.FindByEmailAsync("glenn.schrooyen@student.kdg.be");
                await userManager.AddToRolesAsync(user, new[] {"Moderator", "Admin", "SuperAdmin"});
            }
            if (userManager.FindByEmailAsync("yohann.vetters@student.kdg.be").Result == null) {
                var user1 = realUserManager.GetUserByEmail("yohann.vetters@student.kdg.be");
            
                var tempUser1 = new PoCIdentityUser{ UserName = user1.Email, Email = user1.Email, Id = user1.UserId};
                await userManager.CreateAsync(tempUser1, user1.PasswordHash);
                await userManager.AddToRoleAsync(tempUser1, "User");
                realUserManager.ChangeUser(user1);
            
                var user = await userManager.FindByEmailAsync("yohann.vetters@student.kdg.be");
                await userManager.AddToRoleAsync(user, "User");
            }
            
           
            serviceProvider.GetRequiredService<MqttHandler>();
        }
    }
}