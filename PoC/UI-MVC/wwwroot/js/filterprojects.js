

document.querySelectorAll(".cat").forEach(function (elem) {
    elem.addEventListener('click', function (e) {
        filter(e.srcElement.innerHTML)
    })
});

function filter(categorie) {
    if (categorie === "Alles weergeven") {
        document.querySelectorAll(".proj").forEach(function (elem) {
            elem.hidden = false;
        });
    }else {
        document.querySelectorAll(".proj").forEach(function (elem) {
            elem.hidden = true;
        });

        document.querySelectorAll("." + categorie.toUpperCase()).forEach(function (elem) {
            elem.hidden = false;
        });
    }
}