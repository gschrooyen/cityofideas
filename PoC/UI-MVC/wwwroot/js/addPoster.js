let projectselect = document.getElementById("projectselect");
projectselect.addEventListener('change', function (e) {
    setselector();
});
setselector();


let buttoncounter = document.getElementById("buttonsdiv").childElementCount;
function addbutton() {
    let buttonsdiv = document.getElementById("buttonsdiv");
    let button = "<div><input type='text' name='button"+buttoncounter+"' class='form-control col-3' placeholder='knop nummer'/> <button id='deletebutton"+buttoncounter+"' class='btn btn-danger'>X</button><select class='form-control col-4' name='button"+buttoncounter+"selector' id='select"+buttoncounter+"'></select></div>";
    buttonsdiv.insertAdjacentHTML("beforeend", button);
    document.getElementById("deletebutton"+buttoncounter).addEventListener('click', function (e) {
        e.srcElement.parentElement.remove();
        document.getElementById("numberOfButtons").value -= 1;
    });
    let buttonselector = document.getElementById("select"+buttoncounter);
    if (document.getElementById("inputmethodselect").value === "questionlist" && document.getElementById("questiondiv").innerHTML !== "") {
        fetch("../api/questionlists/getquestion/" + document.getElementById("selectquestion").value).then(function (response) {
            if (response.status === 200) {
                return response.json();
            }else{
                return []
            }
        }).then(function (result) {
            result.options.forEach(function (elem) {
                buttonselector.innerHTML += "<option value='"+elem.id+"'>"+elem.title+"</option>"
            })
        })
    }else{
        fetch("../api/ideations/getIdeasFromIdeation/"+document.getElementById("ideationselect").value).then(function (response) {
            if (response.status === 200) {
                return response.json()
            }else{
                return []
            }
        }).then(function (result) {
            if (result.length !== 0){
                result.forEach(function (elem) {
                    buttonselector.innerHTML += "<option value='"+elem.id+"'>"+elem.title+"</option>"
                })
            } 
        })
    }
    buttoncounter++;
    document.getElementById("numberOfButtons").value -= -1;
}

function changemethod() {
    fetch("../api/projects/getproject/"+projectselect.value).then(function (response) {
        if (response.status === 200) {
            return response.json();
        }else{
            return []
        }
    }).then(function (result) {
        let methodselector = document.getElementById("inputmethodselect");
        let inputmethoddiv = document.getElementById("inputmethoddiv");
        if (methodselector.value === "questionlist") {
            let questionlists = result.currentFase.inputMethods.filter(r => r.title != null);
            inputmethoddiv.innerHTML = "<select id='questionlistSelect' name='questionlistSelect' class='form-control'></select>";
            let selectqlist = document.getElementById("questionlistSelect");
            questionlists.forEach(function (elem) {
                selectqlist.innerHTML += "<option value='" + elem.id + "'>" + elem.title + "</option>"
            })
        } else {
            let ideations = result.currentFase.inputMethods.filter(r => r.title == null);
            inputmethoddiv.innerHTML = "<select id='ideationselect' name='ideationselect' class='form-control'></select>";
            let selectideation = document.getElementById("ideationselect");
            ideations.forEach(function (elem) {
                selectideation.innerHTML += "<option value='"+elem.id+"'>"+elem.titleIdeation+"</option>"
            });
            let questiondiv = document.getElementById("questiondiv");
            questiondiv.innerHTML = "";
        }
        let buttonsdiv = document.getElementById("buttonsdiv");
        let buttondiv = document.getElementById("buttondiv");
        buttonsdiv.innerHTML = "";
        buttondiv.innerHTML = "<button id='addbutton' type='button' class='btn btn-primary'>add button</button>";
        document.getElementById("addbutton").addEventListener('click', function (e) {
            addbutton()
        });
    })
}

function setselector(){
    let selectordiv = document.getElementById("inputmethodselector");
    let projectselect = document.getElementById("projectselect");
    fetch("../api/projects/getproject/"+projectselect.value).then(function (response) {
        if (response.status === 200) {
            return response.json();
        }else{
            return [];
        }
    }).then(function (result) {
        if (result.currentFase.inputMethods.length > 0){
            let inputmethods = result.currentFase.inputMethods;
            console.log(inputmethods);
            let methods = [];
            inputmethods.forEach(function (elem) {
                if (elem.title != null){
                    methods.push("questionlist")
                } else {
                    methods.push("ideation")
                }
                
                
            });
            let unique = methods.filter((v,i,a)=>a.indexOf(v)===i);
            if (unique.length > 0) {
                selectordiv.innerHTML = "<select id='inputmethodselect' class='form-control'></select>";
                unique.forEach(function (elem) {
                    document.getElementById("inputmethodselect").innerHTML += "<option value="+elem+">"+elem+"</option>"
                })
            }
            let methodselector = document.getElementById("inputmethodselect");
            let inputmethoddiv = document.getElementById("inputmethoddiv");
            if (methodselector.value === "questionlist"){
                let questionlists = result.currentFase.inputMethods.filter(r => r.title != null);
                inputmethoddiv.innerHTML ="<select id='questionlistSelect' name='questionlistSelect' class='form-control'></select>";
                let selectqlist = document.getElementById("questionlistSelect");
                questionlists.forEach(function (elem) {
                    selectqlist.innerHTML += "<option value='"+ elem.id+"'>"+elem.title+"</option>"
                });
                fetch("../api/questionlists/getQuestionsOfList/"+questionlists[0].id).then(function (response) {
                    if (response.status === 200) {
                        return response.json();
                    }else {
                        console.error("status: "+ response.status);
                        return [];
                    }
                }).then(function (result) {
                    let questiondiv = document.getElementById("questiondiv");
                    
                    // noinspection EqualityComparisonWithCoercionJS
                    let choise = result.filter(q => q.choiseQuestionType != null && q.choiseQuestionType != 1);
                    if (choise.length !== 0) {
                        questiondiv.innerHTML = "<select id='selectquestion' name='selectquestion' class='form-control'></select>"; 
                        document.getElementById("selectquestion").addEventListener('change', function (e) {
                            document.getElementById("buttonsdiv").innerHTML = "";
                        })
                    }else{
                        questiondiv .innerHTML = "";
                    }
                    console.log(choise);
                    choise.forEach(function (elem) {
                        console.log(elem)
                        document.getElementById("selectquestion").innerHTML += "<option value='"+elem.questionId+"'>"+elem.title+"</option>"
                    })
                })
            } else{
                let ideations = result.currentFase.inputMethods.filter(r => r.title == null);
                inputmethoddiv.innerHTML = "<select id='ideationselect' name='ideationselect' class='form-control'></select>";
                let selectideation = document.getElementById("ideationselect");
                let questiondiv = document.getElementById("questiondiv");
                questiondiv.innerHTML = "";
                ideations.forEach(function (elem) {
                    selectideation.innerHTML += "<option value='"+elem.id+"'>"+elem.titleIdeation+"</option>"
                })
            }
            
            let buttondiv = document.getElementById("buttondiv");
            let buttonsdiv = document.getElementById("buttonsdiv");
            buttonsdiv.innerHTML ="";
            buttondiv.innerHTML = "<button id='addbutton' type='button' class='btn btn-primary'>add button</button>";
            document.getElementById("addbutton").addEventListener('click', function (e) {
                addbutton();
            });

            methodselector.addEventListener('change', function (e) {
                changemethod();
            })
        } 
    });
}