import * as ajaxCalls from './ajax';

export function getUserHtml(user){
    let temp = "<td uid=\"" + user.userId + "\">\n" +
        "<button type=\"button\" class=\"btn btn-warning usrpromote\">Maak moderator</button>\n" +
        "<button type=\"button\" class=\"btn btn-danger usrdel\">Verwijder gebruiker</button>\n";
    if(!user.blocked){
        temp += "\n<button id=\"unblocked_" + user.userId + "\" type=\"button\" class=\"btn btn-warning usrBlock\">Blokkeer gebruiker</button>\n";
    } else {
        temp += "<button id=\"blocked_" + user.userId + "\" type=\"button\" class=\"btn btn-warning usrUnblock\">Deblokkeer gebruiker</button>\n";
    }
    temp += "</td>";
    return temp;
}

export function autofillsearch(imp, met) {
    fetch("/api/organisations/getAllOrganisations").then(function (response) {
        if (response.ok) {
            return response.json()

        }
    }).then(function (result) {
        let names = result.map(o => {
            return o.name + " - " + o.zipCode
        });
        met(imp, names)
    })
}


export function promoteUser(uid) {
    fetch("../superAdmin/PromoteAdmin/" + uid).then(function (response) {
        if (response.ok) {
            document.getElementById(uid).hidden = true;
        }
    });
}

export function demoteAdmin(uid) {
    fetch("../superAdmin/demoteAdmin/" + uid).then(function (response) {
        if (response.ok) {
            document.getElementById(uid).hidden = true;
        }
    });
}

export function block(uid) {
    fetch("../admin/block/" + uid).then(function (response) {
        if (response.ok) {
            document.getElementById("input_" + uid).checked = true;
            document.getElementById("unblocked_" + uid).disabled = true;
        }
    })
}

export function unblock(uid) {
    fetch("../admin/unblock/" + uid).then(function (response) {
        if (response.ok) {
            document.getElementById("input_" + uid).checked = false;
            document.getElementById("blocked_" + uid).disabled = true;
        }
    })
}

export function promote(url, uid) {
    fetch(url + uid).then(function (response) {
        if (response.ok) {
            document.getElementById(uid).hidden = true;
        }
    })
}

export function deleteUser(uid) {
    fetch("../admin/removeUser/" + uid).then(function (response) {
        if (response.ok) {
            document.getElementById(uid).hidden = true;
        }
    });
}

export function demoteMod(uid) {
    fetch("../admin/demoteMod/" + uid).then(function (response) {
        if (response.ok) {
            document.getElementById(uid).hidden = true;
        }
    });
}

export function promoteMod(uid) {
    fetch("../admin/promoteMod/" + uid).then(function (response) {
        if (response.ok) {
            document.getElementById(uid).hidden = true;
        }
    })
}

export function searchMods(org, query, url) {
    let tbody = document.getElementById("tableContentBody");
    fetch(url).then(function (response) {
        if (response.ok) {
            return response.json()
        }
    }).then(function (result) {

            let foo = result.filter(u => checkSearch(u, query));

            tbody.innerHTML = "";
            foo.forEach(function (user) {
                    console.log(user);
                    let verified = "<input type='checkbox' disabled/>";
                    let blocked = "<input id='input_" + user.userId + "' type='checkbox' disabled/>";
                    let blockedbtn = "<button id='unblocked_" + user.userId + "' type='button' class='btn btn-warning usrblock'>Blokkeer gebruiker</button>";
                    if (user.verified) {
                        verified = "<input type='checkbox' checked disabled/>"
                    }
                    if (user.blocked) {
                        blocked = "<input id='input_" + user.userId + "' type='checkbox' disabled checked/>"
                        blockedbtn = "<button id='unblocked_" + user.userId + "' type='button' class='btn btn-warning usrUnblock'>Deblokkeer gebruiker</button>"
                    }
                    let temp = "<tr id='" + user.userId + "'>" +
                        "<td>" + user.userName + "</td>" +
                        "<td>" + user.firstName + " " + user.lastName + "</td>" +
                        "<td>" + user.email + "</td>" +
                        "<td>" +
                        verified +
                        "</td>" +
                        "<td>" +
                        blocked +
                        "</td>" +
                        getUserHtml(user) +
                        "</tr>";
                    tbody.insertAdjacentHTML("beforeend", temp);

                }
            );
            const deluser = document.querySelectorAll(".usrdel");
            deluser.forEach(function (elem) {
                elem.addEventListener('click', function (e) {
                    e.preventDefault();
                    if (confirm("ben je zeker dat je deze gebruiker wil verwijderen?")) {
                        ajaxCalls.deleteUser(e.srcElement.parentElement.getAttribute("uid"))
                    }
                })
            });
            const demoteMod = document.querySelectorAll(".moddemote");
            demoteMod.forEach(function (elem) {
                elem.addEventListener('click', function (e) {
                    e.preventDefault();
                    if (confirm("ben je zeker dat je de privileges van deze persoon wil afnemen?")) {
                        ajaxCalls.demoteMod(e.srcElement.parentElement.getAttribute("uid"));
                    }
                })
            });

            const usrPromote = document.querySelectorAll(".usrpromote");
            usrPromote.forEach(function (elem) {
                elem.addEventListener('click', function (e) {
                    e.preventDefault();
                    if (confirm("ben je zeker dat je deze gebruike wil promoverren naar moderator?")) {
                        ajaxCalls.promote("../admin/promoteToMod/", e.srcElement.parentElement.getAttribute("uid"))
                    }
                })
            });

            const usrBlock = document.querySelectorAll(".usrBlock");
            usrBlock.forEach(function (elem) {
                elem.addEventListener('click', function (e) {
                    e.preventDefault();
                    if (confirm("ben je zeker dat je deze gebruiker wil blokkeren?")) {
                        ajaxCalls.block(e.srcElement.parentElement.getAttribute("uid"));
                    }
                })
            });

            const usrUNblock = document.querySelectorAll(".usrUnblock");
            usrUNblock.forEach(function (elem) {
                elem.addEventListener('click', function (e) {
                    e.preventDefault();
                    if (confirm("ben je zeker dat je deze gebruiker wil deblokkeren?")) {
                        ajaxCalls.unblock(e.srcElement.parentElement.getAttribute("uid"))
                    }
                })
            });


        }
    )
}

function checkSearch(user, query) {
    if (user.firstName.includes(query)
        || user.lastName.includes(query)
        || user.email.includes(query)
        || user.userName.includes(query)
        || (user.firstName + " " + user.lastName).includes(query)
        || (user.lastName + " " + user.firstName).includes(query)) {
        return true;
    }
    if (query === "verified") {
        if (user.verified) {
            return true
        }
    }
    if (query === "blocked") {
        if (user.blocked) {
            return true
        }
    }
    if (query === "!verified") {
        if (!user.verified) {
            return true
        }
    }
    if (query === "!blocked") {
        if (!user.blocked) {
            return true
        }
    }
    return false
}


export function blockUser(userId, nid) {

    fetch("/Moderator/Block/" + userId + "/" + nid).then(
        data =>
            document.getElementById(nid).hidden = true
    )
        .catch(error => {
            console.log(error);
            alert("Er ging iets mis")
        });

}

export function deleteComment(nid) {
    fetch("/Moderator/Delete/" + nid).then(
        data =>
            document.getElementById(nid).hidden = true
    )
        .catch(error => {
            console.log(error);
            alert("Er ging iets mis")
        });

}

export function IgnoreNotification(nid) {
    fetch("/Moderator/ignore/" + nid);
    document.getElementById(nid).hidden = true;
}

export function showEditQuestionsDiv() {
    let divEditQuestions = document.getElementById('divEditQuestions');
    if (divEditQuestions.style.display === "none") {
        divEditQuestions.style.display = "";
    } else {
        divEditQuestions.style.display = "none";
    }
}

export function changeSelectQuestion() {
    if (document.getElementById('selectQuestionType') != null) {
        let selectQuestionType = document.getElementById('selectQuestionType').value;
        let divAddChoiceQuestion = document.getElementById('divAddChoiceQuestion');
        let divAddOpenQuestion = document.getElementById('divAddOpenQuestion');

        if (selectQuestionType === "Keuzevraag" && divAddChoiceQuestion.style.display === "none") {
            divAddChoiceQuestion.style.display = "";
            divAddOpenQuestion.style.display = "none";
        }

        if (selectQuestionType === "Openvraag" && divAddOpenQuestion.style.display === "none") {
            divAddOpenQuestion.style.display = "";
            divAddChoiceQuestion.style.display = "none";
        }


    }
}

export function showAddQuestionDiv() {
    if (document.getElementById('selectQuestionType') != null) {
        let divQuestionType = document.getElementById('divQuestionType');
        let selectQuestionType = document.getElementById('selectQuestionType').value;

        let divAddChoiceQuestion = document.getElementById('divAddChoiceQuestion');
        let divAddOpenQuestion = document.getElementById('divAddOpenQuestion');
        if (divQuestionType.style.display === "none") {
            divQuestionType.style.display = "";
        } else {
            divQuestionType.style.display = "none";
        }

        if (selectQuestionType === "Keuzevraag" && divAddChoiceQuestion.style.display === "none") {
            divAddChoiceQuestion.style.display = "";
        } else {
            divAddChoiceQuestion.style.display = "none";
        }

        if (selectQuestionType === "Openvraag" && divAddOpenQuestion.style.display === "none") {
            divAddOpenQuestion.style.display = "";
        } else {
            divAddOpenQuestion.style.display = "none";
        }

    }
}

export function removeChoiceQuestion(choiceQuestionId) {
    let confirmBox = confirm("Bent u zeker dat u deze keuzevraag wil verwijderen?");

    if (confirmBox === true) {
        fetch("./removeChoiceQuestion/" + choiceQuestionId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);
    }
}

export function removeOpenQuestion(openQuestionId) {
    let confirmBox = confirm("Bent u zeker dat u deze openvraag wil verwijderen?");

    if (confirmBox === true) {
        fetch("./removeOpenQuestion/" + openQuestionId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);
    }
}

export function changeStateIdeation(ideationId) {

    let confirmBox = confirm("Bent u zeker dat u deze Ideation wil afsluiten/heropstarten?");
    if (confirmBox === true) {
        fetch("./DeleteIdeation/" + ideationId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);

    }
}

export function validateTitleChoiceQuestion() {
    let titleChoiceQuestion = document.getElementById('titleChoiceQuestion').value;
    let titleChoiceQuestionWarning = document.getElementById('titleChoiceQuestionValidation');

    if (titleChoiceQuestion.length > 100 || titleChoiceQuestion === "") {
        titleChoiceQuestionWarning.style.display = "";
    }
    else {
        titleChoiceQuestionWarning.style.display = "none";
    }
}

export function validateRangChoiceQuestion() {
    let rangChoiceQuestion = document.getElementById('rangChoiceQuestion').value;
    let rangChoiceQuestionWarning = document.getElementById('rangChoiceQuestionValidation');

    if (rangChoiceQuestion <= 0) {
        rangChoiceQuestionWarning.style.display = "";
    }
    else {
        rangChoiceQuestionWarning.style.display = "none";
    }
}

export function validateOptionsChoiceQuestion() {
    let optionOneChoiceQuestion = document.getElementById('options[0]').value;
    let optionTwoChoiceQuestion = document.getElementById('options[1]').value;
    let optionsChoiceQuestionWarning = document.getElementById('optionsChoiceQuestionValidation');

    if (optionOneChoiceQuestion === "" || optionTwoChoiceQuestion === "") {
        optionsChoiceQuestionWarning.style.display = "";
    }
    else {
        optionsChoiceQuestionWarning.style.display = "none";
    }
}

export function checkAllChoiceQuestionWarnings() {
    let titleChoiceQuestionWarning = document.getElementById('titleChoiceQuestionValidation');
    let rangChoiceQuestionWarning = document.getElementById('rangChoiceQuestionValidation');
    let optionsChoiceQuestionWarning = document.getElementById('optionsChoiceQuestionValidation');
    let formAddChoiceQuestion = document.getElementById('formAddChoiceQuestion');

    if (titleChoiceQuestionWarning.style.display === "" || rangChoiceQuestionWarning.style.display === "" || optionsChoiceQuestionWarning.style.display === "") {
        formAddChoiceQuestion.onsubmit = function () {
            return false;
        }
    }
    if (titleChoiceQuestionWarning.style.display === "none" && rangChoiceQuestionWarning.style.display === "none" && optionsChoiceQuestionWarning.style.display === "none") {
        formAddChoiceQuestion.onsubmit = function () {
            return true;
        }
    }
}

export function validateTitleOpenQuestion() {
    let titleOpenQuestion = document.getElementById('titleOpenQuestion').value;
    let titleOpenQuestionWarning = document.getElementById('titleOpenQuestionValidation');

    if (titleOpenQuestion.length > 100 || titleOpenQuestion === "") {
        titleOpenQuestionWarning.style.display = "";
    }
    else {
        titleOpenQuestionWarning.style.display = "none";
    }
}

export function validateRangOpenQuestion() {
    let rangOpenQuestion = document.getElementById('rangOpenQuestion').value;
    let rangOpenQuestionWarning = document.getElementById('rangOpenQuestionValidation');
    if (rangOpenQuestion <= 0) {
        rangOpenQuestionWarning.style.display = "";
    }
    else {
        rangOpenQuestionWarning.style.display = "none";
    }
}

export function checkAllOpenQuestionWarnings() {
    let titleOpenQuestionWarning = document.getElementById('titleOpenQuestionValidation');
    let rangOpenQuestionWarning = document.getElementById('rangOpenQuestionValidation');
    let formAddOpenQuestion = document.getElementById('formAddOpenQuestion');

    if (titleOpenQuestionWarning.style.display === "" || rangOpenQuestionWarning.style.display === "") {
        formAddOpenQuestion.onsubmit = function () {
            return false;
        }
    }
    if (titleOpenQuestionWarning.style.display === "none" && rangOpenQuestionWarning.style.display === "none") {
        formAddOpenQuestion.onsubmit = function () {
            return true;
        }
    }
}

export function showCreateIdeaForm() {
    let divCreateIdea = document.getElementById('divCreateIdea');
    if (divCreateIdea.style.display === "none") {
        divCreateIdea.style.display = "";
    } else {
        divCreateIdea.style.display = "none";
    }
}

function checkElement(elem) {
    switch (elem.style.display) {
        case "":
            return false;
        case "none":
            return true;
    }

    return false;
}

export function checkAllIdeaWarnings() {
    let titleIdeaWarning = document.getElementById('titleIdeaValidation');
    let descriptionIdeaWarning = document.getElementById('descriptionIdeaValidation');
    let imageWarning = document.getElementById('imageIdeaValidation');
    let imageNumberWarning = document.getElementById('imageNumberValidation');
    let videoWarning = document.getElementById('videoIdeaValidation');
    let videoNumberWarning = document.getElementById('videoNumberValidation');
    let videoLinkWarning = document.getElementById('videoLinkIdeaValidation');
    let formCreateIdea = document.getElementById('formCreateIdea');

    if (imageWarning == null && videoWarning == null && videoLinkWarning == null) {
        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }

    if (imageWarning != null && videoWarning == null && videoLinkWarning == null) {

        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "" || imageWarning.style.display === "" || imageNumberWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none" && imageWarning.style.display === "none" && imageNumberWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }

    if (imageWarning == null && videoWarning != null && videoLinkWarning == null) {

        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "" || videoWarning.style.display === "" || videoNumberWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none" && videoWarning.style.display === "none" && videoNumberWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }

    if (imageWarning == null && videoWarning == null && videoLinkWarning != null) {

        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "" || videoLinkWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none" && videoLinkWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }

    if (imageWarning == null && videoWarning != null && videoLinkWarning != null) {
        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "" || videoWarning.style.display === "" || videoNumberWarning.style.display === "" || videoLinkWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none" && videoWarning.style.display === "none" && videoNumberWarning.style.display === "none" && videoLinkWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }
    if (imageWarning != null && videoWarning == null && videoLinkWarning != null) {
        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "" || imageWarning.style.display === "" || imageNumberWarning.style.display === "" || videoLinkWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none" && imageWarning.style.display === "none" && imageNumberWarning.style.display === "none" && videoLinkWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }

    if (imageWarning != null && videoWarning != null && videoLinkWarning == null) {
        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "" || imageWarning.style.display === "" || imageNumberWarning.style.display === "" || videoWarning.style.display === "" || videoNumberWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none" && imageWarning.style.display === "none" && imageNumberWarning.style.display === "none" && videoWarning.style.display === "none" && videoNumberWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }

    if (imageWarning != null && videoWarning != null && videoLinkWarning != null) {
        if (titleIdeaWarning.style.display === "" || descriptionIdeaWarning.style.display === "" || imageWarning.style.display === "" || imageNumberWarning.style.display === "" || videoWarning.style.display === "" || videoNumberWarning.style.display === "" || videoLinkWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none" && descriptionIdeaWarning.style.display === "none" && imageWarning.style.display === "none" && imageNumberWarning.style.display === "none" && videoWarning.style.display === "none" && videoNumberWarning.style.display === "none" && videoLinkWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }
}

export function warnVideoLinkIdea() {
    if (document.getElementById('videoLinkIdeaValidation') != null) {
        let videoLinkWarning = document.getElementById('videoLinkIdeaValidation');
        let formCreateIdea = document.getElementById('formCreateIdea');
        if (videoLinkWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (videoLinkWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }
}

export function validateVideoLinkIdea() {
    if (document.getElementById('videoLinkIdea') != null) {
        let videoLink = document.getElementById('videoLinkIdea').value;
        let videoLinkWarning = document.getElementById('videoLinkIdeaValidation');
        let videoLinkRequired = document.getElementById('videoLinkRequired').value;
        if (videoLinkRequired === "value") {
            if (!(videoLink.includes('https://www.youtube') && videoLink.includes('=') && videoLink.includes('watch')) || videoLink === "") {
                videoLinkWarning.style.display = "";
            } else {
                videoLinkWarning.style.display = "none";
            }
        }
        if (videoLinkRequired === "") {
            if (!(videoLink.includes('https://www.youtube') && videoLink.includes('=') && videoLink.includes('watch') || videoLink === "")) {
                videoLinkWarning.style.display = "";
            } else {
                videoLinkWarning.style.display = "none";
            }
        }
    }
}

export function warnVideoIdea() {
    if (document.getElementById('videoIdeaValidation') != null) {
        let videoWarning = document.getElementById('videoIdeaValidation');
        let formCreateIdea = document.getElementById('formCreateIdea');
        if (videoWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (videoWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }
}


export function validateFileSize(fileValue) {
    let i;
    let totalSize = 0;
    if (document.getElementById('imageIdea') != null) {
        let image = document.getElementById('imageIdea');
        for (i = 0; i < image.files.length; i++) {
            totalSize += image.files.length;
        }
    }

    if (document.getElementById('videoIdea') != null) {
        let video = document.getElementById('videoIdea');
        for (i = 0; i < video.files.length; i++) {
            totalSize += video.files[i].size;
        }
    }
    if (totalSize >= 29989273.6) {
        alert("Maximale grootte van alle bestanden in totaal is 30MB");
        fileValue.value = "";
    }
}

export function validateVideoIdea() {
    if (document.getElementById('videoIdea') != null) {
        let video = document.getElementById('videoIdea');
        let videoValue = document.getElementById('videoIdea').value;
        let videoWarning = document.getElementById('videoIdeaValidation');
        let videoNumberWarning = document.getElementById('videoNumberValidation');
        let videoRequired = document.getElementById('videoRequired').value;
        let numberOfVideos = document.getElementById('numberOfVideos').value;
        if (videoRequired === "value") {
            if (!(videoValue.includes('.mp4') || videoValue.includes('.webm') || videoValue.includes('.ogg')) || videoValue === "") {
                videoWarning.style.display = "";
            } else {
                videoWarning.style.display = "none";
            }
        }
        if (videoRequired === "") {
            if (!(videoValue.includes('.mp4') || videoValue.includes('.webm') || videoValue.includes('.ogg') || videoValue === "")) {
                videoWarning.style.display = "";
            } else {
                videoWarning.style.display = "none";
            }
        }
        if (numberOfVideos >= 2) {
            if (video.files.length > numberOfVideos) {
                videoNumberWarning.style.display = "";
            }
            else {
                videoNumberWarning.style.display = "none";
            }
        }
    }
}

export function warnImageIdea() {
    if (document.getElementById('imageIdeaValidation') != null) {
        let imageWarning = document.getElementById('imageIdeaValidation');
        let formCreateIdea = document.getElementById('formCreateIdea');
        if (imageWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (imageWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }
}

export function validateImageIdea() {
    if (document.getElementById('imageIdea') != null) {
        let image = document.getElementById('imageIdea');
        let imageValue = document.getElementById('imageIdea').value;
        let imageWarning = document.getElementById('imageIdeaValidation');
        let imageNumberWarning = document.getElementById('imageNumberValidation');
        let imageRequired = document.getElementById('imageRequired').value;
        let numberOfImages = document.getElementById('numberOfImages').value;
        if (imageRequired === "value") {
            if (!(imageValue.includes('.jpg') || imageValue.includes('.png')) || imageValue === "") {

                imageWarning.style.display = "";
            } else {
                imageWarning.style.display = "none";
            }
        }
        if (imageRequired === "") {
            if (!(imageValue.includes('.jpg') || imageValue.includes('.png') || imageValue === "")) {

                imageWarning.style.display = "";
            } else {
                imageWarning.style.display = "none";
            }
        }
        if (numberOfImages >= 2) {
            if (image.files.length > numberOfImages) {
                imageNumberWarning.style.display = "";
            }
            else {
                imageNumberWarning.style.display = "none";
            }
        }
    }
}

export function warnDescriptionIdea() {
    if (document.getElementById('descriptionIdeaValidation') != null) {
        let descriptionIdeaWarning = document.getElementById('descriptionIdeaValidation');
        let formCreateIdea = document.getElementById('formCreateIdea');
        if (descriptionIdeaWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (descriptionIdeaWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }
}

export function validateDescriptionIdea() {
    let descriptionIdea = document.getElementById('descriptionIdea').value;
    let descriptionIdeaWarning = document.getElementById('descriptionIdeaValidation');

    if (descriptionIdea.length > 1000 || descriptionIdea === "") {
        descriptionIdeaWarning.style.display = "";
    } else {
        descriptionIdeaWarning.style.display = "none";
    }
}

export function warnTitleIdea() {
    if (document.getElementById('titleIdeaValidation') != null) {
        let titleIdeaWarning = document.getElementById('titleIdeaValidation');
        let formCreateIdea = document.getElementById('formCreateIdea');
        if (titleIdeaWarning.style.display === "") {
            formCreateIdea.onsubmit = function () {
                return false;
            }
        }
        if (titleIdeaWarning.style.display === "none") {
            formCreateIdea.onsubmit = function () {
                return true;
            }
        }
    }
}

export function validateTitleIdea() {
    let titleIdea = document.getElementById('titleIdea').value;
    let titleIdeaWarning = document.getElementById('titleIdeaValidation');

    if (titleIdea.length > 50 || titleIdea === "") {
        titleIdeaWarning.style.display = "";
    } else {
        titleIdeaWarning.style.display = "none";
    }
}

export function finishProject(projectId) {
    let confirmBox = confirm("Bent u zeker dat u het project wil afsluiten?");

    if (confirmBox === true) {
        fetch("./DeleteProject/" + projectId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);

    }
}

export function rebootProject(projectId) {
    let confirmBox = confirm("Bent u zeker dat u het project wil heropstarten?");

    if (confirmBox === true) {
        fetch("./DeleteProject/" + projectId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);
    }

}

export function reportComment(commentId) {
    let reason = document.querySelector('.reason_' + commentId);
    let extra = document.querySelector('.remarks_textbox_' + commentId);
    fetch("/Moderator/report?reason=" + reason.value + "&extra=" + extra.value + "&commentId=" + commentId);
    let modal = document.querySelector('#modal_' + commentId);
    reason.value = "language";
    extra.value = "";
}

export function emptyReportBox(commentId) {
    let reason = document.querySelector('.reason_' + commentId);
    let remark = document.querySelector('.remarks_textbox_' + commentId);
    reason.value = "language";
    remark.value = "";
}

export function addShareToIdeation(ideationId) {
    fetch("/Ideation/AddShare?id=" + ideationId);
    document.getElementById("number-shares_" + ideationId).innerHTML -= -1;
}

export function addShareToIdea(ideaid) {
    fetch("/Ideation/addShareToIdea?ideaid=" + ideaid);
    document.getElementById("number-shares_" + ideaid).innerHTML -= -1;


}

export function addLikeToIdea(ideaid, uid) {
    fetch("/Ideation/addLikeToIdea/" + ideaid + "/" + uid);
    setTimeout(function () {
        window.location.reload(false);
    }, 500);
    document.getElementById("number-likes_" + ideaid).innerHTML -= -1;
}

export function removeIdeaIdeation(ideationId, ideaId) {
    let confirmBox = confirm("Bent u zeker dat u dit idee wilt verwijderen?");

    if (confirmBox === true) {
        fetch("./" + ideationId + "/DeleteIdea?ideaId=" + ideaId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);
    }
}

export function addIdeaToIdeation(ideationid) {

    let title = document.getElementById("title_" + ideationid).value;

    let description = document.getElementById("description_" + ideationid).value;

    //let informatie = "Informatie over het idee:";

    if (title.trim() !== "" && description.trim() !== "") {
        document.getElementById("title_" + ideationid).value = "";
        document.getElementById("description_" + ideationid).value = "";
        fetch("/Ideation/AddIdeaToIdeation?title=" + title + "&description=" + description + "&ideationId=" + ideationid);
        setTimeout(function () {
            document.location.reload(true);
        }, 600);
    }

}

export function addReactionToIdea(ideaid, userName) {
    let text = document.getElementById("text_" + ideaid).value;
    document.getElementById("text_" + ideaid).value = "";

    if (text.trim() !== "") {
        fetch("/Ideation/AddReactionToIdea?text=" + text + "&ideaId=" + ideaid);
        document.getElementById("reactions_" + ideaid).innerHTML += "<div class=\"reaction\">\n" +
            "                        <div class=\"card-body\">\n" +
            "                            <p style=\"display : inline\">" + userName +
            "                            </p>\n" +
            "                            <hr/>\n" +
            "                            <p class=\"text-center\">" + text + "</p>\n" +
            "                        </div>\n" +
            "                    </div>\n" +
            "                    </br>"
    }
}

export function addShareToProject(pid) {
    fetch("/projecten/addsharetoproject/" + pid);
    document.querySelector('#number-shares_' + pid).innerHTML -= -1;
    //document.getElementById("share_" + pid).children[0].innerHTML -= -1;
}

export function addLikeToProject(pid, uid) {
    fetch("/projecten/addLikeToProject/" + pid + "/" + uid);
    setTimeout(function () {
        window.location.reload(false);
    }, 500);
    document.querySelector('#number-likes_' + pid).innerHTML -= -1;
    //document.getElementById("like_" + pid).children[0].innerHTML -= -1;

}

//Organisation

export function validateImage(imageValidation, imageValidationWarning) {
    if (imageValidation.style.display !== "none") {
        let image = imageValidation.value;
        let imageWarning = imageValidationWarning;
        if (!(image.includes('.jpg') || image.includes('.jpeg') || image.includes('.png')) || image === "") {

            imageWarning.style.display = "";
        } else {
            imageWarning.style.display = "none";
        }
    }
}

export function warnImage(imageValidationWarning, formSubmit) {
    let imageWarning = imageValidationWarning;
    let form = formSubmit;
    if (imageWarning.style.display === "") {
        form.onsubmit = function () {
            return false;
        }
    }
    if (imageWarning.style.display === "none") {
        form.onsubmit = function () {
            return true;
        }
    }
}

export function showImage(imageToShow, imageValidationToHide) {
    let image = imageToShow;
    if (image.style.display === "none") {
        image.style.display = "";
    }
    else {
        image.style.display = "none";
        imageValidationToHide.style.display = "none";
        image.value = "";
    }
}

export function checkAllOrganisationWarnings(imageValidationWarning, logoValidationWarning, dia1ValidationWarning, dia2ValidationWarning, dia3ValidationWarning, formSubmitOrganisation) {
    let imageWarning = imageValidationWarning;
    let logoWarning = logoValidationWarning;
    let formOrganisation = formSubmitOrganisation;
    let dia1Warning = dia1ValidationWarning;
    let dia2Warning = dia2ValidationWarning;
    let dia3Warning = dia3ValidationWarning;
    if (imageWarning.style.display === "" || logoWarning.style.display === "" || dia1Warning.style.display === "" || dia2Warning.style.display === "" || dia3Warning.style.display === "") {
        formOrganisation.onsubmit = function () {
            return false;
        }
    }
    if (imageWarning.style.display === "none" && logoWarning.style.display === "none" && dia1Warning.style.display === "none" && dia2Warning.style.display === "none" && dia3Warning.style.display === "none") {
        formOrganisation.onsubmit = function () {
            return true;
        }
    }
}

export function deactivateOrganisation(organisationId) {
    let confirmBox = confirm("Bent u zeker dat u deze organisatie wil deactiveren?");

    if (confirmBox === true) {
        fetch("../superAdmin/deleteorganisation/" + organisationId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);

    }
}

export function reactivateOrganisation(organisationId) {
    let confirmBox = confirm("Bent u zeker dat u deze organisatie wil heractiveren?");

    if (confirmBox === true) {
        fetch("../superAdmin/deleteorganisation/" + organisationId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);

    }
}

export function deactivatePoster(posterId){
    if(confirm("Weet u zeker dat u deze poster wilt deactiveren?")){
        fetch("../iot/deactivatePoster/" + posterId, {
            method: 'POST'
        });
        setTimeout(function(){
            window.location.reload(false);
        }, 700);
    }
}

export function reactivatePoster(posterId) {
    let confirmBox = confirm("Bent u zeker dat u deze poster wil heractiveren?");

    if (confirmBox === true) {
        fetch("../iot/deactivatePoster/" + posterId, {
            method: 'POST'
        });
        setTimeout(function () {
            window.location.reload(false);
        }, 700);

    }
}

