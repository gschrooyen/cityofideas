export function checkImage(e){
    document.querySelectorAll(".imgDisable").forEach(function(elem){
        changeState(elem);
        elem.disabled = !e.srcElement.checked;
    });
}

export function checkVideo(e){
    document.querySelectorAll(".vidDisable").forEach(function(elem){
        changeState(elem);
        elem.disabled = !e.srcElement.checked;
    });
}

export function checkVideoLink(e){
    document.querySelectorAll(".vidLinkDisable").forEach(function(elem){
        changeState(elem);
        elem.disabled = !e.srcElement.checked;
    });
}

function changeState(elem){
    if(elem.checked)
        elem.checked = false;
}