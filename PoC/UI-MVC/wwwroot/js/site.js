﻿import 'bootstrap/dist/css/bootstrap.css'
import 'jquery'
let qr = require('qr-image');
// Custom JS imports
import * as ajaxCalls from './ajax';

// Custom CSS imports
import './../css/site.css'

//import searchbar logic
import * as searchbar from './searchOrganisation';

import * as addIdeation from './addIdeations';

import * as scroll from './navbarAnimation'

//searchbar.setUpSearchBar();

window.onscroll = function () {
    scroll.scrollFunction()
};

console.log('The \'site\' bundle has been loaded!')
document.addEventListener('DOMContentLoaded', function () {


    const likeBtnProject = document.querySelectorAll('.like-project');
    likeBtnProject.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            console.log(e.srcElement.getAttribute('pid'));
            console.log(e.srcElement.getAttribute('uid'));
            ajaxCalls.addLikeToProject(e.srcElement.getAttribute('pid'),e.srcElement.getAttribute('uid'))

        });
    });

    document.querySelectorAll("#myInput").forEach(function (elem) {
        ajaxCalls.autofillsearch(elem, searchbar.autocomplete);

    });


    const shareBtnProject = document.querySelectorAll('.share-project');
    shareBtnProject.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            ajaxCalls.addShareToProject(e.srcElement.getAttribute('pid'))
        });
    });

    const likeBtnIdea = document.querySelectorAll('.like-idea');
    likeBtnIdea.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            ajaxCalls.addLikeToIdea(e.srcElement.getAttribute('iid'), e.srcElement.getAttribute('uid'))
        });
    });

    const shareBtnIdea = document.querySelectorAll('.share-idea');
    shareBtnIdea.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            ajaxCalls.addShareToIdea(e.srcElement.getAttribute('iid'))
        });
    });
    
    const deleteBtnIdea = document.querySelectorAll('#deleteIdeabtn');
    deleteBtnIdea.forEach(function (elem){
        elem.addEventListener('click',function (e){
            e.preventDefault();
            ajaxCalls.removeIdeaIdeation(e.srcElement.getAttribute('ideationId'),e.srcElement.getAttribute('ideaId'));
        })
    });


    const commentBtn = document.querySelectorAll('.reactbutton');
    commentBtn.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            ajaxCalls.addReactionToIdea(e.srcElement.getAttribute('iid'),e.srcElement.getAttribute('uN'))
        });
    });

    const shareBtnIdeation = document.querySelectorAll('.share-ideation');
    shareBtnIdeation.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            ajaxCalls.addShareToIdeation(e.srcElement.getAttribute('iid'));
        });
    });

    const closeModal = document.querySelectorAll(".btn-closeModal");
    closeModal.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.emptyReportBox(e.srcElement.getAttribute('rid'))
        });
    });

    const submitbtn = document.querySelectorAll(".btn-submit");
    submitbtn.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.reportComment(e.srcElement.getAttribute('rid'));

        });
    });

    const finishProjectbtn = document.querySelectorAll('.sluitAfProjectbtn');
    finishProjectbtn.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.finishProject(e.srcElement.getAttribute('pid'));
        });
    });

    const rebootProjectbtn = document.querySelectorAll('.herstartProjectbtn');
    rebootProjectbtn.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.rebootProject(e.srcElement.getAttribute('pid'));
        });
    });

    /*const removeChoiceQuestionbtn = document.querySelectorAll('.removeChoiceQuestionbtn');
    removeChoiceQuestionbtn.forEach(function (ele){
        ele.addEventListener('click', function(e){
            e.preventDefault();
            ajaxCalls.removeChoiceQuestion(e.srcElement.getAttribute('qid'));
        });
    });*/

    /*const removeOpenQuestionbtn = document.querySelectorAll('.removeOpenQuestionbtn');
    removeOpenQuestionbtn.forEach(function (ele){
        ele.addEventListener('click', function(e){
            e.preventDefault();
            ajaxCalls.removeOpenQuestion(e.srcElement.getAttribute('qid'));
        });
    });*/

    const changeStateIdeationbtn = document.querySelectorAll('.changeStateIdeationbtn');
    changeStateIdeationbtn.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.changeStateIdeation(e.srcElement.getAttribute('ideationid'));
        });
    });

    const validationTitleChoiceQuestion = document.querySelectorAll('#titleChoiceQuestion');
    validationTitleChoiceQuestion.forEach(function (ele) {
        ele.addEventListener('blur', function (e) {
            e.preventDefault();
            ajaxCalls.validateTitleChoiceQuestion();
        });
    });

    const validationRangChoiceQuestion = document.querySelectorAll('#rangChoiceQuestion');
    validationRangChoiceQuestion.forEach(function (ele) {
        ele.addEventListener('blur', function (e) {
            e.preventDefault();
            ajaxCalls.validateRangChoiceQuestion();
        });
    });

    const checkAllChoiceQuestionWarnings = document.querySelectorAll('#addChoiceQuestionbtn');
    checkAllChoiceQuestionWarnings.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateTitleChoiceQuestion();
            ajaxCalls.validateRangChoiceQuestion();
            ajaxCalls.validateOptionsChoiceQuestion();
            ajaxCalls.checkAllChoiceQuestionWarnings();
        });
    });

    const validationTitleOpenQuestion = document.querySelectorAll('#titleOpenQuestion');
    validationTitleOpenQuestion.forEach(function (ele) {
        ele.addEventListener('blur', function (e) {
            e.preventDefault();
            ajaxCalls.validateTitleOpenQuestion();
        });
    });
    const validationRangOpenQuestion = document.querySelectorAll('#rangOpenQuestion');
    validationRangOpenQuestion.forEach(function (ele) {
        ele.addEventListener('blur', function (e) {
            e.preventDefault();
            ajaxCalls.validateRangOpenQuestion();
        });
    });

    const checkAllOpenQuestionWarnings = document.querySelectorAll('#addOpenQuestionbtn');
    checkAllOpenQuestionWarnings.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateTitleOpenQuestion();
            ajaxCalls.validateRangOpenQuestion();
            ajaxCalls.checkAllOpenQuestionWarnings();
        });
    });

    const showDivAddQuestion = document.querySelectorAll("#showAddQuestion");
    showDivAddQuestion.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.showAddQuestionDiv();
        });
    });

    const showSelectQuestion = document.querySelectorAll("#selectQuestionType");
    showSelectQuestion.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.changeSelectQuestion();
        });
    });

    const showDivEditQuestions = document.querySelectorAll("#showEditQuestions");
    showDivEditQuestions.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showEditQuestionsDiv();
        });
    });

    //Ideation

    const validationImageCreateIdeation = document.querySelectorAll("#imageCreateIdeation");
    validationImageCreateIdeation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('imageCreateIdeation'), document.getElementById('createImageIdeationValidation'));
        });
    });

    const warningImageCreateIdeation = document.querySelectorAll('#createIdeationbtn');
    warningImageCreateIdeation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateImage(document.getElementById('imageCreateIdeation'), document.getElementById('createImageIdeationValidation'));
            ajaxCalls.warnImage(document.getElementById('createImageIdeationValidation'), document.getElementById('formCreateIdeation'));
        });
    });

    const showImageIdeation = document.querySelectorAll('.showEditImageIdeation');
    showImageIdeation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('imageIdeation'), document.getElementById('imageIdeationValidation'));
        })
    });
    const validationImageIdeation = document.querySelectorAll("#imageIdeation");
    validationImageIdeation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('imageIdeation'), document.getElementById('imageIdeationValidation'));
        });
    });

    const warningImageIdeation = document.querySelectorAll("#editIdeationbtn");
    warningImageIdeation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateImage(document.getElementById('imageIdeation'), document.getElementById('imageIdeationValidation'));
            ajaxCalls.warnImage(document.getElementById('imageIdeationValidation'), document.getElementById('formEditIdeation'));
        });
    });


    //Project

    const validationImageCreateProject = document.querySelectorAll("#imageCreateProject");
    validationImageCreateProject.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('imageCreateProject'), document.getElementById('createImageProjectValidation'));
        });
    });

    const warningImageCreateProject = document.querySelectorAll("#createProjectbtn");
    warningImageCreateProject.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateImage(document.getElementById('imageCreateProject'), document.getElementById('createImageProjectValidation'));
            ajaxCalls.warnImage(document.getElementById('createImageProjectValidation'), document.getElementById('formCreateProject'));
        });
    });


    const showImageProject = document.querySelectorAll('.showEditImage');
    showImageProject.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('imageProject'), document.getElementById('imageProjectValidation'));
        })
    });
    const validationImageProject = document.querySelectorAll("#imageProject");
    validationImageProject.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('imageProject'), document.getElementById('imageProjectValidation'));
        });
    });

    const warningImageProject = document.querySelectorAll("#editProjectbtn");
    warningImageProject.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateImage(document.getElementById('imageProject'), document.getElementById('imageProjectValidation'));
            ajaxCalls.warnImage(document.getElementById('imageProjectValidation'), document.getElementById('formEditProject'));
        });
    });

    // Idea
    const validationTitleIdea = document.querySelectorAll('#titleIdea');
    validationTitleIdea.forEach(function (ele) {
        ele.addEventListener('blur', function (e) {
            e.preventDefault();
            ajaxCalls.validateTitleIdea();

        });
    });

    const validationDescriptionIdea = document.querySelectorAll('#descriptionIdea');
    validationDescriptionIdea.forEach(function (ele) {
        ele.addEventListener('blur', function (e) {
            e.preventDefault();
            ajaxCalls.validateDescriptionIdea();

        });
    });

    const validationImageIdea = document.querySelectorAll("#imageIdea");
    validationImageIdea.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImageIdea();
            ajaxCalls.validateFileSize(document.getElementById('imageIdea'));
        });
    });


    const validationVideoIdea = document.querySelectorAll("#videoIdea");
    validationVideoIdea.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateVideoIdea();
            ajaxCalls.validateFileSize(document.getElementById('videoIdea'));
        });
    });


    const validationVideoLinkIdea = document.querySelectorAll("#videoLinkIdea");
    validationVideoLinkIdea.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateVideoLinkIdea();
        });
    });


    const warningsIdea = document.querySelectorAll("#createIdeabtn");
    warningsIdea.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateTitleIdea();
            ajaxCalls.warnTitleIdea();
            ajaxCalls.validateDescriptionIdea();
            ajaxCalls.warnDescriptionIdea();
            ajaxCalls.validateImageIdea();
            ajaxCalls.warnImageIdea();
            ajaxCalls.validateVideoIdea();
            ajaxCalls.warnVideoIdea();
            ajaxCalls.validateVideoLinkIdea();
            ajaxCalls.warnVideoLinkIdea();
            ajaxCalls.checkAllIdeaWarnings();
        });
    });

    const showFormCreateIdea = document.querySelectorAll("#showCreateIdea");
    showFormCreateIdea.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.showCreateIdeaForm();
        });
    });


    const ignorebtn = document.querySelectorAll(".ignore");
    ignorebtn.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            let awnser = confirm("bent u zeker dat u dit wil negeren?");
            if (awnser) {
                let nid = e.srcElement.parentElement.getAttribute('nid');
                ajaxCalls.IgnoreNotification(nid);
            }

        });
    });

    const deletebtn = document.querySelectorAll(".delete");
    deletebtn.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            if (confirm("wilt u deze reactie echt verwijderen?")) {
                let nid = e.srcElement.parentElement.getAttribute('nid');
                ajaxCalls.deleteComment(nid);
            }
        })
    });

    const blockbtn = document.querySelectorAll(".block");
    blockbtn.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            if (confirm("wilt u deze user echt blokkeren?")) {
                let nid = e.srcElement.parentElement.getAttribute('nid');
                let uid = e.srcElement.parentElement.getAttribute('uid');
                ajaxCalls.blockUser(uid, nid);
            }
        })
    });

    const modsearch = document.querySelectorAll("#searchMods");
    modsearch.forEach(function (elem) {
        elem.addEventListener('input', (e) => {
            let query = e.srcElement.value;
            ajaxCalls.searchMods('antwerpen', query, "../api/users/searchMods")
        });
    });


    const usrsearch = document.querySelectorAll("#searchUsrs");
    usrsearch.forEach(function (elem) {
        elem.addEventListener('input', (e) => {
            let query = e.srcElement.value;
            ajaxCalls.searchMods('antwerpen', query, '../api/users/searchUsers')
        });
    });

    const demoteMod = document.querySelectorAll(".moddemote");
    demoteMod.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            if (confirm("ben je zeker dat je de privileges van deze persoon wil afnemen?")) {
                ajaxCalls.demoteMod(e.srcElement.parentElement.getAttribute("uid"));
            }
        })
    });

    const promoteMod = document.querySelectorAll(".modpromote");
    promoteMod.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            if (confirm("ben je zeker dat je deze user wil promoveren tot admin?")) {
                ajaxCalls.promoteMod(e.srcElement.parentElement.getAttribute("uid"));
            }
        })
    });

    const deluser = document.querySelectorAll(".usrdel");
    deluser.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            if (confirm("ben je zeker dat je deze gebruiker wil verwijderen?")) {
                ajaxCalls.deleteUser(e.srcElement.parentElement.getAttribute("uid"))
            }
        })
    });

    const usrPromote = document.querySelectorAll(".usrpromote");
    usrPromote.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            if (confirm("ben je zeker dat je deze gebruike wil promoveren naar moderator?")) {
                ajaxCalls.promote("../admin/promoteToMod/", e.srcElement.parentElement.getAttribute("uid"))
            }
        })
    });

    const usrBlock = document.querySelectorAll(".usrBlock");
    usrBlock.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            if (confirm("ben je zeker dat je deze gebruiker wil blokkeren?")) {
                ajaxCalls.block(e.srcElement.parentElement.getAttribute("uid"));
            }
        })
    });

    const usrUNblock = document.querySelectorAll(".usrUnblock");
    usrUNblock.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            if (confirm("ben je zeker dat je deze gebruiker wil deblokkeren?")) {
                ajaxCalls.unblock(e.srcElement.parentElement.getAttribute("uid"))
            }
        })
    });

    const searchMods = document.querySelectorAll("#searchAdmins");
    searchMods.forEach(function (elem) {
        elem.addEventListener('input', function (e) {
            let query = e.srcElement.value;
            ajaxCalls.searchMods('antwerpen', query, '../api/users/SearchAdmins')
        })
    });

    const btnDemoteAdmin = document.querySelectorAll(".demoteAdmin");
    btnDemoteAdmin.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            if (confirm("Ben je zeker dat je deze user admin-rechten wil afnemen?")) {
                ajaxCalls.demoteAdmin(e.srcElement.parentElement.getAttribute("uid"))
            }
        })
    });

    const btnPromoteAdmin = document.querySelectorAll(".promoteAdmin");
    btnPromoteAdmin.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.promoteUser(e.srcElement.parentElement.getAttribute(("uid")));
        })
    });

    const chkCheckImage = document.querySelectorAll(".checkImage");
    chkCheckImage.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            addIdeation.checkImage(e);
        })
    });

    const chkCheckVideo = document.querySelectorAll(".checkVideo");
    chkCheckVideo.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            addIdeation.checkVideo(e);
        })
    });

    const chkCheckVideoLink = document.querySelectorAll(".checkVideoLink");
    chkCheckVideoLink.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            if (elem.checked === true) {

            }
            addIdeation.checkVideoLink(e);
        })
    });
    const validationImageCreateOrganisation = document.querySelectorAll("#createImageOrganisation");
    validationImageCreateOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('createImageOrganisation'), document.getElementById('createImageOrganisationValidation'));
        });
    });

    const validationLogoCreateOrganisation = document.querySelectorAll("#createLogoOrganisation");
    validationLogoCreateOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('createLogoOrganisation'), document.getElementById('createLogoOrganisationValidation'));
        });
    });

    const validationDia1CreateOrganisation = document.querySelectorAll("#createDia1Organisation");
    validationDia1CreateOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('createDia1Organisation'), document.getElementById('createDia1OrganisationValidation'));
        });
    });

    const validationDia2CreateOrganisation = document.querySelectorAll("#createDia2Organisation");
    validationDia2CreateOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('createDia2Organisation'), document.getElementById('createDia2OrganisationValidation'));
        });
    });

    const validationDia3CreateOrganisation = document.querySelectorAll("#createDia3Organisation");
    validationDia3CreateOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('createDia3Organisation'), document.getElementById('createDia3OrganisationValidation'));
        });
    });

    const checkAllCreateOrganisationWarnings = document.querySelectorAll("#createOrganisationbtn");
    checkAllCreateOrganisationWarnings.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateImage(document.getElementById('createImageOrganisation'), document.getElementById('createImageOrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('createLogoOrganisation'), document.getElementById('createLogoOrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('createDia1Organisation'), document.getElementById('createDia1OrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('createDia2Organisation'), document.getElementById('createDia2OrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('createDia3Organisation'), document.getElementById('createDia3OrganisationValidation'));
            ajaxCalls.checkAllOrganisationWarnings(document.getElementById('createImageOrganisationValidation'),
                document.getElementById('createLogoOrganisationValidation'), document.getElementById('createDia1OrganisationValidation'),
                document.getElementById('createDia2OrganisationValidation'), document.getElementById('createDia3OrganisationValidation'),
                document.getElementById('formCreateOrganisation'));
        });
    });

    const showImageEditOrganisation = document.querySelectorAll('.showEditImageOrg');
    showImageEditOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('imageOrganisation'), document.getElementById('imageOrganisationValidation'));
        })
    });

    const validationImageEditOrganisation = document.querySelectorAll("#imageOrganisation");
    validationImageEditOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('imageOrganisation'), document.getElementById('imageOrganisationValidation'));
        });
    });

    const showLogoEditOrganisation = document.querySelectorAll('.showEditLogoOrg');
    showLogoEditOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('logoOrganisation'), document.getElementById('logoOrganisationValidation'));
        })
    });

    const validationLogoEditOrganisation = document.querySelectorAll("#logoOrganisation");
    validationLogoEditOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('logoOrganisation'), document.getElementById('logoOrganisationValidation'));
        });
    });

    const showDia1EditOrganisation = document.querySelectorAll('.showEditDia1Org');
    showDia1EditOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('dia1Organisation'), document.getElementById('dia1OrganisationValidation'));
        })
    });

    const validationDia1EditOrganisation = document.querySelectorAll("#dia1Organisation");
    validationDia1EditOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('dia1Organisation'), document.getElementById('dia1OrganisationValidation'));
        });
    });

    const showDia2EditOrganisation = document.querySelectorAll('.showEditDia2Org');
    showDia2EditOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('dia2Organisation'), document.getElementById('dia2OrganisationValidation'));
        })
    });

    const validationDia2EditOrganisation = document.querySelectorAll("#dia2Organisation");
    validationDia2EditOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('dia2Organisation'), document.getElementById('dia2OrganisationValidation'));
        });
    });

    const showDia3EditOrganisation = document.querySelectorAll('.showEditDia3Org');
    showDia3EditOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('dia3Organisation'), document.getElementById('dia3OrganisationValidation'));
        })
    });

    const validationDia3EditOrganisation = document.querySelectorAll("#dia3Organisation");
    validationDia3EditOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('dia3Organisation'), document.getElementById('dia3OrganisationValidation'));
        });
    });

    const checkAllEditOrganisationWarnings = document.querySelectorAll("#editOrganisationbtn");
    checkAllEditOrganisationWarnings.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateImage(document.getElementById('imageOrganisation'), document.getElementById('imageOrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('logoOrganisation'), document.getElementById('logoOrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('dia1Organisation'), document.getElementById('dia1OrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('dia2Organisation'), document.getElementById('dia2OrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('dia3Organisation'), document.getElementById('dia3OrganisationValidation'));
            ajaxCalls.checkAllOrganisationWarnings(document.getElementById('imageOrganisationValidation'), document.getElementById('logoOrganisationValidation'),
                document.getElementById('dia1OrganisationValidation'), document.getElementById('dia2OrganisationValidation'),
                document.getElementById('dia3OrganisationValidation'), document.getElementById('formEditOrganisation'));
        });
    });

    const showImageCopyOrganisation = document.querySelectorAll('.showCopyImageOrg');
    showImageCopyOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('copyImageOrganisation'), document.getElementById('copyImageOrganisationValidation'));
        })
    });

    const validationImageCopyOrganisation = document.querySelectorAll("#copyImageOrganisation");
    validationImageCopyOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('copyImageOrganisation'), document.getElementById('copyImageOrganisationValidation'));
        });
    });

    const showLogoCopyOrganisation = document.querySelectorAll('.showCopyLogoOrg');
    showLogoCopyOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('copyLogoOrganisation'), document.getElementById('copyLogoOrganisationValidation'));
        })
    });

    const validationLogoCopyOrganisation = document.querySelectorAll("#copyLogoOrganisation");
    validationLogoCopyOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('copyLogoOrganisation'), document.getElementById('copyLogoOrganisationValidation'));
        });
    });

    const showDia1CopyOrganisation = document.querySelectorAll('.showCopyDia1Org');
    showDia1CopyOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('copyDia1Organisation'), document.getElementById('copyDia1OrganisationValidation'));
        })
    });

    const validationDia1CopyOrganisation = document.querySelectorAll("#copyDia1Organisation");
    validationDia1CopyOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('copyDia1Organisation'), document.getElementById('copyDia1OrganisationValidation'));
        });
    });

    const showDia2CopyOrganisation = document.querySelectorAll('.showCopyDia2Org');
    showDia2CopyOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('copyDia2Organisation'), document.getElementById('copyDia2OrganisationValidation'));
        })
    });

    const validationDia2CopyOrganisation = document.querySelectorAll("#copyDia2Organisation");
    validationDia2CopyOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('copyDia2Organisation'), document.getElementById('copyDia2OrganisationValidation'));
        });
    });

    const showDia3CopyOrganisation = document.querySelectorAll('.showCopyDia3Org');
    showDia3CopyOrganisation.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.showImage(document.getElementById('copyDia3Organisation'), document.getElementById('copyDia3OrganisationValidation'));
        })
    });

    const validationDia3CopyOrganisation = document.querySelectorAll("#copyDia3Organisation");
    validationDia3CopyOrganisation.forEach(function (ele) {
        ele.addEventListener('change', function (e) {
            e.preventDefault();
            ajaxCalls.validateImage(document.getElementById('copyDia3Organisation'), document.getElementById('copyDia3OrganisationValidation'));
        });
    });

    const checkAllCopyOrganisationWarnings = document.querySelectorAll("#copyOrganisationbtn");
    checkAllCopyOrganisationWarnings.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            ajaxCalls.validateImage(document.getElementById('copyImageOrganisation'), document.getElementById('copyImageOrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('copyLogoOrganisation'), document.getElementById('copyLogoOrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('copyDia1Organisation'), document.getElementById('copyDia1OrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('copyDia2Organisation'), document.getElementById('copyDia2OrganisationValidation'));
            ajaxCalls.validateImage(document.getElementById('copyDia3Organisation'), document.getElementById('copyDia3OrganisationValidation'));
            ajaxCalls.checkAllOrganisationWarnings(document.getElementById('copyImageOrganisationValidation'), document.getElementById('copyLogoOrganisationValidation'),
                document.getElementById('copyDia1OrganisationValidation'), document.getElementById('copyDia2OrganisationValidation'),
                document.getElementById('copyDia3OrganisationValidation'), document.getElementById('formCopyOrganisation'));
        });
    });

    /*const btndelOrg = document.querySelectorAll(".delOrg");
    btndelOrg.forEach(function(elem){
        elem.addEventListener('click', function (e){
            e.preventDefault();
            if(confirm("Bent u zeker dat u deze organisatie wil verwijderen?")){
                console.log(e.srcElement.parentElement.getAttribute("oid"));
                ajaxCalls.deleteOrganisation(e.srcElement.parentElement.getAttribute("oid"))
            }
        })
    }, );
    */
    const deactivateOrganisationbtn = document.querySelectorAll('.deactivateOrgbtn');
    deactivateOrganisationbtn.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.deactivateOrganisation(e.srcElement.parentElement.getAttribute("oid"));
        });
    });
    const reactivateOrganisationbtn = document.querySelectorAll('.reactivateOrgbtn');
    reactivateOrganisationbtn.forEach(function (ele) {
        ele.addEventListener('click', function (e) {
            e.preventDefault();
            ajaxCalls.reactivateOrganisation(e.srcElement.parentElement.getAttribute("oid"));
        });
    });
    
    const deactivatePosterbtn = document.querySelectorAll('.deactivatePosterbtn');
    deactivatePosterbtn.forEach(function (ele){
        ele.addEventListener('click', function(e){
            e.preventDefault();
            ajaxCalls.deactivatePoster(e.srcElement.parentElement.getAttribute("pid"));
        });
    });

    const reactivatePosterbtn = document.querySelectorAll('.reactivatePosterbtn');
    reactivatePosterbtn.forEach(function (ele){
        ele.addEventListener('click', function(e){
            e.preventDefault();
            ajaxCalls.reactivatePoster(e.srcElement.parentElement.getAttribute("pid"));
        });
    });
}, false);

// Organisation



