/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./wwwroot/js/addQuestionList.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./wwwroot/js/addQuestionList.js":
/*!***************************************!*\
  !*** ./wwwroot/js/addQuestionList.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

let questionCounter = document.querySelectorAll("tbody tr").length;

console.log("AddquestionList.js loaded");

/*const rows = document.querySelectorAll("tbody tr");
rows.forEach(function (element) {
    //console.log(element.getAttribute('id'));
});*/
document.querySelectorAll("#randomidke").forEach(function (elem) {
    elem.value = elem.getAttribute("type");
});

document.querySelectorAll(".deleteoption").forEach(function (elem) {
    elem.addEventListener('click', function (e) {
        e.srcElement.parentElement.remove();
        
    })
})

function fixQuestionType(t) {
    let parent = t.parentElement.parentElement.parentElement;
    switch (t.value.toUpperCase()) {
        case "OPEN_QUESTION":
        case "EMAIL":
        case "MAP":
            parent.childNodes[5].childNodes.forEach(function (e) {
                e.childNodes.forEach(function (childElement) {
                    if(childElement.className != null){
                        if(childElement.className.includes("optiesContainer")){
                            childElement.innerHTML = ""
                        }else{
                            childElement.disabled = true;
                        }
                    }          
                });
            });
            break;
        case "SINGLE_CHOICE":
        case "MULTIPLE_CHOICE":
        case "DROPDOWN":
            parent.childNodes[5].childNodes.forEach(function (e) {
                e.childNodes.forEach(function (childElement) {
                    childElement.disabled = false;
                });
            });
            break;
    }
}

const selectedType = document.querySelectorAll(".selectedType");
selectedType.forEach(function(select){
    let t = select.options[select.selectedIndex];
    
    select.addEventListener('change', function (e) {
        let u = select.options[select.selectedIndex];
        fixQuestionType(u);
    });
    
    fixQuestionType(t);
});

const btnAddRow = document.querySelectorAll(".addRow");
btnAddRow.forEach(function (elem) {
    elem.addEventListener('click', function (e) {
        addNewQuestion(e)
    })
});


const btnAddOption = document.querySelectorAll(".addOption");
btnAddOption.forEach(function (elem) {
    elem.addEventListener('click', function (e) {
        addNewOption(e)
    })
});

const btnDeleteRow = document.querySelectorAll(".removeQuestion");
btnDeleteRow.forEach(function(elem){
    elem.addEventListener('click', function(e){
        let row = elem.parentElement.parentElement;
        row.parentElement.removeChild(row);
    })
});

function addNewOption(e) {
    let srcelem = e.srcElement;
    //console.log(srcelem.parentElement.parentElement.parentElement);
    let qid = srcelem.parentElement.parentElement.parentElement.getAttribute('id');
    let currentOptionIndex = srcelem.parentElement.childNodes[srcelem.parentElement.childNodes.length-2].className.split("_")[1];
    let option = getOption(qid, currentOptionIndex);
    
    let container = document.querySelector(".optiesContainer_" + currentOptionIndex);
    let oid = (container.childNodes.length);
    container.insertAdjacentHTML("beforeend", option);
    console.log(document.getElementById("btnDeleteOption_"+oid));
    console.log(oid);
    document.getElementById("btnDeleteOption_"+oid).addEventListener('click', function (e) {
        e.srcElement.parentElement.remove();
    });
}

function getOption(qid, currentOptionIndex) {
    let container = document.querySelector(".optiesContainer_" + currentOptionIndex);
    let oid = (container.childNodes.length);
    let t = document.getElementsByName("optioncount_" + currentOptionIndex.toString())[0];
    t.value -= -1;
    return `<div class="optiesDiv"><input name="option_${qid}_${oid}" type="text" qid="${qid}" oid="${oid}" placeholder="optie"/><button id="btnDeleteOption_${oid}" type="button" qid="${qid}" oid="${oid}">delete option</button></div>`;
}

function getQuestion() {
    
    let question = `<tr id=${questionCounter}>
    <td class="col-sm-6">       
        <input type="hidden" name="questionid_${questionCounter}" value="0"/>         
        <input type="text" class="form-control" name="questionTitle_${questionCounter}"/>            
    </td>           
        <td class="col-sm-2">                
            <select class="form-control selectedType" name="questionType_${questionCounter}">                    
                <option>Open vraag</option>                    
                <option>Email</option>                    
                <option>Map</option>                    
                <option>Single choice</option>                    
                <option>Multiple choice</option>                    
                <option>Drop down list</option>                
            </select>
        </td>
    <td class="col-sm-3" id="option">
        <div class="container">
            <input type="button" class="btn btn-primary btn-block addOption" value="Voeg een optie toe"/>
            <input type="hidden" value="0" name="optioncount_${questionCounter}"/>
            <div class="optiesContainer_${questionCounter}"></div>
        </div>            
    </td>            
    <td class="col-sm-1">
        <input type="button" qid="${questionCounter}" class="btn btn-primary btn-md removeQuestion" value="X"/>            
    </td>        
</tr>`;
    questionCounter++;
    return question;
}

function addNewQuestion(e) {
    let body = document.querySelector('.questionTableBody');
    body.insertAdjacentHTML("beforeend", getQuestion());

    for(let i = 0; i<questionCounter; i++){
        let u = document.getElementsByName("questionid_"+i.toString());
        u.forEach(function (elem) {
            if(elem.value === "0"){
                elem.value = i;
            }
            console.log(elem);
        });
    }
    
    let bdd = document.querySelectorAll("tbody tr");
    bdd.forEach(function (element) {
        
        
    });
    

    let selects = body.querySelectorAll(".selectedType");
    selects.forEach(function (select) {
        let t = select.options[select.selectedIndex];

        select.addEventListener('change', function (e) {
            let u = select.options[select.selectedIndex];
            fixQuestionType(u);
        });

        fixQuestionType(t);
    });

    let btns = body.querySelectorAll(".addOption");
    btns.forEach(function (elem) {       
        elem.addEventListener('click', function (e) {
            addNewOption(e);        
        })
    });

    let btnsDelete = body.querySelectorAll(".removeQuestion");
    btnsDelete.forEach(function (elem) {
        elem.addEventListener('click', function (e) {
            let row = elem.parentElement.parentElement;
            row.parentElement.removeChild(row);
            getRows()
        });
    });
    
    getRows()
}

function getRows(){
    let questionCount = document.querySelectorAll(".questionCount")[0];
    questionCount.value = questionCounter;
}



/***/ })

/******/ });
//# sourceMappingURL=addQuestionList.entry.js.map