using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using PoC.Domain.datatype;
using PoC.Domain.project;

namespace PoC.Models
{
  public class ProjectViewModel : IValidatableObject
  {
    [Required(ErrorMessage = "Naam verplicht in te vullen")]
    [StringLength(100, ErrorMessage = "Maximaal 100 tekens")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Hoofdvraag verplicht in te vullen")]
    [StringLength(500, ErrorMessage = "Maximaal 500 tekens")]
    public string MainQuestion { get; set; }

    [Required(ErrorMessage = "Beschrijving verplicht in te vullen")]
    [StringLength(2000, ErrorMessage = "Maximaal 2000 tekens")]
    public string Description { get; set; }

    public Category Category { get; set; }

    [DataType(DataType.Date)] public DateTime Begin { get; set; }
    [DataType(DataType.Date)] public DateTime End { get; set; }
    public IFormFile ImageCreateProject { get; set; }
    public Fase Fase { get; set; }


    IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
    {
      List<ValidationResult> errors = new List<ValidationResult>();

      if (Begin < DateTime.Now)
      {
        errors.Add(new ValidationResult("Begin van het project kan niet in het verleden liggen"));
      }

      if (End <= Begin)
      {
        errors.Add(new ValidationResult("Einde van het project kan niet voor het begin van het project liggen"));
      }

      return errors;
    }
  }
}