using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using PoC.Domain.input;
using PoC.Domain.inputmethod;

namespace PoC.Models
{
  public class QuestionListViewModel : IValidatableObject
  {
    [Required(ErrorMessage = "Titel verplicht in te vullen")]
    [StringLength(100,ErrorMessage = "Maximaal 100 characters")]
    public string Title { get; set; }
    [Required(ErrorMessage = "Beschrijving verplicht in te vullen")]
    [StringLength(2000, ErrorMessage = "Maximaal 2000 characters")]
    public string Description { get; set; }
    
    [DataType(DataType.Date)]
    public DateTime Begin { get; set; }

    
    [DataType(DataType.Date)]
    public DateTime End { get; set; }
    
    
    
    public List<Question> Questions { get; set; }


    public QuestionListViewModel()
    {
      Questions = new List<Question>();
    }
    
    
    IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
    {
      List<ValidationResult> errors = new List<ValidationResult>();

      if (Begin == DateTime.MinValue)
      {
        errors.Add(new ValidationResult("test"));
      }
      
      if (End < DateTime.Now)
      {
        errors.Add(new ValidationResult("Einddatum kan niet in het verleden liggen"));
      }

      if (End <= Begin)
      {
        errors.Add(new ValidationResult("Einddatum kan niet voor de Startdatum liggen"));
      }

      return errors;
    }
  }
  
}