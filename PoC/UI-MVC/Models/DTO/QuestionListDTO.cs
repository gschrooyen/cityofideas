using System;
using System.Collections.Generic;
using PoC.Domain.input;
using PoC.Domain.inputmethod;

namespace PoC.Models.DTO
{
    public class QuestionListDTO
    {
        public IEnumerable<Question> questions { get; set; }
    }
}