using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.inputmethod;
using PoC.Domain.iot;
using PoC.Domain.project;

namespace PoC.Models
{
    public class PosterViewModel: IValidatableObject
    {
        [Required]
        public string Description { get; set; }
        
        [Required]
        public bool Active { get; set; }
        
        [Required]
        public List<Button> Buttons { get; set; }
        
        public DateTime Begin { get; set; }
        [Required]
        public DateTime End { get; set; }
      
        [Required]
        public Project Project { get; set; }
        [Required]
        public Ideation Ideation { get; set; }
        [Required]
        public Question Question { get; set; }
        
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (End < DateTime.Now)
            {
                errors.Add(new ValidationResult("Einde van de fase kan niet in het verleden liggen"));
            }

            if (End <= Begin)
            {
                errors.Add(new ValidationResult("Einde van de fase kan niet voor het begin van de fase liggen"));
            }

            return errors;
        }
    }
}