using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.user;
using PoC.Domain.user;

namespace PoC.Models.Services
{
    public class UserService: IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly UserManager<PoCIdentityUser> _userManager;
        private readonly IUserManager _realUserManager;

        public UserService(IOptions<AppSettings> appSettings, UserManager<PoCIdentityUser> userManager, IUserManager realUserManager)
        {
            _appSettings = appSettings.Value;
            _userManager = userManager;
            _realUserManager = realUserManager;
        }
        
        public PoCIdentityUser Authenticate(string username, string password)
        {
            var user = _userManager.FindByEmailAsync(username).Result;
            
            if (user == null)
                return null;
            
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new [] 
                {
                    new Claim(ClaimTypes.Name, user.Id)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            
            user.Token = tokenHandler.WriteToken(token);

           
            return user;
        }

        public User CorrectPassword(string username, string password)
        {
            var user = Authenticate(username, password);
            
            if (user == null)
                return null;

            var b = _userManager.CheckPasswordAsync(user, password).Result;

            user.PasswordHash = null;
            
            var u = _realUserManager.GetUser(user.Id);

            return b ? u : null;
        }
    }
}