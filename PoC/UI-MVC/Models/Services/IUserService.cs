using PoC.Areas.Identity.Data;
using PoC.Domain.user;

namespace PoC.Models.Services
{
    public interface IUserService
    {
        PoCIdentityUser Authenticate(string username, string password);
        User CorrectPassword(string username, string password);
    }
}