using Microsoft.AspNetCore.Authorization;

namespace PoC.Models {
    public class MinimumAgeRequirement : IAuthorizationRequirement{
        
        public int MinimumAge { get; set; }

        public MinimumAgeRequirement(int minimumAge) {
            MinimumAge = minimumAge;
        }
    }
}