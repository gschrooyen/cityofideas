using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using PoC.Domain.datatype;

namespace PoC.Models
{
  public class IdeationViewModel : IValidatableObject
  {
    public IdeationType Type { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "Maximaal 100 tekens")]
    public string TitleIdeation { get; set; }

    [Required]
    [StringLength(2000, ErrorMessage = "Maximaal 2000 tekens")]
    public string DescriptionIdeation { get; set; }

    [DataType(DataType.Date)] public DateTime Begin { get; set; }
    [DataType(DataType.Date)] public DateTime End { get; set; }

    [Required(ErrorMessage = "Afbeelding verplicht mee te geven")]
    public IFormFile ImageIdeation { get; set; }

    public string BackgroundLink { get; set; }
    
    // De verschillende InputMogelijkheden die mogen meegegeven worden.
    public bool AllowImage { get; set; }
    public int NumberOfImages { get; set; }
    public bool ImageRequired { get; set; }
    public bool AllowVideo { get; set; }
    public int NumberOfVideos { get; set; }
    public bool VideoRequired { get; set; }
    public bool AllowVideoLink { get; set; }
    public bool VideoLinkRequired { get; set; }

    IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
    {
      List<ValidationResult> errors = new List<ValidationResult>();

      if (End < DateTime.Now)
      {
        errors.Add(new ValidationResult("Einddatum kan niet in het verleden liggen"));
      }

      if (End <= Begin)
      {
        errors.Add(new ValidationResult("Einddatum kan niet voor de Startdatum liggen"));
      }

      return errors;
    }
  }
}