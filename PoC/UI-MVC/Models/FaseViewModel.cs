using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PoC.Domain.datatype;

namespace PoC.Models
{
  public class FaseViewModel : IValidatableObject
  {
    [Required(ErrorMessage = "Naam verplicht in te vullen")]
    [StringLength(100, ErrorMessage = "Maximaal 100 tekens")]
    public string FaseName { get; set; }

    public ProjectFase ProjectFase { get; set; }
    [DataType(DataType.Date)] public DateTime Begin { get; set; }
    [DataType(DataType.Date)] public DateTime End { get; set; }


    IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
    {
      List<ValidationResult> errors = new List<ValidationResult>();

      if (End < DateTime.Now)
      {
        errors.Add(new ValidationResult("Einde van de fase kan niet in het verleden liggen"));
      }

      if (End <= Begin)
      {
        errors.Add(new ValidationResult("Einde van de fase kan niet voor het begin van de fase liggen"));
      }

      return errors;
    }
  }
}