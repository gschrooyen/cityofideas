using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.user;
using PoC.Domain.user;

namespace PoC.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly UserManager<PoCIdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IUserManager _realUserManager;

        public RegisterModel(
            UserManager<PoCIdentityUser> userManager,
            IUserManager realUserManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender)
        {    
            _realUserManager = realUserManager;
            _userManager = userManager;
            _logger = logger;
            _emailSender = emailSender;
        }
        //method to create user name based on first and last name
        private string createUserName(User user) {
            var username = user.FirstName + "." + user.LastName.Substring(0,1);
            return username;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }
        
        public List<SelectListItem> Genders { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "M", Text = "Male" },
            new SelectListItem { Value = "F", Text = "Female" },
            new SelectListItem { Value = "X", Text = "Undefined"  },
        };

        public class InputModel
        {
            
            [Required]
            [Display(Name = "First name")]
            public string FirstName { get; set; }
            
            [Required]
            [Display(Name = "Last name")]
            public string LastName { get; set; }
            
            [Required]
            [Display(Name = "Gender")]
            public string Gender { get; set; }
            
            [Required]
            [Display(Name = "Postal Code")]
            public int PostalCode { get; set; }
            
            [Required]
            [Display(Name = "Date of birth")]
            [DataType(DataType.Date)]
            public DateTime Birthdate { get; set; }
            
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid) {
                var user = new PoCIdentityUser { UserName = Input.Email, Email = Input.Email};
                //add user to regular database
                var newUser = new User {FirstName = Input.FirstName, LastName = Input.LastName};
                _realUserManager.AddUser(new User
                   {UserId = user.Id, Email = Input.Email, FirstName = Input.FirstName, LastName = Input.LastName,
                       UserName = createUserName(newUser), Gender = Input.Gender, Address = Input.PostalCode, Birthdate = Input.Birthdate});
                

                var result = await _userManager.CreateAsync(user, Input.Password);
                //add default role for now users to user upon registration
                await _userManager.AddToRoleAsync(user, "User");
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");
                       
                    
                    
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { userId = user.Id, code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
                    //commenting this line makes sure that the user must confirm his email address before being able to login
                    //await _signInManager.SignInAsync(user, isPersistent: false);
                    return LocalRedirect(returnUrl);
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }

}
