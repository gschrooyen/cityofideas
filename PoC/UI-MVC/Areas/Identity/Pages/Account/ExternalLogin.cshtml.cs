﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using PoC.Areas.Identity.Data;
using PoC.BL.manager.user;
using PoC.Domain.user;

namespace PoC.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ExternalLoginModel : PageModel
    {
        private readonly SignInManager<PoCIdentityUser> _signInManager;
        private readonly UserManager<PoCIdentityUser> _userManager;
        private readonly ILogger<ExternalLoginModel> _logger;
        private readonly IUserManager _realUserManager;

        public ExternalLoginModel(
            SignInManager<PoCIdentityUser> signInManager,
            UserManager<PoCIdentityUser> userManager,
            IUserManager realUserManager,
            ILogger<ExternalLoginModel> logger)
        {
            _realUserManager = realUserManager;
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string LoginProvider { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }
        
        public List<SelectListItem> Genders { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "M", Text = "Male" },
            new SelectListItem { Value = "F", Text = "Female" },
            new SelectListItem { Value = "X", Text = "Undefined"  },
        };

        public class InputModel
        {
            [Required]
            [Display(Name = "First name")]
            public string FirstName { get; set; }
            
            [Required]
            [Display(Name = "Last name")]
            public string LastName { get; set; }
            
            [Required]
            [Display(Name = "Gender")]
            public string Gender { get; set; }
            
            [Required]
            [Display(Name = "Postal Code")]
            public int PostalCode { get; set; }
            
            [Required]
            [Display(Name = "Date of birth")]
            [DataType(DataType.Date)]
            public DateTime Birthdate { get; set; }
            
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

        }

        public IActionResult OnGetAsync()
        {
            return RedirectToPage("./Login");
        }

        public IActionResult OnPost(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }

        public async Task<IActionResult> OnGetCallbackAsync(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToPage("./Login", new {ReturnUrl = returnUrl });
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ErrorMessage = "Error loading external login information.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor : true);
            if (result.Succeeded)
            {
                _logger.LogInformation("{Name} logged in with {LoginProvider} provider.", info.Principal.Identity.Name, info.LoginProvider);
                return LocalRedirect(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return RedirectToPage("./Lockout");
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ReturnUrl = returnUrl;
                LoginProvider = info.LoginProvider;
                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
                {
                    Input = new InputModel
                    {
                        Email = info.Principal.FindFirstValue(ClaimTypes.Email),
                        FirstName = info.Principal.FindFirstValue(ClaimTypes.Name).Split(' ').First(),
                        LastName = info.Principal.FindFirstValue(ClaimTypes.Surname),
                        Gender = info.Principal.FindFirstValue(ClaimTypes.Gender),
                        PostalCode = Convert.ToInt32(info.Principal.FindFirstValue(ClaimTypes.PostalCode)),
                        Birthdate = Convert.ToDateTime(info.Principal.FindFirstValue(ClaimTypes.DateOfBirth))
                    };
                }
                return Page();
                
            }
        }
        
        //method to create user name based on first and last name
        private string createUserName(User user) {
            var username = user.FirstName + "." + user.LastName.Substring(0,1);
            return username;
        }

        public async Task<IActionResult> OnPostConfirmationAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            // Get the information about the user from the external login provider
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ErrorMessage = "Error loading external login information during confirmation.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }

            if (ModelState.IsValid)
            {
                var user = new PoCIdentityUser { UserName = Input.Email, Email = Input.Email };
                var tempUser = new User { 
                    FirstName = Input.FirstName,
                    LastName = Input.LastName };
                var trueUser = new User {
                    UserId = user.Id,
                    Email = Input.Email, 
                    Gender = Input.Gender,
                    Birthdate = Input.Birthdate,
                    FirstName = Input.FirstName,
                    LastName = Input.LastName,
                    UserName = createUserName(tempUser),
                    Address = Input.PostalCode
                };
                _realUserManager.AddUser(trueUser);
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            LoginProvider = info.LoginProvider;
            ReturnUrl = returnUrl;
            return Page();
        }
    }
}
