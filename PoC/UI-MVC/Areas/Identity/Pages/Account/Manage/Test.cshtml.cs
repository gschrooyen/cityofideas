using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PoC.Areas.Identity.Data;

namespace PoC.Areas.Identity.Pages.Account.Manage
{
    public class TestModel : PageModel
    {

        public readonly UserManager<PoCIdentityUser> UserManager;
        
        public TestModel(UserManager<PoCIdentityUser> userManager)
        {
            UserManager = userManager;
        }
        
        public async Task<IActionResult> OnGet()
        {
            return Page();
        }
    }
}