using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace PoC.Areas.Identity.Services {
    public class EmailSender : IEmailSender{
        
        public AuthMessageSenderOptions Options { get; }

        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor) {
            Options = optionsAccessor.Value;
        }

        public Task Execute(string apiKey, string email, string subject, string message) {
            var client = new SendGridClient(apiKey);
            var msg = new SendGridMessage() {
                From = new EmailAddress("vanloo.ruben@outlook.com", "Ruben Vanloo"),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));
            msg.SetClickTracking(false, false);
            return client.SendEmailAsync(msg);
        }
        
        public Task SendEmailAsync(string email, string subject, string message) {
            return Execute(Options.SendGridKey, email, subject, message);
        }
    }
}