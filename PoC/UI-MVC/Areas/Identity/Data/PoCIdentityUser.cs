using System;
using Microsoft.AspNetCore.Identity;

namespace PoC.Areas.Identity.Data {
    public class PoCIdentityUser : IdentityUser {


        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNr { get; set; }
        public DateTime Birthdate { get; set; }
        public override string UserName { get; set; }
        public string Token { get; set; }
    }
}