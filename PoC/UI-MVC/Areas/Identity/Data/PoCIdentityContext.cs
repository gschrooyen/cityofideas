using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace PoC.Areas.Identity.Data {
    public sealed class PoCIdentityContext : IdentityDbContext<PoCIdentityUser> {
        public PoCIdentityContext () {
        
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder) {

            builder.UseSqlite("Data Source=..\\PoCIdentity.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
        }
    }
}