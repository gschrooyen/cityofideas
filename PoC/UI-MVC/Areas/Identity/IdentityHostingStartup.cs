﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(PoC.Areas.Identity.IdentityHostingStartup))]
namespace PoC.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}