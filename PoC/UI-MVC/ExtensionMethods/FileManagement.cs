using System;
using System.IO;
using PoC.Domain.organisation;
using Microsoft.AspNetCore.Http;

namespace PoC.ExtensionMethods
{
  public static class FileManagement
  {
    public static string CreateFileName(string fileName)
    {
      fileName = Path.GetFileName(fileName);
      return Path.GetFileNameWithoutExtension(fileName)
             + "_"
             + Guid.NewGuid().ToString().Substring(0, 4)
             + Path.GetExtension(fileName);
    }

    public static string UploadFile(string root,string path, IFormFile file)
    {
      var fileNameImage = CreateFileName(file.FileName);
      var uploadToRoot = Path.Combine(root, path);
      var filePath = Path.Combine(uploadToRoot, fileNameImage);
      file.CopyTo(new FileStream(filePath, FileMode.Create));
      return fileNameImage;
    }

    public static string CopyAndMoveFile(string root,string sourcePath, string destinationPath, string pictureName)
    {
      var filePath = Path.Combine(root, sourcePath, pictureName);
      var destinationPathCopy = Path.Combine(root, "temporary\\", pictureName);
      var fileName = Path.GetFileName(filePath);
      var fileNameBase = fileName.Split('_')[0];
      var moveFileName = fileNameBase + "_" + Guid.NewGuid().ToString().Substring(0, 4) + Path.GetExtension(filePath);
      var destinationPathMove =
        Path.Combine(root, destinationPath, moveFileName);
      System.IO.File.Copy(filePath, destinationPathCopy);
      System.IO.File.Move(destinationPathCopy, destinationPathMove);
      return moveFileName;
    }

    public static bool CheckFileExtension(string fileName)
    {
      bool allowFile = (Path.GetExtension(fileName).Equals(".jpg") ||
                        Path.GetExtension(fileName).Equals(".png") ||
                        Path.GetExtension(fileName).Equals(".jpeg"));
      return allowFile;
    }
  }
}