using PoC.Domain.moderation;

namespace PoC.DAL.repo.moderation
{
    public interface IModerationRepository
    {
        //Notifications CRUD
        Notification CreateNotification(Notification notification);
        Notification UpdateNotification(Notification notification);
        Notification DeleteNotification(string id);
        Notification ReadNotification(string id);
    }
}