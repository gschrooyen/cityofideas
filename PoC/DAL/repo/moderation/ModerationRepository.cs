using System.Linq;
using Microsoft.EntityFrameworkCore;
using PoC.DAL.EF;
using PoC.Domain.moderation;

namespace PoC.DAL.repo.moderation
{
    public class ModerationRepository: IModerationRepository
    {
        
        private PoCDbContext ctx;

        public ModerationRepository( PoCDbContext ctx)
        {
            this.ctx = ctx;
        }
        public Notification CreateNotification(Notification notification)
        {
            ctx.Notifications.Add(notification);
            ctx.SaveChanges();
            return notification;
        }

        public Notification UpdateNotification(Notification notification)
        {
            ctx.Notifications.Update(notification);
            ctx.SaveChanges();
            return notification;
        }

        public Notification DeleteNotification(string id)
        {
            Notification notification = ctx.Notifications.Find(id);
            ctx.Notifications.Remove(notification);
            ctx.SaveChanges();
            return notification;
        }

        public Notification ReadNotification(string id)
        {
            return ctx.Notifications.Include(n => n.Reaction).Include(n => n.User).SingleOrDefault(n => n.NotificationId == id);
        }
    }
}