using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PoC.DAL.EF;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.user;

namespace PoC.DAL.repo.Input
{
  public class InputRepository : IInputRepository
  {
    private PoCDbContext ctx;

    public InputRepository(PoCDbContext ctx)
    {
      this.ctx = ctx;
    }

    //Idea CRUD
    public Idea CreateIdea(Idea idea)
    {
      ctx.Ideas.Add(idea);
      ctx.SaveChanges();
      return idea;
    }

    public Idea UpdateIdea(Idea idea)
    {
      ctx.Ideas.Update(idea);
      ctx.SaveChanges();
      return idea;
    }

    public Idea DeleteIdea(string id)
    {
      Idea idea = ctx.Ideas.Find(id);
      List<Image> images = ctx.Images.Where(i => i.Idea == idea).ToList();
      foreach (var image in images)
      {
        ctx.Remove(image);
      }

      List<Video> videos = ctx.Videos.Where(v => v.Idea == idea).ToList();

      foreach (var video in videos)
      {
        ctx.Remove(video);
      }
      ctx.Entry(idea).State = EntityState.Modified;
      ctx.Ideas.Remove(idea);
      ctx.SaveChanges();
      return idea;
    }

    public Idea ReadIdea(string id)
    {
      //return ctx.Ideas.Find(id);
      //Idea ideaTest = ctx.Ideas.Include(i => i.Likes).Include(i => i.Shares).SingleOrDefault(idea => idea.Id == id);


      return ctx.Ideas.Include(i => i.Likes)
        .Include(i => i.Shares)
        .Include(i => i.Reactions).ThenInclude(a => a.User)
        .Include(i => i.ImageNames)
        .Include(i => i.VideoNames)
        .SingleOrDefault(idea => idea.Id == id);
    }
        public IEnumerable<Like> ReadLikesOfUser(string id)
        {
            return ctx.Likes.Include(l => l.User).Where(l => l.User.UserId == id);
        }
        
        
        

    //Like CRUD
    public Like CreateLike(Like like)
    {
      ctx.Likes.Add(like);
      ctx.SaveChanges();
      return like;
    }

    public Like UpdateLike(Like like)
    {
      ctx.Likes.Update(like);
      ctx.SaveChanges();
      return like;
    }

    public Like DeleteLike(string id)
    {
      Like like = ctx.Likes.Find(id);
      ctx.Likes.Remove(like);
      ctx.SaveChanges();
      return like;
    }

    public Like ReadLike(string id)
    {
      return ctx.Likes.Find(id);
    }


    //Reaction CRUD
    public Reaction CreateReaction(Reaction reaction)
    {
      ctx.Reactions.Add(reaction);
      ctx.SaveChanges();
      return reaction;
    }

    public Reaction UpdateReaction(Reaction reaction)
    {
      ctx.Reactions.Update(reaction);
      ctx.SaveChanges();
      return reaction;
    }

    public Reaction DeleteReaction(string id)
    {
      Reaction reaction = ctx.Reactions.Find(id);
      ctx.Reactions.Remove(reaction);
      ctx.SaveChanges();
      return reaction;
    }

    public Reaction ReadReaction(string id)
    {
      return ctx.Reactions.Include(r => r.Notifications).Include(r => r.User).SingleOrDefault(r => r.Id == id);
    }


    //Response CRUD
    public Response CreateResponse(Response response)
    {
      ctx.Responses.Add(response);
      ctx.SaveChanges();
      return response;
    }

    public Response UpdateResponse(Response response)
    {
      ctx.Responses.Update(response);
      ctx.SaveChanges();
      return response;
    }

    public Response DeleteResponse(string id)
    {
      Response response = ctx.Responses.Find(id);
      ctx.Responses.Remove(response);
      ctx.SaveChanges();
      return response;
    }

    public Response ReadResponse(string id)
    {
      return ctx.Responses.Find(id);
    }


    //Share CRUD
    public Share CreateShare(Share share)
    {
      ctx.Shares.Add(share);
      ctx.SaveChanges();
      return share;
    }

    public Share UpdateShare(Share share)
    {
      ctx.Shares.Update(share);
      ctx.SaveChanges();
      return share;
    }

    public Share DeleteShare(string id)
    {
      Share share = ctx.Shares.Find(id);
      ctx.Shares.Remove(share);
      ctx.SaveChanges();
      return share;
    }

        public IEnumerable<Idea> GetIdeasOfIdeation(string iid)
        {
            return ctx.Ideas.Include(i => i.Likes)
                .Include(i => i.Shares)
                .Include(i => i.Reactions)
                .Include(i => i.Votes).AsEnumerable();
        }
    public Share ReadShare(string id)
    {
      return ctx.Shares.Find(id);
    }


    //Vote CRUD
    public Vote CreateVote(Vote vote)
    {
      ctx.Votes.Add(vote);
      ctx.SaveChanges();
      return vote;
    }

    public Vote UpdateVote(Vote vote)
    {
      ctx.Votes.Update(vote);
      ctx.SaveChanges();
      return vote;
    }

    public Vote DeleteVote(string id)
    {
      Vote vote = ctx.Votes.Find(id);
      ctx.Votes.Remove(vote);
      ctx.SaveChanges();
      return vote;
    }

    public Vote ReadVote(string id)
    {
      return ctx.Votes.Find(id);
    }


    //Awnser CRUD
    public Answer CreateAwnser(Answer answer)
    {
      ctx.Awnsers.Add(answer);
      ctx.SaveChanges();
      return answer;
    }

    public Answer UpdateAwnser(Answer answer)
    {
      ctx.Awnsers.Update(answer);
      ctx.SaveChanges();
      return answer;
    }

    public Answer DeleteAwnser(string id)
    {
      Answer answer = ctx.Awnsers.Find(id);
      ctx.Awnsers.Remove(answer);
      ctx.SaveChanges();
      return answer;
    }

    public Answer ReadAwnser(string id)
    {
      return ctx.Awnsers.Find(id);
    }

    public Option CreateOption(Option option)
    {
      ctx.Options.Add(option);
      ctx.SaveChanges();
      return option;
    }

    public Option UpdateOption(Option option)
    {
      ctx.Options.Update(option);
      ctx.SaveChanges();
      return option;
    }

    public Option DeleteOption(string id)
    {
      Option option = ReadOption(id);
      ctx.Options.Remove(option);
      ctx.SaveChanges();
      return option;
    }

    public Option ReadOption(string id)
    {
      return ctx.Options.Find(id);
    }

    public IEnumerable<Idea> getIdeasOfIdeation(string iid)
    {
      return ctx.Ideas.Include(i => i.Likes)
        .Include(i => i.Shares)
        .Include(i => i.Reactions)
        .Include(i => i.ImageNames)
        .Include(i => i.VideoNames)
        .Include(i => i.Votes).AsEnumerable();
    }


    public IEnumerable<Reaction> ReadMarkedReactions()
    {
      return ctx.Reactions.Include(r => r.User).Include(r => r.Notifications).Where(r => r.Notifications.Count() != 0);
    }

    public Idea ReadIdeaByReaction(Reaction reaction)
    {
      return ctx.Ideas.Include(i => i.Ideation).Include(i => i.Reactions).ThenInclude(r => r.User)
        .SingleOrDefault(i => i.Reactions.Contains(reaction));
    }

    public IEnumerable<Idea> ReadMarkedIdeas()
    {
      return ctx.Ideas.Include(i => i.Notifications).Where(i => i.Notifications.Count() != 0);
    }
  }
}