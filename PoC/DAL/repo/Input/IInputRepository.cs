using System.Collections.Generic;
using PoC.Domain.input;
using PoC.Domain.inputmethod;

namespace PoC.DAL.repo.Input
{
    public interface IInputRepository
    {
        //Idea CRUD
        Idea CreateIdea(Idea idea);
        Idea UpdateIdea(Idea idea);
        Idea DeleteIdea(string id);
        Idea ReadIdea(string id);
        //Like CRUD
        Like CreateLike(Like like);
        Like UpdateLike(Like like);
        Like DeleteLike(string id);
        Like ReadLike(string id);
        //Reaction CRUD
        Reaction CreateReaction(Reaction reaction);
        Reaction UpdateReaction(Reaction reaction);
        Reaction DeleteReaction(string id);
        Reaction ReadReaction(string id);
        //Response CRUD
        Response CreateResponse(Response response);
        Response UpdateResponse(Response response);
        Response DeleteResponse(string id);
        Response ReadResponse(string id);
        //Share CRUD
        Share CreateShare(Share share);
        Share UpdateShare(Share share);
        Share DeleteShare(string id);
        Share ReadShare(string id);
        //Vote CRUD
        Vote CreateVote(Vote vote);
        Vote UpdateVote(Vote vote);
        Vote DeleteVote(string id);
        Vote ReadVote(string id);
        //Awnser CRUD
        Answer CreateAwnser(Answer answer);
        Answer UpdateAwnser(Answer answer);
        Answer DeleteAwnser(string id);
        Answer ReadAwnser(string id);
        //Option CRUD
        Option CreateOption(Option option);
        Option UpdateOption(Option option);
        Option DeleteOption(string id);
        Option ReadOption(string id);
        IEnumerable<Idea> GetIdeasOfIdeation(string iid);
        
        
        IEnumerable<Reaction> ReadMarkedReactions();
        Idea ReadIdeaByReaction(Reaction reaction);
        IEnumerable<Idea> ReadMarkedIdeas();
        IEnumerable<Like> ReadLikesOfUser(string id);
    }
}