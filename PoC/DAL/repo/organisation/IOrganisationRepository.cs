using System.Collections.Generic;
using PoC.Domain.organisation;
using PoC.Domain.project;

namespace PoC.DAL.repo.organisation
{
    public interface IOrganisationRepository
    {
        //organisation CRUD
        Organisation CreateOrganisation(Organisation organisation);
        Organisation UpdateOrganisation(Organisation organisation);
        Organisation DeleteOrganisation(string id);
        Organisation ReadOrganisation(string id);
        Organisation ReadOrganisationByName(string org);
        List<Organisation> ReadAllOrganisations();
        List<Project> ReadProjectsOfOrganisation(string orgId);
    }
}