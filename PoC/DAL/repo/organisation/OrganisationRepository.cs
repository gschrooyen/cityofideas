using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PoC.DAL.EF;
using PoC.Domain.organisation;
using PoC.Domain.project;

namespace PoC.DAL.repo.organisation
{
    public class OrganisationRepository: IOrganisationRepository
    {
        
        private PoCDbContext ctx;

        public OrganisationRepository(PoCDbContext ctx)
        {
            this.ctx = ctx;
        }
        public Organisation CreateOrganisation(Organisation organisation)
        {
            ctx.Organisations.Add(organisation);
            ctx.SaveChanges();
            return organisation;
        }

        public Organisation UpdateOrganisation(Organisation organisation)
        {
            ctx.Organisations.Update(organisation);
            ctx.SaveChanges();
            return organisation;
        }

        public Organisation DeleteOrganisation(string id)
        {
            Organisation org = ctx.Organisations.Find(id);
            ctx.Organisations.Remove(org);
            ctx.SaveChanges();
            return org;

        }

        public Organisation ReadOrganisation(string id)
        {
            return ctx.Organisations.Include(o => o.Projects).SingleOrDefault(o => o.id == id);
            
        }

        public Organisation ReadOrganisationByName(string org)
        {
            var organisation = ctx.Organisations.Include(o => o.Projects).ThenInclude(p => p.CurrentFase).ThenInclude(fase => fase.InputMethods).SingleOrDefault(o => o.FormattedName == org);
            return organisation;
        }

        public List<Organisation> ReadAllOrganisations()
        {
            List<Organisation> organisations = ctx.Organisations.Include(o => o.Projects).ToList();
            return organisations.OrderBy(o => o.Name.Replace(" ","").ToLower()).ToList();
        }

        public List<Project> ReadProjectsOfOrganisation(string orgId)
        {
            Organisation o = ReadOrganisation(orgId);
            return o.Projects.ToList();
        }
    }
}