
using System.Collections.Generic;
using PoC.Domain.iot;

namespace PoC.DAL.repo.iot
{
    public interface IIotRepository
    {
        //Poster CRUD
        Poster CreatePoster(Poster poster);
        Poster UpdatePoster(Poster poster);
        Poster DeletePoster(string id);
        Poster ReadPoster(string id);
        //Placement CRUD
        Placement CreatePlacement(Placement placement);
        Placement UpdatePlacement(Placement placement);
        Placement DeletePlacement(string id);
        Placement ReadPlacement(string id);
        List<Poster> ReadPosters();
        Button GetButton(string btnNumber);
    }
}