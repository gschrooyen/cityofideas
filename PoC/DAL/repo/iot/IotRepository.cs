using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PoC.DAL.EF;
using PoC.Domain.iot;

namespace PoC.DAL.repo.iot
{
    public class IotRepository: IIotRepository
    {
        private readonly PoCDbContext _ctx;

        public IotRepository(PoCDbContext ctx)
        {
            _ctx = ctx;
        }
        public Poster CreatePoster(Poster poster)
        {
            _ctx.Posters.Add(poster);
            _ctx.SaveChanges();
            return poster;
        }

        public Poster UpdatePoster(Poster poster)
        {
            _ctx.Posters.Update(poster);
            _ctx.SaveChanges();
            return poster;
        }

        public Poster DeletePoster(string id)
        {
            Poster poster = _ctx.Posters.Find(id);
            _ctx.Posters.Remove(poster);
            _ctx.SaveChanges();
            return poster;
        }

        public Poster ReadPoster(string id)
        {
            return _ctx.Posters.Include(p => p.Ideation).Include(p => p.Idea).Include(p => p.Question).Include(p => p.CurrentPlacement).ThenInclude(p => p.Location).Include(p => p.CurrentPlacement).ThenInclude(p => p.Project).Include(p => p.Buttons).ThenInclude(b => b.Option).First(p=>p.Id==id);
        }

        public Placement CreatePlacement(Placement placement)
        {
            _ctx.Placements.Add(placement);
            _ctx.SaveChanges();
            return placement;
        }

        public Placement UpdatePlacement(Placement placement)
        {
            _ctx.Placements.Update(placement);
            _ctx.SaveChanges();
            return placement;
        }

        public Placement DeletePlacement(string id)
        {
            Placement placement = _ctx.Placements.Find(id);
            _ctx.Placements.Remove(placement);
            _ctx.SaveChanges();
            return placement;
        }

        public Placement ReadPlacement(string id)
        {
            return _ctx.Placements.Include(p => p.Location).Include(p => p.Project).SingleOrDefault(p => p.Id == id);
        }

        public List<Poster> ReadPosters()
        {
            return _ctx.Posters.Include(p => p.Question).ThenInclude(q => q.QuestionList).Include(p => p.Ideation).Include(p =>p.Buttons    ).Include(p => p.CurrentPlacement).ThenInclude(p => p.Location).ToList();
        }

        public Button GetButton(string btnNumber)
        {
            
            return _ctx.Buttons.Include(b => b.Poster).ThenInclude(p => p.Question).ThenInclude(q => q.Awnsers).Include(b => b.Option).Include(b => b.Input).Include(p => p.Poster).ThenInclude(p => p.Ideation).First(b => b.Id == btnNumber);
        }
    }
}