using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PoC.DAL.EF;
using PoC.Domain.user;

namespace PoC.DAL.repo.user
{
    public class UserRepository: IUserRepository
    {
        
        private PoCDbContext ctx;

        public UserRepository(PoCDbContext ctx)
        {
            this.ctx = ctx;
        }
        public User CreateUser(User user)
        {
            ctx.Users.Add(user);
            ctx.SaveChanges();
            return user;
        }

        public User UpdateUser(User user)
        {
            ctx.Users.Update(user);
            ctx.SaveChanges();
            return user;
        }

        public User DeleteUser(string id)
        {
            User u = ctx.Users.Find(id);
            ctx.Remove(u);
            return u;
        }

        public User ReadUser(string id)
        {
            return ctx.Users.Include(u => u.Inputs).Include(u => u.Notifications).Where(u => !u.Deleted).SingleOrDefault(u => u.UserId == id);
        }

        public string ReturnUserName(User user) {
            return user.UserName;
        }

        public User ReadUserWithEmail(string email)
        {
            return ctx.Users.First(u => u.Email == email);
        }

        public List<User> ReadUsers(string org)
        {
            return ctx.Users.Where(u => u.Organisation.FormattedName == org).Where(u => !u.Deleted).ToList();
        }

        public List<User> ReadUsersOfOrganisation(string org)
        {
            return ctx.Users.Where(u => u.Organisation.FormattedName == org && !u.Blocked).Include(u => u.Inputs).ToList();
        }

        public IEnumerable<User> ReadUsers()
        {
            return ctx.Users;
        }
    }
}