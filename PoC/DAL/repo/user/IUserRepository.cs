using System;
using System.Collections.Generic;
using PoC.Domain.user;

namespace PoC.DAL.repo.user
{
    public interface IUserRepository
    {
        //user CRUD
        User CreateUser(User user);
        User UpdateUser(User user);
        User DeleteUser(string id);
        User ReadUser(string id);
        String ReturnUserName(User user);
        User ReadUserWithEmail(string email);
        List<User> ReadUsers(string org);
        List<User> ReadUsersOfOrganisation(string org);
        IEnumerable<User> ReadUsers();
    }
}