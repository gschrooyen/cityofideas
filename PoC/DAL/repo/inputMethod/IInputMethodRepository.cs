using System.Collections.Generic;
using PoC.Domain.inputmethod;
using PoC.Domain.inputMethod;
using PoC.Domain.project;

namespace PoC.DAL.repo.inputMethod
{
    public interface IInputMethodRepository
    {
        //Questionlist CRUD
        QuestionList CreateQuestionList(QuestionList questionList);
        QuestionList UpdateQuestionList(QuestionList questionList);
        QuestionList DeleteQuestionList(string id);
        QuestionList ReadQuestionList(string id);
        QuestionList ReadQuestionListWithoutAnswers(string id);
        //Question CRUD
        Question CreateQuestion(Question question);
        Question UpdateQuestion(Question question);
        Question DeleteOpenQuestion(string id);
        Question DeleteChoiceQuestion(string id);
        Question ReadOpenQuestion(string id);
        Question ReadChoiceQuestion(string id);

        IEnumerable<Question> ReadQuestionsOfList(string listId);
        //Ideation CRUD
        Ideation CreateIdeation(Ideation ideation);
        Ideation UpdateIdeation(Ideation ideation);
        Ideation DeleteIdeation(string id);
        Ideation ReadIdeation(string id);
        
        //Options
        IEnumerable<Option> ReadOptionOfChoiceQuestion(ChoiceQuestion question);
        IEnumerable<Ideation> ReadIdeationsFromFase(Fase projectCurrentFase);
        IEnumerable<QuestionList> ReadQuestionListFromFase(Fase projectCurrentFase);
    }
}