using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PoC.DAL.EF;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.inputMethod;
using PoC.Domain.project;

namespace PoC.DAL.repo.inputMethod
{
    public class InputMethodRepository : IInputMethodRepository
    {
        private PoCDbContext ctx;

        public InputMethodRepository(PoCDbContext ctx)
        {
            this.ctx = ctx;
        }

        public QuestionList CreateQuestionList(QuestionList questionList)
        {
            ctx.QuestionLists.Add(questionList);
            ctx.SaveChanges();
            return questionList;
        }

        public QuestionList UpdateQuestionList(QuestionList questionList)
        {
            ctx.QuestionLists.Update(questionList);
            ctx.SaveChanges();
            return questionList;
        }

        public QuestionList DeleteQuestionList(string id)
        {
            QuestionList questionList = ctx.QuestionLists.Find(id);
            ctx.QuestionLists.Remove(questionList);
            ctx.SaveChanges();
            return questionList;
        }

        public QuestionList ReadQuestionList(string id)
        {
            return ctx.QuestionLists
                .Include(i => i.Questions).ThenInclude(i => i.Awnsers)
                .Include(i => i.Fase)
                .SingleOrDefault(i => i.Id == id);
        }

        public QuestionList ReadQuestionListWithoutAnswers(string id)
        {
            return ctx.QuestionLists
                .Include(i => i.Questions)
                .Include(i => i.Fase)
                .SingleOrDefault(i => i.Id == id);
        }

        public Question CreateQuestion(Question question)
        {
            if (question.GetType() == typeof(OpenQuestion))
            {
                ctx.OpenQuestions.Add((OpenQuestion) question);
                ctx.SaveChanges();
                return question;
            }

            ctx.ChoiceQuestions.Add((ChoiceQuestion) question);
            ctx.SaveChanges();
            return question;
        }

        public Question UpdateQuestion(Question question)
        {
            if (question.GetType() == typeof(OpenQuestion))
            {
                ctx.OpenQuestions.Update((OpenQuestion) question);
                ctx.SaveChanges();
                return question;
            }

            ctx.ChoiceQuestions.Update((ChoiceQuestion) question);
            ctx.SaveChanges();
            return question;
        }

        public Question DeleteOpenQuestion(string id)
        {
            Question question = ctx.OpenQuestions.Find(id);
            ctx.Remove(question);
            ctx.SaveChanges();
            return question;
        }

        public Question DeleteChoiceQuestion(string id)
        {
            Question question = ctx.ChoiceQuestions.Find(id);
            IEnumerable<Option> options = ctx.Options.Where(o => o.Question == question).AsEnumerable();
            foreach (var option in options)
            {
                ctx.Remove(option);
            }

            ctx.Entry(question).State = EntityState.Modified;
            ctx.Remove(question);
            ctx.SaveChanges();
            return question;
        }

        public Question ReadOpenQuestion(string id)
        {
            return ctx.OpenQuestions.Find(id);
        }

        public Ideation ReadIdeation(string id)
        {
            Ideation ideation = ctx.Ideations.Include(i => i.Shares)
                .Include(i => i.Ideas).ThenInclude(i => i.Reactions).ThenInclude(r => r.User)
                .Include(i => i.Ideas).ThenInclude(i => i.Shares)
                .Include(i => i.Ideas).ThenInclude(i => i.Likes)
                .Include(i => i.Ideas).ThenInclude(i => i.ImageNames)
                .Include(i => i.Ideas).ThenInclude(i => i.VideoNames)
                .SingleOrDefault(i => i.Id == id);
            if (ideation != null)
            {
                IEnumerable<Idea> ideas = ideation.Ideas.OrderBy(i => i.Time);
                ideation.Ideas = ideas.ToList();
                ctx.SaveChanges();
            }

            return ideation;
        }

        public Question ReadChoiceQuestion(string id)
        {
            return ctx.ChoiceQuestions.Include(q => q.Options).First(q => q.id == id);
        }

        public IEnumerable<Question> ReadQuestionsOfList(string listId)
        {
            List<Question> questions =
                new List<Question>(ctx.OpenQuestions.Where(q => q.QuestionList.Id == listId).ToList());
            questions.AddRange(ctx.ChoiceQuestions.Where(q => q.QuestionList.Id == listId).ToList());
            questions.Sort((q1, q2) => String.Compare(q1.id, q2.id, StringComparison.Ordinal));
            return questions;
        }

        public Ideation CreateIdeation(Ideation ideation)
        {
            ctx.Ideations.Add(ideation);
            ctx.SaveChanges();
            return ideation;
        }

        public Ideation UpdateIdeation(Ideation ideation)
        {
            ctx.Ideations.Update(ideation);
            ctx.SaveChanges();
            Console.WriteLine(ideation.Shares.Count());
            return ideation;
        }

        public Ideation DeleteIdeation(string id)
        {
            Ideation ideation = ctx.Ideations.Find(id);
            ctx.Ideations.Remove(ideation);
            ctx.SaveChanges();
            return ideation;
        }

        public IEnumerable<Option> ReadOptionOfChoiceQuestion(ChoiceQuestion question)
        {
            return ctx.Options.Where(o => o.Question == question).AsEnumerable();
        }

        public IEnumerable<Ideation> ReadIdeationsFromFase(Fase projectCurrentFase)
        {
            return ctx.Ideations.Where(i => i.Fase == projectCurrentFase).Include(i => i.Shares).Include(i => i.Ideas)
                .Where(i => i.HideIdeation == false);
        }

        public IEnumerable<QuestionList> ReadQuestionListFromFase(Fase projectCurrentFase)
        {
            return ctx.QuestionLists.Where(q => q.Fase == projectCurrentFase)
                .Include(q => q.Shares)
                .Include(q => q.Questions); //.ThenInclude(q => q.Awnsers).ThenInclude(q => q.Votes);
        }
    }
}