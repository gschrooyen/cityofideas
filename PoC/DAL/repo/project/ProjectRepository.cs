using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PoC.DAL.EF;
using PoC.Domain.inputmethod;
using PoC.Domain.project;

namespace PoC.DAL.repo.project
{
  public class ProjectRepository : IProjectRepository
  {
    private PoCDbContext ctx;

    public ProjectRepository(PoCDbContext ctx)
    {
      this.ctx = ctx;
    }

    public Project CreateProject(Project project)
    {
      //this.Validate(project);
      ctx.Projects.Add(project);
      ctx.SaveChanges();
      return project;
    }

    public Project UpdateProject(Project project)
    {
      //this.Validate(project);
      ctx.Projects.Update(project);
      ctx.SaveChanges();
      Console.WriteLine("updated project");
      return project;
    }

    // Nog probleem ivm PastFases, in db ligt er linkt maar wordt toch niet toegevoegd?
    public Project UpdateFaseProject(Project project, Fase fase)
    {
      project.PastFases.Add(project.CurrentFase);
      project.CurrentFase = fase;
      ctx.Projects.Update(project);
      ctx.SaveChanges();
      return project;
    }

    public Project UpdateIdeationProject(Project project, Ideation ideation)
    {
      project.CurrentFase.InputMethods.Add(ideation);
      ctx.Projects.Update(project);
      ctx.SaveChanges();
      return project;
    }

    public Project UpdateQuestionListProject(Project project, QuestionList questionList)
    {
      project.CurrentFase.InputMethods.Add(questionList);
      ctx.Projects.Update(project);
      ctx.SaveChanges();
      return project;
    }

    public Project DeleteProject(string id)
    {
      Project project = ctx.Projects.Find(id);
      ctx.Projects.Remove(project);
      ctx.SaveChanges();
      return project;
    }

    public Project ReadProject(string id)
    {
      return ctx.Projects.Include(p => p.CurrentFase).Include(p => p.CurrentFase.InputMethods).Include(p => p.PastFases)
        .Include(p => p.Shares).Include(p => p.Likes).ThenInclude(l => l.User).First(p => p.Id == id);
    }
    public Fase CreateFase(Fase fase)
    {
      ctx.Fases.Add(fase);
      ctx.SaveChanges();
      return fase;
    }

    public Fase UpdateFase(Fase fase)
    {
      ctx.Fases.Update(fase);
      ctx.SaveChanges();
      return fase;
    }

    public Fase UpdateFase(string id)
    {
      Fase fase = ctx.Fases.Single(f => f.Id == id);
      ctx.Fases.Update(fase);
      ctx.SaveChanges();
      return fase;
    }

    public Fase DeleteFase(string id)
    {
      Fase fase = ctx.Fases.Find(id);
      ctx.Fases.Remove(fase);
      ctx.SaveChanges();
      return fase;
    }

    public Fase ReadFase(string id)
    {
      return ctx.Fases.Find(id);
    }

    public IEnumerable<Project> ReadProjects()
    {
      return ctx.Projects.Include(f => f.CurrentFase).Include(f => f.CurrentFase.InputMethods).Include(l => l.Likes)
        .Include(l => l.Shares).AsEnumerable();
    }

    public IEnumerable<InputMethod> ReadInputmethodsOfFase(string faseId)
    {
      Fase fase = ReadFase(faseId);

      List<InputMethod> questionLists = new List<InputMethod>();
      questionLists = new List<InputMethod>(ctx.QuestionLists.Where(p => p.Fase == fase).ToList());

      List<InputMethod> ideations = new List<InputMethod>();
      ideations = new List<InputMethod>(ctx.Ideations.Where(p => p.Fase == fase).ToList());

      List<InputMethod> inputMethods = new List<InputMethod>();
      inputMethods.AddRange(questionLists);
      inputMethods.AddRange(ideations);

      return inputMethods;
    }

    public Project ReadProjectFromFaseId(string faseId)
    {
      return ctx.Projects.Include(p => p.Likes)
        .Include(p => p.Shares)
        .Include(p => p.CurrentFase).ThenInclude(f => f.InputMethods)
        .Include(p => p.PastFases).ThenInclude(f => f.InputMethods)
        .First(p => p.CurrentFase.Id == faseId);
    }

    public List<Fase> ReadPastFasesOfProject(string pId)
    {
      Project p = ReadProject(pId);
      return p.PastFases.ToList();
    }

   /* private void Validate(Project project)
    {
      List<ValidationResult> errors = new List<ValidationResult>();
      Validator.TryValidateObject(project, new ValidationContext(project), errors, validateAllProperties: true);

      //if(!valid) throw new ValidationException("Project niet geldig!");
    }
    */
  }
}