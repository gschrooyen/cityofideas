using System.Collections.Generic;
using PoC.Domain.inputmethod;
using PoC.Domain.project;

namespace PoC.DAL.repo.project
{
    public interface IProjectRepository
    {
        //project CRUD
        Project CreateProject(Project project);
        Project UpdateProject(Project project);
        Project DeleteProject(string id);
        Project ReadProject(string id);
        
        // Update Ideation/QuestionList/faseProject voor linking project, later verwijderen indien niet noodzakelijk
        Project UpdateIdeationProject(Project project, Ideation ideation);
        Project UpdateQuestionListProject(Project project, QuestionList questionList);
        Project UpdateFaseProject(Project project, Fase fase);
        
        //fase CRUD
        Fase CreateFase(Fase fase);
        Fase UpdateFase(Fase fase);
        Fase UpdateFase(string id);
        Fase DeleteFase(string id);
        Fase ReadFase(string id);
        IEnumerable<Project> ReadProjects();
        IEnumerable<InputMethod> ReadInputmethodsOfFase(string faseId);

        Project ReadProjectFromFaseId(string faseId);

        List<Fase> ReadPastFasesOfProject(string pId);
    }
}