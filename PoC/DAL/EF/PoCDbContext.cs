﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;
using PoC.Domain.iot;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.inputMethod;
using PoC.Domain.moderation;
using PoC.Domain.organisation;
using PoC.Domain.project;
using PoC.Domain.user;


namespace PoC.DAL.EF
{
    public class PoCDbContext : DbContext
    {
       

        public PoCDbContext(DbContextOptions options) : base(options)
        {
            PoCDbInitializer.Initialize(this);
        }

        public PoCDbContext()
        {
            PoCDbInitializer.Initialize(this, dropCreateDatabase: true);
        }

        //Input
        public DbSet<Answer> Awnsers { get; set; }
        public DbSet<Idea> Ideas { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Reaction> Reactions { get; set; }
        public DbSet<Response> Responses { get; set; }
        public DbSet<Share> Shares { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Video> Videos { get; set; }
        
        //InputMethod
        public DbSet<Ideation> Ideations { get; set; }
        public DbSet<OpenQuestion> OpenQuestions { get; set; }
        public DbSet<ChoiceQuestion> ChoiceQuestions { get; set; }
        public DbSet<QuestionList> QuestionLists { get; set; }
        
        //IoT
        public DbSet<Placement> Placements { get; set; }
        public DbSet<Poster> Posters { get; set; }
        
        //moderation
        public DbSet<Notification> Notifications { get; set; }
        
        //organisation
        public DbSet<Organisation> Organisations { get; set; }
        
        //project
        public DbSet<Project> Projects { get; set; }
        public DbSet<Fase> Fases { get; set; }
        
        //user
        public DbSet<User> Users { get; set; }
        public DbSet<Button> Buttons { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Data Source=..\\PoCDatabase.db");
            optionsBuilder.UseMySql("Server=35.205.26.168; Port=3306; Database=pocdatabase; User=root; Pwd=paswoord");

            optionsBuilder.UseLoggerFactory(new LoggerFactory(
               new[] { new DebugLoggerProvider(
                    (category, level) => category == DbLoggerCategory.Database.Command.Name
                                         && level == LogLevel.Information
                )}
           ));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
//            
//            
//            //users
//            modelBuilder.Entity<User>().Property<int>("UserOrgFK");
//            modelBuilder.Entity<User>().HasOne(u => u.Organisation).WithMany(o => o.Users).HasForeignKey("UserOrgFK");
//            modelBuilder.Entity<User>().Property<int>("UserNotFK");
//            modelBuilder.Entity<User>().HasMany(u => u.Notifications).WithOne(n => n.User).HasForeignKey("UserNotFK");
//            modelBuilder.Entity<User>().Property<int>("UserInpFK");
//            modelBuilder.Entity<User>().HasMany(u => u.Inputs).WithOne(i => i.User).HasForeignKey("UserInptFK");
//            
//            //project
//            modelBuilder.Entity<Project>().Property<int>("ProjOrgFK");
//            modelBuilder.Entity<Project>().HasOne(p => p.Organisation).WithMany(o => o.Projects).HasForeignKey("ProjOrgFK");
//            modelBuilder.Entity<Project>().Property<int>("ProjPlacFK");
//            modelBuilder.Entity<Project>().HasMany(p => p.Placements).WithOne(p => p.Project).HasForeignKey("ProjPlacFK");
//
//            
////            modelBuilder.Entity<Project>().Property<int>("ProjSharesFk");
////            modelBuilder.Entity<Project>().HasMany(p => p.Shares).WithOne(s => s.Shareable);
//            
//            

            
             
           

            
            
            modelBuilder.Entity<QuestionList>().HasOne(ql => ql.Fase);
            
            modelBuilder.Entity<QuestionList>().HasMany(ql => ql.Questions);

            //modelBuilder.Entity<Reaction>().HasMany(r => r.Notifications).WithOne(n => n.Reaction);


        }
    }
}
