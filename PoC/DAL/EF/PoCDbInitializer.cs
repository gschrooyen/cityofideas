using System;
using System.Collections.Generic;
using System.Linq;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.inputMethod;
using PoC.Domain.iot;
using PoC.Domain.moderation;
using PoC.Domain.organisation;
using PoC.Domain.project;
using PoC.Domain.user;

namespace PoC.DAL.EF
{
  public static class PoCDbInitializer
  {


    public static void Initialize(PoCDbContext context, bool dropCreateDatabase = false)
    {
      if (dropCreateDatabase)
      {
        if (context.Database.EnsureDeleted())
        {
          if (context.Database.EnsureCreated())
          {
            Console.WriteLine("database has been deleted and created");
            Seed(context);
            
          }
        }
      }
      else
      {
        try
        {
          Console.WriteLine(context.Projects.First());
        }
        catch (Exception e)
        {
                    
          Console.WriteLine("the database does not exist, ");
                    
          if (context.Database.EnsureCreated())
          {
            Seed(context);
          }
        }
      }
    }


    
    private static void Seed(PoCDbContext context)
        {
            //USERS
            User user1 = new User();
            user1.Email = "ruben.vanloo@student.kdg.be";
            user1.PasswordHash = "aaaaaa";
            user1.FirstName = "Ruben";
            user1.LastName = "Vanloo";
            user1.UserName = "ruben.v";
            context.Users.Add(user1);
      
            User user2 = new User();
            user2.Email = "philippe.colpaert@student.kdg.be";
            user2.PasswordHash = "aaaaaa";
            user2.FirstName = "Philippe";
            user2.LastName = "Colpaert";
            user2.UserName = "philippe.c";
            context.Users.Add(user2);
      
            User user3 = new User();
            user3.Email = "eline.meeusen@student.kdg.be";
            user3.PasswordHash = "aaaaaa";
            user3.FirstName = "Eline";
            user3.LastName = "Meeusen";
            user3.UserName = "eline.m";
            context.Users.Add(user3);
      
            User user4 = new User();
            user4.Email = "ward.driesen@student.kdg.be";
            user4.PasswordHash = "aaaaaa";
            user4.FirstName = "Ward";
            user4.LastName = "Driesen";
            user4.UserName = "ward.d";
            context.Users.Add(user4);
      
            User user5 = new User();
            user5.Email = "glenn.schrooyen@student.kdg.be";
            user5.PasswordHash = "aaaaaa";
            user5.FirstName = "Glenn";
            user5.LastName = "Schrooyen";
            user5.UserName = "glenn.s";
            context.Users.Add(user5);
      
            User user6 = new User();
            user6.Email = "yohann.vetters@student.kdg.be";
            user6.PasswordHash = "aaaaaa";
            user6.FirstName = "Yohann";
            user6.LastName = "Vetters";
            user6.UserName = "yohann.v";
            context.Users.Add(user6);
      
            //ORGANISATION ANTWERPEN
            Organisation org1 = new Organisation();
            org1.Name = "Antwerpen";
            org1.FormattedName = org1.Name.ToUpper();
            org1.ZipCode = "2000";
            org1.Image = "Antwerpen_image.jpeg";
            org1.Logo = "Antwerpen_logo.jpg";
            org1.Description = "Welkom op het platform van stad Antwerpen. Op dit platform vind je de diverse projecten waarvan de stad uw mening wil horen." +
                               "Heb je een opmerking of beter idee betreffende het project? Voel je dan vrij om dit achter te laten." +
                               "Twijfel dus zeker niet en bekijk de actuele projecten.";
            org1.FeaturedText = "Er zijn nieuwe projecten toegevoegd, vergeet zeker geen kijkje te nemen! ";

            org1.TelephoneNumber = "0470809060";
            org1.Street = "Langewapperstraat 250";
            org1.City = "Antwerpen";
            org1.Country = "België";
            org1.Email = "AntwerpenStad@antwerpen.be";

            //PROJECT 1: DRINKBAAR WATER VOOR IEDEREEN
            Project project = new Project();
            project.ImageName = "drinkfontein.jpg";
            project.Name = "Drinkbaar water voor iedereen";
            project.Organisation = org1;
            List<Like> likes = new List<Like>();
            List<Share> shares = new List<Share>();
            for (int i = 0; i < 35; i++)
            {
                likes.Add(new Like());
            }
            for (int i = 0; i < 28; i++)
            {
                shares.Add(new Share());
            }
            project.Likes = likes;
            project.Shares = shares;
            project.Begin = new DateTime(2019, 1, 1);
            project.End = DateTime.Now;
            project.Description =
                "Meer dan ooit is drinkbaar water 1 van de rechten van de mens. Het openbaar beschikbaarstellen van drinkwater kan in dat opzicht niet achterblijven." +
                "We zijn dan ook op zoek naar suggesties voor het plaatsen van nieuwe drinkfonteinen.";
            project.MainQuestion = "wilt u meer groen?";
            project.Category = Category.GEZONDHEID;

            //QuestionList 1 van fase 1 van project 1 (drinkbaar water)
            List<Share> shareList = new List<Share>();
            for (int i = 0; i < 12; i++)
            {
                shareList.Add(new Share());
            }

            QuestionList questionList = new QuestionList();
            questionList.Title = "Hoe krijgen we meer drinkbaar water in de stad?";
            questionList.Description =
                "Met het invullen van deze vragenlijst helpt u ons met het verbeteren van het project 'drinkbaar water?'+" +
                "Uw mening/input zal ons voor een groot deel begeleiden doorheen het project. Hopelijk kunnen we op uw +" +
                "eerlijkheid vertrouwen! Hartelijk dank!";
            questionList.Begin = new DateTime(2000, 1, 1);
            questionList.End = DateTime.Now;
            questionList.Shares = shareList;
            questionList.Open = true;
            List<Question> questions = new List<Question>();
            AddQuestions(questions, 1);
            questionList.Questions = questions;


            //Fase 1 van project 1 (drinkbaar water)
            Fase faseProject1 = new Fase();
            faseProject1.FaseName = "DrinkFase";
            faseProject1.ProjectFase = ProjectFase.STARTED;
            faseProject1.Begin = DateTime.Now;
            faseProject1.End = DateTime.Now;
            project.CurrentFase = faseProject1;

            faseProject1.InputMethods = new List<InputMethod>();
            faseProject1.InputMethods.Add(questionList);
            questionList.Fase = faseProject1;

            //Ideation 1 (fontein) van fase 1 van project 1 (drinkbaar water)
            Ideation waterIdeation1 = new Ideation();
            waterIdeation1.TitleIdeation = "Locatie drinkfonteintjes?";
            waterIdeation1.Id = "test";
            waterIdeation1.DescriptionIdeation =
                "Verschillende locaties in Antwerpen kunnen wel gebruikmaken van drinkfonteintjes. Stem hier op je favoriete locaties" +
                " of stel zelf een locatie voor.";
            waterIdeation1.Begin = new DateTime(2000, 1, 1);
            waterIdeation1.End = DateTime.Now;

            //Idea 1 van ideation 1 (fontein) van fase 1 van project 1 (drinkbaar water)
            Idea fonteinIdea1 = new Idea();
            fonteinIdea1.Ideation = waterIdeation1;
            fonteinIdea1.Title = "Drinkfontein Groenplaats";
            fonteinIdea1.Description =
                "De Groenplaats is een populaire plek voor mensen om rond te hangen en af te spreken. Wanneer mensen dorst hebben" +
                "en ze enkel een beetje water willen zou dit een geweldige plek zijn om een paar drinkfonteinen te plaatsen";
            fonteinIdea1.User = user1;
            List<Like> likesFontein1 = new List<Like>();
            for (int i = 0; i < 16; i++)
            {
                likesFontein1.Add(new Like());
            }
            fonteinIdea1.Likes = likesFontein1;
            List<Share> sharesFontein1 = new List<Share>();
            for (int i = 0; i < 7; i++)
            {
                sharesFontein1.Add(new Share());
            }

            fonteinIdea1.Shares = sharesFontein1;
            //Reacties
            List<Reaction> reactiesFontein1 = new List<Reaction>();
            Reaction reaction1Fontein1 = new Reaction();
            reaction1Fontein1.User = user2;
            reaction1Fontein1.Deleted = false;
            reaction1Fontein1.Idea = fonteinIdea1;
            reaction1Fontein1.Comment = "Ik vind dit een super goed idee!";
            Reaction reaction2Fontein1 = new Reaction();
            reaction2Fontein1.User = user3;
            reaction2Fontein1.Deleted = false;
            reaction2Fontein1.Idea = fonteinIdea1;
            reaction2Fontein1.Comment = "Ik ben geen fan van dit idee, iedereen kan de pot op!!!";
            List<Notification> notsFontein1 = new List<Notification>();
            Notification not1Fontein1 = new Notification(
                NotificationType.INAPPROPRIATE_LANGUAGE,
                "test",
                reaction2Fontein1
            );
            notsFontein1.Add(not1Fontein1);
      
            reactiesFontein1.Add(reaction1Fontein1);
            reactiesFontein1.Add(reaction2Fontein1);
            fonteinIdea1.Reactions = reactiesFontein1;
            fonteinIdea1.Notifications = notsFontein1;
      
            //Idea 2 van ideation 1 (fontein) van fase 1 van project 1 (drinkbaar water) + reacties
            Idea fonteinIdea2 = new Idea();
            fonteinIdea2.Ideation = waterIdeation1;
            fonteinIdea2.Title = "Drinkfontein Paardenmarkt";
            fonteinIdea2.Description =
                "De Paardenmarkt is een populaire locatie voor studenten. Indien studenten snel iets willen drinken zonder dat ze" +
                "geld op zak hebben zouden ze dan van deze fonteintjes gebruik kunnen maken.";
            fonteinIdea2.User = user2;
            List<Like> likesFontein2 = new List<Like>();
            for (int i = 0; i < 31; i++)
            {
                likesFontein2.Add(new Like());
            }
            fonteinIdea2.Likes = likesFontein2;
            List<Share> sharesFontein2 = new List<Share>();
            for (int i = 0; i < 17; i++)
            {
                sharesFontein2.Add(new Share());
            }

            fonteinIdea2.Shares = sharesFontein2;
            //Reacties
            List<Reaction> reactiesFontein2 = new List<Reaction>();
            Reaction reaction1Fontein2 = new Reaction();
            reaction1Fontein2.User = user4;
            reaction1Fontein2.Deleted = false;
            reaction1Fontein2.Idea = fonteinIdea2;
            reaction1Fontein2.Comment = "Ik vind dit een super idee!";
            Reaction reaction2Fontein2 = new Reaction();
            reaction2Fontein2.User = user5;
            reaction2Fontein2.Deleted = false;
            reaction2Fontein2.Idea = fonteinIdea2;
            reaction2Fontein2.Comment = "Ik vind dit een super idee!";
            List<Notification> notsFontein2 = new List<Notification>();
            Notification not1Fontein2 = new Notification(
                NotificationType.DOUBLE,
                "test",
                reaction2Fontein2
            );
            notsFontein2.Add(not1Fontein2);
      
            reactiesFontein2.Add(reaction1Fontein2);
            reactiesFontein2.Add(reaction2Fontein2);
            fonteinIdea2.Reactions = reactiesFontein2;
            fonteinIdea2.Notifications = notsFontein2;
      
            //Ideas toevoegen aan ideation 1 (fontein) van fase 1 van project 1 (drinkbaar water)
            List<Idea> ideasFontein = new List<Idea>();
            ideasFontein.Add(fonteinIdea1);
            ideasFontein.Add(fonteinIdea2);
            waterIdeation1.Ideas = ideasFontein;

            //Ideation 2 (water) van fase 1 van project 1 (drinkbaar water)          
            Ideation waterIdeation2 = new Ideation();
            waterIdeation2.TitleIdeation = "Verdeelplaatsen drinkwater?";
            waterIdeation2.DescriptionIdeation =
                "Er moeten meer manieren zijn om aan gratis drinkwater te komen. Daarom stellen we voor om op verschillende locaties" +
                " drinkwater posten op te stellen waar mensen in nood gratis drinkwater kunnen verkrijgen. Geef hier je mening en waarom" +
                " je dit een goed idee vindt.";
            waterIdeation2.Begin = new DateTime(1978, 4, 23);
            waterIdeation2.End = DateTime.Now;

            //Idea 1 van ideation 2 (water) van fase 1 van project 1 (drinkbaar water) + reacties
            Idea drinkIdea1 = new Idea();
            drinkIdea1.Ideation = waterIdeation2;
            drinkIdea1.Title = "Goed Idee, drinkwater zou veel makkelijker verkrijgbaar moeten zijn.";
            drinkIdea1.Description =
                "Niet iedereen heeft de mogelijkheid om genoeg drinkbaar water te verschaffen, deze soort verdeelposten zou een grote stap" +
                "in de juiste richting zijn.";
            drinkIdea1.User = user3;
            List<Like> likesDrink1 = new List<Like>();
            for (int i = 0; i < 15; i++)
            {
                likesDrink1.Add(new Like());
            }
            drinkIdea1.Likes = likesDrink1;
            List<Share> sharesDrink1 = new List<Share>();
            for (int i = 0; i < 6; i++)
            {
                sharesDrink1.Add(new Share());
            }
            drinkIdea1.Shares = sharesDrink1;
            //Reacties
            List<Reaction> reactiesDrink1 = new List<Reaction>();
            Reaction reaction1Drink1 = new Reaction();
            reaction1Drink1.User = user6;
            reaction1Drink1.Deleted = false;
            reaction1Drink1.Idea = drinkIdea1;
            reaction1Drink1.Comment = "Ik ben hier grote fan van!";
            Reaction reaction2Drink1 = new Reaction();
            reaction2Drink1.User = user1;
            reaction2Drink1.Deleted = false;
            reaction2Drink1.Idea = drinkIdea1;
            reaction2Drink1.Comment = "Hebben jullie al op mijn site gekeken???";
            List<Notification> notsDrink1 = new List<Notification>();
            Notification not1Drink1 = new Notification(
                NotificationType.ADVERTISMENT,
                "test",
                reaction2Drink1
            );
            notsDrink1.Add(not1Drink1);
      
            reactiesDrink1.Add(reaction1Drink1);
            reactiesDrink1.Add(reaction2Drink1);
            drinkIdea1.Reactions = reactiesDrink1;
            drinkIdea1.Notifications = notsDrink1;
      
            //Idea 2 van ideation 2 (water) van fase 1 van project 1 (drinkbaar water) + reacties
            Idea drinkIdea2 = new Idea();
            drinkIdea2.Ideation = waterIdeation2;
            drinkIdea2.Title = "Goed Idee, voorstel om een post op te stellen in het Stadspark";
            drinkIdea2.Description =
                "Het Stadspark is te vinden in hartje Antwerpen, op deze manier kunnen de meeste mensen deze post benuttigen.";
            drinkIdea2.User = user4;
            List<Like> likesDrink2 = new List<Like>();
            for (int i = 0; i < 5; i++)
            {
                likesDrink2.Add(new Like());
            }
            drinkIdea2.Likes = likesDrink2;
            List<Share> sharesDrink2 = new List<Share>();
            for (int i = 0; i < 9; i++)
            {
                sharesDrink2.Add(new Share());
            }
            drinkIdea2.Shares = sharesDrink2;
            //Reacties
            List<Reaction> reactiesDrink2 = new List<Reaction>();
            Reaction reaction1Drink2 = new Reaction();
            reaction1Drink2.User = user2;
            reaction1Drink2.Deleted = false;
            reaction1Drink2.Idea = drinkIdea2;
            reaction1Drink2.Comment = "Ik vind dit een super idee!";
            Reaction reaction2Drink2 = new Reaction();
            reaction2Drink2.User = user3;
            reaction2Drink2.Deleted = false;
            reaction2Drink2.Idea = drinkIdea2;
            reaction2Drink2.Comment = "Ik vind dit geen goed idee!";
      
            reactiesDrink2.Add(reaction1Drink2);
            reactiesDrink2.Add(reaction2Drink2);
            drinkIdea2.Reactions = reactiesDrink2;
      
            //Ideas toevoegen aan ideation 2 (water) van fase 1 van project 1 (drinkbaar water)
            List<Idea> ideasDrink = new List<Idea>();
            ideasDrink.Add(drinkIdea1);
            ideasDrink.Add(drinkIdea2);
            waterIdeation2.Ideas = ideasDrink;

            //Ideations toevoegen aan fase 1 van project 1 (drinkbaar water)
            faseProject1.InputMethods.Add(waterIdeation1);
            faseProject1.InputMethods.Add(waterIdeation2);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //PROJECT 2: GROEN IN DE STAD
            Project project2 = new Project();
            project2.Id = "groen";
            project2.Description =
                "Het straatbeeld in Antwerpen kan nog een grote boost gebruiken. In dit topic behandelen we orignele en duurzame manieren om onze stad " +
                "een groener uitzicht te geven. Alle suggesties zijn welkom";
            project2.Category = Category.MILIEU;
            project2.ImageName = "groen_stad.jpg";
            project2.Name = "Groen in de stad";
            project2.Organisation = org1;
            List<Like> likes2 = new List<Like>();
            List<Share> shares2 = new List<Share>();
            for (int i = 0; i < 17; i++)
            {
                likes2.Add(new Like());
            }
            for (int i = 0; i < 24; i++)
            {
                shares2.Add(new Share());
            }
            project2.Likes = likes2;
            project2.Shares = shares2;
            project2.Begin = new DateTime(2019, 2, 2);
            project2.End = DateTime.Now;
            project2.MainQuestion = "Moet er meer groen in de stad zijn?";

            //QuestionList 1 van fase 1 van project 2 (groen in de stad)
            QuestionList questionList2 = new QuestionList();
            questionList2.Title = "Hoe krijgen we meer groen in de stad?";
            questionList2.Description =
                "Met het invullen van deze vragenlijst helpt u ons met het verbeteren van het project 'groen in de stad'+" +
                "Uw mening/input zal ons voor een groot deel begeleiden doorheen het project. Hopelijk kunnen we op uw +" +
                "eerlijkheid vertrouwen! Hartelijk dank!";
            questionList2.Begin = new DateTime(2000, 1, 1);
            questionList2.End = DateTime.Now;
            questionList2.Shares = shareList;
            questionList2.Open = true;
            List<Question> questions2 = new List<Question>();
            AddQuestions(questions2, 2);
            questionList2.Questions = questions2;
            //questionList2.Shares = shareList2;

            //Fase 1 van project 2 (groen in de stad)
            Fase faseProject2 = new Fase();
            faseProject2.FaseName = "GroenFase";
            faseProject2.ProjectFase = ProjectFase.STARTED;
            faseProject2.Begin = DateTime.Now;
            faseProject2.End = DateTime.Now;
            project2.CurrentFase = faseProject2;
            faseProject2.InputMethods = new List<InputMethod>();
            faseProject2.InputMethods.Add(questionList2);
            questionList2.Fase = faseProject2;

            //Ideation 1 (park) van fase 1 van project 2 (groen in de stad)
            Ideation groenIdeation1 = new Ideation();
            //groenIdeation1.Type = IdeationType.IDEATION1;
            groenIdeation1.TitleIdeation = "Parken heraanleggen?";
            groenIdeation1.DescriptionIdeation =
                "Verscheidene parken in Antwerpen kunnen wel eens een opfrisbeurt gebruiken. In deze ideation kan je stemmen welk park voor jou " +
                "prioriteit heeft, of zelf een park voorstellen.";
            groenIdeation1.Begin = new DateTime(2000, 1, 1);
            groenIdeation1.End = DateTime.Now;

            //Idea 1 van ideation 1 van fase 1 van project 2 (groen in de stad) + reacties
            Idea parkIdea1 = new Idea();
            parkIdea1.Ideation = groenIdeation1;
            parkIdea1.Title = "Opfrisbeurt Stadspark";
            parkIdea1.Description =
                "Momenteel ligt het Stadpark er niet al te goed bij, de paden zouden heraangelegd of opgekuist moeten worden" +
                "en verschillende banken zijn versleten of hebben een serieuse kuisbeurt nodig.";
            parkIdea1.User = user5;
            List<Like> likesPark1 = new List<Like>();
            for (int i = 0; i < 11; i++)
            {
                likesPark1.Add(new Like());
            }
            parkIdea1.Likes = likesPark1;
            List<Share> sharesPark1 = new List<Share>();
            for (int i = 0; i < 2; i++)
            {
                sharesPark1.Add(new Share());
            }
            parkIdea1.Shares = sharesPark1;
            //Reacties
            List<Reaction> reactiesPark1 = new List<Reaction>();
            Reaction reaction1Park1 = new Reaction();
            reaction1Park1.User = user4;
            reaction1Park1.Deleted = false;
            reaction1Park1.Idea = parkIdea1;
            reaction1Park1.Comment = "Ik vind dit een super idee!";
            Reaction reaction2Park1 = new Reaction();
            reaction2Park1.User = user5;
            reaction2Park1.Deleted = false;
            reaction2Park1.Idea = parkIdea1;
            reaction2Park1.Comment = "Wist je dat pinguïns niet kunnen vliegen?";
            List<Notification> notsPark1 = new List<Notification>();
            Notification not1Park1 = new Notification(
                NotificationType.IRRELEVANT,
                "test",
                reaction2Park1
            );
            notsPark1.Add(not1Park1);
      
            reactiesPark1.Add(reaction1Drink1);
            reactiesPark1.Add(reaction2Drink1);
            parkIdea1.Reactions = reactiesPark1;
            parkIdea1.Notifications = notsPark1;
      
            //Idea 2 van ideation 1 van fase 1 van project 2 (groen in de stad) + reacties
            Idea parkIdea2 = new Idea();
            parkIdea2.Ideation = groenIdeation1;
            parkIdea2.Title = "Opfrisbeurt BoekenbergPark";
            parkIdea2.Description =
                "Mij stem gaat naar BoekenbergPark. De faciliteiten binnenin het park zijn verouderd en vereisen renovaties. Vooral de " +
                "verschillende kleine speeltuintjes hebben een goede kijk nodig. ";
            parkIdea2.User = user6;
            List<Like> likesPark2 = new List<Like>();
            for (int i = 0; i < 41; i++)
            {
                likesPark2.Add(new Like());
            }
            parkIdea2.Likes = likesPark2;
            List<Share> sharesPark2 = new List<Share>();
            for (int i = 0; i < 17; i++)
            {
                sharesPark2.Add(new Share());
            }
            parkIdea2.Shares = sharesPark2;
            //Reacties
            List<Reaction> reactiesPark2 = new List<Reaction>();
            Reaction reaction1Park2 = new Reaction();
            reaction1Park2.User = user1;
            reaction1Park2.Deleted = false;
            reaction1Park2.Idea = parkIdea2;
            reaction1Park2.Comment = "Ik vind dit een super idee!";
            Reaction reaction2Park2 = new Reaction();
            reaction2Park2.User = user2;
            reaction2Park2.Deleted = false;
            reaction2Park2.Idea = parkIdea2;
            reaction2Park2.Comment = "Ik vind dit een kak idee!";
            List<Notification> notsPark2 = new List<Notification>();
            Notification not1Park2 = new Notification(
                NotificationType.INAPPROPRIATE_LANGUAGE,
                "test",
                reaction2Park2
            );
            notsPark2.Add(not1Park2);
      
            reactiesPark2.Add(reaction1Park2);
            reactiesPark2.Add(reaction2Park2);
            parkIdea2.Reactions = reactiesPark2;
            parkIdea2.Notifications = notsPark2;

            //Ideas toevoegen aan ideation 1 (park) van fase 1 van project 2 (groen in de stad)
            List<Idea> ideasPark = new List<Idea>();
            ideasPark.Add(parkIdea1);
            ideasPark.Add(parkIdea2);
            groenIdeation1.Ideas = ideasPark;

            //Ideation 2 (groen) van fase 1 van project 2 (groen in de stad)        
            Ideation groenIdeation2 = new Ideation();
            groenIdeation2.TitleIdeation = "Groene subsidies?";
            groenIdeation2.DescriptionIdeation =
                "Is het een goed idee om een soort groene subsidie in te voeren voor iedereen die groen aanlegt? Geef hier je mening en waarom je dit " +
                "een goed idee vindt.";
            groenIdeation2.Begin = new DateTime(1978, 4, 23);
            groenIdeation2.End = DateTime.Now;

            //Idea 1 van ideation 2 (groen) van fase 1 van project 2 (groen in de stad) + reacties
            Idea groenIdea1 = new Idea();
            groenIdea1.Ideation = groenIdeation2;
            groenIdea1.Title = "Goed Idee, meer specifieker voor zonnepanelen!";
            groenIdea1.Description =
                "Het zou een goede manier zijn om mensen te motiveren een groenere statdsomgeving te creëren." +
                "Momenteel is het niet al te aantrekkelijk om nieuwe zonnepanelen aan te leggen. Het is vrij kostelijk zonder al te veel" +
                "voordelen. Ik stel voor om een kleine zonnepaneel subsidie in te voeren specifiek voor de regio Antwerpen om de stad groener te maken.";
            groenIdea1.User = user1;
            List<Like> likesGroen1 = new List<Like>();
            for (int i = 0; i < 14; i++)
            {
                likesGroen1.Add(new Like());
            }
            groenIdea1.Likes = likesGroen1;
            List<Share> sharesGroen1 = new List<Share>();
            for (int i = 0; i < 23; i++)
            {
                sharesGroen1.Add(new Share());
            }
            groenIdea1.Shares = sharesGroen1;
            //Reacties
            List<Reaction> reactiesGroen1 = new List<Reaction>();
            Reaction reaction1Groen1 = new Reaction();
            reaction1Groen1.User = user6;
            reaction1Groen1.Deleted = false;
            reaction1Groen1.Idea = parkIdea1;
            reaction1Groen1.Comment = "Ik vind dit een super idee!";
            Reaction reaction2Groen1 = new Reaction();
            reaction2Groen1.User = user3;
            reaction2Groen1.Deleted = false;
            reaction2Groen1.Idea = groenIdea1;
            reaction2Groen1.Comment = "Ik vind dit een slecht idee!";
            List<Notification> notsGroen1 = new List<Notification>();
      
            reactiesGroen1.Add(reaction1Groen1);
            reactiesGroen1.Add(reaction2Groen1);
            groenIdea1.Reactions = reactiesGroen1;
      
            //Idea 2 van ideation 2 (groen) van fase 1 van project 2 (groen in de stad) + reacties
            Idea groenIdea2 = new Idea();
            groenIdea2.Ideation = groenIdeation2;
            groenIdea2.Title = "Goed Idee, maar is het wel noodzakelijk?.";
            groenIdea2.Description =
                "Een subsidie zou niet noodzakelijk moeten zijn om mensen te motiveren. Indien de omgeving en het klimaat" +
                "hun werkelijk iets kan schelen, dan doen ze dit nu al. Het geeft natuurlijk wel een extra push aan de mensen" +
                "die niet zeker zijn of ze dit initiatief wel moeten nemen.";
            groenIdea2.User = user2;
            List<Like> likesGroen2 = new List<Like>();
            for (int i = 0; i < 4; i++)
            {
                likesGroen2.Add(new Like());
            }
            groenIdea2.Likes = likesGroen2;
            List<Share> sharesGroen2 = new List<Share>();
            for (int i = 0; i < 7; i++)
            {
                sharesGroen2.Add(new Share());
            }
            groenIdea2.Shares = sharesGroen2;
            //Reacties
            List<Reaction> reactiesGroen2 = new List<Reaction>();
            Reaction reaction1Groen2 = new Reaction();
            reaction1Groen2.User = user4;
            reaction1Groen2.Deleted = false;
            reaction1Groen2.Idea = groenIdea2;
            reaction1Groen2.Comment = "Ik ben fan!";
            Reaction reaction2Groen2 = new Reaction();
            reaction2Groen2.User = user5;
            reaction2Groen2.Deleted = false;
            reaction2Groen2.Idea = groenIdea2;
            reaction2Groen2.Comment = "Ik haat kippen!";
            List<Notification> notsGroen2 = new List<Notification>();
            Notification not1Groen2 = new Notification(
                NotificationType.IRRELEVANT,
                "test",
                reaction2Groen2
            );
            notsGroen2.Add(not1Groen2);
      
            reactiesGroen2.Add(reaction1Groen2);
            reactiesGroen2.Add(reaction2Groen2);
            groenIdea2.Reactions = reactiesGroen2;
            groenIdea2.Notifications = notsGroen2;
      
            //Ideas toevoegen aan ideation 2 (groen) van fase 1 van project  (groen in de stad)
            List<Idea> ideasGroen = new List<Idea>();
            ideasGroen.Add(groenIdea1);
            ideasGroen.Add(groenIdea2);
            groenIdeation2.Ideas = ideasGroen;

            //Ideations toevoegen aan fase 1 van project 2 (groen in de stad)
            faseProject2.InputMethods.Add(groenIdeation1);
            faseProject2.InputMethods.Add(groenIdeation2);

            /////////////////////////////////////////////////////////////

            //PROJECT 3: VUILZAKKEN IN BEELD
            Project project3 = new Project();
            project3.ImageName = "vuilniszakken.jpg";
            project3.Name = "Vuilzakken in beeld";
            project3.Organisation = org1;
            List<Like> likes3 = new List<Like>();
            List<Share> shares3 = new List<Share>();
            for (int i = 0; i < 13; i++)
            {
                likes3.Add(new Like());
            }
            for (int i = 0; i < 19; i++)
            {
                shares3.Add(new Share());
            }
            project3.Likes = likes3;
            project3.Shares = shares3;
            project3.Begin = new DateTime(2019, 1, 1);
            project3.End = DateTime.Now;
            project3.Description =
                "De vuilzakken in Antwerpen worden opgehaald op verschillende tijdstippen en dagen. Dit zorgt er voor dat je er amper naast kan kijken tijdens het wandelen." +
                "We zoeken oplossingen voor het probleem.";
            project3.MainQuestion = "wilt u meer groen?";
            project3.Category = Category.MILIEU;
            //QuestionList 1 van fase 1 van project 3 (vuilzakken)
            QuestionList questionList3 = new QuestionList();
            questionList3.Title = "Hoe overlast voorkomen van vuilzakken op straat?";
            questionList3.Description =
                "Met het invullen van deze vragenlijst helpt u ons met het verbeteren van het project 'vuilzakken in beeld'+" +
                "Uw mening/input zal ons voor een groot deel begeleiden doorheen het project. Hopelijk kunnen we op uw +" +
                "eerlijkheid vertrouwen! Hartelijk dank!";
            questionList3.Begin = new DateTime(2000, 1, 1);
            questionList3.End = DateTime.Now;
            questionList3.Shares = shareList;
            questionList3.Open = true;
            List<Question> questions3 = new List<Question>();
            AddQuestions(questions3, 3);
            questionList3.Questions = questions3;

            //Fase 1 van project 3 (vuilzakken)
            Fase faseProject3 = new Fase();
            faseProject3.FaseName = "VuilFase";
            faseProject3.ProjectFase = ProjectFase.STARTED;
            faseProject3.Begin = DateTime.Now;
            faseProject3.End = DateTime.Now;
            project3.CurrentFase = faseProject3;
            faseProject3.InputMethods = new List<InputMethod>();
            faseProject3.InputMethods.Add(questionList3);
            questionList.Fase = faseProject3;

            //Ideation 1 (bak) van fase 1 van project 3 (vuilzakken)
            Ideation vuilIdeation1 = new Ideation();
            vuilIdeation1.TitleIdeation = "Vuilbakken per buurt?";
            vuilIdeation1.DescriptionIdeation =
                "Er kunnen per buurt een paar grote vuilbakken verschijnen waar de buurt zijn afval in kan plaatsen,hierdoor" +
                " zouden de straten niet meer vol met vuilzakken staan. Geef hier je mening en waarom je dit een goed idee vindt.";
            vuilIdeation1.Begin = new DateTime(2000, 1, 1);
            vuilIdeation1.End = DateTime.Now;

            //Idea 1 van ideation 1 (bak) van fase 1 van project 3 (vuilzakken) + reacties
            Idea bakIdea1 = new Idea();
            bakIdea1.Ideation = vuilIdeation1;
            bakIdea1.Title = "Goed voorstel, moeilijker in de realiteit.";
            bakIdea1.Description =
                "Het is een goed idee om de straten wat properder te maken, maak zullen mensen in de realiteit werkelijk naar deze" +
                "  bakken willen lopen?";
            bakIdea1.User = user3;
            List<Like> likesBak1 = new List<Like>();
            for (int i = 0; i < 10; i++)
            {
                likesBak1.Add(new Like());
            }
            bakIdea1.Likes = likesBak1;
            List<Share> sharesBak1 = new List<Share>();
            for (int i = 0; i < 12; i++)
            {
                sharesBak1.Add(new Share());
            }
            bakIdea1.Shares = sharesBak1;
            //Reacties
            List<Reaction> reactiesBak1 = new List<Reaction>();
            Reaction reaction1Bak1 = new Reaction();
            reaction1Bak1.User = user6;
            reaction1Bak1.Deleted = false;
            reaction1Bak1.Idea = bakIdea1;
            reaction1Bak1.Comment = "Ik sluit me hierbij aan, het is te moeilijk in werkelijkheid!";
            Reaction reaction2Bak1 = new Reaction();
            reaction2Bak1.User = user1;
            reaction2Bak1.Deleted = false;
            reaction2Bak1.Idea = bakIdea1;
            reaction2Bak1.Comment = "Ik zou niet naar zo'n bak lopen, veel te fucking ver!";
            List<Notification> notsBak1 = new List<Notification>();
            Notification not1Bak1 = new Notification(
                NotificationType.INAPPROPRIATE_LANGUAGE,
                "test",
                reaction2Bak1
            );
            notsBak1.Add(not1Bak1);
      
            reactiesBak1.Add(reaction1Bak1);
            reactiesBak1.Add(reaction2Bak1);
            bakIdea1.Reactions = reactiesBak1;
            bakIdea1.Notifications = notsBak1;

            //Idea 2 van ideation 1 (bak) van fase 1 van project 3 (vuilzakken) + reacties
            Idea bakIdea2 = new Idea();
            bakIdea2.Ideation = vuilIdeation1;
            bakIdea2.Title = "Geweldig Idee, maak mijn buurt terug proper.";
            bakIdea2.Description =
                "Het klinkt me als muziek in mijn oren, het is niet al te veel moeite om naar deze bak te lopen en het zou het uitzicht" +
                " van mijn buurt verbeteren.";
            bakIdea2.User = user4;
            List<Like> likesBak2 = new List<Like>();
            for (int i = 0; i < 20; i++)
            {
                likesBak2.Add(new Like());
            }
            bakIdea2.Likes = likesBak2;
            List<Share> sharesBak2 = new List<Share>();
            for (int i = 0; i < 17; i++)
            {
                sharesBak2.Add(new Share());
            }
            bakIdea2.Shares = sharesBak2;
            //Reacties
            List<Reaction> reactiesBak2 = new List<Reaction>();
            Reaction reaction1Bak2 = new Reaction();
            reaction1Bak2.User = user5;
            reaction1Bak2.Deleted = false;
            reaction1Bak2.Idea = bakIdea2;
            reaction1Bak2.Comment = "Ik vind dit een super idee!";
            Reaction reaction2Bak2 = new Reaction();
            reaction2Bak2.User = user6;
            reaction2Bak2.Deleted = false;
            reaction2Bak2.Idea = bakIdea2;
            reaction2Bak2.Comment = "Over muziek gesproken, kennen jullie dit liedje? https://www.youtube.com/watch?v=dQw4w9WgXcQ";
            List<Notification> notsBak2 = new List<Notification>();
            Notification not1Bak2 = new Notification(
                NotificationType.COMMERCIAL,
                "test",
                reaction2Bak2
            );
            notsBak2.Add(not1Bak2);
      
            reactiesBak2.Add(reaction1Bak2);
            reactiesBak2.Add(reaction2Bak2);
            bakIdea2.Reactions = reactiesBak2;
            bakIdea2.Notifications = notsBak2;
      
            //Ideas toevoegen aan ideation 1 (bak) van fase 1 van project 3 (vuilzakken)
            List<Idea> ideasBak = new List<Idea>();
            ideasBak.Add(bakIdea1);
            ideasBak.Add(bakIdea2);
            vuilIdeation1.Ideas = ideasBak;
            faseProject3.InputMethods.Add(vuilIdeation1);

            //Ideation 2 (simultaan) van fase 1 van project 3 (vuilzakken)          
            Ideation vuilIdeation2 = new Ideation();
            vuilIdeation2.TitleIdeation = "Simultaan ophalen vuilnis?";
            vuilIdeation2.DescriptionIdeation =
                "De ophaal tijdstippen van de vuilzakken in Antwerpen zo dicht mogelijk bij elkaar plaatsen. Hierdoor zouden" +
                " de straten niet constant meer vol liggen met vuilzakken. Geef hier je mening en waarom je dit een goed idee vindt.";
            vuilIdeation2.Begin = new DateTime(1978, 4, 23);
            vuilIdeation2.End = DateTime.Now;

            //Idea 1 van ideation 2 (simultaan) van fase 1 van project 3 (vuilzakken) + reacties
            Idea simultaanIdea1 = new Idea();
            simultaanIdea1.Ideation = vuilIdeation2;
            simultaanIdea1.Title = "Goed Idee, maar moeilijk om uit te voeren";
            simultaanIdea1.Description =
                "Ik denk niet dat het in de praktijk mogelijk is om dit uit te voeren, er is hier volgens mij niet genoeg mankracht voor";
            simultaanIdea1.User = user5;
            List<Like> likesSimultaan1 = new List<Like>();
            for (int i = 0; i < 19; i++)
            {
                likesSimultaan1.Add(new Like());
            }
            simultaanIdea1.Likes = likesSimultaan1;
            List<Share> sharesSimultaan1 = new List<Share>();
            for (int i = 0; i < 8; i++)
            {
                sharesSimultaan1.Add(new Share());
            }
            simultaanIdea1.Shares = sharesSimultaan1;
            //Reacties
            List<Reaction> reactiesSimultaan1 = new List<Reaction>();
            Reaction reaction1Simultaan1 = new Reaction();
            reaction1Simultaan1.User = user2;
            reaction1Simultaan1.Deleted = false;
            reaction1Simultaan1.Idea = simultaanIdea1;
            reaction1Simultaan1.Comment = "Ik vind dit een super idee!";
            Reaction reaction2Simultaan1 = new Reaction();
            reaction2Simultaan1.User = user3;
            reaction2Simultaan1.Deleted = false;
            reaction2Simultaan1.Idea = simultaanIdea1;
            reaction2Simultaan1.Comment = "Niet genoeg mankracht? En de vrouwen dan???";
            List<Notification> notsSimultaan1 = new List<Notification>();
            Notification not1Simultaan1 = new Notification(
                NotificationType.IRRELEVANT,
                "test",
                reaction2Simultaan1
            );
            notsSimultaan1.Add(not1Simultaan1);
      
            reactiesSimultaan1.Add(reaction1Simultaan1);
            reactiesSimultaan1.Add(reaction2Simultaan1);
            simultaanIdea1.Reactions = reactiesSimultaan1;
            simultaanIdea1.Notifications = notsSimultaan1;

            //Idea 2 van ideation 2 (simultaan) van fase 1 van project 3 (vuilzakken) + reacties
            Idea simultaanIdea2 = new Idea();
            simultaanIdea2.Ideation = vuilIdeation2;
            simultaanIdea2.Title = "Fantastisch idee, maak de straten terug  wandelbaar";
            simultaanIdea2.Description =
                "Als dit idee lukt, zou het de Antwerpse buurten terug mooier en bewandelbaarder maken zonder dat" +
                " wij als burgers iets extra moeten doen.";
            simultaanIdea2.User = user6;
            List<Like> likesSimultaan2 = new List<Like>();
            for (int i = 0; i < 24; i++)
            {
                likesSimultaan2.Add(new Like());
            }
            simultaanIdea2.Likes = likesSimultaan2;
            List<Share> sharesSimultaan2 = new List<Share>();
            for (int i = 0; i < 18; i++)
            {
                sharesSimultaan2.Add(new Share());
            }
            simultaanIdea2.Shares = sharesSimultaan2;
            //Reacties
            List<Reaction> reactiesSimultaan2 = new List<Reaction>();
            Reaction reaction1Simultaan2 = new Reaction();
            reaction1Simultaan2.User = user4;
            reaction1Simultaan2.Deleted = false;
            reaction1Simultaan2.Idea = simultaanIdea2;
            reaction1Simultaan2.Comment = "Helemaal akkoord!";
            Reaction reaction2Simultaan2 = new Reaction();
            reaction2Simultaan2.User = user5;
            reaction2Simultaan2.Deleted = false;
            reaction2Simultaan2.Idea = simultaanIdea2;
            reaction2Simultaan2.Comment = "Ik vind dit een slecht idee!";
            List<Notification> notsSimultaan2 = new List<Notification>();
      
            reactiesSimultaan2.Add(reaction1Simultaan2);
            reactiesSimultaan2.Add(reaction2Simultaan2);
            simultaanIdea2.Reactions = reactiesSimultaan2;
      
            //Ideas toevoegen aan ideation 2 (simultaan) van fase 1 van project 3 (vuilzakken)
            List<Idea> ideasSimultaan = new List<Idea>();
            ideasSimultaan.Add(simultaanIdea1);
            ideasSimultaan.Add(simultaanIdea2);
            vuilIdeation2.Ideas = ideasSimultaan;
            faseProject3.InputMethods.Add(vuilIdeation2);

            //Persisteren projecten naar databank
            context.Projects.Add(project);
            context.Projects.Add(project2);
            context.Projects.Add(project3);

            //Persisteren vragenlijsten naar databank
            context.QuestionLists.Add(questionList);
            context.QuestionLists.Add(questionList2);
            context.QuestionLists.Add(questionList3);
            
            var buttons = new List<Button>();
            buttons.Add(new Button()
            {
                Id = "10001",
                Input = parkIdea1
            });
            buttons.Add(new Button()
            {
                Id = "10002",
                Input = parkIdea2
            });
            var poster1 = new Poster()
            {
                Active = true,
                Buttons = buttons,
                Description = "welk park vindt u dat een update verdient",
                Ideation = groenIdeation1
            };
            foreach (var button in buttons)
            {
                button.Poster = poster1;
            }

            context.Posters.Add(poster1);
            context.SaveChanges();
        }

    private static void AddQuestions(List<Question> questions, int hoeveelste)
    {
      //Drinkbaar water
      if (hoeveelste == 1)
      {
        OpenQuestion o1 = new OpenQuestion();
        o1.OpenQuestionType = OpenQuestionType.OPEN_QUESTION;
        o1.Title = "Hoe zou u meer drinkbaar water beschikbaar stellen?";
        o1.Rang = "1";

        List<Option> options = new List<Option>();
        options.Add(new Option() {Title = "Heel belangrijk"});
        options.Add(new Option() {Title = "Belangrijk"});
        options.Add(new Option() {Title = "Neutraal"});
        options.Add(new Option() {Title = "Niet belangrijk"});

        ChoiceQuestion c1 = new ChoiceQuestion();
        c1.ChoiseQuestionType = ChoiseQuestionType.MULTIPLE_CHOICE;
        c1.Title = "Hoe belangrijk vindt u de aanwezigheid van de drinkbaar water in de stad?";
        c1.Options = options;
        c1.Rang = "2";

        OpenQuestion o2 = new OpenQuestion();
        o2.OpenQuestionType = OpenQuestionType.OPEN_QUESTION;
        o2.Title = "Heeft u nog opmerkingen of eventuele feedback voor ons? Laat het ons hier weten!";
        o2.Rang = "3";

        OpenQuestion o3 = new OpenQuestion();
        o3.OpenQuestionType = OpenQuestionType.EMAIL;
        o3.Title = "Ontvangt u graag elke maand de nieuwsbrief? Geef dan hier uw email mee";
        o3.Rang = "4";

        questions.Add(o1);
        questions.Add(c1);
        questions.Add(o2);
        questions.Add(o3);
      }

      //Groen
      if (hoeveelste == 2)
      {
        OpenQuestion o1 = new OpenQuestion();
        o1.OpenQuestionType = OpenQuestionType.OPEN_QUESTION;
        o1.Title = "Heeft u zelf nog opmerkingen of zijn er nog onduidelijkheden? Laat ze hier achter.";
        o1.Rang = "4";

        OpenQuestion o2 = new OpenQuestion();
        o2.OpenQuestionType = OpenQuestionType.EMAIL;
        o2.Title = "Ontvangt u graag elke maand de nieuwsbrief? Geef dan hier uw email mee";
        o2.Rang = "5";

        OpenQuestion o3 = new OpenQuestion();
        o3.OpenQuestionType = OpenQuestionType.MAP;
        o3.Title = "Open question - Questiontype: MAP";
        o3.Lattitude = (float) 51.2205810000;
        o3.Longtitude = (float) 4.3997220000;
        List<Option> options = new List<Option>();
        options.Add(new Option() {Title = "Het gebruik van zonnepanelen"});
        options.Add(new Option() {Title = "Het verminderen van afval"});
        options.Add(new Option() {Title = "Het planten van bomen"});
        options.Add(new Option() {Title = "Het toevoegen van parken"});

        List<Option> options2 = new List<Option>();
        options2.Add(new Option() {Title = "Bomen"});
        options2.Add(new Option() {Title = "Bloembakken"});
        options2.Add(new Option() {Title = "Struiken"});
        options2.Add(new Option() {Title = "Grasvelden"});

        List<Option> options3 = new List<Option>();
        options3.Add(new Option() {Title = "Heel belangrijk"});
        options3.Add(new Option() {Title = "Belangrijk"});
        options3.Add(new Option() {Title = "Neutraal"});
        options3.Add(new Option() {Title = "Niet belangrijk"});

        ChoiceQuestion c1 = new ChoiceQuestion();
        c1.ChoiseQuestionType = ChoiseQuestionType.DROPDOWN;
        c1.Title = "Voor welk soort 'groen'-initiatief zou u liefst een subsidie willen zien?";
        c1.Options = options;
        c1.Rang = "2";

        ChoiceQuestion c2 = new ChoiceQuestion();
        c2.ChoiseQuestionType = ChoiseQuestionType.MULTIPLE_CHOICE;
        c2.Title = "Wat voor soort 'groen' zou willen terug zien in uw stad? (meerdere antwoorden mogelijk)";
        c2.Options = options2;
        c2.Rang = "1";

        ChoiceQuestion c3 = new ChoiceQuestion();
        c3.ChoiseQuestionType = ChoiseQuestionType.SINGLE_CHOICE;
        c3.Title = "Hoe belangrijk vindt u het toevoegen van 'groen' in uw stad?";
        c3.Options = options3;
        c3.Rang = "3";

        var i = 0;
        foreach (var question in questions)
        {
          question.Rang = i.ToString();
          i++;
        }

        questions.Add(c1);
        questions.Add(c2);
        questions.Add(c3);
        questions.Add(o3);
        questions.Add(o1);
        questions.Add(o2);
        
        
      }


      /*OpenQuestion o1 = new OpenQuestion();
      o1.OpenQuestionType = OpenQuestionType.OPEN_QUESTION;
      o1.Lol = "Test";
      o1.Title = "Open question - Questiontype: OPEN_QUESTION";
      
      /*OpenQuestion o2 = new OpenQuestion();
      o2.OpenQuestionType = OpenQuestionType.EMAIL;
      o2.Title = "Open question - Questiontype: EMAIL";
      
      OpenQuestion o3 = new OpenQuestion();
      o3.OpenQuestionType = OpenQuestionType.MAP;
      o3.Title = "Open question - Questiontype: MAP";*/

      /*List<Option> options = new List<Option>();
      for (int a = 0; a < 5; a++)
      {
          options.Add(new Option(){title = "Option: " + a.ToString()});
      }
      
      ChoiceQuestion c1 = new ChoiceQuestion();
      c1.ChoiseQuestionType = ChoiseQuestionType.DROPDOWN;
      c1.Title = "Choice question: 1 - ChoiceQuestionType: DROPDOWN";
      c1.Options = options;
      
      /*ChoiceQuestion c2 = new ChoiceQuestion();
      c2.ChoiseQuestionType = ChoiseQuestionType.SINGLE_CHOICE;
      c2.Title = "Choice question: 2 - ChoiceQuestionType: SINGLE_CHOICE";
      c2.Options = options;
      
      ChoiceQuestion c3 = new ChoiceQuestion();
      c3.ChoiseQuestionType = ChoiseQuestionType.MULTIPLE_CHOICE;
      c3.Title = "Choice question: 3 - ChoiceQuestionType: MULTIPLE_CHOICE";
      c3.Options = options;*/


      //questions.Add(o1);
      //questions.Add(o2);
      //questions.Add(o3);
      //questions.Add(c1);
      //questions.Add(c2);
      //questions.Add(c3);
    }
  }
}