using System;
using System.Collections.Generic;
using System.Linq;
using PoC.BL.manager.input;
using PoC.Domain.inputmethod;
using PoC.BL.manager.InputMethod;
using PoC.DAL.repo.project;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.project;
using PoC.Domain.user;

namespace PoC.BL.manager.project
{
  public class ProjectManager : IProjectManager
  {
    private IProjectRepository _repo;
    private IInputMethodManager _mgr;
    private IInputManager _inputManager;
    
    public ProjectManager(IProjectRepository repo, IInputMethodManager mgr, IInputManager inputManager)
    {
      _mgr = mgr;
      _repo = repo;
      _inputManager = inputManager;
    }


    public Project GetProject(string id)
    {
      return _repo.ReadProject(id);
    }

    public Project AddProject(Project project)
    {
      return _repo.CreateProject(project);
    }

    public Project AddProjectViewModel(string projectName, string imageName,
      string mainQuestion, string description, Category category, DateTime begin, DateTime end,string faseName, ProjectFase projectFase, DateTime beginFase, DateTime endFase)
    {

      Project project = new Project()
      {
        Name = projectName,
        ImageName = imageName,
        MainQuestion = mainQuestion,
        Description = description,
        Category = category,
        Begin = begin,
        End = end,
        CurrentFase = new Fase()
        {
          FaseName = faseName,
          ProjectFase = projectFase,
          Begin = beginFase,
          End = endFase
        }
      };
      
      return project;
    }

    public Project ChangeProject(Project project)
    {
      return _repo.UpdateProject(project);
    }

    public Project AddFaseToProject(Project project,string faseName, ProjectFase projectFase,DateTime begin, DateTime end)
    {
      Fase fase = new Fase()
      {
        FaseName = faseName,
        ProjectFase = projectFase,
        Begin = begin,
        End= end
      };
      return _repo.UpdateFaseProject(project, fase);
    }

    public Project AddIdeationToProject(Project project, Ideation ideation)
    {
      return _repo.UpdateIdeationProject(project, ideation);
    }

    public Project AddQuestionListToProject(Project project, QuestionList questionList)
    {
      return _repo.UpdateQuestionListProject(project, questionList);
    }

    public Project RemoveProject(string id)
    {
      return _repo.DeleteProject(id);
    }

    public void CloseProject(Project project)
    {
      bool afsluiten = false;
      if (project.Closed == false)
      {
        afsluiten = true;
      }

      else
      {
        afsluiten = false;
      }

      if (afsluiten)
      {
        project.Closed = true;
      }
      else
      {
        project.Closed = false;
      }

      _repo.UpdateProject(project);
    }

    public Fase GetFase(string id)
    {
      return _repo.ReadFase(id);
    }

    public Fase AddFase(Fase fase)
    {
      return _repo.CreateFase(fase);
    }

    public Fase ChangeFase(Fase fase)
    {
      return _repo.UpdateFase(fase);
    }

    public Fase ChangeFase(string id)
    {
      return _repo.UpdateFase(id);
    }

    public Fase RemoveFase(string id)
    {
      return _repo.DeleteFase(id);
    }

    public IEnumerable<Project> GetProjects()
    {
      return _repo.ReadProjects();
    }

    public IEnumerable<global::PoC.Domain.inputmethod.InputMethod> GetInputMethodsOfFase(string faseId)
    {
      return _repo.ReadInputmethodsOfFase(faseId);
    }

    public void AddLikeToProject(Project p, User u)
    {
      List<Like> likes = p.Likes.ToList();
      Like l = new Like();
      l.User = u;
      likes.Add(l);
      _inputManager.AddLike(l);
      p.Likes = likes;
      _repo.UpdateProject(p);
    }

    public void AddShareToProject(Project project)
    {
      List<Share> shares = project.Shares.ToList();
      shares.Add(new Share());
      project.Shares = shares;
      _repo.UpdateProject(project);
    }

    public QuestionList GetQuestionList(string questionListId)
    {
      return _mgr.GetQuestionList(questionListId);
    }

    public Project GetProjectFromFaseId(string faseId)
    {
      return _repo.ReadProjectFromFaseId(faseId);
    }

    public List<Fase> GetPastFasesOfProject(string pId)
    {
      return _repo.ReadPastFasesOfProject(pId);
    }
  }
}