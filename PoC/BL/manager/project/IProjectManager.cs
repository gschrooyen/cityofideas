using System;
using System.Collections.Generic;
using PoC.Domain.inputmethod;
using PoC.Domain.datatype;
using PoC.Domain.project;
using PoC.Domain.user;

namespace PoC.BL.manager.project
{
  public interface IProjectManager
  {
    //project
    Project GetProject(string id);
    Project AddProject(Project project);

    Project AddProjectViewModel(string projectName, string imageName,
      string mainQuestion, string description, Category category, DateTime begin, DateTime end,
      string faseName, ProjectFase projectFase, DateTime beginFase, DateTime endFase);

    Project AddFaseToProject(Project project, string faseName, ProjectFase projectFase, DateTime begin, DateTime end);
    Project ChangeProject(Project project);
    Project RemoveProject(string id);

    void CloseProject(Project project);

    // Add Ideation/QuestionList/faseProject voor linking project, later verwijderen indien niet noodzakelijk
    Project AddIdeationToProject(Project project, Ideation ideation);
    Project AddQuestionListToProject(Project project, QuestionList questionList);

    //Fase
    Fase GetFase(string id);
    Fase AddFase(Fase fase);
    Fase ChangeFase(Fase fase);
    Fase ChangeFase(string id);
    Fase RemoveFase(string id);
    IEnumerable<Project> GetProjects();
    IEnumerable<global::PoC.Domain.inputmethod.InputMethod> GetInputMethodsOfFase(string faseId);

    void AddLikeToProject(Project p, User u);
    void AddShareToProject(Project project);
    QuestionList GetQuestionList(string questionListId);
    Project GetProjectFromFaseId(string faseId);


    List<Fase> GetPastFasesOfProject(string pId);
  }
}