using System;
using System.Collections.Generic;
using PoC.Domain.inputmethod;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.inputMethod;
using PoC.Domain.project;
using PoC.Domain.user;

namespace PoC.BL.manager.InputMethod
{
  public interface IInputMethodManager
  {
    //QuestionList
    QuestionList GetQuestionList(string id);
    QuestionList GetQuestionListWithoudAnswers(string id);
    QuestionList AddQuestionList(QuestionList questionList);
    QuestionList ChangeQuestionList(QuestionList questionList);

    QuestionList AddQuestionListViewModel(string title, string description, DateTime begin, DateTime end);
    QuestionList RemoveQuestionList(string id);

    //Question
    Question GetOpenQuestion(string id);
    Question GetChoiceQuestion(string id);
    Question AddQuestion(Question question);
    Question ChangeQuestion(Question question);
    Question RemoveOpenQuestion(string id);
    Question RemoveChoiceQuestion(string id);

    IEnumerable<Question> GetQuestionsOfList(string listId);

    //Ideation
    Ideation GetIdeation(string id);
    Ideation AddIdeation(Ideation ideation);

    Ideation AddIdeationViewModel(IdeationType type, string title, string description, DateTime begin,
      DateTime end, string backgroundLink, string imageName, bool allowImage, int numberOfImages, bool imageRequired,
      bool allowVideo, int numberOfVideos,
      bool videoRequired, bool allowVideoLink, bool videoLinkRequired);

    Ideation ChangeIdeation(Ideation ideation);
    Ideation RemoveIdeation(string id);
    void CloseIdeation(Ideation ideation);

    //Options
    IEnumerable<Option> GetOptionsOfChoiceQuestion(ChoiceQuestion question);

    //Idea
    Reaction AddReactionToIdea(string ideaId, string text, string userId);
    void AddIdeaToIdeation(string ideationId, Idea idea, string userId);
    void AddQuestionToQuestionlist(string questionListId, Question question);
    void AddLikeToIdea(string ideaId, User user);
    void AddShareToIdea(string ideaId);
    void AddShareToIdeation(string id);
    IEnumerable<Ideation> GetIdeationsFromProject(Project project);
    IEnumerable<QuestionList> GetQuestionListFromProject(Project project);
  }
}