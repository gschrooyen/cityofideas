using System;
using System.Collections.Generic;
using System.Linq;
using PoC.Domain.inputmethod;
using PoC.BL.manager.input;
using PoC.BL.manager.user;
using PoC.DAL.repo.inputMethod;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.inputMethod;
using PoC.Domain.project;
using PoC.Domain.user;

namespace PoC.BL.manager.InputMethod
{
  public class InputMethodManager : IInputMethodManager
  {
    private IInputMethodRepository _repo;
    private readonly IInputManager _mgr;
    private readonly IUserManager _userManager;

    public InputMethodManager(IInputMethodRepository repo, IInputManager manager, IUserManager userManager)
    {
      _mgr = manager;
      _repo = repo;
      _userManager = userManager;
    }


    public QuestionList GetQuestionList(string id)
    {
      return _repo.ReadQuestionList(id);
    }

    public QuestionList GetQuestionListWithoudAnswers(string id)
    {
      return _repo.ReadQuestionListWithoutAnswers(id);
    }

    public QuestionList AddQuestionList(QuestionList questionList)
    {
      return _repo.CreateQuestionList(questionList);
    }

    public QuestionList ChangeQuestionList(QuestionList questionList)
    {
      return _repo.UpdateQuestionList(questionList);
    }

    public QuestionList RemoveQuestionList(string id)
    {
      return _repo.DeleteQuestionList(id);
    }

    public Question GetOpenQuestion(string id)
    {
      return _repo.ReadOpenQuestion(id);
    }

    public Question GetChoiceQuestion(string id)
    {
      return _repo.ReadChoiceQuestion(id);
    }

    public Question AddQuestion(Question question)
    {
      return _repo.CreateQuestion(question);
    }

    public Question ChangeQuestion(Question question)
    {
      return _repo.UpdateQuestion(question);
    }

    public Question RemoveOpenQuestion(string id)
    {
      return _repo.DeleteOpenQuestion(id);
    }

    public Question RemoveChoiceQuestion(string id)
    {
      return _repo.DeleteChoiceQuestion(id);
    }

    public IEnumerable<Question> GetQuestionsOfList(string listId)
    {
      return _repo.ReadQuestionsOfList(listId);
    }

    public Ideation GetIdeation(string id)
    {
      return _repo.ReadIdeation(id);
    }

    public Ideation AddIdeation(Ideation ideation)
    {
      return _repo.CreateIdeation(ideation);
    }

    public Ideation AddIdeationViewModel(IdeationType type, string title, string description, DateTime begin,
      DateTime end, string backgroundLink, string imageName, bool allowImage, int numberOfImages, bool imageRequired,
      bool allowVideo, int numberOfVideos,
      bool videoRequired, bool allowVideoLink, bool videoLinkRequired)
    {
      Ideation ideation = new Ideation()
      {
        Type = type,
        TitleIdeation = title,
        DescriptionIdeation = description,
        Begin = begin,
        End = end,
        BackgroundLink = backgroundLink,
        ImageName = imageName,

        AllowImage = allowImage,
        NumberOfImages = numberOfImages,
        ImageRequired = imageRequired,

        AllowVideo = allowVideo,
        NumberOfVideos = numberOfVideos,
        VideoRequired = videoRequired,

        AllowVideoLink = allowVideoLink,
        VideoLinkRequired = videoLinkRequired
      };
      return ideation;
    }

    public QuestionList AddQuestionListViewModel(string title, string description, DateTime begin, DateTime end)
    {
      QuestionList questionList = new QuestionList()
      {
        Title = title,
        Description = description,
        Begin = begin,
        End = end,
        Open = true
      };
      return questionList;
    }

    public Ideation ChangeIdeation(Ideation ideation)
    {
      return _repo.UpdateIdeation(ideation);
    }

    public Ideation RemoveIdeation(string id)
    {
      return _repo.DeleteIdeation(id);
    }

    public void CloseIdeation(Ideation ideation)
    {
      bool afsluiten = false;
      if (ideation.HideIdeation == false)
      {
        afsluiten = true;
      }

      else
      {
        afsluiten = false;
      }

      if (afsluiten)
      {
        ideation.HideIdeation = true;
      }
      else
      {
        ideation.HideIdeation = false;
      }

      _repo.UpdateIdeation(ideation);
    }

    public IEnumerable<Option> GetOptionsOfChoiceQuestion(ChoiceQuestion question)
    {
      return _repo.ReadOptionOfChoiceQuestion(question);
    }

    public Reaction AddReactionToIdea(string ideaId, string text, string userId)
    {
      User user = _userManager.GetUser(userId);
      Idea idea = _mgr.GetIdea(ideaId);
      var reactions = idea.Reactions.ToList();
      List<Input> inputs;
      if (user != null)
      {
        inputs = new List<Input>();
        foreach (Input input in user.Inputs) inputs.Add(input);
        Reaction reaction = new Reaction(idea, text, user);
        reactions.Add(reaction);
        inputs.Add(reaction);
        user.Inputs = inputs;
        _userManager.ChangeUser(user);
        idea.Reactions = reactions;
        _mgr.ChangeIdea(idea);
        return reaction;
      }

      return null;
    }

    public void AddIdeaToIdeation(string ideationId, Idea idea, string userId)
    {
      User user = _userManager.GetUser(userId);
      List<Idea> ideas = new List<Idea>();
      Ideation ideation = _repo.ReadIdeation(ideationId);
      ideas = ideation.Ideas.ToList();
      //Idea idea = new Idea( title, description, user);
      idea.User = user;
      idea.Time = DateTime.Now;
      ideas.Add(idea);
      
      var input = user.Inputs.ToList();
      input.Add(idea);
      user.Inputs = input;
      _userManager.ChangeUser(user);
      ideation.Ideas = ideas;
      var inpunts = user.Inputs.ToList();
      inpunts.Add(idea);
      user.Inputs = inpunts;
      _userManager.ChangeUser(user);
      _repo.UpdateIdeation(ideation);
    }

    public void AddOptionToQuestion(string questionId, List<Option> options)
    {
      List<Option> optionsQuestion = new List<Option>();
      ChoiceQuestion choiceQuestion = (ChoiceQuestion) _repo.ReadChoiceQuestion(questionId);
      optionsQuestion = choiceQuestion.Options.ToList();
      foreach (var option in options)
      {
        optionsQuestion.Add(option);
        //_repo.CreateOption();
      }

      choiceQuestion.Options = optionsQuestion;
      _repo.UpdateQuestion(choiceQuestion);
    }

    public void AddQuestionToQuestionlist(string questionListId, Question question)
    {
      List<Question> questionsQuestionlist = new List<Question>();
      QuestionList questionList = _repo.ReadQuestionList(questionListId);
      questionsQuestionlist = questionList.Questions.ToList();
      questionsQuestionlist.Add(question);

      questionList.Questions = questionsQuestionlist;
      _repo.UpdateQuestionList(questionList);
    }

    public void AddLikeToIdea(string ideaId, User user)
    {
      Idea idea = _mgr.GetIdea(ideaId);
      List<Like> likes = idea.Likes.ToList();
      Like l = new Like();
      l.User = user;
      likes.Add(l);
      idea.Likes = likes;

      _mgr.ChangeIdea(idea);
    }

    public void AddShareToIdea(string ideaId)
    {
      Idea idea = _mgr.GetIdea(ideaId);
      List<Share> shares = idea.Shares.ToList();
      shares.Add(new Share());
      idea.Shares = shares;

      _mgr.ChangeIdea(idea);
    }

    public void AddShareToIdeation(string id)
    {
      Ideation ideation = GetIdeation(id);
      var shares = ideation.Shares.ToList();
      shares.Add(new Share());
      ideation.Shares = shares;
      Console.WriteLine(ideation.Shares.Count());
      ChangeIdeation(ideation);
    }

    public IEnumerable<Ideation> GetIdeationsFromProject(Project project)
    {
      return _repo.ReadIdeationsFromFase(project.CurrentFase);
    }

    public IEnumerable<QuestionList> GetQuestionListFromProject(Project project)
    {
      return _repo.ReadQuestionListFromFase(project.CurrentFase);
    }
  }
}