using PoC.Domain.moderation;
using PoC.Domain.user;

namespace PoC.BL.manager.moderation
{
    public interface IModerationManager
    {
        //Notification
        Notification GetNotification(string id);
        Notification AddNotification(Notification notification);
        Notification ChangeNotification(Notification notification);
        Notification RemoveNotification(string id);
        User LoopBlocked(User user);
    }
}