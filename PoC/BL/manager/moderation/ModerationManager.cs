using PoC.BL.manager.user;
using PoC.DAL.repo.moderation;
using PoC.Domain.moderation;
using PoC.Domain.user;

namespace PoC.BL.manager.moderation
{
    public class ModerationManager: IModerationManager
    {
        private IModerationRepository _repo;
        private IUserManager _userManager;

        public ModerationManager(IModerationRepository repo, IUserManager userManager)
        {
            _repo = repo;
            _userManager = userManager;
        }
        public Notification GetNotification(string id)
        {
            return _repo.ReadNotification(id);
        }

        public Notification AddNotification(Notification notification)
        {
            return _repo.CreateNotification(notification);
        }

        public Notification ChangeNotification(Notification notification)
        {
            return _repo.UpdateNotification(notification);
        }

        public Notification RemoveNotification(string id)
        {
            return _repo.DeleteNotification(id);
        }
        
        public User LoopBlocked(User user)
        {
            foreach (var i in user.Inputs)
            {
                i.Deleted = true;
            }

            _userManager.ChangeUser(user);
            return user;
        }
    }
}