using System.Collections.Generic;
using System.Linq;
using PoC.Domain.organisation;
using PoC.BL.manager.project;
using PoC.DAL.repo.organisation;
using PoC.Domain.project;

namespace PoC.BL.manager.organisation {
    public class OrganisationManager : IOrganisationManager {
        private IOrganisationRepository _repo;
        private IProjectManager _projectmgr;

        public OrganisationManager(IOrganisationRepository repo, IProjectManager projectmgr) {
            _repo = repo;
            _projectmgr = projectmgr;
        }

        public Organisation GetOrganisation(string id) {
            return _repo.ReadOrganisation(id);
        }

        public List<Organisation> GetAllOrganisations() {
            return _repo.ReadAllOrganisations();
        }

        public Organisation AddOrganisation(Organisation organisation) {
            return _repo.CreateOrganisation(organisation);
        }

        public Organisation AddOrganisationViewModel(string orgName, string imageName, string logoName,
            string dia1, string dia2, string dia3,string telephoneNumber, string street, string city, string country, string email,
            string description, string featuredText, string zipCode, string primaryColor, string secondaryColor,
            string tertiaryColor) {
            Organisation organisation = new Organisation() {
                Name = orgName,
                FormattedName = orgName.ToUpper().Replace(" ", ""),
                Image = imageName,
                Logo = logoName,
                Dia1 = dia1,
                Dia2 = dia2,
                Dia3 = dia3,
                TelephoneNumber = telephoneNumber,
                Street = street,
                City = city,
                Country = country,
                Email = email,
                Description = description,
                FeaturedText = featuredText,
                ZipCode = zipCode
            };

            if (primaryColor != null) {
                organisation.PrimaryColor = primaryColor;
            }

            if (secondaryColor != null) {
                organisation.SecondaryColor = secondaryColor;
            }

            if (tertiaryColor != null) {
                organisation.TertiaryColor = tertiaryColor;
            }

            return _repo.CreateOrganisation(organisation);
        }

        public Organisation ChangeOrganisation(Organisation organisation) {
            organisation.FormattedName = organisation.Name.ToUpper();
            return _repo.UpdateOrganisation(organisation);
        }

        public Organisation RemoveOrganisation(string id) {
            return _repo.DeleteOrganisation(id);
        }

        public void CloseOrganisation(Organisation organisation) {
            bool afsluiten = false;
            if (organisation.Closed == false) {
                afsluiten = true;
            }

            else {
                afsluiten = false;
            }

            if (afsluiten) {
                organisation.Closed = true;
            }
            else {
                organisation.Closed = false;
            }

            _repo.UpdateOrganisation(organisation);
        }

        public Organisation GetOrganisationByName(string name) {
            return _repo.ReadOrganisationByName(name);
        }

        public void AddProjectToOrganisation(Organisation organisation, Project project) {
            List<Project> projects = new List<Project>();
            projects = organisation.Projects.ToList();
            projects.Add(project);
            organisation.Projects = projects;

            _repo.UpdateOrganisation(organisation);
        }

        public Organisation CopyOrganisation(Organisation organisation,
            string fileNameImage, string fileNameLogo, string dia1, string dia2, string dia3) {
            Organisation org = new Organisation();
            org.Name = organisation.Name;
            org.FormattedName = organisation.Name.ToUpper().Replace(" ", "");
            org.ZipCode = organisation.ZipCode;
            org.TelephoneNumber = organisation.TelephoneNumber;
            org.Street = organisation.Street;
            org.City = organisation.City;
            org.Country = organisation.Country;
            org.Email = organisation.Email;
            org.Image = fileNameImage;
            org.Logo = fileNameLogo;
            org.Dia1 = dia1;
            org.Dia2 = dia2;
            org.Dia3 = dia3;
            org.PrimaryColor = organisation.PrimaryColor;
            org.SecondaryColor = organisation.SecondaryColor;
            org.TertiaryColor = organisation.TertiaryColor;
            org.Users = organisation.Users;
            org.Closed = organisation.Closed;
            org.Description = organisation.Description;
            org.FeaturedText = organisation.FeaturedText;
            AddOrganisation(org);
            return org;
        }

        public List<Project> GetProjectsOfOrganisation(string orgId) {
            return _repo.ReadProjectsOfOrganisation(orgId);
        }
    }
}