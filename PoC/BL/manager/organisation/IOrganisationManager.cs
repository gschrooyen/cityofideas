using System.Collections.Generic;
using PoC.Domain.organisation;
using PoC.Domain.project;

namespace PoC.BL.manager.organisation
{
  public interface IOrganisationManager
  {
    //Organisation
    Organisation GetOrganisation(string id);
    Organisation AddOrganisation(Organisation organisation);

    Organisation AddOrganisationViewModel(string orgName, string imageName, string logoName, string dia1, string dia2,
      string dia3,string telephoneNumber, string street, string city, string country,string email,
      string description, string featuredText, string zipCode, string primaryColor, string secondaryColor,
      string tertiaryColor);
    Organisation ChangeOrganisation(Organisation organisation);
    Organisation RemoveOrganisation(string id);
    void CloseOrganisation(Organisation organisation);
    Organisation GetOrganisationByName(string name);
    void AddProjectToOrganisation(Organisation organisation, Project project);
    Organisation CopyOrganisation(Organisation organisation, string fileNameImage, string fileNameLogo, string dia1, string dia2, string dia3);
    List<Project> GetProjectsOfOrganisation(string orgId);
    List<Organisation> GetAllOrganisations();

  }
}