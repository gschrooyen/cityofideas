using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using PoC.BL.manager.input;
using PoC.BL.manager.InputMethod;
using PoC.BL.manager.project;
using PoC.DAL.repo.iot;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.inputMethod;
using PoC.Domain.iot;
using PoC.Domain.project;

namespace PoC.BL.manager.iot
{
    public class IotManager : IIotManager
    {
        private IIotRepository _repo;
        private IInputManager _inputManager;
        private IInputMethodManager _inputMethodManager;
        private IProjectManager _projectManager;

        
        public IotManager(IIotRepository repo, IInputManager inputManager, IInputMethodManager inputMethodManager, IProjectManager projectManager)
        {
            _repo = repo;
            _inputManager = inputManager;
            _inputMethodManager = inputMethodManager;
            _projectManager = projectManager;
        }

        public Poster GetPoster(string id)
        {
            return _repo.ReadPoster(id);
        }

        public Poster AddPoster(Poster poster)
        {
            return _repo.CreatePoster(poster);
        }

        public Poster ChangePoster(Poster poster)
        {
            return _repo.UpdatePoster(poster);
        }

        public Poster RemovePoster(string id)
        {
            return _repo.DeletePoster(id);
        }

        public Placement GetPlacement(string id)
        {
            return _repo.ReadPlacement(id);
        }

        public Placement AddPlacement(Placement placement)
        {
            return _repo.CreatePlacement(placement);
        }

        public Placement ChangePlacement(Placement placement)
        {
            return _repo.UpdatePlacement(placement);
        }

        public Placement RemovePlacement(string id)
        {
            return _repo.DeletePlacement(id);
        }

        public void AddVote(string payload)
        {
            
            Console.WriteLine(payload);
            string btnNumber = payload.Substring(1, 5);
            Console.WriteLine(btnNumber);
            var button = _repo.GetButton(btnNumber);
            Console.WriteLine(button);
            Console.WriteLine(button.Id);
            if (button.Poster.Ideation == null)
            {
                var answer = (Answer) button.Input;
                ChoiceQuestion q = (ChoiceQuestion) button.Poster.Question;
                var awnsers = q.Awnsers.ToList();
                awnsers.Add(new Answer(button.Option.Title));
                q.Awnsers = awnsers;
                _inputMethodManager.ChangeQuestion(q);
            }
            else
            {
                var idea = (Idea) button.Input;
                var foo = idea.Likes.ToList();
                foo.Add(new Like());
                idea.Likes = foo;
                _inputManager.ChangeIdea(idea);
            }
        }

        public List<Poster> GetPosters()
        {
            return _repo.ReadPosters();
        }

        public Poster MakePosterFromForm(IFormCollection formValues)
        {
            Poster p = new Poster();
            p.Description = formValues["description"];
            Input type = null;
            if (formValues["active"].Equals("on"))
            {
                p.Active = true;
            }
            else
            {
                p.Active = false;
            }

            if (formValues.ContainsKey("questionlistSelect"))
            {
                type = new Answer();
                IEnumerable<Question> questions = _inputMethodManager.GetQuestionsOfList(formValues["questionlistSelect"]);
                if (formValues.ContainsKey("selectQuestion"))
                {
                    p.Question = questions.SingleOrDefault(q => q.id == formValues["selectQuestion"]);
                }
            }
            else
            {
                type = new Vote();
            }
           
            List<Button> buttons = new List<Button>();
            List<String> keys = new List<string>();
            foreach (var key in formValues.Keys)
            {
                if ((key.Contains("button")) && (!key.Contains("selector")))
                {
                    keys.Add(key);
                }
            }

            foreach (var k in keys)
            {
                Button b = new Button();
                b.Poster = p;
                b.Input = type;
                if (formValues.ContainsKey("selectQuestion"))
                {
                    IEnumerable<Option> o = _inputMethodManager.GetOptionsOfChoiceQuestion((ChoiceQuestion)p.Question);
                    b.Option = o.SingleOrDefault(op => op.Id == formValues[k+"selector"]);
                }
                else
                {
                    if (formValues.ContainsKey("ideationselect"))
                    {
                        type = new Vote();
                        p.Ideation = _inputMethodManager.GetIdeation(formValues["ideationselect"]);
                        p.Idea = _inputManager.GetIdea(formValues[k + "selector"]);
                        }
                }
                buttons.Add(b);
            }

            p.Buttons = buttons;
            Placement placement = new Placement();
            Location loc = new Location();
            String lat = formValues["latitude"].ToString().Replace(".", ",");
            String lng =formValues["longitude"].ToString().Replace(".", ",");
            loc.Latitude = Double.Parse(lat);
            loc.Longitude = Double.Parse(lng);
            placement.Location = loc;
            placement.Begin = DateTime.Parse(formValues["start"]);
            placement.End = DateTime.Parse(formValues["einde"]);
            Project project = _projectManager.GetProject(formValues["projectselect"]);
            placement.Project = project;
            //placement.Poster = p;
            project.Placements.Append(placement);
            p.Placements.Add(placement);
            AddPlacement(placement);
            AddPoster(p);
            AddPlacementToPoster(p, placement);
            return p;
        }

        public void ChangePosterFromForm(Poster p, IFormCollection formValues)
        {
            p.Description = formValues["description"];
            Placement placement = new Placement();
            Location loc = new Location();
            String lat = formValues["latitude"].ToString().Replace(".", ",");
            String lng =formValues["longitude"].ToString().Replace(".", ",");
            loc.Latitude = Double.Parse(lat);
            loc.Longitude = Double.Parse(lng);
            placement.Location = loc;
            placement.Begin = DateTime.Parse(formValues["start"]);
            placement.End = DateTime.Parse(formValues["einde"]);
            Project project = _projectManager.GetProject(formValues["project"]);
            placement.Project = project;
            //placement.Poster = p;
            project.Placements.Append(placement);
            p.Placements.Add(placement);
            AddPlacement(placement);
            ChangePoster(p);
            AddPlacementToPoster(p, placement);
        }

        private void AddPlacementToPoster(Poster poster, Placement placement)
        {
            poster.CurrentPlacement = placement;
            ChangePoster(poster);
        }

        public void ClosePoster(Poster poster){
            bool afsluiten = false;
            if (poster.Active == false) {
                afsluiten = true;
            }

            else {
                afsluiten = false;
            }

            if (afsluiten) {
                poster.Active = true;
            }
            else {
                poster.Active = false;
            }

            _repo.UpdatePoster(poster);
        }
    }
    
}