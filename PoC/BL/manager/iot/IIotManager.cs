
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using PoC.Domain.iot;

namespace PoC.BL.manager.iot
{
    public interface IIotManager
    {
        //Poster
        Poster GetPoster(string id);
        Poster AddPoster(Poster poster);
        Poster ChangePoster(Poster poster);
        Poster RemovePoster(string id);
        //Placement
        Placement GetPlacement(string id);
        Placement AddPlacement(Placement placement);
        Placement ChangePlacement(Placement placement);
        Placement RemovePlacement(string id);
        void AddVote(string applicationMessagePayload);
        List<Poster> GetPosters();
        Poster MakePosterFromForm(IFormCollection formValues);
        void ClosePoster(Poster poster);
        void ChangePosterFromForm(Poster p, IFormCollection formValues);
    }
}