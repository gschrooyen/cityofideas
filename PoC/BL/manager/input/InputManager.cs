using System.Collections.Generic;
using System.Linq;
using PoC.BL.manager.moderation;
using PoC.BL.manager.user;
using PoC.DAL.repo.Input;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.inputmethod;
using PoC.Domain.moderation;

namespace PoC.BL.manager.input
{
    public class InputManager: IInputManager
    {
        private IInputRepository _repo;
        private readonly IUserManager _userManager;
        private readonly IModerationManager _moderationManager;
        

        public InputManager(IInputRepository repo, IUserManager userManager, IModerationManager moderationManager)
        {
            _userManager = userManager;
            _repo = repo;
            _moderationManager = moderationManager;
        }
        public Idea GetIdea(string id)
        {
             return _repo.ReadIdea(id);
        }

        public Idea AddIdea(Idea idea)
        {
            return _repo.CreateIdea(idea);
        }

        public Idea ChangeIdea(Idea idea)
        {
            return _repo.UpdateIdea(idea);
        }

        public Idea RemoveIdea(string id)
        {
            return _repo.DeleteIdea(id);
        }
        
       public Answer GetAwnser(string id)
        {
            return _repo.ReadAwnser(id);
        }

        public Answer AddAwnser(Answer answer)
        {
            return _repo.CreateAwnser(answer);
        }

        public Answer ChangeAwnser(Answer answer)
        {
            return _repo.UpdateAwnser(answer);
        }

        public Answer RemoveAwnser(string id)
        {
            return _repo.DeleteAwnser(id);
        }

        public Like GetLike(string id)
        {
            return _repo.ReadLike(id);
        }

        public Like AddLike(Like like)
        {
            return _repo.CreateLike(like);
        }

        public Like ChangeLike(Like like)
        {
            return _repo.UpdateLike(like);
        }

        public Like RemoveLike(string id)
        {
            return _repo.DeleteLike(id);
        }

        public IEnumerable<Like> GetLikesOfUser(string id)
        {
            return _repo.ReadLikesOfUser(id);
        }

        public Reaction GetReaction(string id)
        {
            return _repo.ReadReaction(id);
        }

        public Reaction AddReaction(Reaction reaction)
        {
            return _repo.CreateReaction(reaction);
        }

        public Reaction ChangeReaction(Reaction reaction)
        {
            return _repo.UpdateReaction(reaction);
        }

        public Reaction RemoveReaction(string id)
        {
            return _repo.DeleteReaction(id);
        }

        public Response GetResponse(string id)
        {
            return _repo.ReadResponse(id);
        }

        public Response AddResponse(Response response)
        {
            return _repo.CreateResponse(response);
        }

        public Response ChangeResponse(Response response)
        {
            return _repo.UpdateResponse(response);
        }

        public Response RemoveResponse(string id)
        {
            return _repo.DeleteResponse(id);
        }

        public Share GetShare(string id)
        {
            return _repo.ReadShare(id);
        }

        public Share AddShare(Share share)
        {
            return _repo.CreateShare(share);
        }

        public Share ChangeShare(Share share)
        {
            return _repo.UpdateShare(share);
        }

        public Share RemoveShare(string id)
        {
            return _repo.DeleteShare(id);
        }

        public Vote GetVote(string id)
        {
            return _repo.ReadVote(id);
        }

        public Vote AddVote(Vote vote)
        {
            return _repo.CreateVote(vote);
        }

        public Vote ChangeVote(Vote vote)
        {
            return _repo.UpdateVote(vote);
        }

        public Vote RemoveVote(string id)
        {
            return _repo.DeleteVote(id);
        }

        public Option GetOption(string id)
        {
            return _repo.ReadOption(id);
        }

        public Option AddOption(Option option)
        {
            return _repo.CreateOption(option);
        }

        public Option ChangeOption(Option option)
        {
            return _repo.UpdateOption(option);
        }

        public Option RemoveOption(string id)
        {
            return _repo.DeleteOption(id);
        }

        public IEnumerable<Reaction> GetReactionsOfIdea(string iid)
        {
            return GetIdea(iid).Reactions;
        }

        public IEnumerable<Reaction> GetMarkedReactions()
        {
            var reactions = _repo.ReadMarkedReactions();
            
            //var markedReactions = reactions.ToList();
            var markedReactions = reactions as Reaction[] ?? reactions.ToArray();
            foreach (var r in markedReactions)
            {
                //Console.WriteLine(r.User.UserID + " bla");
                r.Reactable = GetIdeaByReactionId(r);
            }

            return markedReactions;
        }

        public void AddNotification(string reactionId, string userId, NotificationType nt, string extra)
        {
            var reaction = _repo.ReadReaction(reactionId);
            var user = _userManager.GetUser(userId);
            var notification = new Notification(nt, extra, reaction, user);
            //_moderationManager.AddNotification(notification);
            
            reaction.Notifications.ToList().Add(notification);
            user.Notifications.ToList().Add(notification);
            _moderationManager.AddNotification(notification);
            _userManager.ChangeUser(user);
            _repo.UpdateReaction(reaction);
        }

        public Idea GetIdeaByReactionId(Reaction reaction)
        {
            return _repo.ReadIdeaByReaction(reaction);
        }
    }
}