using System.Collections.Generic;
using PoC.Domain.datatype;
using PoC.Domain.input;
using PoC.Domain.inputmethod;

namespace PoC.BL.manager.input
{
    public interface IInputManager
    {
        //Ideas
        Idea GetIdea(string id);
        Idea AddIdea(Idea idea);
        Idea ChangeIdea(Idea idea);
        Idea RemoveIdea(string id);
        //Awnsers
        Answer GetAwnser(string id);
        Answer AddAwnser(Answer answer);
        Answer ChangeAwnser(Answer answer);
        Answer RemoveAwnser(string id);
        //Like
        Like GetLike(string id);
        Like AddLike(Like like);
        Like ChangeLike(Like like);
        Like RemoveLike(string id);

        IEnumerable<Like> GetLikesOfUser(string id);
        //Reactions
        Reaction GetReaction(string id);
        Reaction AddReaction(Reaction reaction);
        Reaction ChangeReaction(Reaction reaction);
        Reaction RemoveReaction(string id);
        //Responses
        Response GetResponse(string id);
        Response AddResponse(Response response);
        Response ChangeResponse(Response response);
        Response RemoveResponse(string id);
        //Share
        Share GetShare(string id);
        Share AddShare(Share share);
        Share ChangeShare(Share share);
        Share RemoveShare(string id);
        //Vote
        Vote GetVote(string id);
        Vote AddVote(Vote vote);
        Vote ChangeVote(Vote vote);
        Vote RemoveVote(string id);
        //Option
        Option GetOption(string id);
        Option AddOption(Option option);
        Option ChangeOption(Option option);
        Option RemoveOption(string id);


        IEnumerable<Reaction> GetReactionsOfIdea(string iid);
        IEnumerable<Reaction> GetMarkedReactions();
        void AddNotification(string reactionid, string userid, NotificationType nt, string extra);
    }
}