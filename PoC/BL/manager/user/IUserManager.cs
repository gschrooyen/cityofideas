using System.Collections.Generic;
using PoC.Domain.user;

namespace PoC.BL.manager.user
{
    public interface IUserManager
    {
        //user
        User GetUser(string id);
        User AddUser(User user);
        User ChangeUser(User user);
        User RemoveUser(string id);
        User GetUserByEmail(string email);
        IEnumerable<User> GetUsersOfOrganisation(string toUpper);
        IEnumerable<User> GetUsers();
    }
}