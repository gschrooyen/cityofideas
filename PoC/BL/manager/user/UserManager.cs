using System.Collections.Generic;
using PoC.DAL.repo.user;
using PoC.Domain.user;

namespace PoC.BL.manager.user
{
    public class UserManager: IUserManager
    {
        private IUserRepository _repo;
        

        public UserManager(IUserRepository repo)
        {
            
            _repo = repo;
        }
        public User GetUser(string id)
        {
            return _repo.ReadUser(id);
        }

        public User AddUser(User user)
        {
            return _repo.CreateUser(user);
        }

        public User ChangeUser(User user)
        {
            return _repo.UpdateUser(user);
        }

        public User RemoveUser(string id)
        {
            var user = _repo.ReadUser(id);
            user.Deleted = true;
            return _repo.UpdateUser(user);
        }

       public IEnumerable<User> GetUsersOfOrganisation(string org)
        {
            return _repo.ReadUsers(org);
        }

        public User GetUserByEmail(string email)
        {
            return _repo.ReadUserWithEmail(email);
        }

        public string ReturnUserName(User user) {
            return _repo.ReturnUserName(user);
        }

        public IEnumerable<User> GetUsers()
        {
            return _repo.ReadUsers();
        }
    }
}