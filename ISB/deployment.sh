#!/bin/bash
#Functie: Script om de infrastructur en de appliactie te deployen
#Arguments: Arg1 kan -i/--import OF -d/--delete OF -da/--deleteall zijn -> niet verplicht
#-p/--password
#-in/--instance -> sql instance
#-t/--tags
#Auteur: Eline Meeusen
#eline.meeusen@student.kdg.be
#Requires: gcloud SDK


#PARAMETERS voor het oproepen van het script
IMPORT=0
DELETE=0
DELETEALL=0

PASSWORD="paswoord"
NAME="poc-sql"
TAG="poc"

PACKAGE1=$(dpkg -l | grep mysql-client)
PACKAGE2=$(dpkg -l | grep cloud)

#TEST : welke parameters worden meegegeven (op het einde worden deze verwijderd)
while [[ ! -z "$1" ]]; do
    case "$1" in
        -i|--import)
            IMPORT=1
            ;;
        -d|--delete)
            DELETE=1
            ;;
        -da|--deleteall)
			DELETEALL=1
			;;
		-p|--password)
				echo "Voer een paswoord in: "
				read -s PASSWORD
			;;
		-in|--instance) #voor uw sql
			shift;
			if [[ ! -z "${1}" ]]; then
				NAME="${1}"
			else
				echo "Gelieve een instance name op te geven (achter -in)" >&2
				exit 1;
			fi
			;;
		-t|--tags)
			shift;
			if [[ ! -z "${1}" ]]; then
				TAG="${1}"
			else
				echo "Gelieve een tag op te geven (achter -t)" >&2
				exit 1;
			fi
			;;
		*)
			echo "Onbekend argument ${1}" >&2
			exit 1;
			;;
    esac
    shift
done

#Constante variabelen
PROJECT=$(gcloud projects list | grep 'cs2-' | cut -d ' ' -f 1)

ADDRESS=""
EX_ADDRESS="gcloud compute addresses create pocvm --region=europe-west1"
RESERVED_IP_ADDRESS="gcloud compute addresses list --filter=name:pocvm --format=value(name)"

VMProject="ubuntu-os-cloud"
VMFamily="ubuntu-1804-lts"

SQL_DATABASE_INSTANCE="gcloud sql instances create ${NAME} --region=europe-west1"
POC_DATABASE="gcloud sql databases create pocdatabase --instance=${NAME}"


FIREWALL_RULES="gcloud compute firewall-rules create http22 --allow tcp:22,tcp:5000,tcp:5001,tcp:1883 --target-tags=${TAG}"

STORAGE_BUCKET="gsutil mb -p $PROJECT gs://poc-eline"

VM_INSTANCE="gcloud compute instances create poc-vm --image-project=$VMProject --image-family=$VMFamily --zone=europe-west1-b --tags=${TAG} --address=${ADDRESS}"

#Als script wordt opgeroepen zonder parameters (-i, -d, -da)
if [ $IMPORT -eq 0 ] && [ $DELETE -eq 0 ] && [ $DELETEALL -eq 0 ]
	then
		echo "Test Package Dependencies"
		if [[ $PACKAGE1 == "" ]] || [[ $PACKAGE2 == "" ]]
			then
   			 echo "Package dependencies zijn niet volledig"
   			 echo "Nodig : 
- MySql-Client : sudo apt install mysql-client-core-5.7
- Gcloud : 1) export CLOUD_SDK_REPO='cloud-sdk-$(lsb_release -c -s)'
2) sudo curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
3) sudo apt-get update && sudo apt-get install google-cloud-sdk
4) gcloud init -> stappen volgen voor verdere initialisatie"
   		 exit 1
		else
		echo "Test Package Dependencies : OK"
		fi

		echo "Creating Cloud SQL database instance"
		$SQL_DATABASE_INSTANCE
		echo
		$POC_DATABASE
		gcloud sql users set-password root --host=% --instance="${NAME}" --password="${PASSWORD}"
		echo "Creating IP address : pocvm"
		$EX_ADDRESS

		if [[ $RESERVED_IP_ADDRESS != "" ]]
			then
				ADDRESS=$RESERVED_IP_ADDRESS
				echo "Creating VM (Linux) instance"
				$VM_INSTANCE
			else
				ADDRESS=""
				echo "Creating VM (Linux) instance"
				$VM_INSTANCE

		fi
		echo "Creating firewall rule" 
		$FIREWALL_RULES

		exit 0
fi

#Als script wordt opgeroepen met parameters
#script oproepen met -i of --import (+ deployen)
if [ $IMPORT = 1 ]
		then
		echo "Test Package Dependencies"
			if [[ $PACKAGE1 == "" ]] || [[ $PACKAGE2 == "" ]]
				then
   			 echo "Package dependencies zijn niet volledig"
   			 echo "Nodig : 
- MySql-Client : sudo apt install mysql-client-core-5.7
- Gcloud : 1) export CLOUD_SDK_REPO='cloud-sdk-$(lsb_release -c -s)'
2) sudo curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
3) sudo apt-get update && sudo apt-get install google-cloud-sdk
4) gcloud init -> stappen volgen voor verdere initialisatie"
    			exit 1
			else
			echo "Test Package Dependencies : OK"
			fi

		echo "Aanmaken SQL database instance"
		$SQL_DATABASE_INSTANCE
		echo " "
		$POC_DATABASE
		IP_SQL=$(gcloud sql instances describe ${NAME} --format='get(ipAddresses[0].ipAddress)')
		echo " "
		echo "Creating IP address : pocvm"
		$EX_ADDRESS
		IP_VM=$(gcloud compute addresses describe pocvm --region=europe-west1 | grep 'address: ' | cut -d ' ' -f 2)
		gcloud sql instances -q patch ${NAME} --authorized-networks=$IP_VM

		echo " "

		if [[ ${RESERVED_IP_ADDRESS} != "" ]];
			then
				ADDRESS=$RESERVED_IP_ADDRESS
				echo "Creating VM (Linux) instance - externe IP"
				gcloud compute instances create poc-vm --image-project=$VMProject --image-family=$VMFamily --zone=europe-west1-b --tags=${TAG} --address=pocvm --metadata ^°^startup-script="#!/bin/bash
#Functie :	Script om de infrastructur en de appliactie te deployen -> inline startup script
#Alles wat op de VM gebeurt
#Auteur: Eline Meeusen
#eline.meeusen@student.kdg.be
#Requires: gcloud SDK

cd /
sudo apt-get update
echo


sudo apt-get update
sudo apt-get install mosquitto
sudo apt-get install mosquitto-clients
sudo /etc/init.d/mosquitto start


sudo apt install mysql-client-core-5.7

sudo apt-get install -y git
sudo rm -rf /cityofideas
sudo git clone https://elinePOC:MR6UFseV5QhVZ3sLxoff@gitlab.com/gschrooyen/cityofideas.git
cd /cityofideas/PoC/UI-MVC

sudo git stash
sudo git checkout master
sudo git pull


sudo wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt-get install -y apt-transport-https
sudo apt-get update
sudo dpkg --purge packages-microsoft-prod && sudo dpkg -i packages-microsoft-prod.deb


sudo apt-get install -y dotnet-sdk-2.2

sudo sed -i s/localhost/$IP_VM/g appsettings.json
sudo sed -i -E 's/\"mosquitto\":.*/\"mosquitto\": \"$IP_VM\",/g' appsettings.json
sudo sed -i -E 's/\"sql\":.*/\"sql\": \"$IP_SQL\"/g' appsettings.json
sudo sed -i -E 's/Server=.*/Server=$IP_SQL; Port=3306; Database=pocdatabase; User=root; Pwd=paswoord\"/g' appsettings.json

sudo sed -i -E 's/Server=.*/Server=$IP_SQL; Port=3306; Database=pocdatabase; User=root; Pwd=paswoord\");/g' /cityofideas/PoC/DAL/EF/PoCDbContext.cs

mkdir /opt/nodejs
curl https://nodejs.org/dist/v8.12.0/node-v8.12.0-linux-x64.tar.gz | tar xvzf - -C /opt/nodejs --strip-components=1
ln -s /opt/nodejs/bin/node /usr/bin/node
ln -s /opt/nodejs/bin/npm /usr/bin/npm
sudo npm install
sudo npm install

sudo dotnet run "
			else
				ADDRESS=""
				echo "Creating VM (Linux) instance"
				$VM_INSTANCE

		fi

		echo " "
		echo "Aanmaken Cloud Storage Bucket"
		$STORAGE_BUCKET
		echo " "
		echo "Aanmaken Firewall Rules"
		$FIREWALL_RULES

		echo " "
		echo "Database importeren vanuit Cloud storage bucket"
		#EXPORTEREN
		ADDRESS=$(gcloud sql instances describe ${NAME} | grep 'serviceAccountEmailAddress' | cut -d ":" -f 2 | xargs)
		gsutil acl ch -u"$ADDRESS":W gs://poc-eline
		gcloud sql export sql "${NAME}" gs://poc-eline/sqldumpfile

		#IMPORTEREN	
		gcloud sql import sql "${NAME}" gs://poc-eline/sqldumpfile -q
		
		echo " "	
		echo "Update user"
		gcloud sql users set-password root --host=% --instance="${NAME}" --password="${PASSWORD}"
		

	echo "Dagelijkse backup database"
	gcloud sql instances patch "${NAME}" --backup-start-time=01:30
	exit 0


#script oproepen met -d of --delete
elif [ $DELETE = 1 ]
	then
			echo "Server, firewall rule en database verwijderen"
			echo "Deleting VM (linux) instance"
			gcloud compute instances delete poc-vm --zone=europe-west1-b -q

			read -p "Wil u de database eerst exporteren naar de cloud storage bucket? y/n : " ANTWOORD
			if [ $ANTWOORD = "y" ]
	 			then
				 	echo "Exporteren"
					gcloud sql export sql "${NAME}" gs://poc-eline/sqldumpfile
					echo "Database verwijderen + Deleting users"
					gcloud sql instances delete "${NAME}" -q
			elif [ $ANTWOORD = "n" ]
				then
					echo "Deleting Cloud SQL database instance + Deleting users"
					gcloud sql instances delete "${NAME}" -q
			else 
					echo "Geen geldig antwoord. Probeer opnieuw."
					exit 1
			fi
			echo "Deleting firewall-rule"
			gcloud compute firewall-rules delete http22 -q

			sudo apt autoremove

			exit 0
 
 #script oproepen met -da of --deleteall
elif [ $DELETEALL = 1 ]
	then
		echo "Server, firewall rules, database, gereserveerde ip adressen en storage bucket verwijderen"
		gcloud compute instances delete poc-vm --zone=europe-west1-b -q
		gcloud compute firewall-rules delete http22 -q
		gsutil rm -r gs://poc-eline
		gcloud compute addresses delete pocvm --region=europe-west1 -q
		gcloud sql databases delete pocdatabase --instance="${NAME}" -q
		echo "Database + deleting users (automatisch)"
		gcloud sql instances delete "${NAME}" -q

		exit 0
	else
		echo "Gelieve één van volgende argumenten op te geven:"
		echo "-i of --import : database importeren uit Cloud storage bucket"
		echo "-d of --delete : server, database en firewall rule verwijderen "
		echo "-da of --deleteall server, database, firewall rule, 
		gereserveerd IP adres en stroage bucket verwijderen"
		exit 1
	fi
fi
exit 0;
